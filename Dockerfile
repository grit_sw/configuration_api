FROM alpine:edge

ENV FLASK_APP run_api.py


WORKDIR /home/user/

COPY requirements.txt requirements.txt
COPY dev_requirements.txt dev_requirements.txt
RUN apk update
RUN apk add python3 postgresql-libs librdkafka-dev
RUN apk add --virtual .build-deps \
    gcc \
    python3-dev \
    musl-dev \
    postgresql-dev \
    libffi-dev && \
    python3 -m venv venv && \
    venv/bin/pip install -r requirements.txt --no-cache-dir && \
    venv/bin/pip install -r dev_requirements.txt --no-cache-dir && \
    apk --purge del .build-deps

COPY prosumer_schema.avsc config_init_schema.avsc config.py logger.py run_api.py start.sh ./
COPY migrations migrations
ENV FLASK_ENV Staging
EXPOSE 5000
RUN chmod +x start.sh
COPY api api

# run-time configuration

ENTRYPOINT ["./start.sh"]
