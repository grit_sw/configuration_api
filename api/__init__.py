import os

from flask import Flask
from flask_restplus import Api
from flask_sqlalchemy import SQLAlchemy
from pymongo import MongoClient

from api.producer import KafkaProducer
from config import config
from logger import logger

db = SQLAlchemy()
api = Api(doc='/doc/')
# client = MongoClient('mongodb://dashboard.grit.systems:27017/')
client = MongoClient(os.environ.get('MONGO_DATABASE_URL'))
mongodb = client['lift-mongo-app']
probes = mongodb['probes']
mongo_users = mongodb['user.users']
configurations = mongodb['configurations']

app = Flask(__name__)


def create_api(config_name):

	try:
		init_config = config[config_name]()
	except KeyError as e:
		logger.exception(e)
		raise

	except Exception as e:
		logger.exception(e)
		# For unforseen exceptions
		raise

	print('Running in {} Mode'.format(init_config))

	app_config = config.get(config_name)
	app.config.from_object(app_config)

	app_config.init_app(app)
	db.init_app(app)

	# from api.controllers import prosumer_api as ns1
	# from api.controllers import prosumer_group_api as ns2
	# from api.controllers import prosumer_switch_api as ns3
	# from api.controllers import prosumer_type_api as ns4
	# from api.controllers import prosumer_class_api as ns5
	from api.controllers import probe_api as ns6
	from api.controllers import device_type_api as ns7

	from api.controllers import current_sensor_api as ns8
	from api.controllers import voltage_sensor_api as ns9

	from api.controllers import configuration_api as ns10
	from api.controllers import configuration_group_api as ns11
	from api.controllers import fuel_api as ns12
	from api.controllers import phase_type_api as ns13
	from api.controllers import single_phase_grid_api as ns14
	from api.controllers import three_phase_grid_api as ns15
	from api.controllers import single_phase_generator_api as ns16
	from api.controllers import three_phase_generator_api as ns17
	from api.controllers import single_phase_inverter_api as ns18
	from api.controllers import three_phase_inverter_api as ns19

	from api.controllers import consumer_group_type_api as ns20
	from api.controllers import consumer_group_api as ns21

	from api.controllers import single_phase_consumer_line_api as ns22
	from api.controllers import three_phase_consumer_line_api as ns23

	from api.controllers import single_phase_consumer_api as ns24
	from api.controllers import three_phase_consumer_api as ns25

	from api.controllers import installation_api as ns26
	from api.controllers import send_config_to_kafka_api as ns27

	# # Prosumer
	# api.add_namespace(ns1, path='/prosumers')

	# # Prosumer Group
	# api.add_namespace(ns2, path='/prosumer-group')

	# # Prosumer switching
	# api.add_namespace(ns3, path='/prosumer-switch')

	# # Prosumer type
	# api.add_namespace(ns4, path='/prosumer-type')

	# # Prosumer class
	# api.add_namespace(ns5, path='/prosumer-class')

	# Probe
	api.add_namespace(ns6, path='/probe')

	# Meter types
	api.add_namespace(ns7, path='/device-type')

	# Sensor
	api.add_namespace(ns8, path='/current-sensor')
	api.add_namespace(ns9, path='/voltage-sensor')

	# Configuration
	api.add_namespace(ns10, path='/configuration')

	# Configuration group
	api.add_namespace(ns11, path='/configuration-group')

	# Fuel
	api.add_namespace(ns12, path='/fuel')

	# Phase type
	api.add_namespace(ns13, path='/phase-type')

	# Grid
	api.add_namespace(ns14, path='/single-phase-grid')
	api.add_namespace(ns15, path='/three-phase-grid')

	# Generator
	api.add_namespace(ns16, path='/single-phase-generator')
	api.add_namespace(ns17, path='/three-phase-generator')

	# Inverter
	api.add_namespace(ns18, path='/single-phase-inverter')
	api.add_namespace(ns19, path='/three-phase-inverter')

	# Consumer group type
	api.add_namespace(ns20, path='/consumer-group-type')

	# Consumer group
	api.add_namespace(ns21, path='/consumer-group')

	# Consumer line
	api.add_namespace(ns22, path='/single-phase-consumer-line')
	api.add_namespace(ns23, path='/three-phase-consumer-line')

	# Consumer
	api.add_namespace(ns24, path='/single-phase-consumer')
	api.add_namespace(ns25, path='/three-phase-consumer')

	# Installation
	api.add_namespace(ns26, path='/installation')

	# Send COnfiguration
	api.add_namespace(ns27, path='/save')

	api.init_app(app)

	with app.app_context():
		# db.reflect()
		# db.drop_all()
		db.create_all()

	return app
