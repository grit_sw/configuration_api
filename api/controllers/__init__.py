# from api.controllers.prosumer import prosumer_api
# from api.controllers.prosumer_group import prosumer_group_api
# from api.controllers.prosumer_switch import prosumer_switch_api
# from api.controllers.prosumer_type import prosumer_type_api
# from api.controllers.prosumer_class import prosumer_class_api
from api.controllers.probe_controller import probe_api
from api.controllers.device_type import device_type_api
from api.controllers.current_sensor import current_sensor_api
from api.controllers.voltage_sensor import voltage_sensor_api
from api.controllers.configuration import configuration_api
from api.controllers.configuration_group import configuration_group_api
from api.controllers.fuel import fuel_api

from api.controllers.phase_type import phase_type_api

from api.controllers.power_sources.grid.one_phase import single_phase_grid_api
from api.controllers.power_sources.grid.three_phase import three_phase_grid_api
from api.controllers.power_sources.generator.one_phase import single_phase_generator_api
from api.controllers.power_sources.generator.three_phase import three_phase_generator_api
from api.controllers.power_sources.inverter.one_phase import single_phase_inverter_api
from api.controllers.power_sources.inverter.three_phase import three_phase_inverter_api

from api.controllers.power_consumers.consumer.one_phase import single_phase_consumer_api
from api.controllers.power_consumers.consumer.three_phase import three_phase_consumer_api

from api.controllers.power_consumers.consumer_group import consumer_group_api
from api.controllers.power_consumers.consumer_group_type import consumer_group_type_api

from api.controllers.power_consumers.consumer_line.one_phase import single_phase_consumer_line_api
from api.controllers.power_consumers.consumer_line.three_phase import three_phase_consumer_line_api

from api.controllers.installation import installation_api

from api.controllers.save_probe import send_config_to_kafka_api
