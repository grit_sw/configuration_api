"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import ConfigurationManager
from api.schema import NewConfigurationSchema
from logger import logger

configuration_api = Namespace('Configuration Api', description='Api for managing probe configurations.')

probe_channel = configuration_api.model('Probe Channel', {
	'probe_id': fields.String(required=True, description='The ID of a probe.'),
	'channel_ids': fields.List(fields.String, required=True, description='A list of channels to be configured on this probe.'),
})

configuration_member = configuration_api.model('Configuration Member', {
	'member_id': fields.String(required=True, description='The ID of the configuration member.'),
	'member_name': fields.String(required=True, description='The name of the configuration member.'),
})

configuration_user = configuration_api.model('Configuration User', {
	'configuration_user_id': fields.String(required=True, description='The ID of the configuration user.'),
	'configuration_user_name': fields.String(required=True, description='The name of the configuration member.'),
})

new_configuration = configuration_api.model('New Configuration', {
	'configuration_name': fields.String(required=True, description='The name of the configuration.'),
	'error_threshold': fields.Float(required=False, description='The error threshold of the configuration.'),
	'display_energy_today': fields.Boolean(required=True, description='The show energy today information.'),
	'configuration_user': fields.Nested(configuration_user, required=False, description='The user who has access to this configuration.'),
	'configuration_members': fields.List(fields.Nested(configuration_member), required=True, description='A list of users that have access to the configuration.'),
	'probe_channels': fields.List(fields.Nested(probe_channel), required=True, description='The channels of a probe that have a configuration assigned to them.'),
})


@configuration_api.route('/new/')
class NewConfiguration(Resource):

	@configuration_api.expect(new_configuration)
	def post(self):
		"""
				HTTP method to create a new configuration
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = NewConfigurationSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		configuration_user_ = dict(dict(new_payload)['configuration_user']._asdict())
		configuration_members = [dict(config_member._asdict()) for config_member in dict(new_payload)['configuration_members']]
		probe_channels = [dict(probe_channel._asdict()) for probe_channel in dict(new_payload)['probe_channels']]
		new_payload['configuration_user'] = configuration_user_
		new_payload['configuration_members'] = configuration_members
		new_payload['probe_channels'] = probe_channels
		new_payload = dict(new_payload)
		manager = ConfigurationManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.new_configuration(role_id, new_payload)
		return resp, code



@configuration_api.route('/edit/<string:configuration_id>/')
class EditConfiguration(Resource):

	@configuration_api.expect(new_configuration)
	def post(self, configuration_id):
		"""
				HTTP method to create a new configuration
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = NewConfigurationSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		configuration_user_ = dict(dict(new_payload)['configuration_user']._asdict())
		configuration_members = [dict(config_member._asdict()) for config_member in dict(new_payload)['configuration_members']]
		probe_channels = [dict(probe_channel._asdict()) for probe_channel in dict(new_payload)['probe_channels']]
		new_payload['configuration_user'] = configuration_user_
		new_payload['configuration_members'] = configuration_members
		new_payload['probe_channels'] = probe_channels
		new_payload = dict(new_payload)
		manager = ConfigurationManager()
		role_id = request.cookies.get('role_id')
		new_payload['configuration_id'] = configuration_id
		resp, code = manager.edit_configuration(role_id, new_payload)
		return resp, code


@configuration_api.route('/one/<string:configuration_id>/')
class GetOneConfiguration(Resource):

	def get(self, configuration_id):
		"""
				HTTP method to create a new configuration
				@returns: response and status code
		"""
		manager = ConfigurationManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.view_configuration(role_id, configuration_id)
		return resp, code


@configuration_api.route('/demo/')
class GetDemoConfiguration(Resource):

	def get(self):
		"""
				HTTP method to get a demo configuration
				@returns: response and status code
		"""
		manager = ConfigurationManager()
		resp, code = manager.get_demo()
		return resp, code


@configuration_api.route('/all/')
class GetAllConfigurations(Resource):

	def get(self):
		"""
				HTTP method to get all configurations
				@returns: response and status code
		"""
		manager = ConfigurationManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all_configurations(role_id)
		return resp, code


@configuration_api.route('/user-configurations/<string:user_id>/')
class GetUserConfigurations(Resource):

	def get(self, user_id):
		"""
				Get all the configuration a user with user ID user_id has
				@returns: response and status code
		"""
		manager = ConfigurationManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_user_configurations(role_id, user_id)
		return resp, code


@configuration_api.route('/me/')
class GetMyConfigurations(Resource):

	def get(self):
		"""
				Route to for a user to view his configurations
				@returns: response and status code
		"""
		manager = ConfigurationManager()
		user_id = request.cookies.get('user_id')
		resp, code = manager.get_my_configurations(user_id)
		return resp, code


@configuration_api.route('/my-ids/')
class GetMyConfigurationIDs(Resource):

	def get(self):
		"""
				Route to for a user to view his configurations
				@returns: response and status code
		"""
		manager = ConfigurationManager()
		user_id = request.cookies.get('user_id')
		resp, code = manager.get_my_configuration_ids(user_id)
		return resp, code


@configuration_api.route('/user/all-power-sources/<string:user_id>/')
@configuration_api.doc(params={
	'user_id': 'The user ID of the user',
})

class GetUserPowerSources(Resource):

	def get(self, user_id):
		"""
				Route to for a user to view his configurations
				@returns: response and status code
		"""
		manager = ConfigurationManager()
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		resp, code = manager.get_user_power_sources(user_id)
		return resp, code


@configuration_api.route('/user/my-power-sources/')
class GetMyPowerSources(Resource):

	def get(self):
		"""
				Route to for a user to view his configurations
				@returns: response and status code
		"""
		manager = ConfigurationManager()
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 1:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		user_id = request.cookies.get('user_id')
		resp, code = manager.get_user_power_sources(user_id)
		return resp, code


@configuration_api.route('/user/all-consumers/<string:user_id>/')
@configuration_api.doc(params={
	'user_id': 'The user ID of the user',
})

class GetUserConsumers(Resource):

	def get(self, user_id):
		"""
				Route to for a user to view his configurations
				@returns: response and status code
		"""
		manager = ConfigurationManager()
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		resp, code = manager.get_user_consumers(user_id)
		return resp, code


@configuration_api.route('/user/my-consumers/')
class GetMyConsumers(Resource):

	def get(self):
		"""
				Route to for a user to view his configurations
				@returns: response and status code
		"""
		manager = ConfigurationManager()
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 1:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		user_id = request.cookies.get('user_id')
		resp, code = manager.get_user_consumers(user_id)
		return resp, code


@configuration_api.route('/unconfigured-probes/')
class UnconfiguredProbes(Resource):

	def get(self):
		"""
				HTTP method to get unconfigured probes
				@returns: response and status code
		"""
		role_id = request.cookies.get('role_id')
		manager = ConfigurationManager()
		resp, code = manager.unconfigured_probes(role_id)
		return resp, code
