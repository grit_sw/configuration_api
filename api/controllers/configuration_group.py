"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import ConfigurationGroupManager
from api.schema import NewConfigurationGroupSchema
from logger import logger

configuration_group_api = Namespace('Configuration Group', description='Api for managing probe configuration groups.')

new_configuration_group = configuration_group_api.model('New Configuration Group', {
	'name': fields.String(required=True, description='The name of the configuration group.'),
	'admin_id': fields.String(required=False, description='The user who has access to this configuration group.'),
	'source_config_id': fields.String(required=True, description='The source configuration for the configuration group.'),
	'profit_margin': fields.Float(required=False, description='The profit margin of the configuration group.'),
	'group_members': fields.List(fields.String, required=True, description='A list of configurations to be added to this group.'),
})

@configuration_group_api.route('/new/')
class NewConfigurationGroup(Resource):

	@configuration_group_api.expect(new_configuration_group)
	def post(self):
		"""
				HTTP method to create a new configuration group
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = NewConfigurationGroupSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		new_payload = dict(new_payload)
		print(new_payload)
		manager = ConfigurationGroupManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.new_configuration_group(role_id, new_payload)
		return resp, code



@configuration_group_api.route('/edit/<string:configuration_group_id>/')
class EditConfigurationGroup(Resource):

	@configuration_group_api.expect(new_configuration_group)
	def post(self, configuration_group_id):
		"""
				HTTP method to create a new configuration_group
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = NewConfigurationGroupSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		new_payload = dict(new_payload)
		manager = ConfigurationGroupManager()
		role_id = request.cookies.get('role_id')
		new_payload['configuration_group_id'] = configuration_group_id
		resp, code = manager.edit_configuration_group(role_id, new_payload)
		return resp, code


@configuration_group_api.route('/one/<string:configuration_group_id>/')
class GetOneConfigurationGroup(Resource):

	def get(self, configuration_group_id):
		"""
				HTTP method to create a new configuration_group
				@returns: response and status code
		"""
		manager = ConfigurationGroupManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.view_configuration_group(role_id, configuration_group_id)
		return resp, code


@configuration_group_api.route('/all/')
class GetAllConfigurationGroups(Resource):

	def get(self):
		"""
				HTTP method to get all configuration_groups
				@returns: response and status code
		"""
		manager = ConfigurationGroupManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all_configuration_groups(role_id)
		return resp, code
