"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import CurrentSensorManager
from api.schema import CurrentSensorSchema
from logger import logger

current_sensor_api = Namespace('Current Sensor', description='Api for managing current sensors')

new_current_sensor = current_sensor_api.model('New Current Sensor', {
	'name': fields.String(required=True, description='The name of the sensor type.'),
	'rating': fields.Float(required=True, description='The rating of the sensor.'),
	'scaling_factor_current': fields.Float(required=True, description='The scaling factor of the sensor.'),
})


@current_sensor_api.route('/new/')
class NewCurrentSensor(Resource):

	@current_sensor_api.expect(new_current_sensor)
	def post(self):
		"""
				HTTP method to create a new current sensor
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		print(payload)
		schema = CurrentSensorSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = CurrentSensorManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.create(role_id, new_payload)
		return resp, code


@current_sensor_api.route('/current/edit/<string:sensor_id>/')
class EditCurrentSensor(Resource):

	@current_sensor_api.expect(new_current_sensor)
	def put(self, sensor_id):
		"""
				HTTP method to edit a sensor type
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = CurrentSensorSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = CurrentSensorManager()
		role_id = request.cookies.get('role_id')
		new_payload['sensor_id'] = sensor_id
		resp, code = manager.edit(role_id, new_payload)
		return resp, code


@current_sensor_api.route('/all/')
class GetAllSensorTypes(Resource):

	def get(self):
		"""
				HTTP method to get all sensor types
				@returns: response and status code
		"""
		manager = CurrentSensorManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all(role_id)
		return resp, code


@current_sensor_api.route('/one/<string:sensor_id>/')
class GetOneSensorType(Resource):

	def get(self, sensor_id):
		"""
				HTTP method to get sensor type
				@returns: response and status code
		"""
		manager = CurrentSensorManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one(role_id, sensor_id)
		return resp, code
