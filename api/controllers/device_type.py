"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import DeviceTypeManager
from api.schema import DeviceTypeSchema
from logger import logger

device_type_api = Namespace('Device Types', description='Api for managing device types')

new_device_type = device_type_api.model('DeviceType', {
	'name': fields.String(required=True, description='The name of the device type.'),
	'number_of_channels': fields.Integer(required=True, description='The name of the wifi network of the device type.'),
})

@device_type_api.route('/new/')
class DeviceType(Resource):

	@device_type_api.expect(new_device_type)
	def post(self):
		"""
				HTTP method to create a device type
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = DeviceTypeSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = DeviceTypeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.new_device_type(role_id, new_payload)
		return resp, code


@device_type_api.route('/edit/<string:device_type_id>/')
class EditDeviceType(Resource):

	@device_type_api.expect(new_device_type)
	def put(self, device_type_id):
		"""
				HTTP method to edit a device type
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = DeviceTypeSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = DeviceTypeManager()
		role_id = request.cookies.get('role_id')
		new_payload['device_type_id'] = device_type_id
		resp, code = manager.edit_device_type(role_id, new_payload)
		return resp, code


@device_type_api.route('/all/')
class GetAllDeviceTypes(Resource):

	def get(self):
		"""
				HTTP method to get all device types
				@returns: response and status code
		"""
		manager = DeviceTypeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all(role_id)
		return resp, code


@device_type_api.route('/one/<string:device_type_id>/')
class GetOneDeviceType(Resource):

	def get(self, device_type_id):
		"""
				HTTP method to get device type
				@returns: response and status code
		"""
		manager = DeviceTypeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one(role_id, device_type_id)
		return resp, code
