"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import FuelManager
from api.schema import NewFuelSchema, EditFuelSchema
from logger import logger

fuel_api = Namespace('Fuel', description='Api for managing fuel types.')

new_fuel = fuel_api.model('New Fuel Type', {
	'name': fields.String(required=True, description='The name of the fuel type.'),
	'unit_price': fields.Float(required=True, description='The unit price of the fuel type.'),
})

edit_fuel = fuel_api.model('Edit Fuel Type', {
	'fuel_id': fields.String(required=False, description='The ID of the fuel type.'),
	'name': fields.String(required=True, description='The name of the fuel type.'),
	'unit_price': fields.Float(required=True, description='The unit price of the fuel type.'),
})


@fuel_api.route('/new/')
class NewFuel(Resource):

	@fuel_api.expect(new_fuel)
	def post(self):
		"""
				HTTP method to create a new one phase generator
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = NewFuelSchema(strict=True)
		try:
			new_payload = dict(schema.load(payload).data._asdict())
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = FuelManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.new(role_id, new_payload)
		return resp, code



@fuel_api.route('/edit/<string:fuel_id>/')
class EditFuel(Resource):

	@fuel_api.expect(edit_fuel)
	def put(self, fuel_id):
		"""
				HTTP method to edit a one phase generator
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = EditFuelSchema(strict=True)
		try:
			new_payload = dict(schema.load(payload).data._asdict())
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = FuelManager()
		role_id = request.cookies.get('role_id')
		new_payload['fuel_id'] = fuel_id
		resp, code = manager.edit(role_id, new_payload)
		return resp, code


@fuel_api.route('/one/<string:fuel_id>/')
class GetOneFuel(Resource):

	def get(self, fuel_id):
		"""
				HTTP method to get details for a one phase generator
				@returns: response and status code
		"""
		manager = FuelManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one(role_id, fuel_id)
		return resp, code

	def delete(self, fuel_id):
		"""
				HTTP method to delete a one phase generator
				@returns: response and status code
		"""
		manager = FuelManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete(role_id, fuel_id)
		return resp, code


@fuel_api.route('/user/<string:user_id>/')
class GetUserFuel(Resource):

	def get(self, user_id):
		"""
				HTTP method to get details for a one phase generator
				@returns: response and status code
		"""
		manager = FuelManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_user(role_id, user_id)
		return resp, code


@fuel_api.route('/all/')
class GetAllFuels(Resource):

	def get(self):
		"""
				HTTP method to get all single phase generator power sources
				@returns: response and status code
		"""
		manager = FuelManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all(role_id)
		return resp, code

@fuel_api.route('/me/')
class GetAllMyFuels(Resource):

	def get(self):
		"""
				HTTP method to get all single phase generator power sources
				@returns: response and status code
		"""
		manager = FuelManager()
		user_id = request.cookies.get('user_id')
		resp, code = manager.get_all_my_fuels(user_id)
		return resp, code


