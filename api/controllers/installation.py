"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource
from api.manager import InstallationManager

installation_api = Namespace('Installation', description='Api for managing installation states of devices.')

@installation_api.route('/install/<string:source_name>/<string:source_id>/')
class InstallDevice(Resource):

	def post(self, source_name, source_id):
		"""
				HTTP method to mark a source as installed.
				@returns: response and status code
		"""
		manager = InstallationManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.install(role_id, source_name, source_id)
		return resp, code

@installation_api.route('/uninstall/<string:source_name>/<string:source_id>/')
class UninstallDevice(Resource):

	def post(self, source_name, source_id):
		"""
				HTTP method to mark a source as installed.
				@returns: response and status code
		"""
		manager = InstallationManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.uninstall(role_id, source_name, source_id)
		return resp, code

