"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource

from api.manager import PhaseTypeManager

phase_type_api = Namespace('Phase Type', description='Read only api for phase management.')


@phase_type_api.route('/one/<string:phase_id>/')
class GetOnePhaseType(Resource):

	def get(self, phase_id):
		"""
				HTTP method to get details for a phase type
				@returns: response and status code
		"""
		manager = PhaseTypeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one(role_id, phase_id)
		return resp, code


@phase_type_api.route('/all/')
class GetAllPhaseTypes(Resource):

	def get(self):
		"""
				HTTP method to get all phase types
				@returns: response and status code
		"""
		manager = PhaseTypeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all(role_id)
		return resp, code
