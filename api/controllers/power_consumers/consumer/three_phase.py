"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import ThreePhaseConsumerManager
from api.schema import NewThreePhaseConsumerSchema, EditThreePhaseConsumerSchema
from logger import logger

three_phase_consumer_api = Namespace('Three Phase Consumer', description='Api for managing three phase consumers.')

new_consumer = three_phase_consumer_api.model('New Three Phase Consumer', {
	'name': fields.String(required=True, description='The name of the consumer.'),
	'line_id': fields.String(required=True, description='The consumer line used with this consumer.'),
})


@three_phase_consumer_api.route('/new/')
class NewThreePhaseConsumer(Resource):

	@three_phase_consumer_api.expect(new_consumer)
	def post(self):
		"""
				HTTP method to create a three phase consumer
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = NewThreePhaseConsumerSchema(strict=True)
		try:
			new_payload = dict(schema.load(payload).data._asdict())
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = ThreePhaseConsumerManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.new(role_id, new_payload)
		return resp, code



@three_phase_consumer_api.route('/edit/<string:consumer_id>/')
class EditThreePhaseConsumer(Resource):

	@three_phase_consumer_api.expect(new_consumer)
	def put(self, consumer_id):
		"""
				HTTP method to edit a three phase consumer
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = EditThreePhaseConsumerSchema(strict=True)
		try:
			new_payload = dict(schema.load(payload).data._asdict())
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = ThreePhaseConsumerManager()
		role_id = request.cookies.get('role_id')
		new_payload['consumer_id'] = consumer_id
		resp, code = manager.edit(role_id, new_payload)
		return resp, code


@three_phase_consumer_api.route('/one/<string:consumer_id>/')
class GetOneThreePhaseConsumer(Resource):

	def get(self, consumer_id):
		"""
				HTTP method to get details for a three phase consumer
				@returns: response and status code
		"""
		manager = ThreePhaseConsumerManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one(role_id, consumer_id)
		return resp, code

	def delete(self, consumer_id):
		"""
				HTTP method to delete a three phase consumer
				@returns: response and status code
		"""
		manager = ThreePhaseConsumerManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete(role_id, consumer_id)
		return resp, code


@three_phase_consumer_api.route('/all/')
class GetAllThreePhaseConsumers(Resource):

	def get(self):
		"""
				HTTP method to get all three phase consumers
				@returns: response and status code
		"""
		manager = ThreePhaseConsumerManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all(role_id)
		return resp, code


@three_phase_consumer_api.route('/installed/all/')
class GetInstalledThreePhaseConsumers(Resource):

	def get(self):
		"""
				HTTP method to get all installed three phase consumers
				@returns: response and status code
		"""
		manager = ThreePhaseConsumerManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_installed(role_id)
		return resp, code


@three_phase_consumer_api.route('/uninstalled/all/')
class GetUninstalledThreePhaseConsumers(Resource):

	def get(self):
		"""
				Three phase consumers that haven't been installed yet
				@returns: response and status code
		"""
		manager = ThreePhaseConsumerManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_uninstalled(role_id)
		return resp, code
