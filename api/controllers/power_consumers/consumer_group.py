"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import ConsumerGroupManager
from api.schema import NewConsumerGroupSchema, EditConsumerGroupSchema
from logger import logger

consumer_group_api = Namespace('Consumer Group', description='Api for managing consumer groups.')

new_group = consumer_group_api.model('New Consumer Group', {
	'name': fields.String(required=True, description='The name of the consumer group.'),
	'configuration_id': fields.String(required=True, description='The configuration to use with this group. Determines the meters to be used with this group.'),
	'consumer_group_type_id': fields.String(required=True, description='The type of consumer group.'),
})


@consumer_group_api.route('/new/')
class NewConsumerGroup(Resource):

	@consumer_group_api.expect(new_group)
	def post(self):
		"""
				HTTP method to create a new consumer group
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = NewConsumerGroupSchema(strict=True)
		try:
			new_payload = dict(schema.load(payload).data._asdict())
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = ConsumerGroupManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.new(role_id, new_payload)
		return resp, code



@consumer_group_api.route('/edit/<string:group_id>/')
class EditConsumerGroup(Resource):

	@consumer_group_api.expect(new_group)
	def put(self, group_id):
		"""
				HTTP method to edit a consumer group
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = EditConsumerGroupSchema(strict=True)
		try:
			new_payload = dict(schema.load(payload).data._asdict())
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = ConsumerGroupManager()
		role_id = request.cookies.get('role_id')
		new_payload['group_id'] = group_id
		resp, code = manager.edit(role_id, new_payload)
		return resp, code


@consumer_group_api.route('/one/<string:group_id>/')
class GetOneConsumerGroup(Resource):

	def get(self, group_id):
		"""
				HTTP method to get details for a consumer group
				@returns: response and status code
		"""
		manager = ConsumerGroupManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one(role_id, group_id)
		return resp, code

	def delete(self, group_id):
		"""
				HTTP method to delete a consumer group
				@returns: response and status code
		"""
		manager = ConsumerGroupManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete(role_id, group_id)
		return resp, code


@consumer_group_api.route('/all/')
class GetAllConsumerGroups(Resource):

	def get(self):
		"""
				HTTP method to get all consumer groups
				@returns: response and status code
		"""
		manager = ConsumerGroupManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all(role_id)
		return resp, code
