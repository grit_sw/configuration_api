"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import ConsumerGroupTypeManager
from api.schema import NewConsumerGroupTypeSchema, EditConsumerGroupTypeSchema
from logger import logger

consumer_group_type_api = Namespace('Consumer Group Type', description='Api for managing consumer group types.')

new_group_type = consumer_group_type_api.model('New Consumer Group Type', {
	'name': fields.String(required=True, description='The name of the consumer group type.'),
})


@consumer_group_type_api.route('/new/')
class NewConsumerGroup(Resource):

	@consumer_group_type_api.expect(new_group_type)
	def post(self):
		"""
				HTTP method to create a new consumer group type
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = NewConsumerGroupTypeSchema(strict=True)
		try:
			new_payload = dict(schema.load(payload).data._asdict())
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = ConsumerGroupTypeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.new(role_id, new_payload)
		return resp, code



@consumer_group_type_api.route('/edit/<string:consumer_group_type_id>/')
class EditConsumerGroup(Resource):

	@consumer_group_type_api.expect(new_group_type)
	def put(self, consumer_group_type_id):
		"""
				HTTP method to edit a consumer group type
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = EditConsumerGroupTypeSchema(strict=True)
		try:
			new_payload = dict(schema.load(payload).data._asdict())
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = ConsumerGroupTypeManager()
		role_id = request.cookies.get('role_id')
		new_payload['consumer_group_type_id'] = consumer_group_type_id
		resp, code = manager.edit(role_id, new_payload)
		return resp, code


@consumer_group_type_api.route('/one/<string:consumer_group_type_id>/')
class GetOneConsumerGroup(Resource):

	def get(self, consumer_group_type_id):
		"""
				HTTP method to get details for a consumer group type
				@returns: response and status code
		"""
		manager = ConsumerGroupTypeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one(role_id, consumer_group_type_id)
		return resp, code

	def delete(self, consumer_group_type_id):
		"""
				HTTP method to delete a consumer group type
				@returns: response and status code
		"""
		manager = ConsumerGroupTypeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete(role_id, consumer_group_type_id)
		return resp, code


@consumer_group_type_api.route('/all/')
class GetAllConsumerGroups(Resource):

	def get(self):
		"""
				HTTP method to get all consumer group types
				@returns: response and status code
		"""
		manager = ConsumerGroupTypeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all(role_id)
		return resp, code
