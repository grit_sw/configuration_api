"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import ThreePhaseConsumerLineManager
from api.schema import NewThreePhaseConsumerLineSchema, EditThreePhaseConsumerLineSchema
from logger import logger

three_phase_consumer_line_api = Namespace('Three Phase Consumer Line', description='Api for managing three phase consumer lines.')

new_three_phase_line = three_phase_consumer_line_api.model('New Three Phase Consumer Line', {
	'name': fields.String(required=True, description='The name of the consumer line.'),
	'signal_threshold': fields.Float(required=False, description='The lowest current reading to be measured by the meter measuring the one phase grid.'),
	'consumer_group_id': fields.String(required=True, description='The consumer group.'),
	'channels': fields.List(fields.String(required=True, description='The channels on a probe that will be used with this device.')),
})


@three_phase_consumer_line_api.route('/new/')
class NewThreePhaseConsumerLine(Resource):

	@three_phase_consumer_line_api.expect(new_three_phase_line)
	def post(self):
		"""
				HTTP method to create a new three phase consumer line
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = NewThreePhaseConsumerLineSchema(strict=True)
		try:
			new_payload = dict(schema.load(payload).data._asdict())
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = ThreePhaseConsumerLineManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.new(role_id, new_payload)
		return resp, code



@three_phase_consumer_line_api.route('/edit/<string:line_id>/')
class EditThreePhaseConsumerLine(Resource):

	@three_phase_consumer_line_api.expect(new_three_phase_line)
	def put(self, line_id):
		"""
				HTTP method to edit a consumer line
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = EditThreePhaseConsumerLineSchema(strict=True)
		try:
			new_payload = dict(schema.load(payload).data._asdict())
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = ThreePhaseConsumerLineManager()
		role_id = request.cookies.get('role_id')
		new_payload['line_id'] = line_id
		resp, code = manager.edit(role_id, new_payload)
		return resp, code


@three_phase_consumer_line_api.route('/one/<string:line_id>/')
class GetOneThreePhaseConsumerLine(Resource):

	def get(self, line_id):
		"""
				HTTP method to get details for a consumer line
				@returns: response and status code
		"""
		manager = ThreePhaseConsumerLineManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one(role_id, line_id)
		return resp, code

	def delete(self, line_id):
		"""
				HTTP method to delete a consumer line
				@returns: response and status code
		"""
		manager = ThreePhaseConsumerLineManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete(role_id, line_id)
		return resp, code


@three_phase_consumer_line_api.route('/all/')
class GetAllThreePhaseConsumerLines(Resource):

	def get(self):
		"""
				HTTP method to get all consumer lines
				@returns: response and status code
		"""
		manager = ThreePhaseConsumerLineManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all(role_id)
		return resp, code
