"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import SinglePhaseGeneratorManager
from api.schema import SinglePhaseGeneratorSchema, EditSinglePhaseGeneratorSchema
from logger import logger

single_phase_generator_api = Namespace('One Phase Generator', description='Api for managing one phase generators.')

new_charge = single_phase_generator_api.model('New One Phase Generator Billing Period', {
	'start_time': fields.String(required=True, description='The start time for this billing period.'),
	'stop_time': fields.String(required=False, description='The stop time for this billing period.'),
	'amount': fields.Float(required=False, description='The amount to be billed.'),
})

edit_charge = single_phase_generator_api.model('Edit One Phase Generator Billing Period', {
	'map_id': fields.String(required=False, description='The ID of the charge billing period.'),
	'start_time': fields.String(required=True, description='The start time for this billing period.'),
	'stop_time': fields.String(required=False, description='The stop time for this billing period.'),
	'amount': fields.Float(required=False, description='The amount to be billed.'),
})

new_window = single_phase_generator_api.model('New One Phase Generator Operating Window', {
	'start_time': fields.String(required=True, description='The start time the generator will be on from.'),
	'stop_time': fields.String(required=False, description='The stop time the generator will be disconnected.'),
})

edit_window = single_phase_generator_api.model('Edit One Phase Generator Operating Window', {
	'window_id': fields.String(required=False, description='The ID of the operating window.'),
	'start_time': fields.String(required=True, description='The start the generator will be on from.'),
	'stop_time': fields.String(required=False, description='The stop time the generator will be disconnected.'),
})

new_generator_map = single_phase_generator_api.model('New One Phase Generator Fuel Consumption Map', {
	'quarter_load_price': fields.Float(required=True, description='The price at quarter load.'),
	'half_load_price': fields.Float(required=False, description='The price at half load.'),
	'three_quarter_load_price': fields.Float(required=False, description='The price at three-quarter load.'),
	'full_load_price': fields.Float(required=False, description='The price at full load.'),
})

edit_generator_map = single_phase_generator_api.model('Edit One Phase Generator Operating Window', {
	'map_id': fields.Float(required=False, description='The ID of the operating window.'),
	'quarter_load_price': fields.Float(required=True, description='The price at quarter load.'),
	'half_load_price': fields.Float(required=False, description='The price at half load.'),
	'three_quarter_load_price': fields.Float(required=False, description='The price at three-quarter load.'),
	'full_load_price': fields.Float(required=False, description='The price at full load.'),
})

new_one_phase_generator = single_phase_generator_api.model('New One Phase Generator', {
	'name': fields.String(required=True, description='The name of the one_phase_generator.'),
	'signal_threshold': fields.Float(required=False, description='The lowest current reading to be measured by the meter measuring the one phase generator.'),
	'configuration_id': fields.String(required=False, description='The configuration to use for this power source.'),
	'probe_id': fields.String(required=True, description='The probe that will be used to measure this power source.'),
	'probe_channel_id': fields.String(required=True, description='The channel on a probe that will be used with this device.'),
	'apparent_power_rating': fields.Float(required=True, description='The rating of the generator.'),
	'fuel_id': fields.String(required=True, description='The ID of the fuel size.'),
	'fuel_store_size': fields.Float(required=True, description='Fuel tank size in litres.'),
	'operating_window': fields.List(fields.Nested(new_window, required=True, description='List of charges for different times.')),
	'charge_map': fields.List(fields.Nested(new_charge, required=True, description='List of new charges and their billing times.')),
	'generator_map': fields.List(fields.Nested(new_generator_map, required=True, description='List of new billing rules at different capacities.')),
})

edit_one_phase_generator = single_phase_generator_api.model('Edit One Phase Generator', {
	'name': fields.String(required=True, description='The name of the one_phase_generator.'),
	'signal_threshold': fields.Float(required=False, description='The lowest current reading to be measured by the meter measuring the one phase generator.'),
	'configuration_id': fields.String(required=False, description='The configuration to use for this power source.'),
	'probe_id': fields.String(required=True, description='The probe that will be used to measure this power source.'),
	'probe_channel_id': fields.String(required=True, description='The channel on a probe that will be used with this device.'),
	'apparent_power_rating': fields.Float(required=True, description='The rating of the generator.'),
	'fuel_id': fields.String(required=True, description='The ID of the fuel size.'),
	'fuel_store_size': fields.Float(required=True, description='Fuel tank size in litres.'),
	'operating_window': fields.List(fields.Nested(new_window, required=True, description='The channel on a probe that will be used with this device.')),
	'new_operating_window': fields.List(fields.Nested(edit_window, required=True, description='The channel on a probe that will be used with this device.')),
	'charge_map': fields.List(fields.Nested(edit_charge, required=True, description='List of charges for different times.')),
	'new_charge_map': fields.List(fields.Nested(new_charge, required=True, description='List of new charges and their billing times.')),
	'generator_map': fields.List(fields.Nested(edit_generator_map, required=True, description='List of billing rules at different capacities.')),
	'new_generator_map': fields.List(fields.Nested(new_generator_map, required=True, description='List of new billing rules at different capacities.')),
})


@single_phase_generator_api.route('/new/')
class NewSinglePhaseGenerator(Resource):

	@single_phase_generator_api.expect(new_one_phase_generator)
	def post(self):
		"""
				HTTP method to create a new one phase generator
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = SinglePhaseGeneratorSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		charge_map = [charge._asdict() for charge in new_payload['charge_map']]
		new_payload['charge_map'] = charge_map
		operating_window = [window._asdict() for window in new_payload['operating_window']]
		new_payload['operating_window'] = operating_window
		generator_map = [gen_map._asdict() for gen_map in new_payload['generator_map']]
		new_payload['generator_map'] = generator_map
		new_payload = dict(new_payload)
		manager = SinglePhaseGeneratorManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.new(role_id, new_payload)
		return resp, code



@single_phase_generator_api.route('/edit/<string:single_phase_generator_id>/')
class EditSinglePhaseGenerator(Resource):

	@single_phase_generator_api.expect(edit_one_phase_generator)
	def put(self, single_phase_generator_id):
		"""
				HTTP method to edit a one phase generator
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = EditSinglePhaseGeneratorSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		charge_map = [charge._asdict() for charge in new_payload['charge_map']]
		new_payload['charge_map'] = charge_map
		operating_window = [window._asdict() for window in new_payload['operating_window']]
		new_payload['operating_window'] = operating_window
		new_charge_map = [charge._asdict() for charge in new_payload['new_charge_map']]
		generator_map = [gen_map._asdict() for gen_map in new_payload['generator_map']]
		new_payload['generator_map'] = generator_map

		new_payload['new_charge_map'] = new_charge_map
		new_operating_window = [window._asdict() for window in new_payload['new_operating_window']]
		new_payload['new_operating_window'] = new_operating_window
		new_generator_map_ = [gen_map._asdict() for gen_map in new_payload['new_generator_map']]
		new_payload['new_generator_map'] = new_generator_map_

		new_payload = dict(new_payload)
		manager = SinglePhaseGeneratorManager()
		role_id = request.cookies.get('role_id')
		new_payload['single_phase_generator_id'] = single_phase_generator_id
		resp, code = manager.edit(role_id, new_payload)
		return resp, code


@single_phase_generator_api.route('/one/<string:single_phase_generator_id>/')
class GetOneSinglePhaseGenerator(Resource):

	def get(self, single_phase_generator_id):
		"""
				HTTP method to get details for a one phase generator
				@returns: response and status code
		"""
		manager = SinglePhaseGeneratorManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one(role_id, single_phase_generator_id)
		return resp, code

	def delete(self, single_phase_generator_id):
		"""
				HTTP method to delete a one phase generator
				@returns: response and status code
		"""
		manager = SinglePhaseGeneratorManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete(role_id, single_phase_generator_id)
		return resp, code


@single_phase_generator_api.route('/all/')
class GetAllSinglePhaseGenerators(Resource):

	def get(self):
		"""
				HTTP method to get all single phase generator power sources
				@returns: response and status code
		"""
		manager = SinglePhaseGeneratorManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all(role_id)
		return resp, code


@single_phase_generator_api.route('/installed/all/')
class GetInstalledSinglePhaseGenerators(Resource):

	def get(self):
		"""
				HTTP method to get all installed single phase generators
				@returns: response and status code
		"""
		manager = SinglePhaseGeneratorManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_installed(role_id)
		return resp, code


@single_phase_generator_api.route('/uninstalled/all/')
class GetUninstalledSinglePhaseGenerators(Resource):

	def get(self):
		"""
				Single phase generators that haven't been installed yet
				@returns: response and status code
		"""
		manager = SinglePhaseGeneratorManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_uninstalled(role_id)
		return resp, code
