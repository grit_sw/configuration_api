"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import ThreePhaseGeneratorManager
from api.schema import ThreePhaseGeneratorSchema, EditThreePhaseGeneratorSchema
from logger import logger

three_phase_generator_api = Namespace('Three Phase Generator', description='Api for managing three phase generators.')

new_charge = three_phase_generator_api.model('Three Phase Generator Billing Period', {
	'start_time': fields.String(required=True, description='The start time for this billinng period.'),
	'stop_time': fields.String(required=False, description='The stop time for this billinng period.'),
	'amount': fields.Float(required=False, description='The amount to be billed.'),
})

edit_charge = three_phase_generator_api.model('Three Phase Generator Edit Billing Period', {
	'map_id': fields.String(required=False, description='The ID of the charge billing period.'),
	'start_time': fields.String(required=True, description='The start time for this billinng period.'),
	'stop_time': fields.String(required=False, description='The stop time for this billinng period.'),
	'amount': fields.Float(required=False, description='The amount to be billed.'),
})

new_window = three_phase_generator_api.model('New Three Phase Generator Operating Window', {
	'start_time': fields.String(required=True, description='The start time the generator will be on from.'),
	'stop_time': fields.String(required=False, description='The stop time the generator will be disconnected.'),
})

edit_window = three_phase_generator_api.model('Edit Three Phase Generator Operating Window', {
	'window_id': fields.String(required=False, description='The ID of the operating window.'),
	'start_time': fields.String(required=True, description='The start the generator will be on from.'),
	'stop_time': fields.String(required=False, description='The stop time the generator will be disconnected.'),
})

new_generator_map = three_phase_generator_api.model('New Three Phase Generator Fuel Consumption Map', {
	'quarter_load_price': fields.Float(required=True, description='The price at quarter load.'),
	'half_load_price': fields.Float(required=False, description='The price at half load.'),
	'three_quarter_load_price': fields.Float(required=False, description='The price at three-quarter load.'),
	'full_load_price': fields.Float(required=False, description='The price at full load.'),
})

edit_generator_map = three_phase_generator_api.model('Edit Three Phase Generator Operating Window', {
	'map_id': fields.Float(required=False, description='The ID of the operating window.'),
	'quarter_load_price': fields.Float(required=True, description='The price at quarter load.'),
	'half_load_price': fields.Float(required=False, description='The price at half load.'),
	'three_quarter_load_price': fields.Float(required=False, description='The price at three-quarter load.'),
	'full_load_price': fields.Float(required=False, description='The price at full load.'),
})

new_three_phase_generator = three_phase_generator_api.model('New Three Phase Generator', {
	'name': fields.String(required=True, description='The name of the generator.'),
	'signal_threshold': fields.Float(required=False, description='The lowest current reading to be measured by the meter measuring the one phase generator.'),
	'configuration_id': fields.String(required=False, description='The configuration to use for this power source.'),
	'probe_id': fields.String(required=True, description='The probe that will be used to measure this power source.'),
	'probe_channels': fields.List(fields.String(required=True, description='The channels on a probe that will be used with this device.')),
	'apparent_power_rating': fields.Float(required=False, description='The power rating of the generator in VA.'),
	'fuel_id': fields.String(required=False, description='The ID of the selected fuel type.'),
	'fuel_store_size': fields.Float(required=False, description='The fuel tank size.'),
	'charge_map': fields.List(fields.Nested(new_charge, required=True, description='The billing info for the generator.')),
	'operating_window': fields.List(fields.Nested(new_window, required=True, description='The billing info for the generator.')),
	'generator_map': fields.List(fields.Nested(edit_generator_map, required=True, description='List of billing rules at different capacities.')),
})

edit_three_phase_generator = three_phase_generator_api.model('Edit Three Phase Generator', {
	'name': fields.String(required=True, description='The name of the generator.'),
	'signal_threshold': fields.Float(required=False, description='The lowest current reading to be measured by the meter measuring the one phase generator.'),
	'configuration_id': fields.String(required=False, description='The configuration to use for this power source.'),
	'probe_id': fields.String(required=True, description='The probe that will be used to measure this power source.'),
	'probe_channels': fields.List(fields.String(required=True, description='The channels on a probe that will be used with this device.')),
	'apparent_power_rating': fields.Float(required=False, description='The power rating of the generator in VA.'),
	'fuel_id': fields.String(required=False, description='The ID of the selected fuel type.'),
	'fuel_store_size': fields.Float(required=False, description='The fuel tank size.'),
	'operating_window': fields.List(fields.Nested(edit_window, required=True, description='Operating time window for the generator.')),
	'new_operating_window': fields.List(fields.Nested(new_window, required=True, description='Operating time window for the generator.')),
	'charge_map': fields.List(fields.Nested(edit_charge, required=True, description='The billing info for the generator.')),
	'new_charge_map': fields.List(fields.Nested(new_charge, required=True, description='The billing info for the generator.')),
	'generator_map': fields.List(fields.Nested(edit_generator_map, required=True, description='List of billing rules at different capacities.')),
	'new_generator_map': fields.List(fields.Nested(new_generator_map, required=True, description='List of new billing rules at different capacities.')),
})


@three_phase_generator_api.route('/new/')
class NewThreePhaseGenerator(Resource):

	@three_phase_generator_api.expect(new_three_phase_generator)
	def post(self):
		"""
				HTTP method to create a new one phase generator
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = ThreePhaseGeneratorSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		charge_map = [charge._asdict() for charge in new_payload['charge_map']]
		new_payload['charge_map'] = charge_map
		operating_window = [window._asdict() for window in new_payload['operating_window']]
		new_payload['operating_window'] = operating_window
		generator_map = [gen_map._asdict() for gen_map in new_payload['generator_map']]
		new_payload['generator_map'] = generator_map

		new_payload = dict(new_payload)
		manager = ThreePhaseGeneratorManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.new(role_id, new_payload)
		return resp, code


@three_phase_generator_api.route('/edit/<string:three_phase_generator_id>/')
class EditThreePhaseGenerator(Resource):

	@three_phase_generator_api.expect(edit_three_phase_generator)
	def put(self, three_phase_generator_id):
		"""
				HTTP method to edit a one phase generator
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = EditThreePhaseGeneratorSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		charge_map = [charge._asdict() for charge in new_payload['charge_map']]
		new_payload['charge_map'] = charge_map
		new_charge_map = [charge._asdict() for charge in new_payload['new_charge_map']]
		new_payload['new_charge_map'] = new_charge_map
		generator_map = [gen_map._asdict() for gen_map in new_payload['generator_map']]
		new_payload['generator_map'] = generator_map

		operating_window = [window._asdict() for window in new_payload['operating_window']]
		new_payload['operating_window'] = operating_window
		new_operating_window = [window._asdict() for window in new_payload['new_operating_window']]
		new_payload['new_operating_window'] = new_operating_window
		new_generator_map_ = [gen_map._asdict() for gen_map in new_payload['new_generator_map']]
		new_payload['new_generator_map'] = new_generator_map_

		new_payload = dict(new_payload)
		manager = ThreePhaseGeneratorManager()
		role_id = request.cookies.get('role_id')
		new_payload['three_phase_generator_id'] = three_phase_generator_id
		resp, code = manager.edit(role_id, new_payload)
		return resp, code


@three_phase_generator_api.route('/one/<string:three_phase_generator_id>/')
class GetThreeThreePhaseGenerator(Resource):

	def get(self, three_phase_generator_id):
		"""
				HTTP method to get details for a one phase generator
				@returns: response and status code
		"""
		manager = ThreePhaseGeneratorManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one(role_id, three_phase_generator_id)
		return resp, code

	def delete(self, three_phase_generator_id):
		"""
				HTTP method to delete a one phase generator
				@returns: response and status code
		"""
		manager = ThreePhaseGeneratorManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete(role_id, three_phase_generator_id)
		return resp, code


@three_phase_generator_api.route('/all/')
class GetAllThreePhaseGenerators(Resource):

	def get(self):
		"""
				HTTP method to get all single phase generator power sources
				@returns: response and status code
		"""
		manager = ThreePhaseGeneratorManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all(role_id)
		return resp, code


@three_phase_generator_api.route('/installed/all/')
class GetInstalledThreePhaseGenerators(Resource):

	def get(self):
		"""
				HTTP method to get all installed three phase generators
				@returns: response and status code
		"""
		manager = ThreePhaseGeneratorManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_installed(role_id)
		return resp, code


@three_phase_generator_api.route('/uninstalled/all/')
class GetUninstalledThreePhaseGenerators(Resource):

	def get(self):
		"""
				Three phase generators that haven't been installed yet
				@returns: response and status code
		"""
		manager = ThreePhaseGeneratorManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_uninstalled(role_id)
		return resp, code
