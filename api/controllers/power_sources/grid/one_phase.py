"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import SinglePhaseGridManager
from api.schema import SinglePhaseGridSchema, EditSinglePhaseGridSchema
from logger import logger

single_phase_grid_api = Namespace('One Phase Grid', description='Api for managing one phase grid.')

new_charge = single_phase_grid_api.model('Single Phase Grid Billing Period', {
	'start_time': fields.String(required=True, description='The start time for this billinng period.'),
	'stop_time': fields.String(required=False, description='The stop time for this billinng period.'),
	'amount': fields.Float(required=False, description='The amount to be billed.'),
})

edit_charge = single_phase_grid_api.model('Single Phase Grid Edit Billing Period', {
	'map_id': fields.String(required=False, description='The ID of the charge billing period.'),
	'start_time': fields.String(required=True, description='The start time for this billinng period.'),
	'stop_time': fields.String(required=False, description='The stop time for this billinng period.'),
	'amount': fields.Float(required=False, description='The amount to be billed.'),
})

new_one_phase_grid = single_phase_grid_api.model('New One Phase Grid', {
	'name': fields.String(required=True, description='The name of the one_phase_grid.'),
	'signal_threshold': fields.Float(required=False, description='The lowest current reading to be measured by the meter measuring the one phase grid.'),
	'configuration_id': fields.String(required=False, description='The configuration to use for this power source.'),
	'probe_id': fields.String(required=True, description='The probe that will be used to measure this power source.'),
	'probe_channel_id': fields.String(required=True, description='The channel on a probe that will be used with this device.'),
	'charge_map': fields.List(fields.Nested(new_charge, required=True, description='The channel on a probe that will be used with this device.')),
})

edit_one_phase_grid = single_phase_grid_api.model('Edit One Phase Grid', {
	'name': fields.String(required=True, description='The name of the one_phase_grid.'),
	'signal_threshold': fields.Float(required=False, description='The lowest current reading to be measured by the meter measuring the one phase grid.'),
	'configuration_id': fields.String(required=False, description='The configuration to use for this power source.'),
	'probe_id': fields.String(required=True, description='The probe that will be used to measure this power source.'),
	'probe_channel_id': fields.String(required=True, description='The channel on a probe that will be used with this device.'),
	'charge_map': fields.List(fields.Nested(edit_charge, required=True, description='The channel on a probe that will be used with this device.')),
	'new_charge_map': fields.List(fields.Nested(new_charge, required=True, description='The channel on a probe that will be used with this device.')),
})


@single_phase_grid_api.route('/new/')
class NewSinglePhaseGrid(Resource):

	@single_phase_grid_api.expect(new_one_phase_grid)
	def post(self):
		"""
				HTTP method to create a new one phase grid
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = SinglePhaseGridSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		charge_map = [charge._asdict() for charge in new_payload['charge_map']]
		new_payload['charge_map'] = charge_map
		new_payload = dict(new_payload)
		manager = SinglePhaseGridManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.new(role_id, new_payload)
		return resp, code



@single_phase_grid_api.route('/edit/<string:single_phase_grid_id>/')
class EditSinglePhaseGrid(Resource):

	@single_phase_grid_api.expect(edit_one_phase_grid)
	def put(self, single_phase_grid_id):
		"""
				HTTP method to edit a one phase grid
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = EditSinglePhaseGridSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		charge_map = [charge._asdict() for charge in new_payload['charge_map']]
		new_payload['charge_map'] = charge_map
		print(new_payload['new_charge_map'])
		new_charge_map = [charge._asdict() for charge in new_payload['new_charge_map']]
		new_payload['new_charge_map'] = new_charge_map
		new_payload = dict(new_payload)
		manager = SinglePhaseGridManager()
		role_id = request.cookies.get('role_id')
		new_payload['single_phase_grid_id'] = single_phase_grid_id
		resp, code = manager.edit(role_id, new_payload)
		return resp, code


@single_phase_grid_api.route('/one/<string:single_phase_grid_id>/')
class GetOneSinglePhaseGrid(Resource):

	def get(self, single_phase_grid_id):
		"""
				HTTP method to get details for a one phase grid
				@returns: response and status code
		"""
		manager = SinglePhaseGridManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one(role_id, single_phase_grid_id)
		return resp, code

	def delete(self, single_phase_grid_id):
		"""
				HTTP method to delete a one phase grid
				@returns: response and status code
		"""
		manager = SinglePhaseGridManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete(role_id, single_phase_grid_id)
		return resp, code


@single_phase_grid_api.route('/all/')
class GetAllSinglePhaseGrids(Resource):

	def get(self):
		"""
				HTTP method to get all single phase grid power sources
				@returns: response and status code
		"""
		manager = SinglePhaseGridManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all(role_id)
		return resp, code


@single_phase_grid_api.route('/installed/all/')
class GetInstalledSinglePhaseGrids(Resource):

	def get(self):
		"""
				HTTP method to get all installed single phase grid
				@returns: response and status code
		"""
		manager = SinglePhaseGridManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_installed(role_id)
		return resp, code


@single_phase_grid_api.route('/uninstalled/all/')
class GetUninstalledSinglePhaseGrids(Resource):

	def get(self):
		"""
				Single phase grid that haven't been installed yet
				@returns: response and status code
		"""
		manager = SinglePhaseGridManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_uninstalled(role_id)
		return resp, code
