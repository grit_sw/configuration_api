"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import ThreePhaseGridManager
from api.schema import ThreePhaseGridSchema, EditThreePhaseGridSchema
from logger import logger

three_phase_grid_api = Namespace('Three Phase Grid', description='Api for managing three phase grid.')

new_charge = three_phase_grid_api.model('Three Phase Grid Billing Period', {
	'start_time': fields.String(required=True, description='The start time for this billinng period.'),
	'stop_time': fields.String(required=False, description='The stop time for this billinng period.'),
	'amount': fields.Float(required=False, description='The amount to be billed.'),
})

edit_charge = three_phase_grid_api.model('Three Phase Grid Edit Billing Period', {
	'map_id': fields.String(required=False, description='The ID of the charge billing period.'),
	'start_time': fields.String(required=True, description='The start time for this billinng period.'),
	'stop_time': fields.String(required=False, description='The stop time for this billinng period.'),
	'amount': fields.Float(required=False, description='The amount to be billed.'),
})

new_three_phase_grid = three_phase_grid_api.model('New Three Phase Grid', {
	'name': fields.String(required=True, description='The name of the one_phase_grid.'),
	'signal_threshold': fields.Float(required=False, description='The lowest current reading to be measured by the meter measuring the one phase grid.'),
	'configuration_id': fields.String(required=False, description='The configuration to use for this power source.'),
	'probe_id': fields.String(required=True, description='The probe that will be used to measure this power source.'),
	'probe_channels': fields.List(fields.String(required=True, description='The channels on a probe that will be used with this device.')),
	'charge_map': fields.List(fields.Nested(new_charge, required=True, description='The channel on a probe that will be used with this device.')),
})

edit_three_phase_grid = three_phase_grid_api.model('Edit Three Phase Grid', {
	'name': fields.String(required=True, description='The name of the one_phase_grid.'),
	'signal_threshold': fields.Float(required=False, description='The lowest current reading to be measured by the meter measuring the one phase grid.'),
	'configuration_id': fields.String(required=False, description='The configuration to use for this power source.'),
	'probe_id': fields.String(required=True, description='The probe that will be used to measure this power source.'),
	'probe_channels': fields.List(fields.String(required=True, description='The channels on a probe that will be used with this device.')),
	'charge_map': fields.List(fields.Nested(edit_charge, required=True, description='The channel on a probe that will be used with this device.')),
	'new_charge_map': fields.List(fields.Nested(new_charge, required=True, description='The channel on a probe that will be used with this device.')),
})


@three_phase_grid_api.route('/new/')
class NewThreePhaseGrid(Resource):

	@three_phase_grid_api.expect(new_three_phase_grid)
	def post(self):
		"""
				HTTP method to create a new one phase grid
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = ThreePhaseGridSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		charge_map = [charge._asdict() for charge in new_payload['charge_map']]
		new_payload['charge_map'] = charge_map
		new_payload = dict(new_payload)
		manager = ThreePhaseGridManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.new(role_id, new_payload)
		return resp, code


@three_phase_grid_api.route('/edit/<string:three_phase_grid_id>/')
class EditThreePhaseGrid(Resource):

	@three_phase_grid_api.expect(edit_three_phase_grid)
	def put(self, three_phase_grid_id):
		"""
				HTTP method to edit a one phase grid
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = EditThreePhaseGridSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		charge_map = [charge._asdict() for charge in new_payload['charge_map']]
		new_payload['charge_map'] = charge_map
		print(new_payload['new_charge_map'])
		new_charge_map = [charge._asdict() for charge in new_payload['new_charge_map']]
		new_payload['new_charge_map'] = new_charge_map
		new_payload = dict(new_payload)
		manager = ThreePhaseGridManager()
		role_id = request.cookies.get('role_id')
		new_payload['three_phase_grid_id'] = three_phase_grid_id
		resp, code = manager.edit(role_id, new_payload)
		return resp, code


@three_phase_grid_api.route('/one/<string:three_phase_grid_id>/')
class GetThreeThreePhaseGrid(Resource):

	def get(self, three_phase_grid_id):
		"""
				HTTP method to get details for a one phase grid
				@returns: response and status code
		"""
		manager = ThreePhaseGridManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one(role_id, three_phase_grid_id)
		return resp, code

	def delete(self, three_phase_grid_id):
		"""
				HTTP method to delete a one phase grid
				@returns: response and status code
		"""
		manager = ThreePhaseGridManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete(role_id, three_phase_grid_id)
		return resp, code


@three_phase_grid_api.route('/all/')
class GetAllThreePhaseGrids(Resource):

	def get(self):
		"""
				HTTP method to get all three phase grid power sources
				@returns: response and status code
		"""
		manager = ThreePhaseGridManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all(role_id)
		return resp, code


@three_phase_grid_api.route('/installed/all/')
class GetInstalledThreePhaseGrids(Resource):

	def get(self):
		"""
				HTTP method to get all installed three phase grids
				@returns: response and status code
		"""
		manager = ThreePhaseGridManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_installed(role_id)
		return resp, code


@three_phase_grid_api.route('/uninstalled/all/')
class GetUninstalledThreePhaseGrids(Resource):

	def get(self):
		"""
				Three phase grids that haven't been installed yet
				@returns: response and status code
		"""
		manager = ThreePhaseGridManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_uninstalled(role_id)
		return resp, code
