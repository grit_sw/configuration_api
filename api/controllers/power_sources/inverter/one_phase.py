"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import SinglePhaseInverterManager
from api.schema import SinglePhaseInverterSchema, EditSinglePhaseInverterSchema
from logger import logger

single_phase_inverter_api = Namespace('One Phase Inverter', description='Api for managing one phase inverter.')

new_battery_meta = single_phase_inverter_api.model('One Phase Inverter New Battery Meta', {
	'depth_of_discharge': fields.Float(required=False, description='The rated depth of discharge of the battery.'),
	'number_of_cycles': fields.Integer(required=False, description='The battery count.'),
	'battery_capacity': fields.Float(required=False, description='The voltage rating of the battery.'),
})

edit_battery_meta = single_phase_inverter_api.model('One Phase Inverter Edit Battery Meta', {
	'battery_id': fields.String(required=False, description='The ID of the battery.'),
	'depth_of_discharge': fields.Float(required=False, description='The rated depth of discharge of the battery.'),
	'number_of_cycles': fields.Integer(required=False, description='The battery count.'),
	'battery_capacity': fields.Float(required=False, description='The voltage rating of the battery.'),
})

new_battery = single_phase_inverter_api.model('One Phase Inverter Billing Period', {
	'capacity': fields.String(required=True, description='The battery capacity.'),
	'count': fields.Integer(required=False, description='The battery count.'),
	'voltage': fields.Float(required=False, description='The voltage rating of the battery.'),
	'bank_voltage': fields.Float(required=False, description='The voltage rating of the battery bank.'),
	'depth_of_discharge': fields.Float(required=False, description='The rated depth of discharge of the battery.'),
	'chemistry': fields.String(required=False, description='The material used to make the battery.'),
	'end_of_life': fields.Float(required=False, description='The end of life of the battery.'),
	'meta': fields.List(fields.Nested(new_battery_meta, required=True, description='Metadata for each battery in the battery bank.')),
})

edit_battery = single_phase_inverter_api.model('One Phase Inverter Edit Billing Period', {
	'bank_id': fields.String(required=False, description='The ID of the battery bank.'),
	'capacity': fields.String(required=True, description='The battery capacity.'),
	'count': fields.Integer(required=False, description='The battery count.'),
	'voltage': fields.Float(required=False, description='The voltage rating of the battery.'),
	'bank_voltage': fields.Float(required=False, description='The voltage rating of the battery bank.'),
	'depth_of_discharge': fields.Float(required=False, description='The rated depth of discharge of the battery.'),
	'chemistry': fields.Float(required=False, description='The material used to make the battery.'),
	'end_of_life': fields.Float(required=False, description='The end of life of the battery.'),
	'meta': fields.List(fields.Nested(edit_battery_meta, required=True, description='Metadata for each battery in the battery bank.')),
})

new_window = single_phase_inverter_api.model('One Phase Inverter New Operating Window', {
	'start_time': fields.String(required=True, description='The start time the inverter will be on from.'),
	'stop_time': fields.String(required=False, description='The stop time the inverter will be disconnected.'),
})

edit_window = single_phase_inverter_api.model('Edit One Phase Inverter Operating Window', {
	'window_id': fields.String(required=False, description='The ID of the operating window.'),
	'start_time': fields.String(required=True, description='The start the inverter will be on from.'),
	'stop_time': fields.String(required=False, description='The stop time the inverter will be disconnected.'),
})


new_one_phase_inverter = single_phase_inverter_api.model('New One Phase Inverter', {
	'name': fields.String(required=True, description='The name of the one_phase_inverter.'),
	'signal_threshold': fields.Float(required=False, description='The lowest current reading to be measured by the meter measuring the one phase inverter.'),
	'configuration_id': fields.String(required=False, description='The configuration to use for this power source.'),
	'input_channel_id': fields.String(required=False, description='The channel to use for this power source.'),
	'output_channel_id': fields.String(required=False, description='The channel to use for this power source.'),
	'apparent_power_rating': fields.String(required=True, description='The power rating of the inverter.'),
	'efficiency': fields.String(required=True, description='The efficiency of this inverter.'),
	'bulk_charging_current': fields.String(required=True, description='The inverter\'s bulk charging current.'),
	'cost': fields.String(required=True, description='The amount of money the inverter cost.'),
	'operating_window': fields.List(fields.Nested(new_window, required=True, description='The period the inverter is expected to be ON.')),
	'battery': fields.List(fields.Nested(new_battery, required=True, description='The period the inverter is expected to be ON.')),
})

edit_one_phase_inverter = single_phase_inverter_api.model('Edit One Phase Inverter', {
	'name': fields.String(required=True, description='The name of the one_phase_inverter.'),
	'signal_threshold': fields.Float(required=False, description='The lowest current reading to be measured by the meter measuring the one phase inverter.'),
	'configuration_id': fields.String(required=False, description='The configuration to use for this power source.'),
	'input_channel_id': fields.String(required=False, description='The channel to use for this power source.'),
	'output_channel_id': fields.String(required=False, description='The channel to use for this power source.'),
	'apparent_power_rating': fields.String(required=True, description='The power rating of the inverter.'),
	'efficiency': fields.String(required=True, description='The efficiency of this inverter.'),
	'bulk_charging_current': fields.String(required=True, description='The inverter\'s bulk charging current.'),
	'cost': fields.String(required=True, description='The amount of money the inverter cost.'),
	'operating_window': fields.List(fields.Nested(edit_window, required=True, description='The period the inverter is expected to be ON.')),
	'new_operating_window': fields.List(fields.Nested(new_window, required=True, description='The period the inverter is expected to be ON.')),
	'battery': fields.List(fields.Nested(edit_battery, required=True, description='The period the inverter is expected to be ON.')),
})


@single_phase_inverter_api.route('/new/')
class NewSinglePhaseInverter(Resource):

	@single_phase_inverter_api.expect(new_one_phase_inverter)
	def post(self):
		"""
				HTTP method to create a new one phase inverter
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = SinglePhaseInverterSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		operating_window = [dict(window._asdict()) for window in new_payload['operating_window']]
		new_payload['operating_window'] = operating_window
		battery = [dict(battery_._asdict()) for battery_ in new_payload['battery']]
		new_payload['battery'] = battery
		print(new_payload['battery'])
		for battery_ in new_payload['battery']:
			meta = [dict(meta_._asdict()) for meta_ in battery_['meta']]
			battery_['meta'] = meta
		# meta = [dict(meta_._asdict()) for battery_ in new_payload['battery'] for meta_ in battery_['meta']]
		# print(meta)
		# new_payload['battery']['meta'] = meta
		new_payload = dict(new_payload)
		print(new_payload)
		manager = SinglePhaseInverterManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.new(role_id, new_payload)
		return resp, code



@single_phase_inverter_api.route('/edit/<string:single_phase_inverter_id>/')
class EditSinglePhaseInverter(Resource):

	@single_phase_inverter_api.expect(edit_one_phase_inverter)
	def put(self, single_phase_inverter_id):
		"""
				HTTP method to edit a one phase inverter
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = EditSinglePhaseInverterSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		operating_window = [dict(window._asdict()) for window in new_payload['operating_window']]
		new_payload['operating_window'] = operating_window
		print(new_payload['new_operating_window'])
		new_operating_window = [dict(window._asdict()) for window in new_payload['new_operating_window']]
		new_payload['new_operating_window'] = new_operating_window
		battery = [dict(battery_._asdict()) for battery_ in new_payload['battery']]
		new_payload['battery'] = battery
		for battery_ in new_payload['battery']:
			meta = [dict(meta_._asdict()) for meta_ in battery_['meta']]
			battery_['meta'] = meta
		new_payload = dict(new_payload)
		manager = SinglePhaseInverterManager()
		role_id = request.cookies.get('role_id')
		new_payload['single_phase_inverter_id'] = single_phase_inverter_id
		resp, code = manager.edit(role_id, new_payload)
		return resp, code


@single_phase_inverter_api.route('/one/<string:single_phase_inverter_id>/')
class GetOneSinglePhaseInverter(Resource):

	def get(self, single_phase_inverter_id):
		"""
				HTTP method to get details for a one phase inverter
				@returns: response and status code
		"""
		manager = SinglePhaseInverterManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one(role_id, single_phase_inverter_id)
		return resp, code

	def delete(self, single_phase_inverter_id):
		"""
				HTTP method to delete a one phase inverter
				@returns: response and status code
		"""
		manager = SinglePhaseInverterManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete(role_id, single_phase_inverter_id)
		return resp, code


@single_phase_inverter_api.route('/all/')
class GetAllSinglePhaseInverters(Resource):

	def get(self):
		"""
				HTTP method to get all single phase inverter power sources
				@returns: response and status code
		"""
		manager = SinglePhaseInverterManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all(role_id)
		return resp, code


@single_phase_inverter_api.route('/installed/all/')
class GetInstalledSinglePhaseInverters(Resource):

	def get(self):
		"""
				HTTP method to get all installed single phase inverters
				@returns: response and status code
		"""
		manager = SinglePhaseInverterManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_installed(role_id)
		return resp, code


@single_phase_inverter_api.route('/uninstalled/all/')
class GetUninstalledSinglePhaseInverters(Resource):

	def get(self):
		"""
				Single phase inverters that haven't been installed yet
				@returns: response and status code
		"""
		manager = SinglePhaseInverterManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_uninstalled(role_id)
		return resp, code
