"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import ThreePhaseInverterManager
from api.schema import ThreePhaseInverterSchema, EditThreePhaseInverterSchema
from logger import logger

three_phase_inverter_api = Namespace('Three Phase Inverter', description='Api for managing one phase inverter.')

new_battery_meta = three_phase_inverter_api.model('Three Phase Inverter New Battery Meta', {
	'depth_of_discharge': fields.Float(required=False, description='The rated depth of discharge of the battery.'),
	'number_of_cycles': fields.Integer(required=False, description='The battery count.'),
	'battery_capacity': fields.Float(required=False, description='The voltage rating of the battery.'),
})

edit_battery_meta = three_phase_inverter_api.model('Three Phase Inverter Edit Battery Meta', {
	'battery_id': fields.String(required=False, description='The ID of the battery.'),
	'depth_of_discharge': fields.Float(required=False, description='The rated depth of discharge of the battery.'),
	'number_of_cycles': fields.Integer(required=False, description='The battery count.'),
	'battery_capacity': fields.Float(required=False, description='The voltage rating of the battery.'),
})

new_battery = three_phase_inverter_api.model('Three Phase Inverter Billing Period', {
	'capacity': fields.String(required=True, description='The battery capacity.'),
	'count': fields.Integer(required=False, description='The battery count.'),
	'voltage': fields.Float(required=False, description='The voltage rating of the battery.'),
	'bank_voltage': fields.Float(required=False, description='The voltage rating of the battery bank.'),
	'depth_of_discharge': fields.Float(required=False, description='The rated depth of discharge of the battery.'),
	'chemistry': fields.String(required=False, description='The material used to make the battery.'),
	'end_of_life': fields.Float(required=False, description='The end of life of the battery.'),
	'meta': fields.List(fields.Nested(new_battery_meta, required=True, description='Metadata for each battery in the battery bank.')),
})

edit_battery = three_phase_inverter_api.model('Edit Three Phase Inverter Billing Period', {
	'bank_id': fields.String(required=False, description='The ID of the battery bank.'),
	'capacity': fields.String(required=True, description='The battery capacity.'),
	'count': fields.Integer(required=False, description='The battery count.'),
	'voltage': fields.Float(required=False, description='The voltage rating of the battery.'),
	'bank_voltage': fields.Float(required=False, description='The voltage rating of the battery bank.'),
	'depth_of_discharge': fields.Float(required=False, description='The rated depth of discharge of the battery.'),
	'chemistry': fields.Float(required=False, description='The material used to make the battery.'),
	'end_of_life': fields.Float(required=False, description='The end of life of the battery.'),
	'meta': fields.List(fields.Nested(edit_battery_meta, required=True, description='Metadata for each battery in the battery bank.')),
})

new_window = three_phase_inverter_api.model('New Three Phase Inverter Operating Window', {
	'start_time': fields.String(required=True, description='The start time the inverter will be on from.'),
	'stop_time': fields.String(required=False, description='The stop time the inverter will be disconnected.'),
})

edit_window = three_phase_inverter_api.model('Edit Three Phase Inverter Operating Window', {
	'window_id': fields.String(required=False, description='The ID of the operating window.'),
	'start_time': fields.String(required=True, description='The start the inverter will be on from.'),
	'stop_time': fields.String(required=False, description='The stop time the inverter will be disconnected.'),
})

input_channel = three_phase_inverter_api.model('Three Phase Inverter Input Channels', {
	'input_channel1': fields.String(required=True, description='The inverter phase one input channel.'),
	'input_channel2': fields.String(required=True, description='The inverter phase two input channel.'),
	'input_channel3': fields.String(required=True, description='The inverter phase three input channel.'),
})

output_channel = three_phase_inverter_api.model('Three Phase Inverter Output Channels', {
	'output_channel1': fields.String(required=True, description='The inverter phase one output channel.'),
	'output_channel2': fields.String(required=True, description='The inverter phase two output channel.'),
	'output_channel3': fields.String(required=True, description='The inverter phase three output channel.'),
})

new_three_phase_inverter = three_phase_inverter_api.model('New Three Phase Inverter', {
	'name': fields.String(required=True, description='The name of the three phase inverter.'),
	'signal_threshold': fields.Float(required=False, description='The lowest current reading to be measured by the meter measuring the one phase inverter.'),
	'configuration_id': fields.String(required=False, description='The configuration to use for this power source.'),
	'input_channels': fields.Nested(input_channel, required=True, description='The inverter input channels per phase.'),
	'output_channels': fields.Nested(output_channel, required=True, description='The inverter output channels per phase.'),
	'apparent_power_rating': fields.String(required=True, description='The power rating of the inverter.'),
	'efficiency': fields.String(required=True, description='The efficiency of this inverter.'),
	'bulk_charging_current': fields.String(required=True, description='The inverter\'s bulk charging current.'),
	'cost': fields.String(required=True, description='The amount of money the inverter cost.'),
	'operating_window': fields.List(fields.Nested(new_window, required=True, description='The period the inverter is expected to be ON.')),
	'battery': fields.List(fields.Nested(new_battery, required=True, description='The period the inverter is expected to be ON.')),
})

edit_three_phase_inverter = three_phase_inverter_api.model('Edit Three Phase Inverter', {
	'name': fields.String(required=True, description='The name of the three phase inverter.'),
	'signal_threshold': fields.Float(required=False, description='The lowest current reading to be measured by the meter measuring the one phase inverter.'),
	'configuration_id': fields.String(required=False, description='The configuration to use for this power source.'),
	'input_channels': fields.Nested(input_channel, required=True, description='The inverter input channels per phase.'),
	'output_channels': fields.Nested(output_channel, required=True, description='The inverter output channels per phase.'),
	'apparent_power_rating': fields.String(required=True, description='The power rating of the inverter.'),
	'efficiency': fields.String(required=True, description='The efficiency of this inverter.'),
	'bulk_charging_current': fields.String(required=True, description='The inverter\'s bulk charging current.'),
	'cost': fields.String(required=True, description='The amount of money the inverter cost.'),
	'operating_window': fields.List(fields.Nested(edit_window, required=True, description='The period the inverter is expected to be ON.')),
	'new_operating_window': fields.List(fields.Nested(new_window, required=True, description='The period the inverter is expected to be ON.')),
	'battery': fields.List(fields.Nested(edit_battery, required=True, description='The period the inverter is expected to be ON.')),
})


@three_phase_inverter_api.route('/new/')
class NewThreePhaseInverter(Resource):

	@three_phase_inverter_api.expect(new_three_phase_inverter)
	def post(self):
		"""
				HTTP method to create a new one phase inverter
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = ThreePhaseInverterSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		operating_window = [dict(window._asdict()) for window in new_payload['operating_window']]
		new_payload['operating_window'] = operating_window

		battery = [dict(battery_._asdict()) for battery_ in new_payload['battery']]
		new_payload['battery'] = battery
		print(new_payload['battery'])

		input_channels = dict(new_payload['input_channels']._asdict())
		new_payload['input_channels'] = input_channels

		output_channels = dict(new_payload['output_channels']._asdict())
		new_payload['output_channels'] = output_channels

		for battery_ in new_payload['battery']:
			meta = [dict(meta_._asdict()) for meta_ in battery_['meta']]
			battery_['meta'] = meta
		# meta = [dict(meta_._asdict()) for battery_ in new_payload['battery'] for meta_ in battery_['meta']]
		# print(meta)
		# new_payload['battery']['meta'] = meta
		new_payload = dict(new_payload)
		print(new_payload)
		manager = ThreePhaseInverterManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.new(role_id, new_payload)
		return resp, code



@three_phase_inverter_api.route('/edit/<string:three_phase_inverter_id>/')
class EditThreePhaseInverter(Resource):

	@three_phase_inverter_api.expect(edit_three_phase_inverter)
	def put(self, three_phase_inverter_id):
		"""
				HTTP method to edit a one phase inverter
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = EditThreePhaseInverterSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		operating_window = [dict(window._asdict()) for window in new_payload['operating_window']]
		new_payload['operating_window'] = operating_window
		print(new_payload['new_operating_window'])
		new_operating_window = [dict(window._asdict()) for window in new_payload['new_operating_window']]
		new_payload['new_operating_window'] = new_operating_window
		battery = [dict(battery_._asdict()) for battery_ in new_payload['battery']]
		new_payload['battery'] = battery

		input_channels = dict(new_payload['input_channels']._asdict())
		new_payload['input_channels'] = input_channels

		output_channels = dict(new_payload['output_channels']._asdict())
		new_payload['output_channels'] = output_channels

		for battery_ in new_payload['battery']:
			meta = [dict(meta_._asdict()) for meta_ in battery_['meta']]
			battery_['meta'] = meta
		new_payload = dict(new_payload)
		manager = ThreePhaseInverterManager()
		role_id = request.cookies.get('role_id')
		new_payload['three_phase_inverter_id'] = three_phase_inverter_id
		resp, code = manager.edit(role_id, new_payload)
		return resp, code


@three_phase_inverter_api.route('/one/<string:three_phase_inverter_id>/')
class GetThreeThreePhaseInverter(Resource):

	def get(self, three_phase_inverter_id):
		"""
				HTTP method to get details for a one phase inverter
				@returns: response and status code
		"""
		manager = ThreePhaseInverterManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one(role_id, three_phase_inverter_id)
		return resp, code

	def delete(self, three_phase_inverter_id):
		"""
				HTTP method to delete a one phase inverter
				@returns: response and status code
		"""
		manager = ThreePhaseInverterManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete(role_id, three_phase_inverter_id)
		return resp, code


@three_phase_inverter_api.route('/all/')
class GetAllThreePhaseInverters(Resource):

	def get(self):
		"""
				HTTP method to get all three phase inverter power sources
				@returns: response and status code
		"""
		manager = ThreePhaseInverterManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all(role_id)
		return resp, code


@three_phase_inverter_api.route('/installed/all/')
class GetInstalledThreePhaseInverters(Resource):

	def get(self):
		"""
				HTTP method to get all installed three phase inverters
				@returns: response and status code
		"""
		manager = ThreePhaseInverterManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_installed(role_id)
		return resp, code


@three_phase_inverter_api.route('/uninstalled/all/')
class GetUninstalledThreePhaseInverters(Resource):

	def get(self):
		"""
				Three phase inverters that haven't been installed yet
				@returns: response and status code
		"""
		manager = ThreePhaseInverterManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_uninstalled(role_id)
		return resp, code
