"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import ProbeManager
from api.schema import NewProbeSchema, EditProbeSchema
from logger import logger

probe_api = Namespace('probes', description='Api for managing probes')

new_channel = probe_api.model('New Channel', {
	'channel_threshold_current': fields.Float(required=True, description='The lowest current the sensor can measure.'),
	'power_factor': fields.Float(required=True, description='The power factor of the channel.'),
	'current_sensor_id': fields.String(required=True, description='The current sensor ID used on the channel.'),
	'voltage_sensor_id': fields.String(required=True, description='The voltage sensor ID used on the channel.'),
})

new_probe = probe_api.model('Probe', {
	'probe_name': fields.String(required=True, description='The name of the probe.'),
	'wifi_network_name': fields.String(required=False, description='The name of the wifi network of the probe.'),
	'wifi_password': fields.String(required=False, description='The name of the wifi password of the probe.'),
	'device_type_id': fields.String(required=True, description='The name of the probe.'),
	'channels': fields.List(fields.Nested(new_channel), required=True, description='A list of channels a probe has.'),
})

edit_channel = probe_api.model('Edit Channel', {
	'channel_id': fields.String(required=True, description='The ID if the channel.'),
	'channel_threshold_current': fields.Float(required=True, description='The lowest current the sensor can measure.'),
	'power_factor': fields.Float(required=True, description='The power factor of the channel.'),
	'current_sensor_id': fields.String(required=True, description='The current sensor ID used on the channel.'),
	'voltage_sensor_id': fields.String(required=True, description='The voltage sensor ID used on the channel.'),
})

edit_probe = probe_api.model('Edit Probe', {
	'probe_name': fields.String(required=True, description='The name of the probe.'),
	'probe_id': fields.String(required=True, description='The ID of the probe.'),
	'wifi_network_name': fields.String(required=False, description='The name of the wifi network of the probe.'),
	'wifi_password': fields.String(required=False, description='The name of the wifi password of the probe.'),
	'channels': fields.List(fields.Nested(edit_channel), required=True, description='A list of channels a probe has.'),
})


@probe_api.route('/new/')
class NewProbe(Resource):
	"""
			Api to manage individual users
	"""

	@probe_api.expect(new_probe)
	def post(self):
		"""
				HTTP method to create probes
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = NewProbeSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		channels = [dict(channel._asdict()) for channel in dict(new_payload)['channels']]
		new_payload['channels'] = channels
		new_payload = dict(new_payload)
		manager = ProbeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.new_probe(role_id, new_payload)
		return resp, code


@probe_api.route('/edit/')
class EditProbe(Resource):
	"""
			Api to manage individual users
	"""

	@probe_api.expect(edit_probe)
	def post(self):
		"""
				HTTP method to edit probes
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = EditProbeSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProbeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.edit_probe(role_id, new_payload)
		return resp, code


@probe_api.route('/user/<string:user_id>/')
@probe_api.doc(params={
	'user_id': 'The user ID of the user',
	'page': 'The database page you want. Default is 1',
	'size': 'The number of responses per request. The default is 20',
})
class GetOneProbeByUserID(Resource):
	"""
			Api to manage individual users
	"""

	def get(self, user_id):
		"""
				HTTP method to get one probe by probe ID
				@returns: response and status code
		"""
		manager = ProbeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all_user_probes(role_id, user_id=user_id)
		return resp, code


@probe_api.route('/config/<string:config_id>/')
@probe_api.doc(params={
	'config_id': 'The configuration ID requested',
	'page': 'The database page you want. Default is 1',
	'size': 'The number of responses per request. The default is 20',
})
class GetAllProbeByConfigID(Resource):
	"""
			Api to manage individual users
	"""

	def get(self, config_id):
		"""
				HTTP method to get one probe by probe ID
				@returns: response and status code
		"""
		manager = ProbeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all_config_probes(role_id, config_id=config_id)
		return resp, code


@probe_api.route('/by-probe-id/<string:probe_id>/')
class GetOneProbeByID(Resource):
	"""
			Api to manage individual users
	"""

	def get(self, probe_id):
		"""
				HTTP method to get one probe by probe ID
				@returns: response and status code
		"""
		manager = ProbeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one(role_id, probe_id=probe_id)
		return resp, code


@probe_api.route('/by-probe-name/<string:probe_name>/')
class GetOneProbeName(Resource):
	def get(self, probe_name):
		"""
				HTTP method to get one probe by probe name
				@returns: response and status code
		"""
		manager = ProbeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one(role_id, probe_name=probe_name)
		return resp, code


@probe_api.route('/all/')
class GetAllProbes(Resource):
	"""
			Api to manage individual users
	"""

	def get(self):
		"""
				HTTP method to get all probes
				@returns: response and status code
		"""
		manager = ProbeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all_probes(role_id)
		return resp, code


@probe_api.route('/me/')
class GetMyProbes(Resource):
	"""
			Api for users to view their probes
	"""

	def get(self):
		"""
				HTTP method for users to get their own probes.
				@returns: response and status code
		"""
		manager = ProbeManager()
		user_id = request.cookies.get('user_id')
		resp, code = manager.get_all_my_probes(user_id)
		return resp, code
