"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import ProsumerManager, RefreshManager
from api.schema import (AssignProsumerSchema, EnergyPlanSchema,
                        NewProsumerSchema, ProsumerParamsSchema)
from logger import logger

prosumer_api = Namespace('prosumers', description='Api for managing prosumers')

prosumer = prosumer_api.model('Prosumer', {
	'name': fields.String(required=True, description='The name of the prosumer'),
	'type_id': fields.String(required=True, description='The type ID of the prosumer'),
	'group_id': fields.String(required=True, description='The group ID of the prosumer'),
})


new_prosumer = prosumer_api.model('NewProsumer', {
	'prosumers': fields.List(fields.Nested(prosumer), required=True, description='\
									A list of prosumers registered to a probe'),
	'probe_id': fields.String(required=False, description='The probe ID'),
	'facility_id': fields.String(required=True, description='The probe\'s facility ID'),
})


assign = prosumer_api.model('Assign', {
	'facility_id': fields.String(required=True, description='The ID of the User\'s facility'),
	'group_id': fields.String(required=True, description='The ID of the User\'s group'),
	'prosumer_id': fields.String(required=True, description='The ID of the prosumer'),
})
remove = prosumer_api.model('Remove', {
	'group_id': fields.String(required=True, description='The ID of the User\'s group'),
	'prosumer_id': fields.String(required=True, description='The ID of the prosumer'),
})

energy_plan = prosumer_api.model('EnergyPlan', {
	'prosumer_id': fields.String(required=True, description='The ID of the prosumer'),
	'plan_id': fields.String(required=True, description='The ID of the plan the prosumer is on'),
})

facility = prosumer_api.model('FacilityID', {
	'facility_id': fields.String(required=True, description='The name of the prosumer'),
})


@prosumer_api.route('/search/')
class Search(Resource):
	"""
			Api for a user to view his prosumer(s)
	"""

	def get(self):
		"""
				HTTP method for a user to view his prosumer(s)
				@returns: response and status code
		"""
		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		search_term = request.args.get('query', type=str)
		size = request.args.get('size', 20, type=int)
		resp, code = manager.search(search_term, size, role_id)
		return resp, code


@prosumer_api.route('/me/')
class MyProsumer(Resource):
	"""
			Api for a user to view his prosumer(s)
	"""

	def get(self):
		"""
				HTTP method for a user to view his prosumer(s)
				@returns: response and status code
		"""
		manager = ProsumerManager()
		group_id = request.cookies.get('group_id')
		user_id = request.cookies.get('user_id')
		role_id = request.cookies.get('role_id')
		page = request.args.get('page', 1, type=int)
		resp, code = manager.my_prosumers(page, user_id, group_id, role_id)
		return resp, code


@prosumer_api.route('/user/<string:group_id>/')
class UserProsumer(Resource):
	"""
			Api to view the prosumers a user has
	"""

	def get(self, group_id):
		"""
				HTTP method to view the prosumers a user has
				@returns: response and status code
		"""
		manager = ProsumerManager()
		page = request.args.get('page', 1, type=int)
		resp, code = manager.user_prosumers(page, group_id)
		return resp, code


@prosumer_api.route('/tx-engine/<string:prosumer_name>/')
class TXEngineProsumers(Resource):
	"""
			TXEngine, view prosumer information.
	"""

	def get(self, prosumer_name):
		"""
				HTTP method for tx engine to view a particular prosumer
				@param: customer_id: ID of the customer
				@returns: response and status code
		"""
		manager = ProsumerManager()
		resp, code = manager.tx_prosumer(prosumer_name)
		return resp, code


@prosumer_api.route('/one-prosumer/<string:prosumer_name>/')
class OneProsumer(Resource):
	"""
			Api to manage individual users
	"""

	def get(self, prosumer_name):
		"""
				HTTP method to view details of a prosumer
				@param: customer_id: ID of the customer
				@returns: response and status code
		"""
		manager = ProsumerManager()
		resp, code = manager.view_prosumer(prosumer_name)
		return resp, code


@prosumer_api.route('/view-prosumer/<string:prosumer_name>/<string:facility_id>/')
class ViewProsumer(Resource):
	"""
			Api to manage individual users
	"""

	def get(self, prosumer_name, facility_id):
		"""
				HTTP method to view details of a prosumer in a facility
				@param: customer_id: ID of the customer
				@returns: response and status code
		"""
		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.view_prosumer_in_facility(role_id, facility_id, prosumer_name)
		return resp, code


@prosumer_api.route('/view-probe/<string:probe_id>/<string:facility_id>/')
class ViewProbe(Resource):
	"""
			Api to manage individual users
	"""

	def get(self, facility_id, probe_id):
		"""
				HTTP method to view details of one probe
				@param: customer_id: ID of the customer
				@returns: response and status code
		"""
		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.view_probe(role_id, facility_id, probe_id)
		return resp, code


@prosumer_api.route('/delete-probe/<string:probe_id>/<string:facility_id>/')
class DeleteProbe(Resource):
	"""
			Api to manage individual users
	"""

	def delete(self, probe_id, facility_id):
		"""
				HTTP method to delete details of one probe
				@param: customer_id: ID of the customer
				@returns: response and status code
		"""
		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete_probe(role_id, probe_id, facility_id)
		return resp, code


@prosumer_api.route('/destroy-probe/<string:probe_id>/<string:facility_id>/')
class DestroyProbe(Resource):
	"""
			Api to manage individual users
	"""

	def delete(self, probe_id, facility_id):
		"""
				HTTP method to delete details of one probe
				@param: customer_id: ID of the customer
				@returns: response and status code
		"""
		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete_probe(role_id, probe_id, facility_id, destroy=True)
		return resp, code


@prosumer_api.route('/delete-prosumer/<string:prosumer_name>/<string:facility_id>/')
class DeleteProsumer(Resource):
	"""
			Api to manage individual users
	"""

	def delete(self, prosumer_name, facility_id):
		"""
				HTTP method to delete details of one prosumer
				@param: customer_id: ID of the customer
				@returns: response and status code
		"""
		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete_prosumer(role_id, prosumer_name, facility_id, destroy=False)
		return resp, code


@prosumer_api.route('/destroy-prosumer/<string:prosumer_name>/<string:facility_id>/')
class DestroyProsumer(Resource):
	"""
			Api to manage individual users
	"""

	def delete(self, prosumer_name, facility_id):
		"""
				HTTP method to destroy details of one prosumer
				@param: customer_id: ID of the customer
				@returns: response and status code
		"""
		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete_prosumer(role_id, prosumer_name, facility_id, destroy=True)
		return resp, code


@prosumer_api.route('/save_energy_plan/')
class MyEnergyPlan(Resource):
	"""
			Api to manage individual users
	"""

	@prosumer_api.expect(energy_plan)
	def post(self):
		"""
				HTTP method to assign an energy plan to a prosumer
				@param: customer_id: ID of the customer
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = EnergyPlanSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProsumerManager()
		resp, code = manager.create_prosumer_plan(**new_payload)
		return resp, code


@prosumer_api.route('/new/')
class NewProsumer(Resource):
	"""
			Api to manage individual users
	"""

	@prosumer_api.expect(new_prosumer)
	def post(self):
		"""
				HTTP method to manually create prosumers
				@param: facility_name: The name of the facility
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = NewProsumerSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			print(payload)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		print(new_payload)
		resp, code = manager.new_prosumer(**new_payload)
		return resp, code


@prosumer_api.route('/refresh-probe/<string:probe_id>/')
class RefreshProbe(Resource):
	"""
			Api to send all the prosumers connected to a probe to Kafka
	"""

	def post(self, probe_id):
		"""
				HTTP method to send all the prosumers connected to a probe to Kafka
				@param: probe_id: The ID of the probe
				@returns: response and status code
		"""
		manager = RefreshManager()
		resp, code = manager.refresh_probe(probe_id)
		return resp, code


@prosumer_api.route('/refresh-prosumer/<string:prosumer_id>/')
class RefreshProsumer(Resource):
	"""
			Api to send a prosumer as well as its all the other prosumers sharing its parent probe to Kafka.
	"""

	def post(self, prosumer_id):
		"""
				HTTP method to send a prosumer as well as its all the other prosumers sharing its parent probe to Kafka.
				@param: prosumer_id: The ID of the prosumer
				@returns: response and status code
		"""
		manager = RefreshManager()
		resp, code = manager.refresh_prosumer(prosumer_id)
		return resp, code


@prosumer_api.route('/all/')
class AllProsumers(Resource):
	"""
			Api to manage individual users
	"""

	def get(self):
		"""
				PRIVATE API to get all prosumers in all facilities
				@param: facility_id: The name of the facility
				@returns: response and status code
		"""
		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		page = request.args.get('page', 1, type=int)
		resp, code = manager.all(page, role_id)
		return resp, code


@prosumer_api.route('/all/<string:facility_id>/')
class AllProsumersInAFacility(Resource):
	"""
			Api to manage individual users
	"""

	def get(self, facility_id):
		"""
				PUBLIC API to get all prosumers in a facility
				@param: facility_id: The name of the facility
				@returns: response and status code
		"""
		manager = ProsumerManager()
		user_id = request.cookies.get('user_id')
		page = request.args.get('page', 1, type=int)
		resp, code = manager.all_in_facility(page, user_id, facility_id)
		return resp, code


@prosumer_api.route('/unassigned/<string:facility_id>/')
class UnassignedProsumers(Resource):
	"""
			Api to manage individual users
	"""

	def get(self, facility_id):
		"""
				HTTP method to create a new prosumer
				@param: customer_id: ID of the customer
				@returns: response and status code
		"""
		user_id = request.cookies.get('user_id')
		manager = ProsumerManager()
		resp, code = manager.unassigned(user_id, facility_id)
		return resp, code


@prosumer_api.route('/assigned/<string:facility_id>/')
class AssignedProsumers(Resource):
	"""
			Api to manage individual users
	"""

	def get(self, facility_id):
		"""
				HTTP method to create a new prosumer
				@param: customer_id: ID of the customer
				@returns: response and status code
		"""
		user_id = request.cookies.get('user_id')
		manager = ProsumerManager()
		resp, code = manager.assigned(user_id, facility_id)
		return resp, code


@prosumer_api.route('/by-type/<string:prosumer_type_id>/<string:facility_id>/')
class ProsumerTypeByFacility(Resource):
	"""
			Api to retrieve prosumers in a facility by prosumer type.
	"""

	def get(self, prosumer_type_id, facility_id):
		"""
				HTTP method to retrieve prosumers in a facility by prosumer type.
				@param: prosumer_type_id: ID of the prosumer type
				@param: facility: ID of the facility
				@returns: response and status code
		"""
		role_id = request.cookies.get('role_id')
		page = request.args.get('page', 1, type=int)
		manager = ProsumerManager()
		resp, code = manager.view_prosumer_by_type_in_facility(role_id, page, prosumer_type_id, facility_id)
		return resp, code


@prosumer_api.route('/by-group/<string:prosumer_group_id>/')
class ProsumersInProsumerGroup(Resource):
	"""
			Api to retrieve prosumers in a prosumer group.
	"""

	def get(self, prosumer_group_id):
		"""
				HTTP method to retrieve prosumers in a prosumer group.
				@param: prosumer_group_id: ID of the prosumer group
				@returns: response and status code
		"""
		role_id = request.cookies.get('role_id')
		page = request.args.get('page', 1, type=int)
		manager = ProsumerManager()
		resp, code = manager.view_prosumer_by_prosumer_group(role_id, page, prosumer_group_id)
		return resp, code


@prosumer_api.route('/assign/')
class AssignToProsumer(Resource):
	"""
			Api to assign prosumers to user group
	"""
	@prosumer_api.expect(assign)
	def post(self):
		"""
				HTTP method to assign a prosumer to a user group ID.
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = AssignProsumerSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			print(payload)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProsumerManager()
		user_id = request.cookies.get('user_id')
		role_id = request.cookies.get('role_id')
		facilities = request.cookies.get('facilities')
		resp, code = manager.assign_to_user(user_id, role_id, facilities, **new_payload)
		return resp, code


@prosumer_api.route('/assign/new/')
class AssignParams(Resource):
	"""
			Class for managing parameters for assignng prosumers to users
	"""

	@prosumer_api.expect(facility)
	def put(self):
		"""
				HTTP method to create a new prosumer
				@param: customer_id: ID of the customer
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		user_id = request.cookies.get('user_id')
		payload['user_id'] = user_id
		schema = ProsumerParamsSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			print(payload)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProsumerManager()
		resp, code = manager.assignment_params(**new_payload)
		return resp, code


@prosumer_api.route('/remove/new/')
class RemoveParams(Resource):
	"""
			Class for managing parameters for removing prosumers from users
	"""
	@prosumer_api.expect(facility)
	def put(self):
		"""
				HTTP method to create a new prosumer
				@param: customer_id: ID of the customer
				@returns: response and status code
		"""

		data = request.values.to_dict()
		payload = api.payload or data
		user_id = request.cookies.get('user_id')
		payload['user_id'] = user_id
		schema = ProsumerParamsSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			print(payload)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProsumerManager()
		resp, code = manager.removal_params(**new_payload)
		return resp, code


@prosumer_api.route('/remove/')
class RemoveFromProsumer(Resource):
	"""
			Api to manage individual users
	"""
	@prosumer_api.expect(assign)
	def post(self):
		"""
				HTTP method to create a new prosumer
				@param: customer_id: ID of the customer
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = AssignProsumerSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			print(payload)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		role_id = request.cookies.get('role_id')
		facilities = request.cookies.get('facilities')
		new_payload['role_id'] = role_id
		new_payload['facilities'] = facilities
		manager = ProsumerManager()
		resp, code = manager.remove_from_user(**new_payload)
		return resp, code


@prosumer_api.route('/unassign-all/<string:user_group_id>/')
class UnaasignAllFromAllUser(Resource):
	"""
			Api to manage individual users
	"""
	def post(self, user_group_id):
		"""
				HTTP method to create a new prosumer
				@param: customer_id: ID of the customer
				@returns: response and status code
		"""
		role_id = request.cookies.get('role_id')
		manager = ProsumerManager()
		resp, code = manager.remove_all_from_user(role_id, user_group_id)
		return resp, code


@prosumer_api.route('/reassign/')
class ReassignProsumer(Resource):
	"""
			Api to manage individual users
	"""
	@prosumer_api.expect(assign)
	def post(self):
		"""
				HTTP method to create a new prosumer
				@param: customer_id: ID of the customer
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = AssignProsumerSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			print(payload)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		user_id = request.cookies.get('user_id')
		role_id = request.cookies.get('role_id')
		facilities = request.cookies.get('facilities')
		manager = ProsumerManager()
		resp, code = manager.reassign_to_user(user_id, role_id, facilities, **new_payload)
		return resp, code


@prosumer_api.route('/save-prosumer-configuration/<string:prosumer_id>/')
class SaveProsumerConfig(Resource):
	"""
			Api to send a configuration for a prosumer
	"""
	def post(self, prosumer_id):
		"""
				HTTP method to create a new prosumer
				@param: customer_id: ID of the customer
				@returns: response and status code
		"""
		user_id = request.cookies.get('user_id')
		role_id = request.cookies.get('role_id')
		manager = ProsumerManager()
		resp, code = manager.init_config_creation(user_id, role_id, prosumer_id)
		return resp, code


@prosumer_api.route('/build-prosumer-configuration/<string:prosumer_id>/')
class BuildProsumerConfig(Resource):
	"""
			Api to send a configuration for a prosumer
	"""
	def get(self, prosumer_id):
		"""
				HTTP method to create a new prosumer
				@param: customer_id: ID of the customer
				@returns: response and status code
		"""
		user_id = request.cookies.get('user_id')
		role_id = request.cookies.get('role_id')
		manager = ProsumerManager()
		resp, code = manager.config_builder(user_id, role_id, prosumer_id)
		return resp, code
