from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError
from api import api
from api.manager import ProsumerClassManager
from api.schema import ProsumerClassSchema
from logger import logger


prosumer_class_api = Namespace(
	'prosumer_class', description='Api for managing prosumer classes.')

prosumer_class = prosumer_class_api.model('NewClass', {
	'name': fields.String(required=True, description='The name of the prosumer class'),
})


@prosumer_class_api.route('/new/')
class NewClass(Resource):
	"""
			Api to create prosumer classes.
	"""

	@prosumer_class_api.expect(prosumer_class)
	def post(self):
		"""
				API to create prosumer classes.
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = ProsumerClassSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = ProsumerClassManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		resp, code = manager.create(**new_payload)
		return resp, code


@prosumer_class_api.route('/edit/<string:class_id>/')
class EditClass(Resource):
	"""
			Api to edit prosumer classes.
	"""

	@prosumer_class_api.expect(prosumer_class)
	def post(self, class_id):
		"""
				API to edit prosumer classes.
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = ProsumerClassSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProsumerClassManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		new_payload['class_id'] = class_id
		resp, code = manager.edit(**new_payload)
		return resp, code


@prosumer_class_api.route('/view-class/<string:class_id>/')
class ViewOneClass(Resource):
	"""
			Api to edit prosumer classes.
	"""

	def get(self, class_id):
		"""
				API to view one prosumer class.
				@returns: response and status code
		"""
		manager = ProsumerClassManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.view_one(class_id, role_id)
		return resp, code


@prosumer_class_api.route('/view-all/')
class ViewAllClasses(Resource):
	"""
			Api to view all prosumer classes.
	"""

	def get(self):
		"""
				API to view all prosumer classes.
				@returns: response and status code
		"""
		manager = ProsumerClassManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.view_all(role_id)
		return resp, code


@prosumer_class_api.route('/delete-class/<string:class_id>/')
class DeleteClass(Resource):
	"""
			Api to delete one prosumer class.
	"""

	def delete(self, class_id):
		"""
				API to delete one prosumer class.
				@returns: response and status code
		"""
		manager = ProsumerClassManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete_class(role_id, class_id)
		return resp, code
