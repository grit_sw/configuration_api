from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError
from api import api
from api.manager import ProsumerGroupManager
from api.schema import (
	ProsumerGroupSchema,
	ProsumerToProsumerGroupSchema,
	ClientAdminProsumerGroupSchema,
	RemoveProsumerFromProsumerGroupSchema,
	RemoveClientAdminProsumerGroupSchema,
)
from logger import logger

prosumer_group_api = Namespace(
	'prosumer_groups', description='Api for managing prosumer groups.')

prosumer_group = prosumer_group_api.model('NewGroup', {
	'name': fields.String(required=True, description='The name of the prosumer group'),
	'facility_id': fields.String(required=True, description='The ID of the facility.')
})

prosumer_to_prosumer_group = prosumer_group_api.model('ProsumerToProsumerGroup', {
	'prosumer_facility': fields.String(required=True, description='The facility ID of the prosumer'),
	'group_facility': fields.String(required=True, description='The facility ID of the prosumer group'),
	'prosumer_id': fields.String(required=True, description='The ID of the prosumer'),
	'prosumer_group_id': fields.String(required=True, description='The ID of the prosumer group.')
})

remove_prosumer_from_prosumer_group = prosumer_group_api.model('RemoveProsumerFromProsumerGroup', {
	'prosumer_id': fields.String(required=True, description='The ID of the prosumer'),
	'prosumer_group_id': fields.String(required=True, description='The ID of the prosumer group.')
})

client_admin_prosumer_group = prosumer_group_api.model('ClientAdminProsumerGroup', {
	'admin_facility': fields.String(required=True, description='The facility ID of the admin.'),
	'group_facility': fields.String(required=True, description='The facility ID of the prosumer group.'),
	'client_admin_id': fields.String(required=True, description='The ID of the client admin'),
	'prosumer_group_id': fields.String(required=True, description='The ID of the prosumer group.')
})

remove_client_admin_prosumer_group = prosumer_group_api.model('RemoveClientAdminProsumerGroup', {
	'client_admin_id': fields.String(required=True, description='The ID of the client admin'),
	'prosumer_group_id': fields.String(required=True, description='The ID of the prosumer group.')
})


@prosumer_group_api.route('/new/')
class NewGroup(Resource):
	"""
			Api to create prosumer groups in a facility.
	"""

	@prosumer_group_api.expect(prosumer_group)
	def post(self):
		"""
				API to create prosumer groups in a facility.
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = ProsumerGroupSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = ProsumerGroupManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		resp, code = manager.create_prosumer_group(**new_payload)
		return resp, code


@prosumer_group_api.route('/assign-prosumer/')
class AssignProsumerToGroup(Resource):
	"""
			Api to add / assign one prosumer to a prosumer group.
	"""

	@prosumer_group_api.expect(prosumer_to_prosumer_group)
	def post(self):
		"""
				Route to add / assign one prosumer to a prosumer group.
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = ProsumerToProsumerGroupSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = ProsumerGroupManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		resp, code = manager.add_prosumer_to_prosumer_group(**new_payload)
		return resp, code


@prosumer_group_api.route('/remove-prosumer/')
class RemoveProsumerFromGroup(Resource):
	"""
			Api for removing one prosumer from a prosumer group.
	"""

	@prosumer_group_api.expect(remove_prosumer_from_prosumer_group)
	def post(self):
		"""
				Route for removing one prosumer from a prosumer group.
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = RemoveProsumerFromProsumerGroupSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = ProsumerGroupManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		resp, code = manager.remove_prosumer_from_prosumer_group(**new_payload)
		return resp, code


@prosumer_group_api.route('/edit/<string:group_id>/')
class EditGroup(Resource):
	"""
			Api to edit prosumer groups in a facility.
	"""

	@prosumer_group_api.expect(prosumer_group)
	def post(self, group_id):
		"""
			API to edit prosumer groups in a facility.
			@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = ProsumerGroupSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProsumerGroupManager()
		role_id = request.cookies.get('role_id')
		new_payload['group_id'] = group_id
		new_payload['role_id'] = role_id
		resp, code = manager.edit_prosumer_group(**new_payload)
		return resp, code


@prosumer_group_api.route('/view-group/<string:group_id>/<string:facility_id>/')
class ViewOneGroup(Resource):
	"""
			Api to edit prosumer groups in a facility.
	"""

	def get(self, group_id, facility_id):
		"""
			API to view one prosumer group in a facility.
			@returns: response and status code
		"""
		manager = ProsumerGroupManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.one_group(group_id, facility_id, role_id)
		return resp, code


@prosumer_group_api.route('/view-all/<string:facility_id>/')
class ViewAllGroups(Resource):
	"""
			Api to view all prosumer groups in a facility.
	"""

	def get(self, facility_id):
		"""
			API to view all prosumer groups in a facility.
			@returns: response and status code
		"""
		manager = ProsumerGroupManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.all_groups(facility_id, role_id)
		return resp, code


@prosumer_group_api.route('/add-client-admin/')
class AddClientAdminToProsumerGroup(Resource):
	"""
			Api to assign client admins to prosumer groups.
	"""

	@prosumer_group_api.expect(client_admin_prosumer_group)
	def post(self):
		"""
			API to assign client admins to prosumer groups.
			@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = ClientAdminProsumerGroupSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProsumerGroupManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		resp, code = manager.add_client_admin(**new_payload)
		return resp, code


@prosumer_group_api.route('/remove-client-admin/')
class RemoveClientAdminFromProsumerGroup(Resource):
	"""
			Api to remove client admins from prosumer groups.
	"""

	@prosumer_group_api.expect(remove_client_admin_prosumer_group)
	def post(self):
		"""
			API to remove client admins from prosumer groups.
			@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = RemoveClientAdminProsumerGroupSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProsumerGroupManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		resp, code = manager.remove_client_admin(**new_payload)
		return resp, code


