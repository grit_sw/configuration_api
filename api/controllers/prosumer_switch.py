"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError
from api import api
from api.manager import SwitchManager

from logger import logger


prosumer_switch_api = Namespace('prosumer-switch', description='Api for switching prosumers')


@prosumer_switch_api.route('/one-on/<string:prosumer_id>/')
class SwitchOneOn(Resource):
	"""
		Api for a user to switch one prosumer ON
	"""

	def post(self, prosumer_id):
		"""
			HTTP method for a user to switch one prosumer ON
			@returns: response and status code
		"""
		role_id = request.cookies.get('role_id')
		user_id = request.cookies.get('user_id')
		group_id = request.cookies.get('group_id')
		facilities = request.cookies.get('facilities')
		manager = SwitchManager()
		resp, code = manager.switch_one(user_id, group_id, role_id, facilities, prosumer_id, state=True)
		return resp, code


@prosumer_switch_api.route('/one-off/<string:prosumer_id>/')
class SwitchOneOff(Resource):
	"""
		Api for a user to switch one prosumer OFF
	"""

	def post(self, prosumer_id):
		"""
			HTTP method for a user to switch one prosumer OFF
			@returns: response and status code
		"""
		role_id = request.cookies.get('role_id')
		user_id = request.cookies.get('user_id')
		group_id = request.cookies.get('group_id')
		facilities = request.cookies.get('facilities')
		manager = SwitchManager()
		resp, code = manager.switch_one(user_id, group_id, role_id, facilities, prosumer_id, state=False)
		return resp, code


@prosumer_switch_api.route('/type-on/<string:prosumer_type_id>/<string:facility_id>/')
class SwitchProsumerTypeOn(Resource):
	"""
		Api for a user to switch all prosumers of a specific type ON
	"""

	def post(self, prosumer_type_id, facility_id):
		"""
			HTTP method for a user to switch all prosumers of a specific type ON
			@returns: response and status code
		"""
		role_id = request.cookies.get('role_id')
		manager = SwitchManager()
		resp, code = manager.switch_type(
			role_id=role_id,
			type_id=prosumer_type_id,
			facility_id=facility_id,
			state=True
		)
		return resp, code


@prosumer_switch_api.route('/type-off/<string:prosumer_type_id>/<string:facility_id>/')
class SwitchProsumerTypeOff(Resource):
	"""
		Api for a user to switch all prosumers of a specific type OFF
	"""

	def post(self, prosumer_type_id, facility_id):
		"""
				HTTP method for a user to switch all prosumers of a specific type OFF
				@returns: response and status code
		"""
		role_id = request.cookies.get('role_id')
		manager = SwitchManager()
		resp, code = manager.switch_type(
			role_id=role_id,
			type_id=prosumer_type_id,
			facility_id=facility_id,
			state=False
		)
		return resp, code


@prosumer_switch_api.route('/group-on/<string:prosumer_group_id>/<string:facility_id>/')
class SwitchProsumerGroupOn(Resource):
	"""
		Api for a user to switch all prosumers in a group ON
	"""

	def post(self, prosumer_group_id, facility_id):
		"""
			HTTP method for a user to switch all prosumers in a group ON
			@returns: response and status code
		"""
		manager = SwitchManager()
		resp, code = manager.switch_group(prosumer_group_id, facility_id, state=True)
		return resp, code


@prosumer_switch_api.route('/group-off/<string:prosumer_group_id>/<string:facility_id>/')
class SwitchProsumerGroupOff(Resource):
	"""
		Api for a user to switch all prosumers in a group OFF
	"""

	def post(self, prosumer_group_id, facility_id):
		"""
			HTTP method for a user to switch all prosumers in a group OFF
			@returns: response and status code
		"""
		manager = SwitchManager()
		resp, code = manager.switch_group(prosumer_group_id, facility_id, state=False)
		return resp, code


@prosumer_switch_api.route('/facility-on/<string:facility_id>/')
class SwitchProsumerFacilityOn(Resource):
	"""
		Api for a user to switch all prosumers in a facility ON
	"""

	def post(self, facility_id):
		"""
			HTTP method for a user to switch all prosumers in a facility ON
			@returns: response and status code
		"""
		manager = SwitchManager()
		facilities = request.cookies.get('facilities')
		resp, code = manager.switch_facility(facilities, facility_id, state=True)
		return resp, code


@prosumer_switch_api.route('/facility-off/<string:facility_id>/')
class SwitchProsumerFacilityOff(Resource):
	"""
		Api for a user to switch all prosumers in a facility OFF
	"""

	def post(self, facility_id):
		"""
			HTTP method for a user to switch all prosumers in a facility OFF
			@returns: response and status code
		"""
		manager = SwitchManager()
		facilities = request.cookies.get('facilities')
		resp, code = manager.switch_facility(facilities, facility_id, state=False)
		return resp, code

