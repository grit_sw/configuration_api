from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError
from api import api
from api.manager import ProsumerTypeManager
from api.schema import ProsumerTypeSchema
from logger import logger

prosumer_type_api = Namespace(
	'prosumer_types', description='Api for managing prosumer types.')

prosumer_type = prosumer_type_api.model('NewType', {
	'name': fields.String(required=True, description='The name of the prosumer type'),
	'class_id': fields.String(required=True, description='The prosumer class ID of the prosumer type'),
})


@prosumer_type_api.route('/new/')
class NewType(Resource):
	"""
			Api to create prosumer types.
	"""

	@prosumer_type_api.expect(prosumer_type)
	def post(self):
		"""
				API to create prosumer types.
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = ProsumerTypeSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = ProsumerTypeManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		resp, code = manager.create(**new_payload)
		return resp, code


@prosumer_type_api.route('/edit/<string:type_id>/')
class EditType(Resource):
	"""
			Api to edit prosumer types.
	"""

	@prosumer_type_api.expect(prosumer_type)
	def post(self, type_id):
		"""
				API to edit prosumer types.
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = ProsumerTypeSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProsumerTypeManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		new_payload['type_id'] = type_id
		resp, code = manager.edit(**new_payload)
		return resp, code


@prosumer_type_api.route('/view-type/<string:type_id>/')
class ViewOneType(Resource):
	"""
			Api to edit prosumer types.
	"""

	def get(self, type_id):
		"""
				API to view one prosumer type.
				@returns: response and status code
		"""
		manager = ProsumerTypeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.view_one(type_id, role_id)
		return resp, code


@prosumer_type_api.route('/view-by-class/<string:class_id>/')
class ViewTypesInClass(Resource):
	"""
			Api to view prosumer types in a prosumer class.
	"""

	def get(self, class_id):
		"""
				API to view prosumer types in a prosumer class.
				@returns: response and status code
		"""
		manager = ProsumerTypeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.view_by_class(class_id, role_id)
		return resp, code


@prosumer_type_api.route('/view-all/')
class ViewAllTypes(Resource):
	"""
			Api to view all prosumer types.
	"""

	def get(self):
		"""
				API to view all prosumer types.
				@returns: response and status code
		"""
		manager = ProsumerTypeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.view_all(role_id)
		return resp, code


@prosumer_type_api.route('/delete/<string:type_id>/')
class DeleteType(Resource):
	"""
			Api to delete one prosumer type.
	"""

	def delete(self, type_id):
		"""
				API to delete one prosumer type.
				@returns: response and status code
		"""
		manager = ProsumerTypeManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete_type(role_id, type_id)
		return resp, code
