"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource

from api.manager import SaveManager

send_config_to_kafka_api = Namespace('Save Probe', description='Api for sending probe configuration to kafka.')


@send_config_to_kafka_api.route('/probe/<string:probe_name>/')
@send_config_to_kafka_api.doc(params={'probe_name': 'The device probe name'})
class SaveProbe(Resource):

	def post(self, probe_name):
		"""
            HTTP method to send the configuration details of a probe to kafka.
            @returns: response and status code
		"""
		manager = SaveManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.save_probe(role_id, probe_name)
		return resp, code


@send_config_to_kafka_api.route('/source/<string:channel_id>/')
@send_config_to_kafka_api.doc(params={'channel_id': 'Any channel ID the source has.'})
class SaveSource(Resource):

	def post(self, channel_id):
		"""
            HTTP method to send the configuration details of a source to kafka.
            @returns: response and status code
		"""
		manager = SaveManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.save_source(role_id, channel_id)
		return resp, code
