"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import VoltageSensorManager
from api.schema import VoltageSensorSchema
from logger import logger

voltage_sensor_api = Namespace('Voltage Sensor', description='Api for managing voltage sensors')

new_voltage_sensor = voltage_sensor_api.model('New Voltage Sensor', {
	'name': fields.String(required=True, description='The name of the sensor type.'),
	'rating': fields.Float(required=True, description='The rating of the sensor.'),
	'default_voltage': fields.Float(required=True, description='The voltage rating of the sensor.'),
	'scaling_factor_voltage': fields.Float(required=True, description='The scaling factor of the sensor.'),
})


@voltage_sensor_api.route('/new/')
class NewVoltageSensor(Resource):

	@voltage_sensor_api.expect(new_voltage_sensor)
	def post(self):
		"""
				HTTP method to create a new voltage sensor
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = VoltageSensorSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = VoltageSensorManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.create(role_id, new_payload)
		return resp, code


@voltage_sensor_api.route('/voltage/edit/<string:sensor_id>/')
class EditVoltageSensor(Resource):

	@voltage_sensor_api.expect(new_voltage_sensor)
	def put(self, sensor_id):
		"""
				HTTP method to edit a sensor type
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = VoltageSensorSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = VoltageSensorManager()
		role_id = request.cookies.get('role_id')
		new_payload['sensor_id'] = sensor_id
		resp, code = manager.edit(role_id, new_payload)
		return resp, code


@voltage_sensor_api.route('/all/')
class GetAllSensorTypes(Resource):

	def get(self):
		"""
				HTTP method to get all sensor types
				@returns: response and status code
		"""
		manager = VoltageSensorManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all(role_id)
		return resp, code


@voltage_sensor_api.route('/one/<string:sensor_id>/')
class GetOneSensorType(Resource):

	def get(self, sensor_id):
		"""
				HTTP method to get sensor type
				@returns: response and status code
		"""
		manager = VoltageSensorManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one(role_id, sensor_id)
		return resp, code
