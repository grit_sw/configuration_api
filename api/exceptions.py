class NotFoundError(AttributeError):
	"""Inheriting from AttributError since sqlalchemy returns None if a user is not found"""
	pass


class FacilityError(AttributeError):
	"""Inheriting from AttributError since sqlalchemy returns None if a user is not found"""
	pass


class UUIDClashError(ValueError):
	"""Inheriting from AttributError since sqlalchemy returns None if a user is not found"""
	pass


class KafkaConnectionError(Exception):
	"""Occurs when prosumer cannot connect to kafka broker."""
	pass


class TopicMissingError(Exception):
	pass


class ValueSchemaNotFound(Exception):
	pass
	