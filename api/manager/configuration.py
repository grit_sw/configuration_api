from api.models import Configuration
from logger import logger


class ConfigurationManager(object):
	"""Class for configuration operations"""

	def new_configuration(self, role_id, configuration_data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		configuration_name = configuration_data['configuration_name']
		configuration_user_id = configuration_data['configuration_user']['configuration_user_id']
		configuration = Configuration.get_user_config(
			configuration_user_id=configuration_user_id,
			configuration_name=configuration_name,
			)
		if configuration is not None:
			response['success'] = False
			response['message'] = f'Configuration {configuration_name} already exists.'
			response['data'] = configuration.to_dict()
			return response, 409

		new_configuration_data = Configuration.new_configuration(**configuration_data)
		response['success'] = True
		response['data'] = new_configuration_data
		return response, 201

	def edit_configuration(self, role_id, configuration_data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		configuration_id = configuration_data['configuration_id']
		configuration = Configuration.get_configuration(configuration_id=configuration_id)
		del configuration_data['configuration_id']
		if not configuration:
			response['success'] = False
			response['message'] = 'Configuration not found.'
			return response, 404

		configuration_data = configuration.edit_configuration(**configuration_data)
		response['success'] = True
		response['data'] = configuration_data
		return response, 201

	def get_all_configurations(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data, has_prev, has_next = Configuration.get_all()
		response['success'] = True
		response['count'] = len(data)
		response['data'] = data
		response['has_prev'] = has_prev
		response['has_next'] = has_next
		return response, 201

	def get_user_configurations(self, role_id, user_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data, has_prev, has_next = Configuration.get_user_configurations(user_id)
		if not data:
			response['success'] = False
			response['message'] = 'No configurations found.'
			response['data'] = data
			return response, 404
		response['success'] = True
		response['count'] = len(data)
		response['data'] = data
		response['has_prev'] = has_prev
		response['has_next'] = has_next
		return response, 200

	def get_my_configurations(self, user_id):
		response = {}
		data, has_prev, has_next = Configuration.get_user_configurations(user_id)
		if not data:
			response['success'] = False
			response['message'] = 'No configurations found.'
			response['data'] = data
			return response, 404
		response['success'] = True
		response['count'] = len(data)
		response['data'] = data
		response['has_prev'] = has_prev
		response['has_next'] = has_next
		return response, 200

	def get_my_configuration_ids(self, user_id):
		response = {}
		data = Configuration.get_configuration_ids(user_id)
		if not data:
			response['success'] = True
			response['message'] = 'No configurations found.'
			return response, 404
		response['success'] = True
		response['data'] = data
		return response, 200

	def get_user_power_sources(self, user_id):
		response = {}
		grid, generator, inverter, count = Configuration.all_user_power_sources(user_id)
		if not count:
			response['success'] = False
			response['message'] = 'No power sources found.'
			return response, 404

		response['success'] = True
		response['count'] = count
		response['grid'] = grid
		response['generator'] = generator
		response['inverter'] = inverter
		return response, 200

	def get_user_consumers(self, user_id):
		response = {}
		data = Configuration.all_user_consumers(user_id)
		if not data:
			response['success'] = False
			response['message'] = 'No power consumers found.'
			response['data'] = data
			return response, 404
		response['success'] = True
		response['count'] = len(data)
		response['data'] = data
		return response, 200

	def view_configuration(self, role_id, configuration_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		configuration = Configuration.get_configuration(configuration_id)
		if configuration is None:
			response['success'] = False
			response['message'] = 'Configuration not found'
			return response, 404

		data = configuration.to_dict()
		response['success'] = True
		response['data'] = data
		return response, 200

	def get_demo(self):
		response = {}

		configuration = Configuration.demo()
		if configuration is None:
			response['success'] = False
			response['message'] = 'Configuration not found'
			return response, 404

		data = configuration.to_dict()
		response['success'] = True
		response['data'] = data
		return response, 200

	def unconfigured_probes(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		data = Configuration.unconfigured_probes()

		response['success'] = True
		response['count'] = len(data)
		response['data'] = data
		return response, 200

	def delete_configuration(self, role_id, configuration_id, facility_id, destroy=False):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		configuration = Configuration.get_configuration(configuration_id)
		if configuration is None:
			response['success'] = False
			response['message'] = 'Configuration {} not found.'.format(configuration_id)
			return response, 404

		if facility_id != configuration.facility_id:
			response['success'] = False
			response['message'] = 'Configuration in {} not found in this facility.'.format(configuration.facility_id)
			return response, 404

		configuration.delete_configuration(destroy=destroy)
		response['success'] = True
		response['message'] = 'Configuration Deleted.'
		return response, 200
