from api.models import ConfigurationGroup
from logger import logger
from api.exceptions import KafkaConnectionError


class ConfigurationGroupManager(object):
	"""Class for configuration group operations"""

	def new_configuration_group(self, role_id, configuration_group_data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		name = configuration_group_data['name']
		admin_id = configuration_group_data['admin_id']
		configuration_group = ConfigurationGroup.get_admin_config(
			admin_id=admin_id,
			name=name,
			)
		if configuration_group is not None:
			response['success'] = False
			response['message'] = f'Configuration group {name} already exists.'
			response['data'] = configuration_group.to_dict()
			return response, 409

		new_configuration_group_data = ConfigurationGroup.new_configuration_group(**configuration_group_data)
		response['success'] = True
		response['data'] = new_configuration_group_data
		return response, 201


	def edit_configuration_group(self, role_id, configuration_group_data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		configuration_group_id = configuration_group_data['configuration_group_id']
		configuration_group = ConfigurationGroup.get_configuration_group(configuration_group_id=configuration_group_id)
		del configuration_group_data['configuration_group_id']
		if not configuration_group:
			response['success'] = False
			response['message'] = 'Configuration group not found.'
			return response, 404

		configuration_group_data = configuration_group.edit_configuration_group(**configuration_group_data)
		response['success'] = True
		response['data'] = configuration_group_data
		return response, 201


	def get_all_configuration_groups(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403
		data, has_prev, has_next = ConfigurationGroup.get_all()
		response['success'] = True
		response['data'] = data
		response['has_prev'] = has_prev
		response['has_next'] = has_next
		return response, 201


	def view_configuration_group(self, role_id, configuration_group_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		configuration_group = ConfigurationGroup.get_configuration_group(configuration_group_id)
		if configuration_group is None:
			response['success'] = False
			response['message'] = 'Configuration group not found'
			return response, 404

		data = configuration_group.to_dict()
		response['success'] = True
		response['data'] = data
		return response, 201


	def unconfigured_probes(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		data = ConfigurationGroup.unconfigured_probes()

		response['success'] = True
		response['count'] = len(data)
		response['data'] = data
		return response, 200

	def delete_configuration_group(self, role_id, configuration_group_id, facility_id, destroy=False):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		configuration_group = ConfigurationGroup.get_configuration_group(configuration_group_id)
		if configuration_group is None:
			response['success'] = False
			response['message'] = 'Configuration group {} not found.'.format(configuration_group_id)
			return response, 404

		if facility_id != configuration_group.facility_id:
			response['success'] = False
			response['message'] = 'Configuration group in {} not found in this facility.'.format(configuration_group.facility_id)
			return response, 404
		try:
			configuration_group.delete_configuration_group(destroy=destroy)
		except KafkaConnectionError as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Service temporarily unavailable.'
			return response, 400
		response['success'] = True
		response['message'] = 'Configuration group Deleted.'
		return response, 200
