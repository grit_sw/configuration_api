from api.models import CurrentSensor
from logger import logger


class CurrentSensorManager(object):
	"""Class for current sensor management operations"""

	def create(self, role_id, sensor_data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		name = sensor_data['name']
		rating = sensor_data['rating']
		current_sensor = CurrentSensor.validate_one(name, rating)
		print('current_sensor = ', current_sensor)
		if current_sensor is not None:
			response['success'] = False
			response['message'] = f'Sensor type {name} already exists.'
			response['data'] = current_sensor.to_dict()
			return response, 409

		new_sensor_data = CurrentSensor.create(**sensor_data).to_dict()
		response['success'] = True
		response['data'] = new_sensor_data
		return response, 201


	def edit(self, role_id, sensor_data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		sensor_id = sensor_data['sensor_id']
		current_sensor = CurrentSensor.get_one(sensor_id=sensor_id)
		if not current_sensor:
			response['success'] = False
			response['message'] = 'Sensor type not found.'
			return response, 404

		del sensor_data['sensor_id']
		data = current_sensor.edit(**sensor_data).to_dict()
		response['success'] = True
		response['data'] = data
		return response, 201


	def get_all(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 1:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403
		data = CurrentSensor.get_all()
		response['success'] = True
		response['data'] = data
		return response, 200


	def get_one(self, role_id, sensor_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 1:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		current_sensor = CurrentSensor.get_one(sensor_id=sensor_id)
		if current_sensor is None:
			response['success'] = False
			response['message'] = 'Sensor type not found'
			return response, 404

		data = current_sensor.to_dict()
		response['success'] = True
		response['data'] = data
		return response, 200

