from api.models import DeviceType
from logger import logger


class DeviceTypeManager(object):
	"""Class for device type operations"""

	def new_device_type(self, role_id, device_data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		device_name = device_data['name']
		device_type = DeviceType.get_device_type(name=device_name)
		if device_type is not None:
			response['success'] = False
			response['message'] = f'Device type {device_name} already exists.'
			response['data'] = device_type.shallow_dict()
			return response, 409

		new_device_data = DeviceType.new_device_type(**device_data)
		response['success'] = True
		response['data'] = new_device_data
		return response, 201


	def edit_device_type(self, role_id, device_data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		device_type_id = device_data['device_type_id']
		device_type = DeviceType.get_device_type(device_type_id=device_type_id)
		if not device_type:
			response['success'] = False
			response['message'] = 'Device type not found.'
			return response, 404

		del device_data['device_type_id']
		data = device_type.edit_device_type(**device_data)
		response['success'] = True
		response['data'] = data
		return response, 201


	def get_all(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = DeviceType.get_all()
		response['success'] = True
		response['data'] = data
		return response, 200


	def get_one(self, role_id, device_type_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		device_type = DeviceType.get_device_type(device_type_id=device_type_id)
		if device_type is None:
			response['success'] = False
			response['message'] = 'Device type not found'
			return response, 404

		data = device_type.shallow_dict()
		response['success'] = True
		response['data'] = data
		return response, 200

