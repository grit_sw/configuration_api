from api.models import Fuel, Configuration
from logger import logger


class FuelManager(object):
	"""Class for fuel management operations"""

	def new(self, role_id, fuel_data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		name = fuel_data['name']
		fuel = Fuel.get_fuel(name=name)
		if fuel is not None:
			response['success'] = False
			response['message'] = f'Fuel type {name} already exists.'
			response['data'] = fuel.to_dict()
			return response, 409

		new_fuel_data = Fuel.new(**fuel_data)
		response['success'] = True
		response['data'] = new_fuel_data
		return response, 201


	def edit(self, role_id, fuel_data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		fuel_id = fuel_data['fuel_id']
		fuel = Fuel.get_fuel(fuel_id=fuel_id)
		if not fuel:
			response['success'] = False
			response['message'] = 'Fuel type not found.'
			return response, 404

		del fuel_data['fuel_id']
		data = fuel.edit(**fuel_data)
		response['success'] = True
		response['data'] = data
		return response, 201


	def get_all(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = Fuel.get_all()
		response['success'] = True
		response['data'] = data
		return response, 200


	def get_one(self, role_id, fuel_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		fuel = Fuel.get_fuel(fuel_id=fuel_id)
		if fuel is None:
			response['success'] = False
			response['message'] = 'Fuel type not found'
			return response, 404

		data = fuel.to_dict()
		response['success'] = True
		response['data'] = data
		return response, 200
	
	def get_user(self, role_id, user_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = Configuration.get_user_fuel(user_id)

		if data is None:
			response['success'] = False
			response['message'] = 'Fuel not found.'
			return response, 404

		response['success'] = True
		response['data'] = data
		return response, 200

	def get_all_my_fuels(self, user_id):
		response = {}
		try:
			user_id = str(user_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403


		data = Configuration.get_user_fuel(user_id)
		if data is None:
			response['success'] = False
			response['message'] = 'Fuel not found.'
			return response, 404

		response['success'] = True
		response['data'] = data
		return response, 200

	def delete(self, role_id, fuel_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		fuel = Fuel.get_fuel(fuel_id=fuel_id)
		if fuel is None:
			response['success'] = False
			response['message'] = 'Fuel type not found'
			return response, 404

		name = fuel.get_name()
		fuel.delete()
		response['success'] = True
		response['message'] = f'Fuel {name} deleted.'
		return response, 200

