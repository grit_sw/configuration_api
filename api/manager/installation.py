from api.models import SinglePhaseConsumer, SinglePhaseGenerator, SinglePhaseGrid, SinglePhaseInverter
from api.models import ThreePhaseConsumer, ThreePhaseGenerator, ThreePhaseGrid, ThreePhaseInverter
from logger import logger


class InstallationManager(object):
	"""Class for installation management operations."""
	models = [
		SinglePhaseConsumer, SinglePhaseGenerator, SinglePhaseGrid, SinglePhaseInverter,
		ThreePhaseConsumer, ThreePhaseGenerator, ThreePhaseGrid, ThreePhaseInverter
	]

	def install(self, role_id, source_name, source_id):
		response = {}
		data = {}

		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		source_name = source_name.title().replace('-', '')
		for model in self.models:
			if model.__name__ == source_name:
				source = model.get_one(source_id=source_id)
				if source:
					data = source.install()
				if source is None:
					logger.info('Source Model not found for source name {source_name} and source id {source_id}')
					response['success'] = False
					response['message'] = 'Source not found.'
					response['data'] = data
					return response, 404
		if not data:
			logger.info('Source Model not found for source name {source_name} and source id {source_id}')
			response['success'] = False
			response['message'] = 'Source Model not found.'
			response['data'] = data
			return response, 404

		response['success'] = True
		response['data'] = data
		return response, 200

	def uninstall(self, role_id, source_name, source_id):
		response = {}
		data = {}

		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		source_name = source_name.title().replace('-', '')
		for model in self.models:
			if model.__name__ == source_name:
				source = model.get_one(source_id=source_id)
				if source:
					data = source.uninstall()
				if source is None:
					response['success'] = False
					response['message'] = 'Source not found.'
					response['data'] = data
					return response, 404
		if not data:
			response['success'] = False
			response['message'] = 'Source Model not found.'
			response['data'] = data
			return response, 404

		response['success'] = True
		response['data'] = data
		return response, 200
