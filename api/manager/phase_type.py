from api.models import PhaseType
from logger import logger


class PhaseTypeManager(object):
	"""Class for phase management operations"""

	def get_all(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = PhaseType.get_all()
		response['success'] = True
		response['data'] = data
		return response, 200


	def get_one(self, role_id, phase_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		phase = PhaseType.get_one(phase_id=phase_id)
		if phase is None:
			response['success'] = False
			response['message'] = 'PhaseType type not found'
			return response, 404

		data = phase.to_dict()
		response['success'] = True
		response['data'] = data
		return response, 200

