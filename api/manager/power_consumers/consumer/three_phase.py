from api.models import ThreePhaseConsumer, ThreePhaseConsumerLine
from logger import logger


class ThreePhaseConsumerManager(object):
	"""Class for three phase consumer operations"""

	def new(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		line_id = data['line_id']
		name = data['name']
		line = ThreePhaseConsumerLine.get_one(line_id=line_id)
		if not line.is_available:
			response['success'] = False
			response['message'] = 'No consumer can be addedd to this consumer line.'
			response['data'] = line.consumer.get_data()
			return response, 409

		consumer = ThreePhaseConsumer.get_one(name=name)
		exists = line.consumer_in_line(consumer)
		if exists:
			response['success'] = False
			response['message'] = f'A {consumer.phase_type.name.lower()} consumer with name {consumer.name} already exists on this line.'
			response['data'] = consumer.get_data()
			return response, 409

		new_data = ThreePhaseConsumer.create(**data)
		response['success'] = True
		response['data'] = new_data
		return response, 201

	def edit(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		line_id = data['line_id']
		name = data['name']
		consumer_line = ThreePhaseConsumerLine.get_one(line_id=line_id)
		consumer = ThreePhaseConsumer.get_one(name=name)
		exists = consumer_line.consumer_in_line(consumer)
		if exists:
			response['success'] = False
			response['message'] = f'Three phase consumer with name {name} already exists on this line.'
			response['data'] = consumer_line.get_data()
			return response, 409

		consumer_id = data['consumer_id']
		consumer = ThreePhaseConsumer.get_one(source_id=consumer_id)
		del data['line_id']
		del data['consumer_id']
		if not consumer:
			response['success'] = False
			response['message'] = 'Three phase consumer not found.'
			return response, 404

		data = consumer.edit(**data)
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_all(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = ThreePhaseConsumer.get_all()
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_one(self, role_id, consumer_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		consumer = ThreePhaseConsumer.get_one(source_id=consumer_id)
		if consumer is None:
			response['success'] = False
			response['message'] = 'Three phase consumer not found'
			return response, 404

		data = consumer.get_data()
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_installed(self, role_id):
		"""Get a list of all three phase consumer devices that have not been installed"""
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = ThreePhaseConsumer.get_installed(installed=True)
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_uninstalled(self, role_id):
		"""Get a list of all three phase consumer devices that have not been installed"""
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = ThreePhaseConsumer.get_installed(installed=False)
		response['success'] = True
		response['data'] = data
		return response, 201

	def delete(self, role_id, consumer_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		consumer = ThreePhaseConsumer.get_one(source_id=consumer_id)
		if consumer is None:
			response['success'] = False
			response['message'] = 'Three phase consumer {} not found.'.format(consumer_id)
			return response, 404

		name = consumer.name
		consumer.delete()
		response['success'] = True
		response['message'] = f'Three phase consumer {name} Deleted.'
		return response, 204
