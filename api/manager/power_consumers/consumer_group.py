from api.models.power_consumers.consumer_group import ConsumerGroup
from logger import logger


class ConsumerGroupManager(object):
	"""Class for consumer group operations"""

	def new(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		name = data['name']
		type_id = data['consumer_group_type_id']
		group = ConsumerGroup.get_one(name=name)
		if group:
			if group.consumer_group_type.consumer_group_type_id == type_id:
				response['success'] = False
				response['message'] = 'A consumer group with the same name already exists.'
				response['data'] = group.get_data()
				return response, 409

		new_data = ConsumerGroup.create(**data)
		response['success'] = True
		response['data'] = new_data
		return response, 201

	def edit(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		name = data['name']
		group_id = data['group_id']
		type_id = data['consumer_group_type_id']
		group = ConsumerGroup.get_one(group_id=group_id)
		del data['group_id']
		if not group:
			response['success'] = False
			response['message'] = 'Consumer group not found.'
			return response, 404

		group_name = ConsumerGroup.get_one(name=name)
		if group_name:
			if group_name.consumer_group_type.consumer_group_type_id == type_id:
				response['success'] = False
				response['message'] = 'A consumer group with the same name already exists.'
				response['data'] = group_name.get_data()
				return response, 409

		data = group.edit(**data)
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_all(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = ConsumerGroup.get_all()
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_one(self, role_id, group_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		group = ConsumerGroup.get_one(group_id=group_id)
		if group is None:
			response['success'] = False
			response['message'] = 'Consumer group not found'
			return response, 404

		data = group.get_data()
		response['success'] = True
		response['data'] = data
		return response, 201

	def delete(self, role_id, group_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if not role_id >= 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		group = ConsumerGroup.get_one(group_id=group_id)
		if group is None:
			response['success'] = False
			response['message'] = 'Consumer group not found.'
			return response, 404

		name = group.name
		group.delete()
		response['success'] = True
		response['message'] = f'Consumer group {name} deleted.'
		return response, 204
