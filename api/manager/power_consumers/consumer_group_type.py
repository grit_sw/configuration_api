from api.models.power_consumers.utils.consumer_group_type import ConsumerGroupType
from logger import logger


class ConsumerGroupTypeManager(object):
	"""Class for consumer group operations"""

	def new(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		name = data['name']
		group_type = ConsumerGroupType.get_one(name=name)
		if group_type:
			response['success'] = False
			response['message'] = 'Group already exists.'
			response['data'] = group_type.get_data()
			return response, 409

		new_data = ConsumerGroupType.create(**data)
		response['success'] = True
		response['data'] = new_data
		return response, 201

	def edit(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		consumer_group_type_id = data['consumer_group_type_id']
		group = ConsumerGroupType.get_one(consumer_group_type_id=consumer_group_type_id)
		del data['consumer_group_type_id']
		if not group:
			response['success'] = False
			response['message'] = 'Consumer group type not found.'
			return response, 404

		data = group.edit(**data)
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_all(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = ConsumerGroupType.get_all()
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_one(self, role_id, group_type_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		group = ConsumerGroupType.get_one(consumer_group_type_id=group_type_id)
		if group is None:
			response['success'] = False
			response['message'] = 'Consumer group type not found'
			return response, 404

		data = group.get_data()
		response['success'] = True
		response['data'] = data
		return response, 201

	def delete(self, role_id, group_type_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if not role_id >= 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		group = ConsumerGroupType.get_one(consumer_group_type_id=group_type_id)
		if group is None:
			response['success'] = False
			response['message'] = 'Consumer group type {} not found.'.format(group_type_id)
			return response, 404

		name = group.name
		group.delete()
		response['success'] = True
		response['message'] = f'Consumer group type {name} Deleted.'
		return response, 204
