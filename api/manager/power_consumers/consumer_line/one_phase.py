from api.models import SinglePhaseConsumerLine
from api.models.power_consumers.consumer_group import ConsumerGroup
from logger import logger


class SinglePhaseConsumerLineManager(object):
	"""Class for single phase consumer line operations"""

	def new(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403
        # todo: check for duplicate consumer lines before creating a new consumer line
        # consumer lines should be unique across consumer groups.
		name = data['name']
		consumer_group_id = data['consumer_group_id']
		consumer_group = ConsumerGroup.get_one(consumer_group_id)
		consumer_line = SinglePhaseConsumerLine.get_one(name=name)
		if consumer_line:
			exists = consumer_group.line_in_group(consumer_line)
			if exists:
				response['success'] = False
				response['message'] = f'A {consumer_line.phase_type.name.lower()} consumer line with called {consumer_line.name} exists in this consumer group'
				response['data'] = consumer_line.get_data()
				return response, 409

		new_data = SinglePhaseConsumerLine.create(**data)
		response['success'] = True
		response['data'] = new_data
		return response, 201

	def edit(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		line_id = data['line_id']
		line = SinglePhaseConsumerLine.get_one(line_id=line_id)
		del data['line_id']
		if not line:
			response['success'] = False
			response['message'] = 'Single phase consumer not found.'
			return response, 404
		name = data['name']
		consumer_group_id = data['consumer_group_id']
		consumer_group = ConsumerGroup.get_one(consumer_group_id)
		consumer_line = SinglePhaseConsumerLine.get_one(name=name)
		if consumer_line:
			exists = consumer_group.line_in_group(consumer_line)
			if exists:
				response['success'] = False
				response['message'] = f'A {consumer_line.phase_type.name.lower()} consumer line with called {consumer_line.name} exists in this consumer group'
				response['data'] = consumer_line.get_data()
				return response, 409

		data = line.edit(**data)
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_all(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = SinglePhaseConsumerLine.get_all()
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_one(self, role_id, line_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		line = SinglePhaseConsumerLine.get_one(line_id=line_id)
		if line is None:
			response['success'] = False
			response['message'] = 'Single Phase consumer not found'
			return response, 404

		data = line.get_data()
		response['success'] = True
		response['data'] = data
		return response, 201

	def delete(self, role_id, line_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if not role_id >= 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		line = SinglePhaseConsumerLine.get_one(line_id=line_id)
		if line is None:
			response['success'] = False
			response['message'] = 'Single phase consumer {} not found.'.format(line_id)
			return response, 404

		name = line.name
		line.delete()
		response['success'] = True
		response['message'] = f'Single Phase consumer {name} Deleted.'
		return response, 204
