from api.models import ThreePhaseConsumerLine
from logger import logger


class ThreePhaseConsumerLineManager(object):
	"""Class for three phase consumer line operations"""

	def new(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403
        # todo: check for duplicate consumer lines before creating a new consumer line
        # consumer lines should be unique across configurations.
        # configuration_id for this consumer line can be
        # obtained from the consumer group it belongs to.
		new_data = ThreePhaseConsumerLine.create(**data)
		response['success'] = True
		response['data'] = new_data
		return response, 201

	def edit(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		line_id = data['line_id']
		line = ThreePhaseConsumerLine.get_one(line_id=line_id)
		del data['line_id']
		if not line:
			response['success'] = False
			response['message'] = 'Three phase consumer not found.'
			return response, 404

		data = line.edit(**data)
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_all(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = ThreePhaseConsumerLine.get_all()
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_one(self, role_id, line_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		line = ThreePhaseConsumerLine.get_one(line_id=line_id)
		if line is None:
			response['success'] = False
			response['message'] = 'Three phase consumer line not found'
			return response, 404

		data = line.get_data()
		response['success'] = True
		response['data'] = data
		return response, 201

	def delete(self, role_id, line_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if not role_id >= 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		line = ThreePhaseConsumerLine.get_one(line_id=line_id)
		if line is None:
			response['success'] = False
			response['message'] = 'Three phase consumer line {} not found.'.format(line_id)
			return response, 404

		name = line.name
		line.delete()
		response['success'] = True
		response['message'] = f'Three phase consumer line {name} Deleted.'
		return response, 204
