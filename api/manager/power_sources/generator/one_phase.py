from api.models import SinglePhaseGenerator, Channels
from logger import logger


class SinglePhaseGeneratorManager(object):
	"""Class for single phase generator operations"""

	def new(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		channel_list = [data['probe_channel_id']]
		source = Channels.get_assigned(channel_list)
		if source is not None:
			response['success'] = False
			response['message'] = 'Channel in use.'
			response['data'] = source.to_dict()
			return response, 409

		new_data = SinglePhaseGenerator.create(**data)
		response['success'] = True
		response['data'] = new_data
		return response, 201

	def edit(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		single_phase_generator_id = data['single_phase_generator_id']
		single_phase_generator = SinglePhaseGenerator.get_one(source_id=single_phase_generator_id)
		del data['single_phase_generator_id']
		if not single_phase_generator:
			response['success'] = False
			response['message'] = 'SinglePhaseGenerator not found.'
			return response, 404

		data = single_phase_generator.edit(**data)
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_all(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = SinglePhaseGenerator.get_all()
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_one(self, role_id, single_phase_generator_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		single_phase_generator = SinglePhaseGenerator.get_one(source_id=single_phase_generator_id)
		if single_phase_generator is None:
			response['success'] = False
			response['message'] = 'Single Phase Generator not found'
			return response, 404

		data = single_phase_generator.get_data()
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_installed(self, role_id):
		"""Get a list of all single phase generator devices that have not been installed"""
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = SinglePhaseGenerator.get_installed(installed=True)
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_uninstalled(self, role_id):
		"""Get a list of all single phase generator devices that have not been installed"""
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = SinglePhaseGenerator.get_installed(installed=False)
		response['success'] = True
		response['data'] = data
		return response, 201

	def delete(self, role_id, single_phase_generator_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if not role_id >= 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		single_phase_generator = SinglePhaseGenerator.get_one(source_id=single_phase_generator_id)
		if single_phase_generator is None:
			response['success'] = False
			response['message'] = 'SinglePhaseGenerator {} not found.'.format(single_phase_generator_id)
			return response, 404

		name = single_phase_generator.name
		single_phase_generator.delete()
		response['success'] = True
		response['message'] = f'Single Phase Generator {name} Deleted.'
		return response, 204
