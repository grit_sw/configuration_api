from api.models import SinglePhaseGrid, Channels
from logger import logger


class SinglePhaseGridManager(object):
	"""Class for single phase grid operations"""

	def new(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		channel_list = [data['probe_channel_id']]
		source = Channels.get_assigned(channel_list)
		if source is not None:
			response['success'] = False
			response['message'] = 'Channel in use.'
			response['data'] = source.to_dict()
			return response, 409

		new_data = SinglePhaseGrid.create(**data)
		response['success'] = True
		response['data'] = new_data
		return response, 201

	def edit(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		single_phase_grid_id = data['single_phase_grid_id']
		single_phase_grid = SinglePhaseGrid.get_one(source_id=single_phase_grid_id)
		del data['single_phase_grid_id']
		if not single_phase_grid:
			response['success'] = False
			response['message'] = 'SinglePhaseGrid not found.'
			return response, 404

		data = single_phase_grid.edit(**data)
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_all(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = SinglePhaseGrid.get_all()
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_one(self, role_id, single_phase_grid_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		single_phase_grid = SinglePhaseGrid.get_one(source_id=single_phase_grid_id)
		if single_phase_grid is None:
			response['success'] = False
			response['message'] = 'Single Phase Grid not found'
			return response, 404

		data = single_phase_grid.get_data()
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_installed(self, role_id):
		"""Get a list of all single phase grid devices that have not been installed"""
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = SinglePhaseGrid.get_installed(installed=True)
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_uninstalled(self, role_id):
		"""Get a list of all single phase grid devices that have not been installed"""
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = SinglePhaseGrid.get_installed(installed=False)
		response['success'] = True
		response['data'] = data
		return response, 201

	def delete(self, role_id, single_phase_grid_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		single_phase_grid = SinglePhaseGrid.get_one(source_id=single_phase_grid_id)
		if single_phase_grid is None:
			response['success'] = False
			response['message'] = f'Single Phase Grid {single_phase_grid_id} not found.'
			return response, 404

		name = single_phase_grid.name
		single_phase_grid.delete()
		response['success'] = True
		response['message'] = f'Single Phase Grid {name} Deleted.'
		return response, 204
