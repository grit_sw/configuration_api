from api.models import ThreePhaseGrid, Channels
from logger import logger


class ThreePhaseGridManager(object):
	"""Class for three phase grid operations"""

	def new(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		channel_list = data['probe_channels']
		source = Channels.get_assigned(channel_list)
		if source:
			response['success'] = False
			response['message'] = 'Channel in use.'
			response['data'] = source.to_dict()
			return response, 409

		new_data = ThreePhaseGrid.create(**data)
		response['success'] = True
		response['data'] = new_data
		return response, 201

	def edit(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		three_phase_grid_id = data['three_phase_grid_id']
		three_phase_grid = ThreePhaseGrid.get_one(source_id=three_phase_grid_id)
		del data['three_phase_grid_id']
		if not three_phase_grid:
			response['success'] = False
			response['message'] = 'ThreePhaseGrid not found.'
			return response, 404

		data = three_phase_grid.edit(**data)
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_all(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = ThreePhaseGrid.get_all()
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_one(self, role_id, three_phase_grid_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		three_phase_grid = ThreePhaseGrid.get_one(source_id=three_phase_grid_id)
		if three_phase_grid is None:
			response['success'] = False
			response['message'] = 'Three Phase Grid not found'
			return response, 404

		data = three_phase_grid.get_data()
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_installed(self, role_id):
		"""Get a list of all three phase grid devices that have not been installed"""
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = ThreePhaseGrid.get_installed(installed=True)
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_uninstalled(self, role_id):
		"""Get a list of all three phase grid devices that have not been installed"""
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = ThreePhaseGrid.get_installed(installed=False)
		response['success'] = True
		response['data'] = data
		return response, 201

	def delete(self, role_id, three_phase_grid_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		three_phase_grid = ThreePhaseGrid.get_one(source_id=three_phase_grid_id)
		if three_phase_grid is None:
			response['success'] = False
			response['message'] = 'Three Phase Grid {} not found.'.format(three_phase_grid_id)
			return response, 404

		name = three_phase_grid.name
		three_phase_grid.delete()
		response['success'] = True
		response['message'] = f'Three Phase Grid {name} Deleted.'
		return response, 204
