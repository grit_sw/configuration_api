from api.models import SinglePhaseInverter, Channels
from logger import logger


class SinglePhaseInverterManager(object):
	"""Class for single phase inverter operations"""

	def new(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		input_channel = data['input_channel_id']
		output_channel = data['output_channel_id']
		channel_list = [input_channel, output_channel]
		source = Channels.get_assigned(channel_list)

		if source:
			response['success'] = False
			response['message'] = f'Channel in use.'
			response['data'] = source.to_dict()
			return response, 409

		new_data = SinglePhaseInverter.create(**data)
		response['success'] = True
		response['data'] = new_data
		return response, 201

	def edit(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		single_phase_inverter_id = data['single_phase_inverter_id']
		single_phase_inverter = SinglePhaseInverter.get_one(source_id=single_phase_inverter_id)
		del data['single_phase_inverter_id']
		if not single_phase_inverter:
			response['success'] = False
			response['message'] = 'SinglePhaseInverter not found.'
			return response, 404

		data = single_phase_inverter.edit(**data)
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_all(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = SinglePhaseInverter.get_all()
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_one(self, role_id, single_phase_inverter_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		single_phase_inverter = SinglePhaseInverter.get_one(source_id=single_phase_inverter_id)
		if single_phase_inverter is None:
			response['success'] = False
			response['message'] = 'Single Phase Inverter not found'
			return response, 404

		data = single_phase_inverter.get_data()
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_installed(self, role_id):
		"""Get a list of all single phase inverter devices that have not been installed"""
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = SinglePhaseInverter.get_installed(installed=True)
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_uninstalled(self, role_id):
		"""Get a list of all single phase inverter devices that have not been installed"""
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = SinglePhaseInverter.get_installed(installed=False)
		response['success'] = True
		response['data'] = data
		return response, 201

	def delete(self, role_id, single_phase_inverter_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		single_phase_inverter = SinglePhaseInverter.get_one(source_id=single_phase_inverter_id)
		if single_phase_inverter is None:
			response['success'] = False
			response['message'] = 'SinglePhaseInverter {} not found.'.format(single_phase_inverter_id)
			return response, 404

		name = single_phase_inverter.name
		single_phase_inverter.delete()
		response['success'] = True
		response['message'] = f'Single Phase Inverter {name} Deleted.'
		return response, 204
