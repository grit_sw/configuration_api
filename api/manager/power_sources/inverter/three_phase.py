from collections import ChainMap
from api.models import ThreePhaseInverter, Channels
from logger import logger


class ThreePhaseInverterManager(object):
	"""Class for three phase inverter operations"""

	def new(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		input_channels = data['input_channels']
		output_channels = data['output_channels']

		channel_list = list(ChainMap(input_channels, output_channels).values())
		source = Channels.get_assigned(channel_list)

		if source:
			response['success'] = False
			response['message'] = 'Channel in use.'
			response['data'] = source.to_dict()
			return response, 409

		new_data = ThreePhaseInverter.create(**data)
		response['success'] = True
		response['data'] = new_data
		return response, 201

	def edit(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		three_phase_inverter_id = data['three_phase_inverter_id']
		three_phase_inverter = ThreePhaseInverter.get_one(source_id=three_phase_inverter_id)
		del data['three_phase_inverter_id']
		if not three_phase_inverter:
			response['success'] = False
			response['message'] = 'ThreePhaseInverter not found.'
			return response, 404

		data = three_phase_inverter.edit(**data)
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_all(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = ThreePhaseInverter.get_all()
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_one(self, role_id, three_phase_inverter_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		three_phase_inverter = ThreePhaseInverter.get_one(source_id=three_phase_inverter_id)
		if three_phase_inverter is None:
			response['success'] = False
			response['message'] = 'Three Phase Inverter not found'
			return response, 404

		data = three_phase_inverter.get_data()
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_installed(self, role_id):
		"""Get a list of all three phase inverter devices that have not been installed"""
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = ThreePhaseInverter.get_installed(installed=True)
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_uninstalled(self, role_id):
		"""Get a list of all three phase inverter devices that have not been installed"""
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data = ThreePhaseInverter.get_installed(installed=False)
		response['success'] = True
		response['data'] = data
		return response, 201

	def delete(self, role_id, three_phase_inverter_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		three_phase_inverter = ThreePhaseInverter.get_one(source_id=three_phase_inverter_id)
		if three_phase_inverter is None:
			response['success'] = False
			response['message'] = 'ThreePhaseInverter {} not found.'.format(three_phase_inverter_id)
			return response, 404

		name = three_phase_inverter.name
		three_phase_inverter.delete()
		response['success'] = True
		response['message'] = f'Three Phase Inverter {name} Deleted.'
		return response, 204
