from api.models import Probe, Configuration
from logger import logger


class ProbeManager(object):
	"""Class for probe operations"""

	def new_probe(self, role_id, probe_data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		probe_name = probe_data['probe_name']
		probe = Probe.get_one(probe_name=probe_name)
		if probe is not None:
			response['success'] = False
			response['message'] = f'Probe {probe_name} already exists.'
			response['data'] = probe.to_dict()
			return response, 409

		new_probe_data = Probe.new_probe(**probe_data)
		response['success'] = True
		response['data'] = new_probe_data
		return response, 201


	def edit_probe(self, role_id, probe_data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		probe_id = probe_data['probe_id']
		probe = Probe.get_one(probe_id=probe_id)
		del probe_data['probe_id']
		if not probe:
			response['success'] = False
			response['message'] = 'Probe not found.'
			return response, 404

		probe_data = probe.edit_probe(**probe_data)
		response['success'] = True
		response['data'] = probe_data
		return response, 201


	def get_all_probes(self, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data, has_prev, has_next = Probe.get_all()
		response['success'] = True
		response['count'] = len(data)
		response['data'] = data
		response['has_prev'] = has_prev
		response['has_next'] = has_next
		return response, 200


	def get_all_user_probes(self, role_id, user_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data, has_prev, has_next = Configuration.all_user_probes(user_id=user_id)
		response['success'] = True
		response['count'] = len(data)
		response['data'] = data
		response['has_prev'] = has_prev
		response['has_next'] = has_next
		return response, 200

	def get_all_my_probes(self, user_id):
		response = {}
		try:
			user_id = str(user_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		data, has_prev, has_next = Configuration.all_user_probes(user_id=user_id)
		response['success'] = True
		response['count'] = len(data)
		response['data'] = data
		response['has_prev'] = has_prev
		response['has_next'] = has_next
		return response, 200

	def get_all_config_probes(self, role_id, config_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		data, has_prev, has_next = Configuration.all_config_probes(configuration_id=config_id)
		response['success'] = True
		response['count'] = len(data)
		response['data'] = data
		response['has_prev'] = has_prev
		response['has_next'] = has_next
		return response, 200


	def get_one(self, role_id, probe_id=None, probe_name=None):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		if probe_id:
			probe = Probe.get_one(probe_id=probe_id)
		elif probe_name:
			probe = Probe.get_one(probe_name=probe_name)
		else:
			probe = None
		if probe is None:
			response['success'] = False
			response['message'] = 'Probe not found'
			return response, 404

		data = probe.get_data()
		response['success'] = True
		response['data'] = data
		return response, 200

	def delete_probe(self, role_id, probe_id, facility_id, destroy=False):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		probe = Probe.get_one(probe_id)
		if probe is None:
			response['success'] = False
			response['message'] = 'Probe {} not found.'.format(probe_id)
			return response, 404

		if facility_id != probe.facility_id:
			response['success'] = False
			response['message'] = 'Probe in {} not found in this facility.'.format(probe.facility_id)
			return response, 404
		try:
			probe.delete_probe(destroy=destroy)
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Service temporarily unavailable.'
			return response, 400
		response['success'] = True
		response['message'] = 'Probe Deleted.'
		return response, 204

