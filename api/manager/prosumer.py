from api import configurations, mongo_users, probes
from api.exceptions import FacilityError, NotFoundError
from api.models import Probe, Prosumer
from logger import logger
from api.exceptions import KafkaConnectionError


def installed_sources(email):
	mongo_user = mongo_users.find_one({'email': email})
	user_id = mongo_user['_id']
	configs = configurations.find({'UserID_FKs': str(user_id)})
	config_ids = [config['_id'] for config in configs]
	user_probes = [
		probes.find({'Channels.ConfigID_FK': str(config_id)}) for config_id in config_ids
	]
	for user_probe in user_probes:
		user_probe = list(user_probe)
		if user_probe:
			probe_id = user_probe[0]['probeName']
			channels = user_probe[0]['Channels']
			sources = [channel['SourceName'] for channel in channels]
			sources = list(set(sources))
			try:
				sources.remove('')
			except ValueError as e:
				logger.exception(e)
			if sources:
				yield probe_id, sources


class ProsumerManager(object):
	"""Class for prosumer operations"""

	def search(self, search_term, size, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 3:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		data, has_prev, has_next = Prosumer.search_for(search_term, size)

		response['success'] = True
		response['data'] = data
		response['has_prev'] = has_prev
		response['has_next'] = has_next
		return response, 200


	def new_prosumer(self, role_id, probe_id, facility_id, prosumers):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		prosumers = [prosumer._asdict() for prosumer in prosumers]
		try:
			existing_prosumers, success = Probe.new_probe(probe_id, facility_id, prosumers)
		except KafkaConnectionError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Service temporarily unavailable.'
			return response, 400

		if not success:
			if len(existing_prosumers) >= 1:
				message = 'These Prosumers already exist {}'.format(
					existing_prosumers)
				logger.info(message)
				response['success'] = False
				response['message'] = message
				return response, 409

		new_probe = Probe.get_probe(probe_id).to_dict()
		logger.info(
			'Created new Prosumers at facility_id {}'.format(facility_id))
		response['success'] = True
		response['message'] = 'Created.'
		response['data'] = new_probe
		if len(existing_prosumers) >= 1:
			response['existing_prosumers'] = existing_prosumers
		return response, 201

	def create_prosumer_plan(self, plan_id, prosumer_id):
		response = {}
		prosumer = Prosumer.get_prosumer(prosumer_id=prosumer_id)
		if prosumer is None:
			response['success'] = False
			response['message'] = 'Prosumer not found'
			return response, 404
		created = prosumer.create_plan(plan_id)
		if not created:
			logger.warn('Plan ID {} creation for prosumer {} failed'.format(
				plan_id, prosumer.prosumer_id))
			response['success'] = False
			response['message'] = 'Service Error'
			return response, 500
		response['success'] = True
		response['message'] = 'Created'
		return response, 201

	def my_prosumers(self, page, user_id, group_id, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		manager = ProsumerManager()
		if role_id >= 5:
			response, status_code = manager.all(page, role_id)
			return response, status_code
		if role_id == 3:
			data, has_prev, has_next = Prosumer.client_admin_prosumers(page, user_id)
			response['success'] = True
			response['count'] = len(data)
			response['data'] = data
			response['has_prev'] = has_prev
			response['has_next'] = has_next
			return response, 200
		response, status_code = manager.user_prosumers(page, group_id)
		return response, status_code

	def user_prosumers(self, page, user_group_id):
		response = {}
		prosumer = Prosumer.get_prosumer(user_group_id=user_group_id)
		if prosumer is None:
			response['success'] = False
			response['message'] = 'No prosumer found'
			return response, 404
		try:
			prosumers, has_prev, has_next = Prosumer.user_prosumers(
				page, user_group_id)
			response['success'] = True
			response['count'] = len(prosumers)
			response['data'] = prosumers
			response['has_prev'] = has_prev
			response['has_next'] = has_next
			return response, 200
		except Exception as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Error during request. Team has been notified. Please try again later'
			return response, 500

	def all(self, page, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 5:
			"""
				This is a compromise done for speed of getting an api to production.
				Correct this by studying how roles are managed in microservices
			"""
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			prosumers, has_prev, has_next = Prosumer.all(page)
			response['success'] = True
			response['count'] = len(prosumers)
			response['data'] = prosumers
			response['has_prev'] = has_prev
			response['has_next'] = has_next
			return response, 200

		except Exception as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Error during request. Team has been notified. Please try again later'
			return response, 500

	def all_in_facility(self, page, user_id, facility_id):
		response = {}
		if user_id is None:
			response['success'] = False
			response['message'] = 'User not found'
			return response, 404
		try:
			prosumers, has_prev, has_next = Prosumer.all_in_facility(
				page, facility_id)
			response['success'] = True
			response['count'] = len(prosumers)
			response['data'] = prosumers
			response['has_prev'] = has_prev
			response['has_next'] = has_next
			return response, 200

		except Exception as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Error during request. Team has been notified. Please try again later'
			return response, 500

	def unassigned(self, user_id, facility_id):
		response = {}
		if user_id is None:
			response['success'] = False
			response['message'] = 'User not found'
			return response, 404
		response = {}
		if not Prosumer.get_by_facility(facility_id=facility_id):
			response['success'] = False
			response['message'] = 'Facility Not Found.'
			return response, 404

		data = Prosumer.unassigned(facility_id)
		response['success'] = True
		response['count'] = len(data)
		response['data'] = data
		return response, 200

	def assigned(self, user_id, facility_id):
		response = {}
		if user_id is None:
			response['success'] = False
			response['message'] = 'User not found'
			return response, 404
		if not Prosumer.get_by_facility(facility_id=facility_id):
			response['success'] = False
			response['message'] = 'Facility Not Found.'
			return response, 404

		data = Prosumer.assigned_prosumers(facility_id)
		response['success'] = True
		response['count'] = len(data)
		response['data'] = data
		return response, 200

	def assign_to_user(self, user_id, role_id, facilities, group_id, prosumer_id, facility_id):
		response = {}
		if user_id is None:
			logger.info("user_id: {}".format(user_id))
			response['success'] = False
			response['message'] = 'User not found'
			return response, 404
		
		if not role_id:
			logger.info("role_id: {}".format(role_id))
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) not in {3, 4, 5}:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if facility_id not in facilities:
			logger.info("facility_id: {} facilities: {}".format(facility_id, facilities))
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		prosumer = Prosumer.get_prosumer(prosumer_id=prosumer_id)
		if not prosumer:
			response['success'] = False
			response['message'] = 'Meter not found'
			return response, 404

		if group_id == prosumer.user_group_id:
			response['success'] = False
			response['message'] = 'This user is already assigned to this meter'
			return response, 409

		if prosumer.user_group_id:
			response['success'] = False
			response['message'] = 'A user is already assigned to this meter'
			return response, 409

		if facility_id != prosumer.facility_id:
			response['success'] = False
			response['message'] = 'User {} and meter {} not in the same facility'.format(
				group_id, prosumer_id)
			return response, 403

		try:
			data = Prosumer.assign_to_user_group(group_id, prosumer)
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Service currently unavailable. Please try again later.'
			return response, 500

		response['success'] = True
		response['data'] = data
		return response, 201

	def reassign_to_user(self, user_id, role_id, facilities, group_id, prosumer_id, facility_id):
		response = {}
		if user_id is None:
			logger.info("user_id: {}".format(user_id))
			response['success'] = False
			response['message'] = 'User not found'
			return response, 404
		
		if not role_id:
			logger.info("role_id: {}".format(role_id))
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) not in {3, 4, 5}:
			logger.info("role_id: {}".format(role_id))
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if facility_id not in facilities:
			logger.info("facility_id {} facilities {}".format(facility_id, facilities))
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		prosumer = Prosumer.get_prosumer(prosumer_id=prosumer_id)
		if not prosumer:
			response['success'] = False
			response['message'] = 'Meter not found'
			return response, 404

		if prosumer.user_group_id == group_id:
			response['success'] = False
			response['message'] = 'This user is already assigned to this meter'
			return response, 409

		if not prosumer.user_group_id:
			response['success'] = False
			response['message'] = 'No user group assigned to the meter'
			return response, 409  # conflict because a reassignment implies there was a user_group assigned to the prosumer before.

		if facility_id != prosumer.facility_id:
			response['success'] = False
			response['message'] = 'Meter not in this facility'
			return response, 404

		try:
			data = Prosumer.reassign_to_user_group(group_id, prosumer)
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Service currently unavailable. Please try again later.'
			return response, 500
		response['success'] = True
		response['data'] = data
		return response, 201

	def remove_from_user(self, role_id, group_id, prosumer_id, facility_id, facilities=None):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) not in {3, 4, 5}:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 6:
			# If request isn't made from cmdctrl aka transaction engine.
			if facility_id not in facilities:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403

		prosumer = Prosumer.get_prosumer(prosumer_id=prosumer_id)
		if not prosumer:
			response['success'] = False
			response['message'] = 'Meter not found'
			return response, 404

		if prosumer.user_group_id is None:
			response['success'] = False
			response['message'] = 'No user assigned to this probe'
			return response, 404

		if group_id != prosumer.user_group_id:
			response['success'] = False
			response['message'] = 'This meter is assigned to another user'
			return response, 403

		if facility_id != prosumer.facility_id:
			response['success'] = False
			response['message'] = 'Meter not in this facility'
			return response, 404

		try:
			data = Prosumer.remove_from_user_group(prosumer)
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Service currently unavailable. Please try again later.'
			return response, 500

		response['success'] = True
		response['data'] = data
		return response, 200

	def remove_all_from_user(self, role_id, user_group_id):
		"""Remove all prosumers in user group from the user."""

		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		# if int(role_id) not in {3, 4, 5}:
		# 	response['success'] = False
		# 	response['message'] = 'Unauthourized'
		# 	return response, 403
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 6:
			# If request isn't made from cmdctrl aka transaction engine.
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		prosumers = Prosumer.get_all_prosumers(user_group_id=user_group_id)
		if not isinstance(prosumers, list):
			response['success'] = False
			response['message'] = 'This user has no meters.'
			return response, 404
		
		if len(prosumers) < 1:
			response['success'] = False
			response['message'] = 'This user has no meters.'
			return response, 404

		try:
			Prosumer.remove_all_from_user_group(prosumers)
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Service currently unavailable. Please try again later.'
			return response, 500

		response['success'] = True
		return response, 201

	def assignment_params(self, user_id, facility_id):
		response = {}
		if user_id is None:
			response['success'] = False
			response['message'] = 'User not found'
			return response, 404
		try:
			data = Prosumer.assignment_params(facility_id)
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Service currently unavailable. Please try again later.'
			return response, 500

		response['success'] = True
		response['data'] = data
		return response, 200

	def removal_params(self, user_id, facility_id):
		response = {}
		if user_id is None:
			response['success'] = False
			response['message'] = 'User not found'
			return response, 404
		try:
			data = Prosumer.removal_params(facility_id)
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Service currently unavailable. Please try again later.'
			return response, 500

		response['success'] = True
		response['data'] = data
		return response, 200

	def view_probe(self, role_id, facility_id, probe_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if not int(role_id) >= 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) != 5:
			response['success'] = False
			response['message'] = 'Not Found. Forbidden.'
			return response, 403

		probe = Probe.get_probe(probe_id)
		if probe is None:
			response['success'] = False
			response['message'] = 'Probe not found'
			return response, 404

		if facility_id != probe.facility_id:
			response['success'] = False
			response['message'] = 'Probe not found in this facility.'
			return response, 404

		data = probe.to_dict()
		response['success'] = True
		response['data'] = data
		return response, 201

	def view_prosumer(self, prosumer_name):
		response = {}
		try:
			prosumer = Prosumer.get_prosumer(name=prosumer_name)
		except AttributeError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Prosumer not found'
			return response, 404

		if prosumer is None:
			response['success'] = False
			response['message'] = 'Prosumer not found'
			return response, 404
		response['success'] = True
		response['data'] = prosumer.to_dict()
		return response, 200

	def view_prosumer_in_facility(self, role_id, facility_id, prosumer_name):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if not int(role_id) >= 3:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			prosumer = Prosumer.get_prosumer(name=prosumer_name)
		except AttributeError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Prosumer not found'
			return response, 404

		if facility_id != prosumer.facility_id:
			response['success'] = False
			response['message'] = 'Prosumer in {} not found in this facility.'.format(prosumer.facility_id)
			return response, 404

		if prosumer is None:
			response['success'] = False
			response['message'] = 'Prosumer not found'
			return response, 404
		response['success'] = True
		response['data'] = prosumer.to_dict()
		return response, 200

	def view_prosumer_by_type_in_facility(self, role_id, page, type_id, facility_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id <= 3:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		try:
			prosumers, has_prev, has_next = Prosumer.get_prosumer_by_type(page, type_id=type_id, facility_id=facility_id)
			response['success'] = True
			response['count'] = len(prosumers)
			response['data'] = prosumers
			response['has_prev'] = has_prev
			response['has_next'] = has_next
			return response, 201
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Error during request. Team has been notified. Please try again later'
			return response, 500

	def view_prosumer_by_prosumer_group(self, role_id, page, prosumer_group_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if not role_id >= 3:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		try:
			prosumers, has_prev, has_next = Prosumer.get_prosumer_by_prosumer_group(page, prosumer_group_id)
			response['success'] = True
			response['count'] = len(prosumers)
			response['data'] = prosumers
			response['has_prev'] = has_prev
			response['has_next'] = has_next
			return response, 201
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Error during request. Team has been notified. Please try again later'
			return response, 500

	def delete_probe(self, role_id, probe_id, facility_id, destroy=False):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if not role_id >= 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		probe = Probe.get_probe(probe_id)
		if probe is None:
			response['success'] = False
			response['message'] = 'Probe {} not found.'.format(probe_id)
			return response, 404

		if facility_id != probe.facility_id:
			response['success'] = False
			response['message'] = 'Probe in {} not found in this facility.'.format(probe.facility_id)
			return response, 404
		try:
			probe.delete_probe(destroy=destroy)
		except KafkaConnectionError as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Service temporarily unavailable.'
			return response, 400
		response['success'] = True
		response['message'] = 'Probe Deleted.'
		return response, 200

	def delete_prosumer(self, role_id, prosumer_name, facility_id, destroy=False):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if not role_id >= 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		prosumer = Prosumer.get_prosumer(name=prosumer_name, deleted=destroy)
		if prosumer is None:
			response['success'] = False
			response['message'] = 'Prosumer {} not found.'.format(prosumer_name)
			return response, 404

		if facility_id != prosumer.facility_id:
			response['success'] = False
			response['message'] = 'Prosumer in {} not found in this facility.'.format(prosumer.facility_id)
			return response, 404
		try:
			prosumer.delete_prosumer(destroy=destroy)
		except KafkaConnectionError as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Service temporarily unavailable.'
			return response, 400
		response['success'] = True
		response['message'] = 'Prosumer deleted {}'.format(prosumer_name)
		return response, 200

	def tx_prosumer(self, prosumer_name):
		response = {}
		prosumer = Prosumer.get_prosumer(name=prosumer_name)
		if prosumer is None:
			response['success'] = False
			response['message'] = 'Prosumer not found'
			return response, 404

		response['success'] = True
		response['data'] = prosumer.to_dict()
		return response, 200

	def config_builder(self, user_id, role_id, prosumer_id):
		response = {}
		prosumer = Prosumer.get_prosumer(prosumer_id=prosumer_id)
		if prosumer is None:
			response['success'] = False
			response['message'] = 'Prosumer not found'
			return response, 404
	
		response['success'] = True
		response['data'] = prosumer.prepare_probe_config()
		return response, 200

	def init_config_creation(self, user_id, role_id, prosumer_id):
		response = {}
		prosumer = Prosumer.get_prosumer(prosumer_id=prosumer_id)
		if prosumer is None:
			response['success'] = False
			response['message'] = 'Prosumer not found'
			return response, 404

		prosumer.start_prosumer_config()
		response['success'] = True
		response['message'] = 'Sent meter settings to device'
		return response, 201


class RefreshManager(object):

	def refresh_prosumer(self, prosumer_id):
		response = {}
		prosumer = Prosumer.get_prosumer(prosumer_id=prosumer_id)
		if prosumer is None:
			response['success'] = False
			response['message'] = 'Meter not found.'
			return response, 404
		prosumer.send_prosumer_to_kafka()
		response['success'] = True
		response['message'] = 'Refreshed meters.'
		return response, 201

	def refresh_probe(self, probe_id):
		response = {}
		probe = Probe.get_probe(probe_id=probe_id)
		if probe is None:
			response['success'] = False
			response['message'] = 'Probe not found.'
			return response, 404
		probe.send_probe_to_kafka()
		response['success'] = True
		response['message'] = 'Refreshed meters.'
		return response, 201

