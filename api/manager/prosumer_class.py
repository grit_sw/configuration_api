from api.models import ProsumerClass
from logger import logger


class ProsumerClassManager(object):
	def create(self, name, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		try:
			if int(role_id) < 5:
				response['success'] = False
				response['message'] = 'Unauthourized.'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		exists = ProsumerClass.find_class(name=name)
		if exists:
			response['success'] = False
			response['message'] = 'Prosumer class {} already exists.'.format(name.title())
			return response, 409

		new_prosumer_class = ProsumerClass.create(name)
		if new_prosumer_class:
			response['success'] = True
			response['data'] = new_prosumer_class
			return response, 201
		response['success'] = False
		response['message'] = 'Error in creating Prosumer {}'.format(name.title())
		return response, 400

	def edit(self, name, class_id, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		try:
			if not int(role_id) >= 5:
				response['success'] = False
				response['message'] = 'Unauthourized.'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		dupe_class = ProsumerClass.find_class(name=name)
		if dupe_class:
			response['success'] = False
			response['message'] = 'Prosumer class already exists'
			return response, 409

		prosumer_class = ProsumerClass.find_class(class_id=class_id)
		if not prosumer_class:
			response['success'] = False
			response['message'] = 'Prosumer Class not found.'
			return response, 404

		data = prosumer_class.edit(name)
		if not data:
			response['success'] = False
			response['message'] = 'Service Unavailable. Try again later.'
			return response, 500
		
		response['success'] = True
		response['data'] = data
		return response, 201

	def view_one(self, class_id, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		try:
			if not int(role_id) >= 3:
				response['success'] = False
				response['message'] = 'Unauthourized.'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		prosumer_class = ProsumerClass.find_class(class_id=class_id)
		if not prosumer_class:
			response['success'] = False
			response['message'] = 'Not found.'
			return response, 404

		data = prosumer_class.to_dict()		
		response['success'] = True
		response['data'] = data
		return response, 201

	def view_all(self, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		try:
			if not int(role_id) >= 3:
				response['success'] = False
				response['message'] = 'Unauthourized.'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		data = ProsumerClass.view_all()
		if not data:
			response['success'] = False
			response['message'] = 'No Prosumer class created.'
			return response, 404

		response['success'] = True
		response['data'] = data
		return response, 200

	def delete_class(self, role_id, class_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		try:
			if not int(role_id) >= 3:
				response['success'] = False
				response['message'] = 'Unauthourized.'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		prosumer_class = ProsumerClass.find_class(class_id=class_id)
		if not prosumer_class:
			response['success'] = False
			response['message'] = 'Not found.'
			return response, 404

		try:
			prosumer_class.delete_class()
		except Exception as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unable to delete prosumer type.'
			return response, 500

		response['success'] = True
		response['message'] = 'Prosumer class deleted.'
		return response, 204
