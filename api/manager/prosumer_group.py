from logger import logger
from api.models import ProsumerGroup, ProsumerClientAdmins


class ProsumerGroupManager(object):

	def create_prosumer_group(self, name, facility_id, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			if int(role_id) < 3:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		exists = ProsumerGroup.get_prosumer_group(name=name, facility_id=facility_id)
		if exists:
			response['success'] = False
			response['message'] = 'Prosumer Group {} already exists.'.format(name.title())
			return response, 409

		new_group = ProsumerGroup.create(name.title(), facility_id)
		if new_group:
			response['success'] = True
			response['data'] = new_group
			return response, 201

		response['success'] = False
		response['message'] = 'Prosumer Group {} not created.'.format(name.title())
		return response, 400

	def edit_prosumer_group(self, name, group_id, facility_id, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			if int(role_id) < 3:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		exists = ProsumerGroup.get_prosumer_group(name=name, facility_id=facility_id)
		if exists:
			response['success'] = False
			response['message'] = 'Prosumer Group {} already exists.'.format(name.title())
			return response, 409

		prosumer_group = ProsumerGroup.view_one(group_id, facility_id)
		if prosumer_group:
			data = ProsumerGroup.edit_group(name, group_id, facility_id)
			if data:
				response['success'] = True
				response['data'] = data
				return response, 201
			response['success'] = False
			response['message'] = 'Error when editing prosumer group. Please try again later.'
			return response, 400

		response['success'] = False
		response['message'] = 'Prosumer group not found.'
		return response, 404

	def one_group(self, group_id, facility_id, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			if int(role_id) < 3:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		if group_id is None:
			response['success'] = False
			response['message'] = 'Prosumer group ID required.'
		prosumer_group = ProsumerGroup.view_one(group_id, facility_id)
		if prosumer_group:
			response['success'] = True
			response['data'] = prosumer_group
			return response, 200
		response['success'] = False
		response['message'] = 'Prosumer group not found.'
		return response, 404

	def all_groups(self, facility_id, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			if int(role_id) < 3:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		prosumer_groups = ProsumerGroup.view_all(facility_id)
		if prosumer_groups:
			response['success'] = True
			response['data'] = prosumer_groups
			return response, 200
		else:
			response['success'] = False
			response['message'] = 'No Prosumer groups found.'
			return response, 404

	def add_prosumer_to_prosumer_group(self, prosumer_facility, group_facility, prosumer_id, prosumer_group_id, role_id):
		# prosumer_facility is the facility ID of the prosumer
		# group_facility is the facility ID of the prosumer group
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			if int(role_id) < 3:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request'
			return response, 400

		prosumer = ProsumerGroup.get_prosumer(prosumer_id=prosumer_id)
		if prosumer is None:
			response['success'] = False
			response['message'] = 'Meter not found.'
			return response, 404

		if prosumer_facility != group_facility:
			response['success'] = False
			response['message'] = 'Meter and meter group facility do not match.'
			return response, 409

		if prosumer.facility_id != prosumer_facility:
			response['success'] = False
			response['message'] = 'Meter not found in this facility.'
			return response, 404

		prosumer_group = ProsumerGroup.get_prosumer_group_without_facility_id(group_id=prosumer_group_id)

		print(prosumer_group)
		print(prosumer_group_id)
		if prosumer_group is None:
			response['success'] = False
			response['message'] = 'Meter group not found'
			return response, 404

		if prosumer_group.facility_id != group_facility:
			response['success'] = False
			response['message'] = 'Meter group not found in this facility.'
			return response, 404

		if prosumer_group.prosumer_in_group(prosumer.prosumer_id):
			response['success'] = False
			response['message'] = 'Meter {} already assigned to meter group {}.'.format(
				prosumer.name, prosumer_group.name
			)
			return response, 409

		added = prosumer_group.add_prosumer_to_prosumer_group(prosumer)
		if added:
			response['success'] = True
			response['data'] = added
			return response, 200
		response['success'] = False
		response['message'] = 'Error when adding prosumer to prosumer group. Please try again later.'
		return response, 400

	def remove_prosumer_from_prosumer_group(self, prosumer_id, prosumer_group_id, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			if int(role_id) < 3:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		prosumer = ProsumerGroup.get_prosumer(prosumer_id=prosumer_id)
		if prosumer is None:
			response['success'] = False
			response['message'] = 'Meter not found'
			return response, 404
		prosumer_group = ProsumerGroup.get_prosumer_group_without_facility_id(group_id=prosumer_group_id)
		if prosumer_group is None:
			response['success'] = False
			response['message'] = 'Meter group not found'
			return response, 404

		if not prosumer_group.prosumer_in_group(prosumer.prosumer_id):
			response['success'] = False
			response['message'] = 'Meter {} not assigned to meter group {}.'.format(
				prosumer.name, prosumer_group.name
			)
			return response, 404

		removed = prosumer_group.remove_prosumer_from_prosumer_group(prosumer)
		if removed:
			response['success'] = True
			response['data'] = removed
			return response, 200
		response['success'] = False
		response['message'] = 'Error when removing prosumer from prosumer group. Please try again later.'
		return response, 400

	def add_client_admin(self, admin_facility, group_facility, client_admin_id, prosumer_group_id, role_id):
		# admin_facility is the facility ID of the client admin
		# group_facility is the facility ID of the prosumer group
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		try:
			if int(role_id) < 4:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403

		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		prosumer_group = ProsumerGroup.get_prosumer_group_without_facility_id(group_id=prosumer_group_id)
		if prosumer_group is None:
			response['success'] = False
			response['message'] = 'Meter group not found'
			return response, 404

		if admin_facility != group_facility:
			response['success'] = False
			response['message'] = 'Meter group and admin must be in the same facility.'
			return response, 409

		if group_facility != prosumer_group.facility_id:
			response['success'] = False
			response['message'] = 'Meter group not found in this facility.'
			return response, 409

		client_admin = ProsumerClientAdmins.get_admin(client_admin=client_admin_id)
		if client_admin:
			if prosumer_group.has_client_admin(client_admin):
				response['success'] = False
				response['message'] = 'Admin already assigned to meter group {}.'.format(
					prosumer_group.name
				)
				return response, 409

		if not client_admin:
			client_admin = ProsumerClientAdmins.create(client_admin=client_admin_id)
		added = prosumer_group.add_client_admin(client_admin)
		if added:
			response['success'] = True
			response['data'] = added
			return response, 200
		response['success'] = False
		response['message'] = 'Error when assigning client admin to meter group. Please try again later.'
		return response, 400

	def remove_client_admin(self, client_admin_id, prosumer_group_id, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			if int(role_id) < 4:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		prosumer_group = ProsumerGroup.get_prosumer_group_without_facility_id(group_id=prosumer_group_id)
		if prosumer_group is None:
			response['success'] = False
			response['message'] = 'Prosumer group not found'
			return response, 404

		client_admin = ProsumerClientAdmins.get_admin(client_admin=client_admin_id)

		if not client_admin:
			response['success'] = False
			response['message'] = 'Client admin not found'
			return response, 404

		if not prosumer_group.has_client_admin(client_admin):
			response['success'] = False
			response['message'] = 'Admin not assigned to prosumer group {}.'.format(
				prosumer_group.name
			)
			return response, 404

		removed = prosumer_group.remove_client_admin(client_admin)
		if removed:
			response['success'] = True
			response['data'] = removed
			return response, 200

		response['success'] = False
		response['message'] = 'Error when removing client admin from prosumer group. Please try again later.'
		return response, 400
