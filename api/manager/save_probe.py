from api.models import Channels, Probe
from api.models.save_probe import Save
from logger import logger


class SaveManager(object):
	"""Class for saving configurations for probes or power sources"""

	def save_probe(self, role_id, probe_name):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403
		probe = Probe.get_one(probe_name=probe_name)
		if not probe:
			response['success'] = False
			response['message'] = 'Probe not found'
			return response, 404

		data = Save.save_probe(probe=probe)
		response['success'] = True
		response['data'] = data
		return response, 200

	def save_source(self, role_id, one_source_channel_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if int(role_id) < 5:
			response['success'] = False
			response['message'] = 'Forbidden.'
			return response, 403

		channel = Channels.get_channel(channel_id=one_source_channel_id)
		if not channel:
			response['success'] = False
			response['message'] = 'Power source not found.'
			return response, 404

		data = Save.save_probe(channel=channel)
		response['success'] = True
		response['data'] = data
		return response, 200
