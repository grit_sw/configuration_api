from api.models import Prosumer, ProsumerType, ProsumerGroup
from logger import logger
from api.mixins import KafkaMixin
from flask import current_app


class SwitchManager(object):
	def switch_one(self, user_id, user_group_id, role_id, facilities, prosumer_id, state=False):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		prosumer = Prosumer.get_prosumer(prosumer_id=prosumer_id)
		if prosumer is None:
			response['success'] = False
			response['message'] = 'Meter not found'
			return response, 404
		if role_id == 4:
			if prosumer.facility_id not in facilities:
				logger.info("facility_id: {} facilities: {}".format(prosumer.facility_id, facilities))
				response['success'] = False
				response['message'] = 'Unauthourized facility admin'
				return response, 403

		if role_id == 3:
			if user_id not in set(prosumer.client_admin_list()):
				logger.info("user_id: {} client_admins: {}".format(user_id, prosumer.client_admin_list()))
				response['success'] = False
				response['message'] = 'Unauthourized client admin'
				return response, 403

		if role_id in {1, 2}:
			if user_group_id != prosumer.user_group_id:
				logger.info("user_id: {} client_admins: {}".format(user_id, prosumer.client_admin_list()))
				response['success'] = False
				response['message'] = 'Unauthourized user admin'
				return response, 403

		if not role_id >= 5:
			response['success'] = False
			response['message'] = 'Unauthourized user.'
			return response, 403

		switch_message = prosumer.switch_prosumer(
			state,
			kafka_topic=current_app.config.get('PROSUMER_SWITCH_TOPIC'),
		)
		KafkaMixin.notify_kafka(switch_message)
		switch_state = 'ON' if state is True else 'OFF'
		response['success'] = True
		response['message'] = 'Meter switched {}'.format(switch_state)
		return response, 200

	def switch_type(self, role_id, type_id, facility_id, state=False):
		response = {}
		if not role_id >= 4:
			response['success'] = False
			response['message'] = 'Unauthourized user.'
			return response, 403

		prosumer_type = ProsumerType.find_type(id=type_id)
		if prosumer_type is None:
			response['success'] = False
			response['message'] = 'Prosumer type not found in facility'
			return response, 404
		switch_message = prosumer_type.switch_prosumer_type(
			state,
			facility_id=facility_id,
			kafka_topic=current_app.config.get('PROSUMER_SWITCH_TOPIC'),
		)
		KafkaMixin.notify_kafka(switch_message)
		switch_state = 'ON' if state is True else 'OFF'
		response['success'] = True
		response['message'] = 'Prosumer type switched {}'.format(switch_state)
		return response, 200

	def switch_group(self, role_id, facilities, facility_id, state=False):
		response = {}
		prosumer_group = ProsumerGroup.get_prosumer_group(id=group_id, facility_id=facility_id)
		if prosumer_group is None:
			response['success'] = False
			response['message'] = 'Prosumer group not found'
			return response, 404
		if role_id < 3:
			response['success'] = False
			response['message'] = 'Unauthourized user.'
			return response, 403
		if role_id in {3, 4}:
			if not prosumer_group.facility_id in set(facilities):
				response['success'] = False
				response['message'] = 'Unauthourized user.'
				return response, 403
		switch_message = prosumer_group.switch_prosumer_group(
			state=state,
			kafka_topic=current_app.config.get('PROSUMER_SWITCH_TOPIC'),
		)
		KafkaMixin.notify_kafka(switch_message)
		switch_state = 'ON' if state is True else 'OFF'
		response['success'] = True
		response['message'] = 'Prosumer group switched {}'.format(switch_state)
		return response, 200

	def switch_facility(self, facilities, facility_id, state=False):
		response = {}
		if not facility_id in set(facilities):
			response['success'] = False
			response['message'] = 'Invalid facility'
			return response, 403
		facility = Prosumer.one_facility(facility_id=facility_id)
		if facility is None:
			response['success'] = False
			response['message'] = 'Facility not found'
			return response, 404
		switch_message = facility.switch_facility(
			state,
			kafka_topic=current_app.config.get('PROSUMER_SWITCH_TOPIC'),
		)
		KafkaMixin.notify_kafka(switch_message)
		switch_state = 'ON' if state is True else 'OFF'
		response['success'] = True
		response['message'] = 'Facility switched {}'.format(switch_state)
		return response, 200
