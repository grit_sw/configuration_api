from time import sleep

from confluent_kafka.avro.error import ClientError
from flask import current_app

from api.exceptions import KafkaConnectionError
from logger import logger

# from requests.exceptions import ConnectionError



class KafkaMixin(object):

	@staticmethod
	def notify_kafka(payload):
		from flask import current_app

		kafka_topic = payload['kafka_topic']
		use_avro = payload['use_avro']
		del payload['kafka_topic']
		del payload['use_avro']

		retry_count = 0
		try:
			while True:
				try:
					current_app.kafka_producer.send_message(
						topic=kafka_topic,
						message=payload,
						use_avro=use_avro
					)
					break
				except (ConnectionError, ClientError) as e:
					logger.info("KAFKA PAYLOAD = {}".format(payload))
					logger.exception(e)
					retry_count += 1
					sleep(5)
					if retry_count == 5:
						raise KafkaConnectionError(
							'Unable to send message to Kafka broker after {} retries.'.format(retry_count)
						)
			logger.info('Produced message = {} to topic {}'.format(payload, kafka_topic))
		except KafkaConnectionError as e:
			logger.critical(e)


	@classmethod
	def before_commit(cls, session):
		added_items = []
		updated_items = []
		deleted_items = []
		session._changes = {}
		kafka_topic = current_app.config.get('PROSUMER_UPDATE_TOPIC')
		for model in list(session.new):
			if model.__tablename__ in {'probe', 'prosumer_group'}:
				prosumers = model.prosumers.all()
				added_items.extend([prosumer.kafka_dict(kafka_topic) for prosumer in prosumers])
			if model.__tablename__ == 'prosumer':
				updated_items.extend([model.kafka_dict(kafka_topic)])

		for model in list(session.dirty):
			if model.__tablename__ in {'probe', 'prosumer_group'}:
				prosumers = model.prosumers.all()
				updated_items.extend([prosumer.kafka_dict(kafka_topic) for prosumer in prosumers])
			if model.__tablename__ == 'prosumer':
				updated_items.extend([model.kafka_dict(kafka_topic)])

		for model in list(session.deleted):
			if model.__tablename__ == 'probe':
				prosumers = model.prosumers.all()
				updated_items.extend([prosumer.kafka_dict(kafka_topic) for prosumer in prosumers])
			if model.__tablename__ == 'prosumer':
				deleted_items.append(model.kafka_dict(kafka_topic))

		session._changes = {
			'add': added_items,
			'update': updated_items,
			'delete': deleted_items
		}

	@classmethod
	def after_commit(cls, session):
		if session._changes:
			for data in session._changes['add']:
				if data:
					KafkaMixin.notify_kafka(data)
			for data in session._changes['update']:
				if data:
					KafkaMixin.notify_kafka(data)
			for data in session._changes['delete']:
				if data:
					KafkaMixin.notify_kafka(data)
		session._changes = None
