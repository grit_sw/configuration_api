from api import db


class Base(db.Model):
	__abstract__ = True
	id = db.Column(db.Integer, autoincrement=True, primary_key=True)
	date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
	date_modified = db.Column(
		db.DateTime,
		default=db.func.current_timestamp(),
		onupdate=db.func.current_timestamp()
	)

class PowerBase(Base):
	__abstract__ = True
	installed = db.Column(db.Boolean, default=False)
	state = db.Column(db.Integer, default=0)  # I dont know what this is meant to be.
	# I included it because it was included in the old dashboard configuration app
	# and it is a field in the configuration message that is sent to the meter.
	#  todo: ask Yoda for what the state means and why it defaults to 0.

	@classmethod
	def get_installed(cls, installed=True):
		return [obj.to_dict() for obj in cls.query.filter_by(installed=installed).order_by(cls.date_created.desc()).all()]

	def install(self):
		self.installed = True
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	def uninstall(self):
		self.installed = False
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()
