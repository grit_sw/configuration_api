"""
	Module for interacting with the database
"""
from uuid import uuid4

from api import db
from api.models.base import Base


class ChargeMap(Base):
	"""docstring for ChargeMap"""
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	map_id = db.Column(db.String(100), unique=True, nullable=False)
	start_time = db.Column(db.Time, nullable=True)
	stop_time = db.Column(db.Time, nullable=True)
	amount = db.Column(db.Float, unique=False, nullable=True)

	single_phase_grid = db.relationship('SinglePhaseGrid', backref='charge_map')  # Many to one relationship with the power sources
	single_phase_grid_id = db.Column(db.String(100), db.ForeignKey('single_phase_grid.single_phase_grid_id'))

	single_phase_generator = db.relationship('SinglePhaseGenerator', backref='charge_map')  # Many to one relationship with the power sources
	single_phase_generator_id = db.Column(db.String(100), db.ForeignKey('single_phase_generator.single_phase_generator_id'))

	# single_phase_inverter = db.relationship('SinglePhaseGrid', backref='charge_map')  # Many to one relationship with the power sources
	# single_phase_inverter_id = db.Column(db.String(100), db.ForeignKey('single_phase_inverter.single_phase_inverter_id'))

	three_phase_grid = db.relationship('ThreePhaseGrid', backref='charge_map')  # Many to one relationship with the power sources
	three_phase_grid_id = db.Column(db.String(100), db.ForeignKey('three_phase_grid.three_phase_grid_id'))

	three_phase_generator = db.relationship('ThreePhaseGenerator', backref='charge_map')  # Many to one relationship with the power sources
	three_phase_generator_id = db.Column(db.String(100), db.ForeignKey('three_phase_generator.three_phase_generator_id'))

	# three_phase_inverter = db.relationship('ThreePhaseGrid', backref='charge_map')  # Many to one relationship with the power sources
	# three_phase_inverter_id = db.Column(db.String(100), db.ForeignKey('three_phase_inverter.three_phase_inverter_id'))

	def __init__(self, start_time, stop_time, amount):
		self.start_time = start_time
		self.stop_time = stop_time
		self.amount = amount
		self.map_id = str(uuid4())

	@staticmethod
	def get_charge_map(map_id):
		charge_map = ChargeMap.query.filter_by(map_id=map_id).first()
		return charge_map

	@staticmethod
	def new(start_time, stop_time, amount):
		new_charge_map = ChargeMap(
			start_time=start_time,
			stop_time=stop_time,
			amount=amount,
		)
		db.session.add(new_charge_map)
		return new_charge_map

	def edit(self, start_time, stop_time, amount):
		self.start_time = start_time
		self.stop_time = stop_time
		self.amount = amount
		db.session.add(self)
		return self

	@staticmethod
	def get_all():
		charge_maps = ChargeMap.query.order_by(ChargeMap.date_created.desc()).all()
		return [charge_map.to_dict() for charge_map in charge_maps]

	def delete_charge_map(self):
		db.session.delete(self)
		db.session.commit()

	def device_config_dict(self):
		response = {
			'start_time': str(self.start_time),
			'amount': self.amount,
		}
		return response

	def to_dict(self):
		type_dict = {
			'map_id': self.map_id,
			'start_time': str(self.start_time),
			'stop_time': str(self.stop_time),
			'amount': self.amount,
		}
		return type_dict
