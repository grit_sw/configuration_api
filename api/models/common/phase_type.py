"""
	Module for interacting with the database
"""
from uuid import uuid4

from api import db
from api.models.base import Base


class PhaseType(Base):
	"""docstring for PhaseType"""
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=False, nullable=False)
	num_id = db.Column(db.Integer, unique=False, nullable=False)
	phase_id = db.Column(db.String(100), unique=True, nullable=False)

	# Consumers
	single_phase_consumer_lines = db.relationship('SinglePhaseConsumerLine', backref='phase_type')
	three_phase_consumer_lines = db.relationship('ThreePhaseConsumerLine', backref='phase_type')

	# Producers
	single_phase_generator = db.relationship('SinglePhaseGenerator', backref='phase_type')
	three_phase_generator = db.relationship('ThreePhaseGenerator', backref='phase_type')
	single_phase_grid = db.relationship('SinglePhaseGrid', backref='phase_type')
	three_phase_grid = db.relationship('ThreePhaseGrid', backref='phase_type')
	single_phase_inverter = db.relationship('SinglePhaseInverter', backref='phase_type')
	three_phase_inverter = db.relationship('ThreePhaseInverter', backref='phase_type')

	def __init__(self, name, num_id):
		self.name = name.title()
		self.num_id = num_id
		self.phase_id = str(uuid4())

	# @staticmethod
	# def create(name):
	# 	new_phase = PhaseType(
	# 		name=name,
	# 		)
	# 	db.session.add(new_phase)
	# 	db.session.commit()
	# 	db.session.refresh(new_phase)
	# 	return new_phase.to_dict()

	# def edit(self, name):
	# 	self.name = name.title()
	# 	db.session.add(self)
	# 	db.session.commit()
	# 	db.session.refresh(self)
	# 	return self.to_dict()

	@staticmethod
	def get_one(phase_id=None, name=None):
		phase_type = None
		if phase_id:
			phase_type = PhaseType.query.filter_by(phase_id=phase_id).first()
		elif name:
			name = name.title()
			phase_type = PhaseType.query.filter_by(name=name).first()
		return phase_type

	@staticmethod
	def get_one_phase():
		name = 'Single Phase'
		phase_type = PhaseType.query.filter_by(name=name).first()
		return phase_type

	@staticmethod
	def get_three_phase():
		name = 'Three Phase'
		phase_type = PhaseType.query.filter_by(name=name).first()
		return phase_type

	@staticmethod
	def init_types():
		names = [
			('Single Phase', 1),
			('Three Phase', 3),
		]
		for name, num_id in names:
			if not PhaseType.get_one(name=name):
				new_phase = PhaseType(
					name=name,
					num_id=num_id,
					)
				db.session.add(new_phase)
		db.session.commit()
		return PhaseType.view_all()

	@staticmethod
	def view_all():
		all_phase_types = PhaseType.query.order_by(PhaseType.name.asc()).all()
		if all_phase_types:
			data = [phase_type.to_dict() for phase_type in all_phase_types]
			return data
		return []

	def delete(self):
		db.session.delete(self)
		db.session.commit()
		return

	@staticmethod
	def get_all():
		return [phase.to_dict() for phase in PhaseType.query.order_by(PhaseType.name.desc()).all()]

	def get_data(self):
		return self.to_dict()

	def small_dict(self):
		type_dict = {
			'name': self.name,
			'phase_id': self.phase_id,
		}
		return type_dict

	def to_dict(self):
		type_dict = {
			'name': self.name,
			'phase_id': self.phase_id,
			'single_phase_consumer_lines': [consumer.small_dict() for consumer in self.single_phase_consumer_lines],
			'three_phase_consumer_lines': [consumer.small_dict() for consumer in self.three_phase_consumer_lines],
		}
		return type_dict
