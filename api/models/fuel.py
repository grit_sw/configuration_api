"""
	Module for interacting with the database
"""
from uuid import uuid4

from api import db
from api.models.base import Base


class Fuel(Base):
	"""docstring for Fuel"""
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	fuel_id = db.Column(db.String(100), unique=True, nullable=False)
	name = db.Column(db.String(20), nullable=True)
	unit_price = db.Column(db.Float, nullable=True)

	single_phase_generator = db.relationship('SinglePhaseGenerator', backref='fuel')  # One to many relationship with the power sources
	three_phase_generator = db.relationship('ThreePhaseGenerator', backref='fuel')  # One to many relationship with the power sources

	def __init__(self, name, unit_price):
		self.name = name.title()
		self.unit_price = unit_price
		self.fuel_id = str(uuid4())

	@staticmethod
	def get_fuel(fuel_id=None, name=None):
		if fuel_id:
			fuel = Fuel.query.filter_by(fuel_id=fuel_id).first()
		if name:
			name = name.title()
			fuel = Fuel.query.filter_by(name=name).first()
		return fuel

	@staticmethod
	def new(name, unit_price):
		fuel = Fuel.get_fuel(name)
		if fuel:
			return fuel.to_dict()

		new_fuel = Fuel(
			name=name,
			unit_price=unit_price,
		)
		db.session.add(new_fuel)
		db.session.commit()
		return new_fuel.to_dict()

	def edit(self, name):
		self.name = name
		db.session.add(self)
		db.session.commit()
		return self.to_dict()

	@staticmethod
	def init_types():
		names = [
			('Diesel', 221.56),
			('Petrol', 145.0),
		]
		for name, price in names:
			if not Fuel.get_fuel(name):
				Fuel.new(name, price)
		return Fuel.get_all()


	def get_name(self):
		return self.name

	@staticmethod
	def get_all():
		fuel_types = Fuel.query.order_by(Fuel.name.asc()).all()
		return [fuel.to_dict() for fuel in fuel_types]

	def delete_fuel(self):
		db.session.delete(self)
		db.session.commit()

	def to_dict(self):
		response = {
			'fuel_id': self.fuel_id,
			'name': self.name,
			'unit_price': self.unit_price,
		}
		return response
