"""
	Module for interacting with the database
"""
from collections import namedtuple
from itertools import chain
from uuid import uuid4

from flask import request
from sqlalchemy import or_

from api import db
from api.mixins import KafkaMixin
from api.models.base import Base
from logger import logger


class DeviceType(Base):
	"""docstring for DeviceType"""
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=True, nullable=False)
	device_type_id = db.Column(db.String(100), unique=True, nullable=False)
	number_of_channels = db.Column(db.Integer, unique=False, nullable=False)
	probes = db.relationship('Probe', backref='device_type', lazy='dynamic', cascade="all, delete-orphan")

	def __init__(self, name, number_of_channels):
		self.name = name.title()
		self.device_type_id = str(uuid4())
		self.number_of_channels = number_of_channels

	@staticmethod
	def new_device_type(name, number_of_channels):
		device_type = DeviceType(
			name=name,
			number_of_channels=number_of_channels,
		)
		db.session.add(device_type)
		db.session.commit()
		db.session.refresh(device_type)
		return device_type.shallow_dict()

	def edit_device_type(self, name, number_of_channels):
		self.name = name.title()
		self.number_of_channels = number_of_channels
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.shallow_dict()

	@staticmethod
	def get_device_type(device_type_id=None, name=None):
		device_type = None
		if device_type_id:
			device_type = DeviceType.query.filter_by(device_type_id=device_type_id).first()
		if name:
			name = name.title()
			device_type = DeviceType.query.filter_by(name=name).first()
		return device_type

	@staticmethod
	def init_types():
		device_types = [
			('G0', 8),
			('G1', 8),
			('GMID', 0),
		]
		for device in device_types:
			if not DeviceType.get_device_type(name=device[0]):
				new_device = DeviceType(
					name=device[0],
					number_of_channels=device[1]
				)
				db.session.add(new_device)
		db.session.commit()
		return DeviceType.get_all()

	@staticmethod
	def get_g1():
		return DeviceType.get_device_type(name='G1')

	@staticmethod
	def get_g0():
		return DeviceType.get_device_type(name='G0')

	@staticmethod
	def get_gmid():
		return DeviceType.get_device_type(name='GMID')

	@staticmethod
	def get_all():
		device_types = DeviceType.query.order_by(DeviceType.id.asc()).all()
		return [device_type.shallow_dict() for device_type in device_types]

	def shallow_dict(self):
		response = {
			'name': self.name,
			'device_type_id': self.device_type_id,
			'number_of_channels': self.number_of_channels,
		}
		return response


class VoltageSensor(Base):
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=True, nullable=False)
	rating = db.Column(db.Float, unique=False, nullable=False)
	sensor_id = db.Column(db.String(100), unique=True)
	default_voltage = db.Column(db.Float, unique=False, default=240)
	scaling_factor_voltage = db.Column(db.Float, unique=False, default=1.0)
	channels = db.relationship('Channels', backref='voltage_sensor', lazy='dynamic')

	@staticmethod
	def create(name, rating, default_voltage, scaling_factor_voltage):
		new_sensor = VoltageSensor(
			name=name,
			rating=float(rating),
			default_voltage=float(default_voltage),
			scaling_factor_voltage=float(scaling_factor_voltage),
		)
		new_sensor.sensor_id = str(uuid4())
		db.session.add(new_sensor)
		db.session.commit()
		db.session.refresh(new_sensor)
		return new_sensor

	def edit(self, name, rating, default_voltage, scaling_factor_voltage):
		self.name = name
		self.rating = rating
		self.default_voltage = default_voltage
		self.scaling_factor_voltage = scaling_factor_voltage
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self

	@staticmethod
	def get_one(sensor_id=None, name=None, scaling_factor_voltage=None):
		sensor = None
		if sensor_id:
			sensor = VoltageSensor.query.filter_by(sensor_id=sensor_id).first()
		if name:
			sensor = VoltageSensor.query.filter_by(name=name).first()
		if scaling_factor_voltage:
			sensor = VoltageSensor.query.filter_by(scaling_factor_voltage=scaling_factor_voltage).first()
		return sensor

	@staticmethod
	def get_all():
		sensors = VoltageSensor.query.order_by(VoltageSensor.id.asc()).all()
		if not sensors:
			return []
		else:
			all_sensors = [sensor.to_dict() for sensor in sensors]
		return all_sensors

	@staticmethod
	def init_types():
		Sensor = namedtuple('Sensor', [
			'name', 'rating', 'default_voltage', 'scaling_factor_voltage'
			])
		sensors = [
			Sensor('Voltage Plugin', 415.0, 0.0, 1.87822),
			Sensor('Not Measured', 415.0, 240.0, 1)
		]
		for sensor in sensors:
			if not VoltageSensor.get_one(name=sensor.name):
				new_sensor = VoltageSensor.create(**dict(sensor._asdict()))
				db.session.add(new_sensor)
		db.session.commit()
		return VoltageSensor.get_all()

	def to_dict(self):
		response = {
			'name': self.name,
			'rating': self.rating,
			'sensor_id': self.sensor_id,
			'default_voltage': self.default_voltage,
			'scaling_factor_voltage': self.scaling_factor_voltage,
		}
		return response


class CurrentSensorRatings(Base):
	rating = db.Column(db.Float, unique=False, nullable=False)
	scaling_factor_current = db.Column(db.Float, unique=False, default=0.1432)
	rating_id = db.Column(db.String(100), unique=True, nullable=False)
	current_sensor_id = db.Column(db.String(100), db.ForeignKey('current_sensor.sensor_id'))
	channels = db.relationship('Channels', backref='current_sensor_rating', lazy='dynamic')

	@staticmethod
	def get_one(rating_id=None, scaling_factor_current=None):
		rating = None
		if rating:
			rating = CurrentSensorRatings.query.filter_by(rating_id=rating_id).first()
		if scaling_factor_current:
			rating = CurrentSensorRatings.query.filter_by(scaling_factor_current=scaling_factor_current).first()
		return rating

	def small_dict(self):
		response = {
			'rating_id': self.rating_id,
			'rating': self.rating,
			'scaling_factor_current': self.scaling_factor_current,
		}
		return response

	def to_dict(self):
		response = {
			'sensor_name': self.current_sensor.name if self.current_sensor else None,
			'sensor_id': self.current_sensor.sensor_id if self.current_sensor else None,
			'rating_id': self.rating_id,
			'rating': self.rating,
			'scaling_factor_current': self.scaling_factor_current,
		}
		return response


class CurrentSensor(Base):
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=True, nullable=False)
	sensor_id = db.Column(db.String(100), unique=True, nullable=False)
	ratings = db.relationship('CurrentSensorRatings', backref='current_sensor', lazy='dynamic')

	def __init__(self, name):
		self.name = name.title()
		self.sensor_id = str(uuid4())

	@staticmethod
	def create(name, rating, scaling_factor_current):
		name = name.title()
		new_sensor = CurrentSensor.get_one(name=name)
		if new_sensor:
			if new_sensor.ratings.filter_by(rating=rating).first():
				return new_sensor
		if not new_sensor:
			new_sensor = CurrentSensor(
				name=name.title(),
			)
		rating_id = str(uuid4())
		sensor_rating = CurrentSensorRatings(
			rating_id=rating_id,
			rating=rating,
			scaling_factor_current=scaling_factor_current
			)
		if sensor_rating not in new_sensor.ratings:
			new_sensor.ratings.append(sensor_rating)
		db.session.add(new_sensor)
		db.session.commit()
		db.session.refresh(new_sensor)
		return new_sensor

	def edit(self, name, rating_id, rating, scaling_factor_current):
		self.name = name.title()
		rating = self.ratings.filter_by(rating_id=rating_id).first()
		rating.rating = float(rating)
		rating.scaling_factor_current = float(scaling_factor_current)
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self

	@staticmethod
	def get_one(sensor_id=None, name=None, rating_id=None):
		sensor = None
		if rating_id:
			rating = CurrentSensorRatings.query.filter_by(rating_id=rating_id).first()
			if rating:
				sensor = rating.current_sensor
		if sensor_id:
			sensor = CurrentSensor.query.filter_by(sensor_id=sensor_id).first()
		if name:
			name = name.title()
			sensor = CurrentSensor.query.filter_by(name=name).first()
		return sensor

	@staticmethod
	def validate_one(name, rating):
		name = str(name).title()
		rating = float(rating)
		sensor = CurrentSensor.query.filter_by(name=name).join(CurrentSensorRatings).filter_by(rating=rating).first()
		return sensor

	@staticmethod
	def init_types():
		Sensor = namedtuple('Sensor', [
			'name', 'rating', 'scaling_factor_current'
			])
		sensors = [
			Sensor('Rogowski', 100.0, 0.34877),
			Sensor('Rogowski', 600.0, 0.52315),
			Sensor('Rogowski', 2000.0, 5.91856),
		]
		for sensor in sensors:
			new_sensor = CurrentSensor.create(**dict(sensor._asdict()))
			db.session.add(new_sensor)
		db.session.commit()
		return CurrentSensor.get_all()

	@staticmethod
	def get_all():
		sensors = CurrentSensor.query.order_by(CurrentSensor.id.asc()).all()
		if not sensors:
			return []
		else:
			all_sensors = [sensor.to_dict() for sensor in sensors]
		return all_sensors

	def to_dict(self):
		response = {
			'name': self.name,
			'sensor_id': self.sensor_id,
			'ratings': [rating.small_dict() for rating in self.ratings]
		}
		return response


class Channels(Base):
	"""docstring for Channels"""
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	channel_name = db.Column(db.String(100), unique=False, nullable=False)
	channel_id = db.Column(db.String(100), unique=True, nullable=False)
	channel_threshold_current = db.Column(db.Float, unique=False, default=1.0)
	power_factor = db.Column(db.Float, unique=False, default=0.90)
	current_sensor_rating_id = db.Column(db.String(100), db.ForeignKey('current_sensor_ratings.rating_id'))
	voltage_sensor_id = db.Column(db.String(100), db.ForeignKey('voltage_sensor.sensor_id'))
	probe_id = db.Column(db.String(100), db.ForeignKey('probe.probe_id'))
	configuration_id = db.Column(db.String(100), db.ForeignKey('configuration.configuration_id'))
	assigned_to_source = db.Column(db.Boolean, default=False)
	reserved = db.Column(db.Boolean, default=False)  # Used to ensure channels assigned to consumer lines
	# but not yet assigned to a consumer are not assigned to other sources.

	single_phase_grid = db.relationship('SinglePhaseGrid', uselist=False, backref='channel', cascade="all, delete-orphan")
	three_phase_grid_id = db.Column(db.String(100), db.ForeignKey('three_phase_grid.three_phase_grid_id'))
	single_phase_generator = db.relationship('SinglePhaseGenerator', uselist=False, backref='channel', cascade="all, delete-orphan")
	three_phase_generator_id = db.Column(db.String(100), db.ForeignKey('three_phase_generator.three_phase_generator_id'))

	single_phase_consumer_line = db.relationship('SinglePhaseConsumerLine', uselist=False, backref='channel', cascade="all, delete-orphan")
	three_phase_consumer_line_id = db.Column(db.String(100), db.ForeignKey('three_phase_consumer_line.line_id'))

	def __init__(self, channel_name, channel_threshold_current, power_factor, probe, current_sensor_id, voltage_sensor_id):
		self.channel_name = channel_name.replace(" ", "_").upper()
		self.channel_id = str(uuid4())
		self.channel_threshold_current = channel_threshold_current
		self.power_factor = power_factor
		self.voltage_sensor = VoltageSensor.get_one(sensor_id=voltage_sensor_id)
		self.current_sensor_rating = CurrentSensorRatings.get_one(rating_id=current_sensor_id)
		self.probe = probe

	@property
	def scaling_factor_current(self):
		return self.current_sensor_rating.scaling_factor_current if self.current_sensor_rating else None

	@property
	def scaling_factor_voltage(self):
		return self.voltage_sensor.scaling_factor_voltage if self.voltage_sensor else None

	@property
	def default_voltage(self):
		return self.voltage_sensor.default_voltage if self.voltage_sensor else None

	@staticmethod
	def get_channel(channel_id=None, channel_name=None):
		channel = None
		if channel_id:
			channel = Channels.query.filter_by(channel_id=channel_id).first()
		if channel_name:
			channel_name = channel_name.replace(" ", "_").upper()
			channel = Channels.query.filter_by(channel_name=channel_name).first()
		return channel

	@staticmethod
	def add_channel(channel_data):
		channel = Channels(**channel_data)
		db.session.add(channel)

	def edit_channel(self, channel_threshold_current, power_factor, current_sensor_id, voltage_sensor_id):
		self.channel_threshold_current = channel_threshold_current
		self.power_factor = power_factor
		self.voltage_sensor = VoltageSensor.get_one(sensor_id=voltage_sensor_id)
		self.current_sensor_rating = CurrentSensorRatings.get_one(rating_id=current_sensor_id)
		db.session.add(self)

	def unconfigured_dict(self):
		response = {
			'channel_name': self.channel_name.replace("_", " ").title(),
			'channel_id': self.channel_id,
			'configuration_id': self.configuration_id,
		}
		return response

	@staticmethod
	def get_assigned(channel_list):
		dupe = None
		print(f'Channel list {channel_list}')
		for channel_id in channel_list:
			channel = Channels.get_channel(channel_id=channel_id)
			if channel:
				if channel.assigned_to_source:
					dupe = channel.get_source()
					break
			continue
		return dupe

	def get_source(self):
		sources = [
			# CONSUMER
			'three_phase_consumer_line',

			# GRID
			'single_phase_grid',
			'three_phase_grid',

			# Generator
			'single_phase_generator',
			'three_phase_generator',

			# Single phase inverter
			'single_phase_input_channel',
			'single_phase_output_channel',

			# Three phase inverter
			'three_phase_input_channel1',
			'three_phase_input_channel2',
			'three_phase_input_channel3',

			'three_phase_output_channel1',
			'three_phase_output_channel2',
			'three_phase_output_channel3',
		]
		# Power source or consumer assigned to this channel
		power_source = None
		for source in sources:
			power_source_ = getattr(self, source)
			if power_source_:
				if isinstance(power_source_, list):
					power_source = [s.to_dict() for s in power_source_]
				else:
					power_source = [power_source_.to_dict()]
				break
		return power_source

	def get_source_obj(self):
		"""Get the source assigned to this channel. Each channel can only be assigned to a source at each point in time."""
		sources = [
			# CONSUMER
			'three_phase_consumer_line',

			# GRID
			'single_phase_grid',
			'three_phase_grid',

			# Generator
			'single_phase_generator',
			'three_phase_generator',

			# Single phase inverter
			'single_phase_input_channel',
			'single_phase_output_channel',

			# Three phase inverter
			'three_phase_input_channel1',
			'three_phase_input_channel2',
			'three_phase_input_channel3',

			'three_phase_output_channel1',
			'three_phase_output_channel2',
			'three_phase_output_channel3',
		]
		# Power source or consumer assigned to this channel
		power_source = None
		for source in sources:
			power_source = getattr(self, source)
			if power_source:
				if isinstance(power_source, list):
					power_source = power_source[0]
				break
		return power_source

	def source_config_dict(self):
		source = self.get_source_obj()
		if hasattr(source, 'device_config_dict'):
			return source.device_config_dict()
		return {}

	def get_source_direction(self, source):
		"""Get if the channl is an input channel or an output channel."""
		source_attrs = [
			# Single phase inverter
			'input_channel_id',
			'output_channel_id',

			# Three phase inverter
			'input_channel1_id',
			'input_channel2_id',
			'input_channel3_id',

			'output_channel1_id',
			'output_channel2_id',
			'output_channel3_id',
		]
		direction = ''
		for attr in source_attrs:
			if hasattr(source, attr):
				channel_id = getattr(source, attr)
				if self.channel_id == channel_id:
					direction = self.get_direction(attr)
				# break
		return direction

	def get_direction(self, attr):
		attr_set = set(attr.split('_'))
		if 'input' in attr_set:
			return 'IN'
		elif 'output' in attr_set:
			return 'OUT'
		else:
			return ''

	def assign_to_source(self):
		self.assigned_to_source = True
		self.reserved = True
		db.session.add(self)
		return self

	def remove_from_source(self):
		self.assigned_to_source = False
		self.reserved = False
		db.session.add(self)
		return self

	def reserve(self):
		self.reserved = True
		db.session.add(self)
		return self

	def release(self):
		self.reserved = False
		db.session.add(self)
		return self

	def small_dict(self):
		response = {
			'channel_name': self.channel_name.replace("_", " ").title(),
			'channel_id': self.channel_id,
			'probe_name': self.probe.probe_name,
			'probe_id': self.probe.probe_id,
			# 'configuration_name': Configuration.query.filter_by(configuration_id=self.configuration_id).first().configuration_name,
			'configuration_id': self.configuration_id,
			'reserved': self.reserved,
			'assigned_to_source': self.assigned_to_source,
		}
		return response

	def migration_dict(self):
		response = {
			'channel_name': self.channel_name.replace("_", " ").title(),
			'channel_id': self.channel_id,
			'probe_name': self.probe.probe_name,
			'probe_id': self.probe.probe_id,
			# 'configuration_name': Configuration.query.filter_by(configuration_id=self.configuration_id).first().configuration_name,
			'configuration_id': self.configuration_id,
			'reserved': self.reserved,
			'assigned_to_source': self.assigned_to_source,
			'source': self.get_source(),
		}
		return response

	def to_dict(self):
		response = {
			'id': self.id,
			'channel_name': self.channel_name.replace("_", " ").title(),
			'channel_id': self.channel_id,
			'probe_id': self.probe.probe_id,
			'reserved': self.reserved,
			'assigned_to_source': self.assigned_to_source,
			'channel_threshold_current': self.channel_threshold_current,
			'power_factor': self.power_factor,
			'scaling_factor_voltage': self.scaling_factor_voltage,
			'scaling_factor_current': self.scaling_factor_current,
			'default_voltage': self.default_voltage,
			'configuration_id': self.configuration_id,
			'voltage_sensor': self.voltage_sensor.to_dict() if self.voltage_sensor else {},
			'current_sensor': self.current_sensor_rating.to_dict() if self.current_sensor_rating else {},
			'source': self.get_source(),
		}
		return response

	def device_config_dict(self, source_channel_id):
		source = self.get_source_obj()
		response = {
			'ScalingFactorCurrent': self.scaling_factor_current,
			'phase': source.phase_type.num_id if source else '',
			'SourceID_FK': source.to_dict()['meter_id'] if source else '',
			'DefaultVoltage': self.default_voltage,
			'SensorID_FK': [self.voltage_sensor.sensor_id, self.current_sensor.sensor_id],
			'PowerFactor': self.power_factor,
			'ChannelThreshold': self.channel_threshold_current,
			'SourceName': source.name if source else '',
			'ConfigID_FK': self.configuration_id,
			'SourceType': source.SOURCE_TYPE.lower().split(' ')[-1] if source else '',
			'ScalingFactorVoltage': self.scaling_factor_current,
			'sourceChannel': source_channel_id,
			'Direction': self.get_source_direction(source),
		}
		return response


probe_configurations = db.Table('probe_configurations',
	db.Column('probe_id', db.String(100), db.ForeignKey('probe.probe_id')),
	db.Column('configuration_id', db.String(100), db.ForeignKey('configuration.configuration_id')),
)


class Probe(Base, KafkaMixin):
	"""docstring for Probe"""
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	probe_name = db.Column(db.String(100), unique=True, nullable=False)
	probe_id = db.Column(db.String(100), unique=True, nullable=False)
	wifi_network_name = db.Column(db.String(50), unique=False, nullable=True)
	wifi_password = db.Column(db.String(50), unique=False, nullable=True)
	device_type_id = db.Column(db.Integer, db.ForeignKey('device_type.id'))
	channels = db.relationship('Channels', backref='probe', lazy='dynamic', cascade="all, delete-orphan")
	configurations = db.relationship('Configuration', secondary='probe_configurations',
		backref=db.backref('probes', lazy='dynamic'))

	# Relationships between a probe and the power sources that can be connected to it.
	single_phase_grids = db.relationship('SinglePhaseGrid', backref='probe', lazy='dynamic', cascade="all, delete-orphan")
	three_phase_grids = db.relationship('ThreePhaseGrid', backref='probe', lazy='dynamic', cascade="all, delete-orphan")
	single_phase_generators = db.relationship('SinglePhaseGenerator', backref='probe', lazy='dynamic', cascade="all, delete-orphan")
	three_phase_generators = db.relationship('ThreePhaseGenerator', backref='probe', lazy='dynamic', cascade="all, delete-orphan")

	deleted = db.Column(db.Boolean, default=False)

	def __init__(self, probe_name, wifi_network_name, wifi_password, device_type_id):
		self.probe_name = probe_name.upper()
		self.probe_id = str(uuid4())
		self.wifi_network_name = wifi_network_name
		self.wifi_password = wifi_password
		self.device_type = DeviceType.get_device_type(device_type_id)

	@staticmethod
	def get_one(probe_id=None, probe_name=None):
		probe = None
		if probe_id:
			probe = Probe.query.filter_by(probe_id=probe_id).first()
		elif probe_name:
			probe_name = probe_name.upper()
			probe = Probe.query.filter_by(probe_name=probe_name).first()
		return probe

	@staticmethod
	def new_probe(probe_name, wifi_network_name, wifi_password, device_type_id, channels, obj=False):
		probe = Probe(
			probe_name=probe_name,
			wifi_network_name=wifi_network_name,
			wifi_password=wifi_password,
			device_type_id=device_type_id,
		)
		db.session.add(probe)
		number_of_channels = 8
		if probe.device_type:
			number_of_channels = probe.device_type.number_of_channels
		channels = channels[0]
		channels['probe'] = probe
		for channel in range(number_of_channels):
			channels['channel_name'] = f'Channel-{channel}'
			Channels.add_channel(channels)
		db.session.commit()
		db.session.refresh(probe)
		if obj:
			return probe
		return probe.to_dict()

	@staticmethod
	def new_migrated_probe(probe_name, probe_id, wifi_network_name, wifi_password, device_type_id, channels, obj=False):
		probe = Probe(
			probe_name=probe_name,
			wifi_network_name=wifi_network_name,
			wifi_password=wifi_password,
			device_type_id=device_type_id,
		)
		probe.probe_id = probe_id
		db.session.add(probe)
		number_of_channels = 8
		if probe.device_type:
			number_of_channels = probe.device_type.number_of_channels
		channels = channels[0]
		channels['probe'] = probe
		for channel in range(number_of_channels):
			channels['channel_name'] = f'Channel-{channel}'
			Channels.add_channel(channels)
		db.session.commit()
		db.session.refresh(probe)
		if obj:
			return probe
		return probe.to_dict()

	@staticmethod
	def get_probe(probe_id=None):
		probe = None
		if probe_id:
			probe = Probe.query.filter_by(probe_id=probe_id).first()
		return  probe

	def get_unassigned_channel_ids(self):
		return [channel.channel_id for channel in self.channels.filter_by(assigned_to_source=False).order_by(Channels.channel_name.asc()).all()]

	def edit_probe(self, probe_name, wifi_network_name, wifi_password, channels):
		self.probe_name = probe_name
		self.wifi_network_name = wifi_network_name
		self.wifi_password = wifi_password
		db.session.add(self)
		# channels = (channels[0])._asdict()
		for channel in channels:
			channel_dict = channel._asdict()
			channel_id = channel_dict['channel_id']
			del channel_dict['channel_id']
			probe_channel = self.channels.filter_by(channel_id=channel_id).first()
			if not probe_channel:
				continue
			probe_channel.edit_channel(**channel_dict)

		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	@staticmethod
	def get_all():
		page = request.args.get('page', 1, type=int)
		size = request.args.get('size', 20, type=int)
		probes = Probe.query.order_by(Probe.id.asc()).paginate(page, size, False)
		if len(probes.items) < 1:
			return [], False, False
		else:
			all_probes = [probe.to_dict() for probe in probes.items]
		return all_probes, probes.has_prev, probes.has_next

	def add_config_to_channels(self, channel_ids, configuration):
		for channel_id in channel_ids:
			channel = self.channels.filter_by(channel_id=channel_id).first()
			channel.configuration = configuration
			db.session.add(channel)

	def unconfigured_channels(self):
		return [channel.unconfigured_dict() for channel in self.channels.filter(Channels.configuration_id == None).order_by(Channels.id.asc()).all()]

	def unconfigured_dict(self):
		response = {
			'probe_name': self.probe_name,
			'probe_id': self.probe_id,
			'unconfigured_channels': self.unconfigured_channels(),
		}
		return response

	def small_dict(self):
		response = {
			'probe_name': self.probe_name,
			'probe_id': self.probe_id,
		}
		return response

	def get_data(self):
		return self.to_dict()

	def channel_dict(self):
		response = {
			'id': self.id,
			'probe_name': self.probe_name,
			'probe_id': self.probe_id,
			'channels': [channel.small_dict() for channel in self.channels.order_by(Channels.id.asc())],
		}
		return response

	def migration_dict(self):
		response = {
			'id': self.id,
			'probe_name': self.probe_name,
			'probe_id': self.probe_id,
			'device_type': self.device_type.shallow_dict() if self.device_type else '',
			'wifi_network_name': self.wifi_network_name,
			'wifi_password': self.wifi_password,
			'channels': [channel.migration_dict() for channel in self.channels.order_by(Channels.id.asc())],
		}
		return response

	def to_dict(self):
		response = {
			'id': self.id,
			'probe_name': self.probe_name,
			'probe_id': self.probe_id,
			'wifi_network_name': self.wifi_network_name,
			'wifi_password': self.wifi_password,
			'device_type': self.device_type.shallow_dict() if self.device_type else '',
			'channels': [channel.to_dict() for channel in self.channels.order_by(Channels.id.asc())],
			'configurations': [configuration.to_dict() for configuration in self.configurations],
		}
		return response


member_configurations = db.Table('member_configurations',
	db.Column('configuration_id', db.String(100), db.ForeignKey('configuration.configuration_id')),
	db.Column('member_id', db.String(100), db.ForeignKey('configuration_member.member_id')),
	db.PrimaryKeyConstraint('configuration_id', 'member_id'),
)


class Configuration(Base):
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	configuration_name = db.Column(db.String(50), unique=False, nullable=False)
	configuration_id = db.Column(db.String(100), unique=True, nullable=False)
	error_threshold = db.Column(db.String(50), unique=False, nullable=True)
	display_energy_today = db.Column(db.Boolean, default=True)
	channels = db.relationship('Channels', backref='configuration', lazy='dynamic')
	configuration_user_id = db.Column(db.String(50), unique=False, nullable=True)
	configuration_user_name = db.Column(db.String(50), unique=False, nullable=True)
	configuration_members = db.relationship('ConfigurationMember', secondary='member_configurations',
		backref=db.backref('configurations', lazy='dynamic'))
	configuration_group_id = db.Column(db.String(100), db.ForeignKey('configuration_group.group_id'))

	single_phase_grids = db.relationship('SinglePhaseGrid', backref='configuration', lazy='dynamic', cascade="all, delete-orphan")
	three_phase_grids = db.relationship('ThreePhaseGrid', backref='configuration', lazy='dynamic', cascade="all, delete-orphan")

	single_phase_generators = db.relationship('SinglePhaseGenerator', backref='configuration', lazy='dynamic', cascade="all, delete-orphan")
	three_phase_generators = db.relationship('ThreePhaseGenerator', backref='configuration', lazy='dynamic', cascade="all, delete-orphan")

	single_phase_inverters = db.relationship('SinglePhaseInverter', backref='configuration', lazy='dynamic', cascade="all, delete-orphan")
	three_phase_inverters = db.relationship('ThreePhaseInverter', backref='configuration', lazy='dynamic', cascade="all, delete-orphan")

	consumer_groups = db.relationship('ConsumerGroup', backref='configuration', lazy='dynamic', cascade="all, delete-orphan")

	def __init__(self, configuration_name, error_threshold, display_energy_today, configuration_user_id, configuration_user_name):
		self.configuration_name = configuration_name.replace("_", " ").title()
		self.configuration_id = str(uuid4())
		self.error_threshold = error_threshold
		self.display_energy_today = display_energy_today
		self.configuration_user_id = configuration_user_id
		self.configuration_user_name = configuration_user_name.title()

	@staticmethod
	def get_user_config(configuration_user_id, configuration_name=None, configuration_id=None):
		configuration = None
		if not any([configuration_id, configuration_name]):
			return configuration
		if configuration_id:
			return Configuration.query.filter_by(
				configuration_id=configuration_id
				).filter_by(configuration_user_id=configuration_user_id).first()

		if configuration_name:
			configuration_name = configuration_name.replace("_", " ").title()
			return Configuration.query.filter_by(
				configuration_name=configuration_name).filter_by(
					configuration_user_id=configuration_user_id).first()
		return configuration

	# @staticmethod
	# def get_user(user_id):
	# 	configuration = Configuration.query.filter_by(configuration_user_id=user_id).first()
	# 	return configuration

	@staticmethod
	def get_user_configurations(user_id):
		configurations = Configuration.user_configurations(user_id)
		if len(configurations.items) < 1:
			return [], False, False
		else:
			all_configurations = [configuration.to_dict() for configuration in configurations.items]
		return all_configurations, configurations.has_prev, configurations.has_next

	@staticmethod
	def get_configuration_ids(user_id):
		return [configuration.configuration_id for configuration in Configuration.user_configurations(user_id, False)]

	@staticmethod
	def user_configurations(user_id, paginate=True):
		page = request.args.get('page', 1, type=int)
		size = request.args.get('size', 20, type=int)
		query = Configuration.query\
				.join(Configuration.configuration_members)\
				.filter(or_((Configuration.configuration_user_id == user_id), (ConfigurationMember.member_id == user_id)))\
				.order_by(Configuration.date_created.desc())
		if paginate:
			return query.paginate(page, size, False)
		else:
			return query.all()

	@staticmethod
	def all_user_probes(user_id):
		configurations = Configuration.user_configurations(user_id)
		probes = [configuration.probes.all() for configuration in configurations.items if configuration]
		probes = list(chain(*probes))
		probes = [probe.channel_dict() for probe in probes if probe]
		return probes, configurations.has_prev, configurations.has_next

	@staticmethod
	def all_configurations(configuration_id):
		page = request.args.get('page', 1, type=int)
		size = request.args.get('size', 20, type=int)
		configurations = Configuration.query.filter_by(configuration_id=configuration_id)\
			.order_by(Configuration.date_created.desc())\
			.paginate(page, size, False)
		return configurations

	@staticmethod
	def all_config_probes(configuration_id):
		configurations = Configuration.all_configurations(configuration_id)
		probes = [configuration.probes.all() for configuration in configurations.items]
		probes = list(chain(*probes))
		probes = [probe.channel_dict() for probe in probes]
		return probes, configurations.has_prev, configurations.has_next

	@staticmethod
	def get_configuration(configuration_id=None):
		configuration = Configuration.query.filter_by(configuration_id=configuration_id).first()
		return configuration

	@staticmethod
	def demo():
		configuration = Configuration.get_configuration(configuration_id='HOI2M31GEUP1JSFPAY3VUHUL43GXMEUJ')
		return configuration

	@staticmethod
	def new_configuration(configuration_name, configuration_user, error_threshold, display_energy_today, configuration_members, probe_channels):
		configuration_user_id = configuration_user['configuration_user_id']
		configuration_user_name = configuration_user['configuration_user_name']

		new_configuration = Configuration(
			configuration_name=configuration_name,
			error_threshold=error_threshold,
			display_energy_today=display_energy_today,
			configuration_user_id=configuration_user_id,
			configuration_user_name=configuration_user_name,
			)
		# db.session.add(new_configuration)
		new_configuration.add_configuration_users(configuration_members)
		new_configuration.add_probe_channels(probe_channels)
		db.session.add(new_configuration)
		db.session.commit()
		db.session.refresh(new_configuration)
		return new_configuration.to_dict()

	@staticmethod
	def migration_config(configuration_id, configuration_name, configuration_user, error_threshold, display_energy_today, configuration_members, probe_channels):
		config = Configuration.get_configuration(configuration_id)
		if config:
			return config.to_dict()
		configuration_user_id = configuration_user['configuration_user_id']
		configuration_user_name = configuration_user['configuration_user_name']

		new_configuration = Configuration(
			configuration_name=configuration_name,
			error_threshold=error_threshold,
			display_energy_today=display_energy_today,
			configuration_user_id=configuration_user_id,
			configuration_user_name=configuration_user_name,
			)
		new_configuration.configuration_id = configuration_id
		# db.session.add(new_configuration)
		new_configuration.add_configuration_users(configuration_members)
		for probe_channel in probe_channels:
			new_configuration.migrate_probe_channels(probe_channel)
		db.session.add(new_configuration)
		db.session.commit()
		db.session.refresh(new_configuration)
		return new_configuration.to_dict()

	def edit_configuration(self, configuration_name, configuration_user, error_threshold, display_energy_today, configuration_members, probe_channels):
		self.configuration_name = configuration_name.replace("_", " ").title()
		self.configuration_user_id = configuration_user['configuration_user_id']
		self.configuration_user_name = configuration_user['configuration_user_name']
		self.error_threshold = error_threshold
		self.display_energy_today = display_energy_today
		self.edit_configuration_members(configuration_members)
		self.edit_probe_channels(probe_channels)
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	@staticmethod
	def member_ids_from_dict(configuration_members):
		# this can be implemented as a function.
		return {config_member['member_id'] for config_member in configuration_members}

	def edit_configuration_members(self, configuration_members):
		old_config_members = self.configuration_members
		for member in old_config_members:
			if member.member_id not in Configuration.member_ids_from_dict(configuration_members):
				if self in set(member.configurations):
					member.configurations.remove(self)
					db.session.add(member)

		for config_member in configuration_members:
			member_id = config_member['member_id']
			member_name = config_member['member_name']
			configuration_member = ConfigurationMember.get_member(member_id=member_id)
			if not configuration_member:
				configuration_member = ConfigurationMember(
					member_id=member_id,
					member_name=member_name.title(),
				)
			if not self in configuration_member.configurations:
				configuration_member.configurations.append(self)
				db.session.add(configuration_member)

	def add_configuration_users(self, configuration_members):
		for config_member in configuration_members:
			member_id = config_member['member_id']
			member_name = config_member['member_name']
			configuration_member = ConfigurationMember.get_member(member_id=member_id)
			if not configuration_member:
				configuration_member = ConfigurationMember(
					member_id=member_id,
					member_name=member_name.title(),
				)
			# db.session.add(configuration_member)
			if not self in configuration_member.configurations:
				configuration_member.configurations.append(self)
				db.session.add(configuration_member)

	@staticmethod
	def channel_ids_from_dict(probe_channels):
		# this can be implemented as a function.
		return {channel_id for probe_channel in probe_channels for channel_id in probe_channel['channel_ids']}

	def edit_probe_channels(self, probe_channels):
		added_channel_ids = {}
		deprecated_channel_ids = {}
		old_channel_ids = set([channel.channel_id for channel in self.channels.all()])
		new_channel_ids = Configuration.channel_ids_from_dict(probe_channels)
		if old_channel_ids != new_channel_ids:
			added_channel_ids = new_channel_ids.difference(old_channel_ids)
			deprecated_channel_ids = old_channel_ids.difference(new_channel_ids)

		if added_channel_ids:
			for channel_id in list(added_channel_ids):
				channel = Channels.get_channel(channel_id=channel_id)
				if channel not in self.channels:
					self.channels.append(channel)
					db.session.add(self)
		if deprecated_channel_ids:
			for channel_id in list(deprecated_channel_ids):
				channel = Channels.get_channel(channel_id=channel_id)
				self.channels.remove(channel)
				db.session.add(self)

	def add_probe_channels(self, probe_channels):
		for probe_channel in probe_channels:
			probe_id = probe_channel['probe_id']
			channel_ids = probe_channel['channel_ids']
			probe = Probe.get_one(probe_id=probe_id)
			probe.add_config_to_channels(channel_ids, self)
			if not probe in self.probes:
				self.probes.append(probe)

	def migrate_probe_channels(self, probe_channel):
		# for probe_channel in probe_channels:
		probe_name = probe_channel['probe_name']
		# channel_ids = probe_channel['channel_ids']
		probe = Probe.get_one(probe_name=probe_name)
		new_probe = None
		if not probe:
			print('creating new probe')
			new_probe = Probe.new_migrated_probe(**probe_channel, obj=True)
			channel_ids = [channel.channel_id for channel in new_probe.channels.all()]
			new_probe.add_config_to_channels(channel_ids, self)
		else:
			print(probe.to_dict())
		if new_probe:
			if not new_probe in self.probes:
				self.probes.append(new_probe)

	@staticmethod
	def get_all():
		# todo: paginate this
		page = request.args.get('page', 1, type=int)
		size = request.args.get('size', 20, type=int)
		configurations = Configuration.query.order_by(Configuration.date_created.desc()).paginate(page, size, False)
		if len(configurations.items) < 1:
			return [], False, False
		else:
			all_configurations = [configuration.to_dict() for configuration in configurations.items]
		return all_configurations, configurations.has_prev, configurations.has_next

	@staticmethod
	def unconfigured_probes():
		# !USE PAGINATION FOR THIS QUERY
		# !Ensure to paginate probes not channels
		logger.warning("Still querying without pagination")
		unconfigured = Probe.query.join(Channels).filter(Channels.configuration_id == None).all()
		unconfigured = [probe.unconfigured_dict() for probe in unconfigured]
		return unconfigured

	@staticmethod
	def all_user_power_sources(user_id):
		configurations = Configuration.user_configurations(user_id).items
		grids = [configuration.grids() for configuration in configurations]
		grids = [item for sublist in grids for item in sublist]
		generators = [configuration.generators() for configuration in configurations]
		generators = [item for sublist in generators for item in sublist]
		inverters = [configuration.inverters() for configuration in configurations]
		inverters = [item for sublist in inverters for item in sublist]
		power_sources_ = [grids, generators, inverters]
		power_sources_ = list(chain(*power_sources_))
		count = len([power_source for power_source in power_sources_ if power_source])
		return grids, generators, inverters, count

	@staticmethod
	def all_user_consumers(user_id):
		configurations = Configuration.user_configurations(user_id).items
		consumers = [configuration.consumers() for configuration in configurations]
		return list(chain(*consumers))

	def grids(self):
		single_phase_grids = [grid.to_dict() for grid in self.single_phase_grids]
		three_phase_grids = [grid.to_dict() for grid in self.three_phase_grids]
		attached_grids = [single_phase_grids, three_phase_grids]
		return list(chain(*attached_grids))

	def generators(self):
		return [generator.to_dict() for generator in self.generator_objects()]

	def generator_objects(self):
		single_phase_generators = [generator for generator in self.single_phase_generators]
		three_phase_generators = [generator for generator in self.three_phase_generators]
		attached_generators = [single_phase_generators, three_phase_generators]
		return list(chain(*attached_generators))

	def inverters(self):
		single_phase_inverters = [inverter.to_dict() for inverter in self.single_phase_inverters]
		three_phase_inverters = [inverter.to_dict() for inverter in self.three_phase_inverters]
		attached_inverters = [single_phase_inverters, three_phase_inverters]
		return list(chain(*attached_inverters))

	def consumers(self):
		consumers = [consumer_group.get_consumers() for consumer_group in self.consumer_groups]
		consumers = [consumer for consumer in list(chain(*consumers)) if consumer]
		return consumers

	@staticmethod
	def get_user_fuel(user_id):
		configurations = Configuration.user_configurations(user_id).items
		generators_unflattened = [configuration.generator_objects() for configuration in configurations]
		generators = chain(*generators_unflattened)
		fuel = [generator.fuel.to_dict() for generator in generators if generator.fuel]
		return fuel

	def config_address(self):
		response = {
			"city":"",
			"latitude":"30",
			"zip":"",
			"state":"",
			"country":"",
			"longitude":"30",
			"_id":"ULRA34WVY421C4TMZMKQARIGHNUBEJL3",
			"postalcode":0,
			"UserID_FK":"",
			"street":"",
			"subnet":"",
			"zone":""
		}
		return response

	def config_geo_point(self):
		response = {
			"lat":0.0,
			"long":0.0
		}
		return response

	def device_config_dict(self):
		response = {
			'DisplayEnergyToday': self.display_energy_today,
			'GroupName': self.configuration_group.name if self.configuration_group else '',
			'ConfigGroupID_FK': self.configuration_group_id,
			'ErrorThreshold': self.error_threshold,
			'AdminID_FK': self.configuration_user_id,
			'_id': self.configuration_id,
			'UserID_FK': self.configuration_user_id,
			'ConfigAddress': {},
			'GeoPoint': {},
			'configurationName': self.configuration_name,
			'UserID_FKs': [member.member_id  for member in self.configuration_members],
			'configurationUser': self.configuration_user_name,
		}
		return response

	def to_dict(self):
		json_prosumer = {
			'configuration_name': self.configuration_name,
			'configuration_id': self.configuration_id,
			'error_threshold': self.error_threshold,
			'display_energy_today': self.display_energy_today,
			'probe_channels': [probe.channel_dict() for probe in self.probes.all()],
			'admin_id': self.configuration_user_id,
			'admin_name': self.configuration_user_name,
			'configuration_members': [member.to_dict() for member in self.configuration_members],
			'date_created': str(self.date_created),
		}
		return json_prosumer

	def migration_dict(self):
		json_prosumer = {
			'configuration_name': self.configuration_name,
			'configuration_id': self.configuration_id,
			'error_threshold': self.error_threshold,
			'display_energy_today': self.display_energy_today,
			'probe_channels': [probe.migration_dict() for probe in self.probes.all()],
			'admin_id': self.configuration_user_id,
			'admin_name': self.configuration_user_name,
			'configuration_members': [member.to_dict() for member in self.configuration_members],
			'date_created': str(self.date_created),
		}
		return json_prosumer

	def small_dict(self):
		json_prosumer = {
			'configuration_name': self.configuration_name,
			'configuration_id': self.configuration_id,
			'configuration_members': [member.to_dict() for member in self.configuration_members],
		}
		return json_prosumer


class ConfigurationMember(Base):
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	member_id = db.Column(db.String(50), unique=True, nullable=False)
	member_name = db.Column(db.String(50), unique=False, nullable=False)

	@staticmethod
	def get_member(member_id):
		member = ConfigurationMember.query.filter_by(member_id=member_id).first()
		return member

	def to_dict(self):
		json_prosumer = {
			'member_id': self.member_id,
			'member_name': self.member_name,
		}
		return json_prosumer


class ConfigurationGroup(Base):
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	group_id = db.Column(db.String(50), unique=True, nullable=False)
	name = db.Column(db.String(50), unique=False, nullable=False)
	admin_id = db.Column(db.String(100), unique=False, nullable=False)
	source_config_id = db.Column(db.String(100), unique=False, nullable=False)
	profit_margin = db.Column(db.Float, unique=False, nullable=False)
	configurations = db.relationship('Configuration', backref='configuration_group', lazy='dynamic')

	def __init__(self, name, admin_id, source_config_id, profit_margin):
		self.name = name.replace(" ", "_").upper()
		self.admin_id = admin_id
		self.source_config_id = source_config_id
		self.profit_margin = profit_margin
		self.group_id = str(uuid4())

	@staticmethod
	def get_admin_config(admin_id, name):
		name = name.replace(" ", "_").upper()
		group = ConfigurationGroup.query.filter_by(admin_id=admin_id
		).filter_by(name=name).first()
		return group

	@staticmethod
	def get_configuration_group(configuration_group_id=None):
		group = ConfigurationGroup.query.filter_by(group_id=configuration_group_id).first()
		return group

	@staticmethod
	def new_configuration_group(name, admin_id, source_config_id, profit_margin, group_members):
		config_group = ConfigurationGroup(
			name=name,
			admin_id=admin_id,
			profit_margin=profit_margin,
			source_config_id=source_config_id,
		)
		db.session.add(config_group)
		for config_id in group_members:
			config = Configuration.get_configuration(configuration_id=config_id)
			if config and config not in config_group.configurations:
				config_group.configurations.append(config)
				db.session.add(config_group)
		db.session.commit()
		db.session.refresh(config_group)
		return config_group.to_dict()

	def edit_configuration_group(self, name, admin_id, source_config_id, profit_margin, group_members):
		self.name = name.replace(" ", "_").upper()
		self.admin_id = admin_id
		self.profit_margin = profit_margin
		self.source_config_id = source_config_id
		self.edit_members(group_members)
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	@staticmethod
	def get_member_ids(config_group):
		return {member.configuration_id for member in config_group.configurations.all()}

	def edit_members(self, new_member_ids):
		old_member_ids = ConfigurationGroup.get_member_ids(self)
		new_member_ids = set(new_member_ids)
		if not old_member_ids == new_member_ids:
			deprecated_members = old_member_ids.difference(new_member_ids)
			new_members = new_member_ids.difference(old_member_ids)
			for member_id in list(deprecated_members):
				config = Configuration.get_configuration(configuration_id=member_id)
				if config and config in self.configurations:
					self.configurations.remove(config)
					db.session.add(self)

			for member_id in list(new_members):
				config = Configuration.get_configuration(configuration_id=member_id)
				if config and config not in self.configurations:
					self.configurations.append(config)
					db.session.add(self)

	@staticmethod
	def get_all():
		page = request.args.get('page', 1, type=int)
		size = request.args.get('size', 20, type=int)
		configurations = ConfigurationGroup.query.order_by(ConfigurationGroup.date_created.desc()).paginate(page, size, False)
		if len(configurations.items) < 1:
			return [], False, False
		else:
			all_configurations = [configuration.to_dict() for configuration in configurations.items]
		return all_configurations, configurations.has_prev, configurations.has_next

	def to_dict(self):
		admin = Configuration.get_user(self.admin_id)
		response = {
			'name': self.name.replace("_", " "),
			'group_id': self.group_id,
			'admin_id': admin.configuration_user_id,
			'admin_name': admin.configuration_user_name,
			'profit_margin': self.profit_margin,
			'source_config': Configuration.get_configuration(self.source_config_id).small_dict(),
			'configurations': [config.small_dict() for config in self.configurations.all()],
		}
		return response


# db.event.listen(db.session, 'before_commit', KafkaMixin.before_commit)
# db.event.listen(db.session, 'after_commit', KafkaMixin.after_commit)
