
import os
from pprint import pprint

from bson.objectid import ObjectId
from pymongo import MongoClient

client = MongoClient(os.getenv('MONGO_DB_URL'))
db = client['lift-mongo-app']
configurations = db['configurations']
probes = db['probes']
powersources = db['powersources']
probes = db['probes']
users = db['user.users']

# workshop_config_id = 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'

configuration_ids = [
						'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF', #GRIT

						'HOI2M31GEUP1JSFPAY3VUHUL43GXMEUJ', #yoda
						'BWCTHCCB1B5V3AORTYVMEOB2CSW05UON', #PowerCell2
						'HXRNJGXKWKRJFPQL3RV3FBIIG5E3VOXN', #PowerCell

						'MQJQ1HWS03IEI3310HH2NX0MGBT3BKIJ',  #rensource
						'0J55UD1FFOSZEN4T0BBEAASKUJXIPLDO',  #rensource

						'RXT1FGDSKPW3CONVCF1PIQDG4HO5T4O1', #M_OKUEYUNGBO
						'5THGUJEDJQDVGPAJ2WBMUGGM32XLCIC3', #M_OKUEYUNGBO
						'YVOYQHOBIR2MYFK3FIER1LFHJJNGCD2L', #CCHUB
						'VPBTAQDRMAPLQVQNSC5C3SAEE3CEVYF5', #Chicken Republic
						'TOPZAPSG1WUHWPWNYCMWIV1PQQ5NBL0D', #GE LAGOS GARAGE
						'C4IEZZ0QPZIBGCNT0GHSYLEXDMQKU3DF', #sbe2
						'IQN25YSXDUFD2B0EXI410OYPE3JW21PA', #vanpeux

						'3Y5YFOYIA3KQKBZYVUB0EOM2JUC0YIK0',  #gbenga 71
						'5EMQQLHRGINFIPSCANYNF2GSAP2YPMEH',  #gbenga 71
						'5QI5UOII1SE02OFWSDR2RY0RJGW3W424',  #gbenga 71
						'EAVYYLRIDZ4RGSLNIKE5LKLCOACK00XO',  #gbenga 71
						'ER5K3SJPK0R21RKDAOYZLO5VNSBXDYMJ',  #gbenga 71
						'JIQ0T035KSK2KVQH0KHA40P3ONNPB1VK',  #gbenga 71
						'LBPD5JWJ0531ZOTJNCZBIDSOUQWHRTVS',  #gbenga 71
						'LF3ETY53W0M4SPYSLX4M2MW2CCMFD1OY',  #gbenga 71
						'LL3AFB2VCPOPTPO51CRY21BP0AN2OGCL',  #gbenga 71
						'MBOJFZRAPZC3VSD4QZYAA1APHL50BCG3',  #gbenga 71
						'P44JJ3QSGNIG1PUSINTRT0ON3TGSTIHA',  #gbenga 71
						'5THGUJEDJQDVGPAJ2WBMUGGM32XLCIC3',  #gbenga 71
					]
# configuration_ids = [
# 						'ER5K3SJPK0R21RKDAOYZLO5VNSBXDYMJ', #gbenga 71
# 					]
# workshop = configurations.find_one({'_id': workshop_config_id})
# workshop_probes = probes.find({'ConfigID_FK': workshop_config_id})
# pprint(workshop_probes)
# all_probes = [probe for probe in workshop_probes]
# pprint(all_probes[0])
def get_user(user_id):
	return users.find_one({'_id': ObjectId(user_id)})

def get_power_sources(config_id=None, _id=None):
	if config_id:
		return powersources.find({'ConfigID_FK': config_id})
	return powersources.find({'_id': _id})

def config_member(user_dict):
	response = {
		'member_id': str(user_dict['_id']),
		'member_name': user_dict['username']
	}
	return response

def all_users(user_list):
	return[config_member(get_user(user_id)) for user_id in user_list]

def get_channel_list(channel_list):
	return [channel_props(channel) for channel in channel_list]

def get_probe_info(probe):
	channel_list = probe['Channels']
	probe_dict = {
		'probe_id': probe['_id'],
		'probe_name': probe['probeName'],
		'wifi_network_name': probe['wifi_ssid'],
		'wifi_password': probe['wifi_password'],
		'device_type_id': '',
		'channels': get_channel_list(channel_list),
	}
	return probe_dict

def get_current_sensor(scaling_factor_current):
	from api.models import CurrentSensorRatings
	sensor = CurrentSensorRatings.get_one(scaling_factor_current=scaling_factor_current)
	if sensor:
		return sensor.rating_id
	else:
		return None

def get_voltage_sensor(scaling_factor_voltage):
	from api.models import VoltageSensor
	sensor = VoltageSensor.get_one(scaling_factor_voltage=scaling_factor_voltage)
	if sensor:
		return sensor.sensor_id
	else:
		return None

def channel_props(channel):
	channel_dict = {
		'channel_threshold_current': channel['ChannelThreshold'],
		'power_factor': channel['PowerFactor'],
		'voltage_sensor_id': get_voltage_sensor(channel['ScalingFactorVoltage']),
		'current_sensor_id': get_current_sensor(channel['ScalingFactorCurrent']),
	}
	return channel_dict

# print(dir(workshop_probes))
# pprint([c for c in workshop_probes][0])

def multiple_probes(probe_cursor):
	return [get_probe_info(probe) for probe in probe_cursor]

def do_config_migration():
	migration_list = []
	from api.models import Configuration
	for configuration_id in configuration_ids:
		config = Configuration.get_configuration(configuration_id=configuration_id)
		if config:
			continue
		configuration_ = configurations.find_one({'_id': configuration_id})
		probes_ = probes.find({'Channels': {'$elemMatch': {'ConfigID_FK': configuration_id}}})
		all_probes = [probe for probe in probes_]
		migration_dict = {}
		if isinstance(configuration_, dict) and '_id' in configuration_:
			user_id_fk = configuration_.get('AdminID_FK')
			config_user_list = configuration_.get('UserID_FKs')
			configuration_user = {
				'configuration_user_id': user_id_fk,
				'configuration_user_name': get_user(user_id_fk)['username'],
			}
			migration_dict = {
				'configuration_id': configuration_.get('_id'),
				'configuration_name': configuration_.get('configurationName'),
				'configuration_user': configuration_user,
				'error_threshold': configuration_.get('ErrorThreshold'),
				'display_energy_today': configuration_.get('DisplayEnergyToday'),
				'configuration_members': all_users(config_user_list),
				'probe_channels': multiple_probes(all_probes)
			}
			migration_list.append(migration_dict)
	# print(f'\n\n\n\nMIGRATION LIST = {migration_list}, TO MIGRATE = {configuration_ids}\n\n\n\n')
	return migration_list


def default_charge_map():
	data = [
		{
			'start_time': "00:00:00",
			'stop_time': "06:00:00",
			'amount': 25,
		},
		{
			'start_time': "06:00:00",
			'stop_time': "12:00:00",
			'amount': 25,
		},
		{
			'start_time': "12:00:00",
			'stop_time': "18:00:00",
			'amount': 25,
		},
		{
			'start_time': "18:00:00",
			'stop_time': "23:59:59",
			'amount': 25,
		}
	]
	return data

def get_three_unassigned_channels(probe_id, count=3):
	from api.models import Probe
	probe = Probe.get_one(probe_id=probe_id)
	if not probe:
		return []
	else:
		return probe.get_unassigned_channel_ids()[:count]

def get_generator_map(power_source):
	resp = []
	m = power_source['GeneratorMap']
	r = {
		'quarter_load_price': m['QuarterLoad'],
		'half_load_price': m['HalfLoad'],
		'three_quarter_load_price': m['ThreeQuarterLoad'],
		'full_load_price': m['FullLoad'],
	}
	resp.append(r)
	return resp

def default_operating_window():
	return [
		{
			'start_time': None,
			'stop_time': None,
		}
	]

def create_battery(power_source):
	bank = []
	meta = []
	for b in power_source['batteryLife']:
		t = {
			'depth_of_discharge': b['DoD'],
			'number_of_cycles': b['numberOfCycles'],
			'battery_capacity': b['bCapacity'],
		}
		meta.append(t)

	resp = {
		'count': power_source['batteryCount'],
		'capacity': power_source['batteryCapacity'],
		'bank_voltage': power_source['batteryBankVoltage'],
		'voltage': power_source['batteryVoltage'],
		'depth_of_discharge': power_source['depthOfDischarge'],
		'chemistry': power_source['batteryChemistry'],
		'end_of_life': power_source['batteryEndOfLife'],
		'meta': meta
	}
	bank.append(resp)
	return bank

def create_three_phase_grid(power_source):
	data = {
		'name': power_source['sourceName'],
		'three_phase_grid_id': power_source['_id'],
		'signal_threshold': power_source['SignalThreshold'],
		'configuration_id': power_source['ConfigID_FK'],
		'probe_id': power_source['ProbeID_FK'],
		'probe_channels': get_three_unassigned_channels(power_source['ProbeID_FK']),
		'charge_map': default_charge_map(),
	}
	from api.models import ThreePhaseGrid
	resp = ThreePhaseGrid.migrate_old(**data)
	# pprint(resp)

def create_three_phase_generator(power_source):
	data = {
		'name': power_source['sourceName'],
		'three_phase_generator_id': power_source['_id'],
		'signal_threshold': power_source['SignalThreshold'],
		'apparent_power_rating': power_source['Rating'],
		'fuel_name': power_source['FuelType'],
		'fuel_store_size': power_source['FuelStore'],
		'configuration_id': power_source['ConfigID_FK'],
		'probe_id': power_source['ProbeID_FK'],
		'synchronized': power_source['Synchronized'],
		'probe_channels': get_three_unassigned_channels(power_source['ProbeID_FK']),
		'operating_window': default_operating_window(),
		'charge_map': default_charge_map(),
		'generator_map': get_generator_map(power_source),
	}
	from api.models import ThreePhaseGenerator
	resp = ThreePhaseGenerator.migrate_old(**data)
	# pprint(resp)

def create_single_phase_inverter(power_source):
	channels = get_three_unassigned_channels(power_source['ProbeID_FK'], count=2)
	one, two = [None] * 2
	if channels:
		one, two = channels
	data = {
		'name': power_source['sourceName'],
		'single_phase_inverter_id': power_source['_id'],
		'signal_threshold': power_source['SignalThreshold'],
		'apparent_power_rating': power_source['Rating'],
		'efficiency': power_source['Efficiency'],
		'bulk_charging_current': power_source['chargeCurrent'],
		'cost': power_source['cost'],
		'configuration_id': power_source['ConfigID_FK'],
		'input_channel_id': one,
		'output_channel_id': two,
		'operating_window': default_operating_window(),
		'battery': create_battery(power_source),
	}
	from api.models import SinglePhaseInverter
	resp = SinglePhaseInverter.migrate_old(**data)
	# pprint(resp)

def do_power_source_migration():
	migration_list = []
	for configuration_id in configuration_ids:
		probe_power_sources = {}
		probes_ = [p for p in probes.find({'Channels': {'$elemMatch': {'ConfigID_FK': configuration_id}}})]
		for probe in probes_:
			channels = probe['Channels']
			for channel in channels:
				if not channel.get('SourceID_FK'):
					continue
				source_id = channel['SourceID_FK']
				if not probe_power_sources.get(source_id):
					if [source for source in get_power_sources(_id=source_id)]:
						source = [source for source in get_power_sources(_id=source_id)][0]
						source['ProbeID_FK'] = probe['_id']
						probe_power_sources[source_id] = source


		for power_source in probe_power_sources.values():
			source_type = power_source['Type'].lower()
			phase_count = power_source['NumberOfPhases']
			if source_type == 'grid':
				if phase_count == 3:
					create_three_phase_grid(power_source)
			if source_type == 'generator':
				if phase_count == 3:
					create_three_phase_generator(power_source)
			if source_type == 'inverter':
				create_single_phase_inverter(power_source)

	from api.models import Configuration
	for configuration_id in configuration_ids:
		config = Configuration.get_configuration(configuration_id=configuration_id)
		if config:
			# pprint(config.migration_dict())
			return []
	return migration_list

# print(workshop)
# pprint(migration_dict)
# Configuration.migration_config(**migration_dict)
