"""
	Module for interacting with the database
"""
from uuid import uuid4

from api import db
from api.models.base import PowerBase
from api.models.power_consumers.consumer_line.one_phase import SinglePhaseConsumerLine


class SinglePhaseConsumer(PowerBase):
	"""docstring for SinglePhaseConsumer"""
	SOURCE_TYPE = 'Single Phase Consumer'
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=False, nullable=False)
	consumer_id = db.Column(db.String(100), unique=True, nullable=False)
	line_id = db.Column(db.String(100), db.ForeignKey('single_phase_consumer_line.line_id'))

	def __init__(self, name, line):
		self.name = name.title()
		self.consumer_line = line.assign_to_source()
		self.consumer_id = str(uuid4())

	@staticmethod
	def create(name, line_id):
		new_group = SinglePhaseConsumer(
			name=name,
			line=SinglePhaseConsumerLine.get_one(line_id=line_id),
			)
		db.session.add(new_group)
		db.session.commit()
		db.session.refresh(new_group)
		return new_group.to_dict()

	def edit(self, name):
		self.name = name.title()
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	@staticmethod
	def get_one(source_id=None, name=None):
		consumer = None
		if source_id:
			consumer = SinglePhaseConsumer.query.filter_by(consumer_id=source_id).first()
		if name:
			name = name.title()
			consumer = SinglePhaseConsumer.query.filter_by(name=name).first()
		return consumer

	@staticmethod
	def view_all():
		all_consumers = SinglePhaseConsumer.query.order_by(SinglePhaseConsumer.name.asc()).all()
		if all_consumers:
			data = [consumer.to_dict() for consumer in all_consumers]
			return data
		return []

	def delete(self):
		db.session.delete(self)
		db.session.commit()
		return

	@staticmethod
	def get_all():
		return [consumer.to_dict() for consumer in SinglePhaseConsumer.query.order_by(SinglePhaseConsumer.date_created.desc()).all()]

	def get_data(self):
		return self.to_dict()

	def probe_list(self):
		"""Return a list of probes used to create this device"""
		return [self.consumer_line.channel.probe] if self.consumer_line else []

	def small_dict(self):
		type_dict = {
			'name': self.name,
			'meter_id': self.consumer_id,
		}
		return type_dict

	def to_dict(self):
		type_dict = {
			'source_type': self.SOURCE_TYPE,
			'installed': self.installed,
			'name': self.name,
			'meter_id': self.consumer_id,
			'meter_url': f"{self.SOURCE_TYPE.lower().replace(' ', '-')}/{self.consumer_id}",
			'consumer_line': self.consumer_line.small_dict() if self.consumer_line else None,
			'probe_names': [self.consumer_line.channel.probe.probe_name] if self.consumer_line else [],
		}
		return type_dict
