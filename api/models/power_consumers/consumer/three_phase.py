"""
	Module for interacting with the database
"""
from uuid import uuid4

from api import db
from api.models.base import PowerBase
from api.models.power_consumers.consumer_line.three_phase import ThreePhaseConsumerLine


class ThreePhaseConsumer(PowerBase):
	"""docstring for ThreePhaseConsumer"""
	SOURCE_TYPE = 'Three Phase Consumer'
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=False, nullable=False)
	consumer_id = db.Column(db.String(100), unique=True, nullable=False)
	line_id = db.Column(db.String(100), db.ForeignKey('three_phase_consumer_line.line_id'))

	def __init__(self, name, line):
		self.name = name.title()
		self.consumer_line = line.assign_to_source()
		self.consumer_id = str(uuid4())

	@staticmethod
	def create(name, line_id):
		new_consumer = ThreePhaseConsumer(
			name=name,
			line=ThreePhaseConsumerLine.get_one(line_id=line_id),
			)
		db.session.add(new_consumer)
		db.session.commit()
		db.session.refresh(new_consumer)
		return new_consumer.to_dict()

	def edit(self, name):
		self.name = name.title()
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	@staticmethod
	def get_one(source_id=None, name=None):
		consumer = None
		if source_id:
			consumer = ThreePhaseConsumer.query.filter_by(consumer_id=source_id).first()
		elif name:
			name = name.title()
			consumer = ThreePhaseConsumer.query.filter_by(name=name).first()
		return consumer

	@staticmethod
	def view_all():
		all_consumers = ThreePhaseConsumer.query.order_by(ThreePhaseConsumer.name.asc()).all()
		if all_consumers:
			data = [consumer.to_dict() for consumer in all_consumers]
			return data
		return []

	def delete(self):
		db.session.delete(self)
		db.session.commit()
		return

	@staticmethod
	def get_all():
		return [consumer.to_dict() for consumer in ThreePhaseConsumer.query.order_by(ThreePhaseConsumer.date_created.desc()).all()]

	def get_data(self):
		return self.to_dict()

	def small_dict(self):
		type_dict = {
			'name': self.name,
			'meter_id': self.consumer_id,
		}
		return type_dict

	def probe_list(self):
		"""Return a list of probes used to create this device"""
		channels = []
		if self.consumer_line:
			channels = [channel for channel in self.consumer_line.channels]
		probe_list = list(set([channel.probe for channel in channels]))
		return probe_list

	def to_dict(self):
		channels = []
		if self.consumer_line:
			channels = [channel for channel in self.consumer_line.channels]
		type_dict = {
			'source_type': self.SOURCE_TYPE,
			'name': self.name,
			'installed': self.installed,
			'meter_id': self.consumer_id,
			'meter_url': f"{self.SOURCE_TYPE.lower().replace(' ', '-')}/{self.consumer_id}",
			'line': self.consumer_line.small_dict() if self.consumer_line else None,
			'probe_names': [channel.probe.probe_name for channel in channels],
		}
		return type_dict
