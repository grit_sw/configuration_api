"""
	Module for interacting with the database
"""
from itertools import chain
from uuid import uuid4

from api import db
from api.models.base import Base
from api.models import Configuration
from api.models.power_consumers.utils.consumer_group_type import ConsumerGroupType


class ConsumerGroup(Base):
	"""docstring for ConsumerGroup"""
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=False, nullable=False)
	group_id = db.Column(db.String(100), unique=True, nullable=False)
	consumer_group_type_id = db.Column(db.String(100), db.ForeignKey('consumer_group_type.consumer_group_type_id'))
	configuration_id = db.Column(db.String(100), db.ForeignKey('configuration.configuration_id'))
	single_phase_consumer_lines = db.relationship('SinglePhaseConsumerLine', backref='consumer_group')
	three_phase_consumer_lines = db.relationship('ThreePhaseConsumerLine', backref='consumer_group')

	def __init__(self, name, configuration, consumer_group_type):
		self.name = name.title()
		self.configuration = configuration
		self.consumer_group_type = consumer_group_type
		self.group_id = str(uuid4())

	@staticmethod
	def create(name, configuration_id, consumer_group_type_id):
		new_group = ConsumerGroup(
			name=name,
			configuration=Configuration.get_configuration(configuration_id=configuration_id),
			consumer_group_type=ConsumerGroupType.get_one(consumer_group_type_id=consumer_group_type_id),
			)
		db.session.add(new_group)
		db.session.commit()
		db.session.refresh(new_group)
		return new_group.to_dict()

	def edit(self, name, configuration_id, consumer_group_type_id):
		self.name = name
		if self.configuration.configuration_id != configuration_id:
			self.configuration = Configuration.get_configuration(configuration_id=configuration_id)
		if self.consumer_group_type.consumer_group_type_id != consumer_group_type_id:
			self.consumer_group_type = ConsumerGroupType.get_one(consumer_group_type_id=consumer_group_type_id)
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	@staticmethod
	def get_one(group_id=None, name=None):
		if group_id:
			group = ConsumerGroup.query.filter_by(group_id=group_id).first()
		if name:
			name = name.title()
			group = ConsumerGroup.query.filter_by(name=name).first()
		return group

	def line_in_group(self, consumer_line):
		return consumer_line in self.single_phase_consumer_lines or consumer_line in self.three_phase_consumer_lines

	@staticmethod
	def view_all():
		all_groups = ConsumerGroup.query.order_by(ConsumerGroup.name.asc()).all()
		if all_groups:
			data = [group.to_dict() for group in all_groups]
			return data
		return []

	def delete(self):
		db.session.delete(self)
		db.session.commit()
		return

	@staticmethod
	def get_all():
		return [group.to_dict() for group in ConsumerGroup.query.order_by(ConsumerGroup.date_created.desc()).all()]

	def get_data(self):
		return self.to_dict()

	def get_consumers(self):
		single_phase_consumer_lines = [consumer.get_consumers() for consumer in self.single_phase_consumer_lines]
		three_phase_consumer_lines = [consumer.get_consumers() for consumer in self.three_phase_consumer_lines]
		single_phase_consumers = list(chain(*single_phase_consumer_lines))
		three_phase_consumers = list(chain(*three_phase_consumer_lines))
		consumers = [single_phase_consumers, three_phase_consumers]
		consumers = list(chain(*consumers))
		return consumers

	def small_dict(self):
		type_dict = {
			'name': self.name,
			'group_id': self.group_id,
			'configuration': self.configuration.small_dict() if self.configuration else None,
			'consumer_group_type': self.consumer_group_type.to_dict() if self.consumer_group_type else None,
		}
		return type_dict

	def to_dict(self):
		type_dict = {
			'name': self.name,
			'group_id': self.group_id,
			'configuration': self.configuration.small_dict() if self.configuration else None,
			'consumer_group_type': self.consumer_group_type.to_dict() if self.consumer_group_type else None,
			'single_phase_consumer_lines': [consumer.small_dict() for consumer in self.single_phase_consumer_lines],
			'three_phase_consumer_lines': [consumer.small_dict() for consumer in self.three_phase_consumer_lines],
		}
		return type_dict
