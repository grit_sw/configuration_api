"""
	Module for interacting with the database
"""
from uuid import uuid4

from api import db
from api.models.base import Base
from api.models.models import Channels
from api.models import PhaseType
from api.models.power_consumers.consumer_group import ConsumerGroup


class SinglePhaseConsumerLine(Base):
	"""docstring for SinglePhaseConsumerLine"""
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=False, nullable=False)
	signal_threshold = db.Column(db.Float, unique=False, nullable=True)
	line_id = db.Column(db.String(100), unique=True, nullable=False)
	phase_id = db.Column(db.String(100), db.ForeignKey('phase_type.phase_id'))
	consumer_group_id = db.Column(db.String(100), db.ForeignKey('consumer_group.group_id'))
	channel_id = db.Column(db.String(100), db.ForeignKey('channels.channel_id'))
	consumer = db.relationship('SinglePhaseConsumer', uselist=False, backref='consumer_line')

	def __init__(self, name, signal_threshold, phase_type, channel, consumer_group):
		self.name = name.title()
		self.signal_threshold = signal_threshold
		self.channel = channel
		self.consumer_group = consumer_group
		self.phase_type = phase_type
		self.line_id = str(uuid4())

	@staticmethod
	def create(name, signal_threshold, channel_id, consumer_group_id):
		channel = Channels.get_channel(channel_id=channel_id)
		new_line = SinglePhaseConsumerLine(
			name=name,
			signal_threshold=signal_threshold,
			channel=channel.reserve(),
			phase_type=PhaseType.get_one_phase(),
			consumer_group=ConsumerGroup.get_one(group_id=consumer_group_id),
			)
		db.session.add(new_line)
		db.session.commit()
		db.session.refresh(new_line)
		return new_line.to_dict()


	def edit(self, name, signal_threshold, consumer_group_id, channel_id):
		self.name = name.title()
		self.signal_threshold = signal_threshold
		if self.channel.channel_id != channel_id:
			self.channel.remove_from_source()
			self.channel = Channels.get_channel(channel_id=channel_id).reserve()
		if self.consumer_group.group_id != consumer_group_id:
			self.consumer_group = ConsumerGroup.get_one(group_id=consumer_group_id)
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	@staticmethod
	def get_one(line_id=None, consumer_group_id=None, name=None):
		consumer_line = None
		if line_id:
			consumer_line = SinglePhaseConsumerLine.query.filter_by(line_id=line_id).first()
		if consumer_group_id:
			consumer_line = SinglePhaseConsumerLine.query.filter_by(consumer_group_id=consumer_group_id).first()
		if name:
			name = name.title()
			consumer_line = SinglePhaseConsumerLine.query.filter_by(name=name).first()
		return consumer_line

	def consumer_in_line(self, consumer):
		"""Check if this consumer is in the consumer line."""
		return consumer == self.consumer

	@property
	def is_available(self):
		"""Check if a consumer can be added to the consumer line."""
		return not bool(self.consumer)

	@staticmethod
	def view_all():
		all_consumer_lines = SinglePhaseConsumerLine.query.order_by(SinglePhaseConsumerLine.name.asc()).all()
		if all_consumer_lines:
			data = [consumer_line.to_dict() for consumer_line in all_consumer_lines]
			return data
		return []

	def unassign_channel(self):
		self.channel.remove_from_source()
		return self

	def assign_to_source(self):
		self.channel.assign_to_source()
		return self

	def delete(self):
		line = self.unassign_channel()
		db.session.delete(line)
		db.session.commit()
		return

	@staticmethod
	def get_all():
		return [consumer_line.to_dict() for consumer_line in SinglePhaseConsumerLine.query.order_by(SinglePhaseConsumerLine.date_created.desc()).all()]

	def get_data(self):
		return self.to_dict()

	def small_dict(self):
		type_dict = {
			'name': self.name,
			'signal_threshold': self.signal_threshold,
			'line_id': self.line_id,
			'consumer_group': self.consumer_group.small_dict() if self.consumer_group else None,
			'channel': [self.channel.small_dict()] if self.channel else None,
		}
		return type_dict

	def get_consumers(self):
		consumer = [self.consumer.to_dict()] if self.consumer else []
		return consumer

	def to_dict(self):
		type_dict = {
			'name': self.name,
			'signal_threshold': self.signal_threshold,
			'line_id': self.line_id,
			'phase_type': self.phase_type.small_dict() if self.phase_type else None,
			'consumer_group': self.consumer_group.small_dict() if self.consumer_group else None,
			'consumer': self.consumer.small_dict() if self.consumer else None,
			'channel': [self.channel.small_dict()] if self.channel else None,
			'is_available': self.is_available,
		}
		return type_dict
