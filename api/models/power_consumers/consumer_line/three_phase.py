"""
	Module for interacting with the database
"""
from uuid import uuid4

from api import db
from api.models.base import Base
from api.models.models import Channels
from api.models import PhaseType
from api.models.power_consumers.consumer_group import ConsumerGroup


class ThreePhaseConsumerLine(Base):
	"""docstring for ThreePhaseConsumerLine"""
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=False, nullable=False)
	signal_threshold = db.Column(db.Float, unique=False, nullable=True)
	line_id = db.Column(db.String(100), unique=True, nullable=False)
	phase_id = db.Column(db.String(100), db.ForeignKey('phase_type.phase_id'))
	consumer_group_id = db.Column(db.String(100), db.ForeignKey('consumer_group.group_id'))
	# channel_id = db.Column(db.String(100), db.ForeignKey('channels.channel_id'))
	consumer = db.relationship('ThreePhaseConsumer', backref='consumer_line', uselist=False)
	channels = db.relationship('Channels', backref='three_phase_consumer_line', lazy='dynamic')

	def __init__(self, name, signal_threshold, phase_type, consumer_group):
		self.name = name.title()
		self.signal_threshold = signal_threshold
		self.phase_type = phase_type
		self.consumer_group = consumer_group
		self.line_id = str(uuid4())

	@staticmethod
	def create(name, signal_threshold, consumer_group_id, channels):
		new_group = ThreePhaseConsumerLine(
			name=name,
			signal_threshold=signal_threshold,
			phase_type=PhaseType.get_three_phase(),
			consumer_group=ConsumerGroup.get_one(group_id=consumer_group_id),
			)
		db.session.add(new_group)
		new_group.add_channels(channels)
		db.session.commit()
		db.session.refresh(new_group)
		return new_group.to_dict()

	def add_channels(self, channel_list):
		for channel_id in channel_list:
			channel = Channels.get_channel(channel_id=channel_id)
			if channel not in self.channels:
				channel.reserve()
				self.channels.append(channel)
				db.session.add(self)

	def edit_channels(self, channel_list):
		existing_channels = set([channel.channel_id for channel in self.channels.all()])
		new_channels = set(channel_list)
		removed_channels = existing_channels.difference(new_channels)
		added_channels = new_channels.difference(existing_channels)
		for channel_id in removed_channels:
			channel = Channels.get_channel(channel_id=channel_id)
			if channel in self.channels:
				self.channels.remove(channel)
				channel.remove_from_source()
				db.session.add(self)
		for channel_id in added_channels:
			channel = Channels.get_channel(channel_id=channel_id)
			if channel not in self.channels:
				if self.consumer:
					channel.assign_to_source()
				else:
					channel.reserve()
				self.channels.append(channel)
				db.session.add(self)
		return

	def edit(self, name, signal_threshold, consumer_group_id, channels):
		self.name = name.title()
		self.signal_threshold = signal_threshold
		if self.consumer_group.group_id != consumer_group_id:
			self.consumer_group = ConsumerGroup.get_one(group_id=consumer_group_id)
		self.edit_channels(channels)
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	def assign_to_source(self):
		for channel in self.channels:
			channel.assign_to_source()
		return self

	def remove_from_source(self):
		for channel in self.channels:
			channel.remove_from_source()
		return self

	@staticmethod
	def get_one(line_id=None, name=None):
		consumer_line = None
		if line_id:
			consumer_line = ThreePhaseConsumerLine.query.filter_by(line_id=line_id).first()
		if name:
			name = name.title()
			consumer_line = ThreePhaseConsumerLine.query.filter_by(name=name).first()
		return consumer_line

	def get_consumer(self, name=None, line_id=None):
		consumer = None
		if name:
			name = name.title()
			consumer = self.consumer.query.filter_by(name=name).first()
		elif line_id:
			consumer = self.consumer.query.filter_by(line_id=line_id).first()
		return consumer

	def consumer_in_line(self, consumer):
		return self.consumer == consumer

	@staticmethod
	def view_all():
		all_consumer_lines = ThreePhaseConsumerLine.query.order_by(ThreePhaseConsumerLine.name.asc()).all()
		if all_consumer_lines:
			data = [consumer_line.to_dict() for consumer_line in all_consumer_lines]
			return data
		return []

	def unassign_all_channels(self):
		for channel in self.channels:
			channel.remove_from_source()
		return self

	def delete(self):
		self = self.unassign_all_channels()
		db.session.delete(self)
		db.session.commit()
		return

	@property
	def is_available(self):
		"""Check if a consumer can be added to the consumer line."""
		return not bool(self.consumer)

	@staticmethod
	def get_all():
		return [consumer_line.to_dict() for consumer_line in ThreePhaseConsumerLine.query.order_by(ThreePhaseConsumerLine.date_created.desc()).all()]

	def get_data(self):
		return self.to_dict()

	def get_consumers(self):
		consumer = [self.consumer.to_dict()] if self.consumer else []
		return consumer

	def small_dict(self):
		type_dict = {
			'name': self.name,
			'signal_threshold': self.signal_threshold,
			'line_id': self.line_id,
			'consumer_group': self.consumer_group.small_dict() if self.consumer_group else None,
			'channels': [channel.small_dict() for channel in self.channels],
		}
		return type_dict

	def to_dict(self):
		type_dict = {
			'name': self.name,
			'signal_threshold': self.signal_threshold,
			'line_id': self.line_id,
			'phase_type': self.phase_type.small_dict() if self.phase_type else None,
			'consumer_group': self.consumer_group.small_dict() if self.consumer_group else None,
			'consumer': self.consumer.small_dict() if self.consumer else None,
			'channels': [channel.small_dict() for channel in self.channels],
		}
		return type_dict
