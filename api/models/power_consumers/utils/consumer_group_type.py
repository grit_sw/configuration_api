"""
	Module for interacting with the database
"""
from uuid import uuid4

from api import db
from api.models.base import Base


class ConsumerGroupType(Base):
	"""docstring for ConsumerGroupType"""
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=False, nullable=False)
	consumer_group_type_id = db.Column(db.String(100), unique=True, nullable=False)
	consumer_groups = db.relationship('ConsumerGroup', backref='consumer_group_type')

	def __init__(self, name):
		self.name = name.title()
		self.consumer_group_type_id = str(uuid4())

	@staticmethod
	def create(name):
		new_group = ConsumerGroupType(
			name=name,
			)
		db.session.add(new_group)
		db.session.commit()
		db.session.refresh(new_group)
		return new_group.to_dict()

	def edit(self, name):
		self.name = name.title()
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	@staticmethod
	def get_one(consumer_group_type_id=None, name=None):
		group_type = None
		if consumer_group_type_id:
			group_type = ConsumerGroupType.query.filter_by(consumer_group_type_id=consumer_group_type_id).first()
		elif name:
			name = name.title()
			group_type = ConsumerGroupType.query.filter_by(name=name).first()
		return group_type

	@staticmethod
	def init_types():
		names = [
			'Estate',
			'Structure',
			'Floor',
			'Room',
		]
		for name in names:
			if not ConsumerGroupType.get_one(name=name):
				new_group = ConsumerGroupType(
					name=name,
					)
				db.session.add(new_group)
		db.session.commit()
		return ConsumerGroupType.view_all()

	@staticmethod
	def view_all():
		all_group_types = ConsumerGroupType.query.order_by(ConsumerGroupType.name.asc()).all()
		if all_group_types:
			data = [group_type.to_dict() for group_type in all_group_types]
			return data
		return []

	def delete(self):
		db.session.delete(self)
		db.session.commit()
		return

	@staticmethod
	def get_all():
		return [group.to_dict() for group in ConsumerGroupType.query.order_by(ConsumerGroupType.date_created.desc()).all()]

	def get_data(self):
		return self.to_dict()

	def to_dict(self):
		type_dict = {
			'name': self.name,
			'consumer_group_type_id': self.consumer_group_type_id,
		}
		return type_dict
