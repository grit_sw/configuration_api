"""
	Module for interacting with the database
"""
from uuid import uuid4

from api import db
from api.models.base import Base


class OperatingWindow(Base):
	"""docstring for OperatingWindow"""
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	window_id = db.Column(db.String(100), unique=True, nullable=False)
	start_time = db.Column(db.Time, nullable=True)
	stop_time = db.Column(db.Time, nullable=True)

	single_phase_generator = db.relationship('SinglePhaseGenerator', backref='operating_window')  # Many to one relationship with the power sources
	single_phase_generator_id = db.Column(db.String(100), db.ForeignKey('single_phase_generator.single_phase_generator_id'))

	three_phase_generator = db.relationship('ThreePhaseGenerator', backref='operating_window')  # Many to one relationship with the power sources
	three_phase_generator_id = db.Column(db.String(100), db.ForeignKey('three_phase_generator.three_phase_generator_id'))

	single_phase_inverter = db.relationship('SinglePhaseInverter', backref='operating_window')  # Many to one relationship with the power sources
	single_phase_inverter_id = db.Column(db.String(100), db.ForeignKey('single_phase_inverter.single_phase_inverter_id'))

	three_phase_inverter = db.relationship('ThreePhaseInverter', backref='operating_window')  # Many to one relationship with the power sources
	three_phase_inverter_id = db.Column(db.String(100), db.ForeignKey('three_phase_inverter.three_phase_inverter_id'))

	def __init__(self, start_time, stop_time):
		self.start_time = start_time
		self.stop_time = stop_time
		self.window_id = str(uuid4())

	@staticmethod
	def get_operating_window(window_id):
		operating_window = OperatingWindow.query.filter_by(window_id=window_id).first()
		return operating_window

	@staticmethod
	def new(start_time, stop_time):
		new_operating_window = OperatingWindow(
			start_time=start_time,
			stop_time=stop_time,
		)
		db.session.add(new_operating_window)
		return new_operating_window

	def edit(self, start_time, stop_time):
		self.start_time = start_time
		self.stop_time = stop_time
		db.session.add(self)
		return self

	@staticmethod
	def get_all():
		operating_windows = OperatingWindow.query.order_by(OperatingWindow.date_created.desc()).all()
		return [operating_window.to_dict() for operating_window in operating_windows]

	def delete_operating_window(self):
		db.session.delete(self)
		db.session.commit()

	def device_config_dict(self):
		response = {
			'start_time': str(self.start_time),
			'stop_time': str(self.stop_time),
			'set': self.start_time and self.stop_time
		}
		return response

	def to_dict(self):
		response = {
			'window_id': self.window_id,
			'start_time': str(self.start_time),
			'stop_time': str(self.stop_time),
		}
		return response
