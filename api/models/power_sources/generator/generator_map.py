"""
	Module for interacting with the database
"""
from uuid import uuid4

from api import db
from api.models.base import Base


class GeneratorMap(Base):
	"""docstring for GeneratorMap"""
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	map_id = db.Column(db.String(100), unique=True, nullable=False)
	quarter_load_price = db.Column(db.Float, nullable=True)
	half_load_price = db.Column(db.Float, nullable=True)
	three_quarter_load_price = db.Column(db.Float, nullable=True)
	full_load_price = db.Column(db.Float, nullable=True)

	single_phase_generator = db.relationship('SinglePhaseGenerator', backref='generator_map')  # Many to one relationship with the power sources
	single_phase_generator_id = db.Column(db.String(100), db.ForeignKey('single_phase_generator.single_phase_generator_id'))

	three_phase_generator = db.relationship('ThreePhaseGenerator', backref='generator_map')  # Many to one relationship with the power sources
	three_phase_generator_id = db.Column(db.String(100), db.ForeignKey('three_phase_generator.three_phase_generator_id'))

	def __init__(self, quarter_load_price, half_load_price, three_quarter_load_price, full_load_price):
		self.quarter_load_price = quarter_load_price
		self.half_load_price = half_load_price
		self.three_quarter_load_price = three_quarter_load_price
		self.full_load_price = full_load_price
		self.map_id = str(uuid4())

	@staticmethod
	def get_one(map_id):
		generator_map = GeneratorMap.query.filter_by(map_id=map_id).first()
		return generator_map

	@staticmethod
	def new(quarter_load_price, half_load_price, three_quarter_load_price, full_load_price):
		new_generator_map = GeneratorMap(
			quarter_load_price=quarter_load_price,
			half_load_price=half_load_price,
			three_quarter_load_price=three_quarter_load_price,
			full_load_price=full_load_price,
		)
		db.session.add(new_generator_map)
		return new_generator_map

	def edit(self, quarter_load_price, half_load_price, three_quarter_load_price, full_load_price):
		self.quarter_load_price = quarter_load_price
		self.half_load_price = half_load_price
		self.three_quarter_load_price = three_quarter_load_price
		self.full_load_price = full_load_price
		db.session.add(self)
		return self

	@staticmethod
	def get_all():
		generator_maps = GeneratorMap.query.order_by(GeneratorMap.date_created.desc()).all()
		return [generator_map.to_dict() for generator_map in generator_maps]

	def delete_generator_map(self):
		db.session.delete(self)
		db.session.commit()

	def device_config_dict(self):
		response = {
			'quarter_load_price': self.quarter_load_price,
			'half_load_price': self.half_load_price,
			'three_quarter_load_price': self.three_quarter_load_price,
			'full_load_price': self.full_load_price,
		}
		return response

	def to_dict(self):
		type_dict = {
			'map_id': self.map_id,
			'quarter_load_price': self.quarter_load_price,
			'half_load_price': self.half_load_price,
			'three_quarter_load_price': self.three_quarter_load_price,
			'full_load_price': self.full_load_price,
		}
		return type_dict
