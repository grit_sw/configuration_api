"""
	Module for interacting with the database
	# todo: module for charge map operations
	# to be used by other power sources
	# todo: check clashing times during
	# creation and editing of charge map.
"""
from uuid import uuid4

from api import db
from api.models import PhaseType
from api.models.base import PowerBase
from api.models.charge_map import ChargeMap
from api.models import Channels, Configuration, Probe, Fuel
from api.models.power_sources.common import OperatingWindow
from api.models.power_sources.generator.generator_map import GeneratorMap


class SinglePhaseGenerator(PowerBase):
	"""docstring for SinglePhaseGenerator"""
	SOURCE_TYPE = 'Single Phase Generator'
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=False, nullable=False)
	signal_threshold = db.Column(db.String(100), unique=False, nullable=False)
	apparent_power_rating = db.Column(db.Float, unique=False, nullable=False)
	fuel_store_size = db.Column(db.Float, unique=False, nullable=False)
	synchronized = db.Column(db.Boolean, nullable=True, default=False)
	single_phase_generator_id = db.Column(db.String(100), unique=True, nullable=False)
	channel_id = db.Column(db.String(100), db.ForeignKey('channels.channel_id'))
	probe_id = db.Column(db.String(100), db.ForeignKey('probe.probe_id'))
	configuration_id = db.Column(db.String(100), db.ForeignKey('configuration.configuration_id'))
	fuel_id = db.Column(db.String(100), db.ForeignKey('fuel.fuel_id'))
	phase_id = db.Column(db.String(100), db.ForeignKey('phase_type.phase_id'))

	def __init__(self, name, signal_threshold, configuration, probe, channel,
		apparent_power_rating, fuel_store_size, fuel):
		self.name = name.title()
		self.signal_threshold = signal_threshold
		self.apparent_power_rating = apparent_power_rating
		self.fuel_store_size = fuel_store_size
		self.configuration = configuration
		self.probe = probe
		self.fuel = fuel
		self.channel = channel.assign_to_source()
		self.single_phase_generator_id = str(uuid4())
		self.phase_type = PhaseType.get_one_phase()

	@staticmethod
	def create(name, signal_threshold, configuration_id, probe_id, probe_channel_id,
		apparent_power_rating, fuel_id, fuel_store_size, operating_window, charge_map, generator_map):
		new_generator = SinglePhaseGenerator(
			name=name,
			signal_threshold=signal_threshold,
			apparent_power_rating=apparent_power_rating,
			fuel_store_size=fuel_store_size,
			configuration=Configuration.get_configuration(configuration_id=configuration_id),
			probe=Probe.get_one(probe_id=probe_id),
			channel=Channels.get_channel(channel_id=probe_channel_id),
			fuel=Fuel.get_fuel(fuel_id=fuel_id),
			)
		new_generator.create_charge_map(charge_map)
		new_generator.create_operating_window(operating_window)
		new_generator.create_generator_map(generator_map)
		db.session.add(new_generator)
		db.session.commit()
		db.session.refresh(new_generator)
		return new_generator.to_dict()

	def create_charge_map(self, charge_data):
		for charge in charge_data:
			charge_map = ChargeMap.new(**charge)
			if charge_map not in self.charge_map:
				self.charge_map.append(charge_map)

	def create_operating_window(self, window_data):
		for window in window_data:
			operating_window = OperatingWindow.new(**window)
			if operating_window not in self.operating_window:
				self.operating_window.append(operating_window)

	def create_generator_map(self, generator_map_data):
		for generator_map in generator_map_data:
			generator_map_obj = GeneratorMap.new(**generator_map)
			if generator_map_obj not in self.generator_map:
				self.generator_map.append(generator_map_obj)

	def get_charge_ids(self):
		return [charge.map_id for charge in self.charge_map]

	def edit_charge_map(self, charge_data):
		existing_charges = set(self.get_charge_ids())
		edited_charge_ids = set([charge['map_id'] for charge in charge_data])
		deleted_charges = existing_charges.difference(edited_charge_ids)
		for map_id in deleted_charges:
			charge_obj = ChargeMap.get_charge_map(map_id)
			if charge_obj in self.charge_map:
				self.charge_map.remove(charge_obj)
				db.session.add(self)
		for charge in charge_data:
			charge_map = ChargeMap.get_charge_map(charge['map_id'])
			if charge_map:
				del charge['map_id']
				charge_map.edit(**charge)

	def get_window_ids(self):
		return [window.window_id for window in self.operating_window]

	def edit_operating_window(self, window_data):
		existing_windows = set(self.get_window_ids())
		edited_window_ids = set([window['window_id'] for window in window_data])
		deleted_windows = existing_windows.difference(edited_window_ids)
		for window_id in deleted_windows:
			window_obj = OperatingWindow.get_operating_window(window_id)
			if window_obj in self.operating_window:
				self.operating_window.remove(window_obj)
				db.session.add(self)
		for window in window_data:
			operating_window = OperatingWindow.get_operating_window(window['window_id'])
			if operating_window:
				del window['window_id']
				operating_window.edit(**window)

	def get_generator_map_ids(self):
		return [generator_map.map_id for generator_map in self.generator_map]

	def edit_generator_map(self, generator_map_data):
		existing_generator_maps = set(self.get_generator_map_ids())
		edited_generator_map_ids = set([generator_map['map_id'] for generator_map in generator_map_data])
		deleted_generator_maps = existing_generator_maps.difference(edited_generator_map_ids)
		for map_id in deleted_generator_maps:
			generator_map_obj = GeneratorMap.get_one(map_id)
			if generator_map_obj in self.generator_map:
				self.generator_map.remove(generator_map_obj)
				db.session.add(self)
		for generator_map in generator_map_data:
			generator_map_obj = GeneratorMap.get_one(generator_map['map_id'])
			if generator_map_obj:
				del generator_map['map_id']
				generator_map_obj.edit(**generator_map)

	def check_clashing_charges(self, added_charge_map):
		"""Method to check if two or more charges do not clash"""
		pass

	def edit(self, name, signal_threshold, configuration_id, probe_id, probe_channel_id,
		apparent_power_rating, fuel_id, fuel_store_size, charge_map, new_charge_map, generator_map,
		new_generator_map, operating_window, new_operating_window):
		self.name = name
		self.signal_threshold = signal_threshold
		self.apparent_power_rating = apparent_power_rating
		self.fuel_store_size = fuel_store_size
		self.configuration = Configuration.get_configuration(configuration_id=configuration_id)
		self.probe = Probe.get_one(probe_id=probe_id)
		if self.channel.channel_id != probe_channel_id:
			self.channel.remove_from_source()
			self.channel = Channels.get_channel(channel_id=probe_channel_id).assign_to_source()
		self.fuel = Fuel.get_fuel(fuel_id=fuel_id)

		self.edit_charge_map(charge_map)
		self.create_charge_map(new_charge_map)
		self.edit_operating_window(operating_window)
		self.create_operating_window(new_operating_window)
		self.edit_generator_map(generator_map)
		self.create_generator_map(new_generator_map)
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	@staticmethod
	def get_one(source_id=None, probe_channel_id=None):
		single_phase_generator = None
		if source_id:
			single_phase_generator = SinglePhaseGenerator.query.filter_by(single_phase_generator_id=source_id).first()
		elif probe_channel_id:
			single_phase_generator = SinglePhaseGenerator.query.filter(SinglePhaseGenerator.channel.has(Channels.channel_id == probe_channel_id)).first()
			print(f'\n\n\nsingle_phase_generators = {single_phase_generator}')
		return single_phase_generator

	@staticmethod
	def view_all():
		all_single_phase_generators = SinglePhaseGenerator.query.order_by(SinglePhaseGenerator.name.asc()).all()
		if all_single_phase_generators:
			data = [single_phase_generator.to_dict() for single_phase_generator in all_single_phase_generators]
			return data
		return []

	def unassign_channel(self):
		self.channel.remove_from_source()
		return self

	def delete(self):
		source = self.unassign_channel()
		db.session.delete(source)
		db.session.commit()
		return

	@staticmethod
	def get_all():
		return [generator.to_dict() for generator in SinglePhaseGenerator.query.order_by(SinglePhaseGenerator.date_created.desc()).all()]

	def get_data(self):
		return self.to_dict()

	def probe_list(self):
		"""Return a list of probes used to create this device"""
		return [self.channel.probe] if self.channel else []

	def device_config_dict(self):
		response = {
			"SignalThreshold": self.signal_threshold,
			"Rating": self.apparent_power_rating,
			"GeneratorMap": [],
			"FuelType": self.fuel.name,
			"ProbeID_FK": self.probe_id,
			"sourceName": self.name,
			"FuelUnitPrice": self.fuel.unit_price,
			"NumberOfPhases": self.phase_type.num_id,
			"Synchronized": self.synchronized,
			"_id": self.single_phase_generator_id,
			"FuelStore": self.fuel_store_size,
			"ConfigID_FK": self.configuration_id,
			"operatingWindow": [operating_window.device_config_dict() for operating_window in self.operating_window],
			"State": self.state,
			"MaxChannels": self.phase_type.num_id,
			"FuelID_FK": self.fuel_id,
			"Type": "generator",
		}
		return response

	def to_dict(self):
		type_dict = {
			'source_type': self.SOURCE_TYPE,
			'name': self.name,
			'installed': self.installed,
			'meter_id': self.single_phase_generator_id,
			'meter_url': f"{self.SOURCE_TYPE.lower().replace(' ', '-')}/{self.single_phase_generator_id}",
			'signal_threshold': self.signal_threshold,
			'apparent_power_rating': self.apparent_power_rating,
			'fuel_store_size': self.fuel_store_size,
			'probe': self.probe.small_dict() if self.probe else None,
			'fuel': self.fuel.to_dict() if self.fuel else None,
			'channel': [self.channel.small_dict()] if self.channel else None,
			'probe_names': [self.channel.probe.probe_name] if self.channel else [],
			'configuration': self.configuration.small_dict() if self.configuration else None,
			'charge_map': [charge_map.to_dict() for charge_map in self.charge_map],
			'operating_window': [operating_window.to_dict() for operating_window in self.operating_window],
			'generator_map': [generator_map.to_dict() for generator_map in self.generator_map],
		}
		return type_dict
