"""
	Module for interacting with the database
	# todo: module for charge map operations
	# to be used by other power sources
	# todo: check clashing times during
	# creation and editing of charge map.
"""
from uuid import uuid4

from api import db
from api.models import PhaseType
from api.models.base import PowerBase
from api.models.charge_map import ChargeMap
from api.models.models import Channels, Configuration, Probe


class ThreePhaseGrid(PowerBase):
	"""docstring for ThreePhaseGrid"""
	SOURCE_TYPE = 'Three Phase Grid'
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=False, nullable=False)
	signal_threshold = db.Column(db.String(100), unique=False, nullable=False)
	rating = db.Column(db.Float, nullable=True)
	three_phase_grid_id = db.Column(db.String(100), unique=True, nullable=False)
	channels = db.relationship('Channels', backref='three_phase_grid', lazy='dynamic')
	probe_id = db.Column(db.String(100), db.ForeignKey('probe.probe_id'))
	configuration_id = db.Column(db.String(100), db.ForeignKey('configuration.configuration_id'))
	phase_id = db.Column(db.String(100), db.ForeignKey('phase_type.phase_id'))

	def __init__(self, name, signal_threshold, configuration, probe):
		self.name = name.title()
		self.signal_threshold = signal_threshold
		self.configuration = configuration
		self.probe = probe
		self.three_phase_grid_id = str(uuid4())
		self.phase_type = PhaseType.get_three_phase()

	@staticmethod
	def create(name, signal_threshold, configuration_id, probe_id, probe_channels, charge_map):
		new_grid = ThreePhaseGrid(
			name=name,
			signal_threshold=signal_threshold,
			configuration=Configuration.get_configuration(configuration_id=configuration_id),
			probe=Probe.get_one(probe_id=probe_id),
		)
		new_grid.add_channels(probe_channels)
		new_grid.create_charge_map(charge_map)
		db.session.add(new_grid)
		db.session.commit()
		db.session.refresh(new_grid)
		return new_grid.to_dict()

	@staticmethod
	def migrate_old(name, three_phase_grid_id, signal_threshold, configuration_id, probe_id, probe_channels, charge_map):
		grid = ThreePhaseGrid.get_one(source_id=three_phase_grid_id)
		if grid:
			return grid.to_dict()
		new_grid = ThreePhaseGrid(
			name=name,
			signal_threshold=signal_threshold,
			configuration=Configuration.get_configuration(configuration_id=configuration_id),
			probe=Probe.get_one(probe_id=probe_id),
		)
		new_grid.three_phase_grid_id = three_phase_grid_id
		new_grid.add_channels(probe_channels)
		new_grid.create_charge_map(charge_map)
		db.session.add(new_grid)
		db.session.commit()
		db.session.refresh(new_grid)
		return new_grid.to_dict()

	def add_channels(self, channel_list):
		for channel_id in channel_list:
			channel = Channels.get_channel(channel_id=channel_id)
			if channel not in self.channels:
				self.channels.append(channel)
				channel.assign_to_source()
				db.session.add(self)

	def edit_channels(self, channel_list):
		existing_channels = set([channel.channel_id for channel in self.channels.all()])
		new_channels = set(channel_list)
		removed_channels = existing_channels.difference(new_channels)
		added_channels = new_channels.difference(existing_channels)
		for channel_id in removed_channels:
			channel = Channels.get_channel(channel_id=channel_id)
			if channel in self.channels:
				self.channels.remove(channel)
				channel.remove_from_source()
				db.session.add(self)
		for channel_id in added_channels:
			channel = Channels.get_channel(channel_id=channel_id)
			if channel not in self.channels:
				channel.assign_to_source()
				self.channels.append(channel)
				db.session.add(self)
		return

	def create_charge_map(self, charge_data):
		for charge in charge_data:
			charge_map = ChargeMap.new(**charge)
			if charge_map not in self.charge_map:
				self.charge_map.append(charge_map)

	def get_charges(self):
		return [charge.map_id for charge in self.charge_map]

	def edit_charge_map(self, charge_data):
		existing_charges = set(self.get_charges())
		edited_charge_ids = set([charge['map_id'] for charge in charge_data])
		deleted_charges = existing_charges.difference(edited_charge_ids)
		for map_id in deleted_charges:
			charge_obj = ChargeMap.get_charge_map(map_id)
			if charge_obj in self.charge_map:
				self.charge_map.remove(charge_obj)
				db.session.add(self)
		for charge in charge_data:
			charge_map = ChargeMap.get_charge_map(charge['map_id'])
			if charge_map:
				del charge['map_id']
				charge_map.edit(**charge)

	def check_clashing_charges(self, added_charge_map):
		"""Method to check if two or more charges do not clash"""
		pass

	def edit(self, name, signal_threshold, configuration_id, probe_id, probe_channels, charge_map, new_charge_map):
		self.name = name
		self.signal_threshold = signal_threshold
		self.configuration = Configuration.get_configuration(configuration_id=configuration_id)
		self.probe = Probe.get_one(probe_id=probe_id)
		self.edit_channels(probe_channels)
		self.edit_charge_map(charge_map)
		self.create_charge_map(new_charge_map)
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	@staticmethod
	def get_one(source_id=None, probe_channel_id=None):
		three_phase_grid = None
		if source_id:
			three_phase_grid = ThreePhaseGrid.query.filter_by(three_phase_grid_id=source_id).first()
		elif probe_channel_id:
			three_phase_grid = ThreePhaseGrid.query.filter(ThreePhaseGrid.channels.any(Channels.channel_id == probe_channel_id)).first()
			print(f'\n\n\nthree_phase_grids = {three_phase_grid}')
		return three_phase_grid

	@staticmethod
	def view_all():
		all_three_phase_grids = ThreePhaseGrid.query.order_by(ThreePhaseGrid.name.asc()).all()
		if all_three_phase_grids:
			data = [three_phase_grid.to_dict() for three_phase_grid in all_three_phase_grids]
			return data
		return []

	def unassign_all_channels(self):
		for channel in self.channels:
			channel.remove_from_source()
		return self

	def delete(self):
		source = self.unassign_all_channels()
		db.session.delete(source)
		db.session.commit()
		return

	def probe_list(self):
		"""Return a list of probes used to create this device"""
		probe_list = [channel.probe for channel in self.channels]
		probe_list = list(set(probe_list))
		return probe_list

	@staticmethod
	def get_all():
		return [grid.to_dict() for grid in ThreePhaseGrid.query.order_by(ThreePhaseGrid.date_created.desc()).all()]

	def get_data(self):
		return self.to_dict()

	def device_config_dict(self):
		response = {
			"SignalThreshold": self.signal_threshold,
			"Rating": self.rating,
			"UtilityMap": [charge_map.device_config_dict() for charge_map in self.charge_map],
			"ProbeID_FK": self.probe_id,
			"sourceName": self.name,
			"NumberOfPhases": self.phase_type.num_id,
			"_id": self.three_phase_grid_id,
			"ConfigID_FK": self.configuration_id,
			"State": self.state,
			"MaxChannels": self.phase_type.num_id,
			"Type": "grid",
		}
		return response

	def to_dict(self):
		type_dict = {
			'source_type': self.SOURCE_TYPE,
			'name': self.name,
			'installed': self.installed,
			'signal_threshold': self.signal_threshold,
			'rating': self.rating,
			'meter_id': self.three_phase_grid_id,
			'meter_url': f"{self.SOURCE_TYPE.lower().replace(' ', '-')}/{self.three_phase_grid_id}",
			'probe': self.probe.small_dict() if self.probe else None,
			'channels': [channel.small_dict() for channel in self.channels],
			'probe_names': list(set([channel.probe.probe_name for channel in self.channels])),
			'configuration': self.configuration.small_dict() if self.configuration else None,
			'charge_map': [charge_map.to_dict() for charge_map in self.charge_map],
		}
		return type_dict
