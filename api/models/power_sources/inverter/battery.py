"""
	Module for interacting with the database
	# todo: module for charge map operations
	# to be used by other power sources
	# todo: check clashing times during
	# creation and editing of charge map.
"""
from uuid import uuid4

from api import db
from api.models.base import Base


class Battery(Base):
	"""docstring for Battery"""
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	battery_id = db.Column(db.String(100), unique=True, nullable=False)
	depth_of_discharge = db.Column(db.String(100), unique=False, nullable=True)
	number_of_cycles = db.Column(db.Float, unique=False, nullable=True)
	battery_capacity = db.Column(db.Float, unique=False, nullable=True)
	bank_id = db.Column(db.String(100), db.ForeignKey('battery_bank.bank_id'))

	def __init__(self, depth_of_discharge, number_of_cycles, battery_capacity):
		self.depth_of_discharge = depth_of_discharge
		self.number_of_cycles = number_of_cycles
		self.battery_capacity = battery_capacity
		self.battery_id = str(uuid4())

	@staticmethod
	def create(depth_of_discharge, number_of_cycles, battery_capacity):
		new_battery = Battery(
			depth_of_discharge=depth_of_discharge,
			number_of_cycles=number_of_cycles,
			battery_capacity=battery_capacity,
			)
		db.session.add(new_battery)
		return new_battery

	def edit(self, depth_of_discharge, number_of_cycles, battery_capacity):
		self.depth_of_discharge = depth_of_discharge
		self.number_of_cycles = number_of_cycles
		self.battery_capacity = battery_capacity
		db.session.add(self)
		return self.to_dict()

	@staticmethod
	def get_one(battery_id=None):
		battery = Battery.query.filter_by(battery_id=battery_id).first()
		return battery

	def delete(self):
		db.session.delete(self)
		db.session.commit()
		return

	@staticmethod
	def get_all():
		return [battery.to_dict() for battery in Battery.query.order_by(Battery.date_created.desc()).all()]

	def get_data(self):
		return self.to_dict()

	def life_dict(self):
		response = {
			'number_of_cycles': self.number_of_cycles,
			'depth_of_discharge': self.depth_of_discharge,
			'battery_capacity': self.battery_capacity,
		}
		return response

	def to_dict(self):
		response = {
			'battery_id': self.battery_id,
			'depth_of_discharge': self.depth_of_discharge,
			'number_of_cycles': self.number_of_cycles,
			'battery_capacity': self.battery_capacity,
			'date_created': str(self.date_created),
			'date_modified': str(self.date_modified),
		}
		return response
