"""
	Module for interacting with the database
	# todo: module for charge map operations
	# to be used by other power sources
	# todo: check clashing times during
	# creation and editing of charge map.
"""
from uuid import uuid4

from api import db
from api.models.base import Base
from api.models.power_sources.inverter.battery import Battery


class BatteryBank(Base):
	"""docstring for BatteryBank"""
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	bank_id = db.Column(db.String(100), unique=True, nullable=False)
	count = db.Column(db.Integer, unique=False, nullable=True)
	capacity = db.Column(db.String(100), unique=False, nullable=True)
	voltage = db.Column(db.Float, unique=False, nullable=True)
	bank_voltage = db.Column(db.Float, unique=False, nullable=True)
	depth_of_discharge = db.Column(db.Float, unique=False, nullable=True)
	chemistry = db.Column(db.String(100), unique=False, nullable=True)
	end_of_life = db.Column(db.String(100), unique=False, nullable=True)

	single_phase_inverter_id = db.Column(db.String(100), db.ForeignKey('single_phase_inverter.single_phase_inverter_id'))
	batteries = db.relationship('Battery', backref='bank', lazy='dynamic', cascade="all, delete-orphan")

	three_phase_inverter_id = db.Column(db.String(100), db.ForeignKey('three_phase_inverter.three_phase_inverter_id'))
	batteries = db.relationship('Battery', backref='bank', lazy='dynamic', cascade="all, delete-orphan")

	def __init__(self, count, capacity, voltage, bank_voltage, depth_of_discharge, chemistry, end_of_life):
		self.count = count
		self.capacity = capacity
		self.voltage = voltage
		self.bank_voltage = bank_voltage
		self.depth_of_discharge = depth_of_discharge
		self.chemistry = chemistry
		self.end_of_life = end_of_life
		self.bank_id = str(uuid4())

	@staticmethod
	def create(count, capacity, voltage, bank_voltage, depth_of_discharge, chemistry, end_of_life, meta):
		new_bank = BatteryBank(
			count=count,
			capacity=capacity,
			voltage=voltage,
			bank_voltage=bank_voltage,
			depth_of_discharge=depth_of_discharge,
			chemistry=chemistry,
			end_of_life=end_of_life,
			)
		db.session.add(new_bank)
		new_bank.new_batteries(meta)
		return new_bank

	def new_batteries(self, battery_list):
		for battery in battery_list:
			new_battery = Battery.create(**battery)
			if not new_battery in self.batteries:
				self.batteries.append(new_battery)

	def edit_batteries(self, battery_list):
		for battery in battery_list:
			battery_obj = Battery.get_one(battery['battery_id'])
			del battery['battery_id']
			battery_obj.edit(**battery)

	def edit(self, count, capacity, voltage, bank_voltage, depth_of_discharge, chemistry, end_of_life, meta):
		self.count = count
		self.capacity = capacity
		self.voltage = voltage
		self.bank_voltage = bank_voltage
		self.depth_of_discharge = depth_of_discharge
		self.chemistry = chemistry
		self.end_of_life = end_of_life
		self.edit_batteries(meta)
		db.session.add(self)
		return self

	@staticmethod
	def get_one(bank_id=None):
		battery = BatteryBank.query.filter_by(bank_id=bank_id).first()
		return battery

	@staticmethod
	def get_all():
		return [bank.to_dict() for bank in BatteryBank.query.order_by(BatteryBank.date_created.desc()).all()]

	def get_data(self):
		return self.to_dict()

	def get_battery_life(self):
		return [battery.life_dict() for battery in self.batteries]

	def to_dict(self):
		response = {
			'bank_id': self.bank_id,
			'capacity': self.capacity,
			'voltage': self.voltage,
			'depth_of_discharge': self.depth_of_discharge,
			'chemistry': self.chemistry,
			'end_of_life': self.end_of_life,
			'batteries': [battery.to_dict() for battery in self.batteries.all()],
			'date_created': str(self.date_created),
			'date_modified': str(self.date_modified),
		}
		return response
