"""
	Module for interacting with the database
"""
from uuid import uuid4

from api import db
from api.models import PhaseType
from api.models.base import PowerBase
from api.models.models import Channels, Configuration
from api.models.power_sources.common import OperatingWindow
from api.models.power_sources.inverter.battery_bank import BatteryBank


class SinglePhaseInverter(PowerBase):
	"""docstring for SinglePhaseInverter"""
	SOURCE_TYPE = 'Single Phase Inverter'
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=False, nullable=False)
	signal_threshold = db.Column(db.String(100), unique=False, nullable=True)
	single_phase_inverter_id = db.Column(db.String(100), unique=True, nullable=False)
	apparent_power_rating = db.Column(db.Float, unique=False, nullable=True)
	efficiency = db.Column(db.Float, unique=False, nullable=True)
	bulk_charging_current = db.Column(db.Float, unique=False, nullable=True)
	cost = db.Column(db.Float, unique=False, nullable=True)
	configuration_id = db.Column(db.String(100), db.ForeignKey('configuration.configuration_id'))
	input_channel_id = db.Column(db.String(100), db.ForeignKey('channels.channel_id'))
	output_channel_id = db.Column(db.String(100), db.ForeignKey('channels.channel_id'))
	input_channel = db.relationship("Channels", foreign_keys=[input_channel_id], backref='single_phase_input_channel')
	output_channel = db.relationship("Channels", foreign_keys=[output_channel_id], backref='single_phase_output_channel')
	battery_bank = db.relationship('BatteryBank', cascade="all, delete-orphan")
	phase_id = db.Column(db.String(100), db.ForeignKey('phase_type.phase_id'))

	def __init__(self, name, signal_threshold, apparent_power_rating, efficiency, bulk_charging_current,
		cost, input_channel, output_channel, configuration):
		self.name = name.title()
		self.signal_threshold = signal_threshold
		self.apparent_power_rating = apparent_power_rating
		self.efficiency = efficiency
		self.bulk_charging_current = bulk_charging_current
		self.cost = cost
		self.configuration = configuration
		self.input_channel_id = input_channel.assign_to_source().channel_id
		self.output_channel_id = output_channel.assign_to_source().channel_id
		# self.single_phase_input_channel.append(input_channel.assign_to_source())
		# self.single_phase_output_channel.append(output_channel.assign_to_source())
		self.single_phase_inverter_id = str(uuid4())
		self.phase_type = PhaseType.get_one_phase()

	@staticmethod
	def create(name, signal_threshold, apparent_power_rating, efficiency, bulk_charging_current,
		cost, input_channel_id, output_channel_id, configuration_id, operating_window, battery):
		new_inverter = SinglePhaseInverter(
			name=name,
			signal_threshold=signal_threshold,
			apparent_power_rating=apparent_power_rating,
			efficiency=efficiency,
			bulk_charging_current=bulk_charging_current,
			cost=cost,
			configuration=Configuration.get_configuration(configuration_id=configuration_id),
			input_channel=Channels.get_channel(channel_id=input_channel_id),
			output_channel=Channels.get_channel(channel_id=output_channel_id),
			)
		new_inverter.create_operating_window(operating_window)
		new_inverter.create_battery_bank(battery)
		db.session.add(new_inverter)
		db.session.commit()
		db.session.refresh(new_inverter)
		return new_inverter.to_dict()

	@staticmethod
	def migrate_old(name, single_phase_inverter_id, signal_threshold, apparent_power_rating, efficiency, bulk_charging_current,
		cost, input_channel_id, output_channel_id, configuration_id, operating_window, battery):
		inverter = SinglePhaseInverter.get_one(source_id=single_phase_inverter_id)
		if inverter:
			return inverter.to_dict()
		new_inverter = SinglePhaseInverter(
			name=name,
			signal_threshold=signal_threshold,
			apparent_power_rating=apparent_power_rating,
			efficiency=efficiency,
			bulk_charging_current=bulk_charging_current,
			cost=cost,
			configuration=Configuration.get_configuration(configuration_id=configuration_id),
			input_channel=Channels.get_channel(channel_id=input_channel_id),
			output_channel=Channels.get_channel(channel_id=output_channel_id),
			)
		new_inverter.single_phase_inverter_id = single_phase_inverter_id
		new_inverter.create_operating_window(operating_window)
		new_inverter.create_battery_bank(battery)
		db.session.add(new_inverter)
		db.session.commit()
		db.session.refresh(new_inverter)
		return new_inverter.to_dict()

	def create_operating_window(self, window_data):
		for window in window_data:
			operating_window = OperatingWindow.new(**window)
			if operating_window not in self.operating_window:
				self.operating_window.append(operating_window)

	def create_battery_bank(self, battery_data):
		for battery in battery_data:
			new_bank = BatteryBank.create(**battery)
			if new_bank not in self.battery_bank:
				self.battery_bank.append(new_bank)

	def get_window_ids(self):
		return [window.window_id for window in self.operating_window]

	def edit_operating_window(self, window_data):
		existing_windows = set(self.get_window_ids())
		edited_window_ids = set([window['window_id'] for window in window_data])
		deleted_windows = existing_windows.difference(edited_window_ids)
		for window_id in deleted_windows:
			window_obj = OperatingWindow.get_operating_window(window_id)
			if window_obj in self.operating_window:
				self.operating_window.remove(window_obj)
				db.session.add(self)
		for window in window_data:
			operating_window = OperatingWindow.get_operating_window(window['window_id'])
			if operating_window:
				del window['window_id']
				operating_window.edit(**window)

	def edit_battery_bank(self, battery_data):
		for battery in battery_data:
			bank_id = battery['bank_id']
			battery_obj = BatteryBank.get_one(bank_id=bank_id)
			del battery['bank_id']
			battery_obj.edit(**battery)

	def check_clashing_charges(self, added_charge_map):
		"""Method to check if two or more charges do not clash"""
		pass

	def edit(self, name, signal_threshold, apparent_power_rating, efficiency, bulk_charging_current,
		cost, input_channel_id, output_channel_id, configuration_id, operating_window,
		new_operating_window, battery):
		self.name = name
		self.signal_threshold = signal_threshold
		self.apparent_power_rating = apparent_power_rating
		self.efficiency = efficiency
		self.bulk_charging_current = bulk_charging_current
		self.cost = cost
		self.configuration = Configuration.get_configuration(configuration_id=configuration_id)
		if self.input_channel.channel_id != input_channel_id:
			self.input_channel.remove_from_source()
			self.input_channel = Channels.get_channel(channel_id=input_channel_id).assign_to_source()
		if self.output_channel.channel_id != output_channel_id:
			self.output_channel.remove_from_source()
			self.output_channel = Channels.get_channel(channel_id=output_channel_id).assign_to_source()
		self.edit_operating_window(operating_window)
		self.create_operating_window(new_operating_window)
		self.edit_battery_bank(battery)
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	@staticmethod
	def get_one(source_id=None, input_channel=None, output_channel=None):
		single_phase_inverter = None
		if source_id:
			single_phase_inverter = SinglePhaseInverter.query.filter_by(single_phase_inverter_id=source_id).first()
		elif input_channel:
			# single_phase_inverter = SinglePhaseInverter.query.filter_by(input_channel=input_channel).first()
			single_phase_inverter = SinglePhaseInverter.query.filter(SinglePhaseInverter.input_channel.has(Channels.channel_id == input_channel)).first()
			print(f'\n\n\nsingle_phase_inverters = {single_phase_inverter}')
		elif output_channel:
			# single_phase_inverter = SinglePhaseInverter.query.filter_by(output_channel=output_channel).first()
			single_phase_inverter = SinglePhaseInverter.query.filter(SinglePhaseInverter.output_channel.has(Channels.channel_id == output_channel)).first()
			print(f'\n\n\nsingle_phase_inverters = {single_phase_inverter}')
		return single_phase_inverter

	@staticmethod
	def view_all():
		all_single_phase_inverters = SinglePhaseInverter.query.order_by(SinglePhaseInverter.name.asc()).all()
		if all_single_phase_inverters:
			data = [single_phase_inverter.to_dict() for single_phase_inverter in all_single_phase_inverters]
			return data
		return []

	def unassign_all_channels(self):
		self.input_channel.remove_from_source()
		self.output_channel.remove_from_source()
		return self

	def delete(self):
		inverter = self.unassign_all_channels()
		db.session.delete(inverter)
		db.session.commit()
		return

	def device_channels(self):
		channel_list = [
			self.input_channel,
			self.output_channel,
		]
		return channel_list

	def error_threshold(self):
		channel_list = [channel.channel_threshold_current for channel in self.device_channels()]
		return channel_list

	def probe_list(self):
		"""Return a list of probes used to create this device"""
		probe_list = [channel.probe for channel in self.device_channels()]
		probe_list = list(set(probe_list))
		return probe_list

	@staticmethod
	def get_all():
		return [inverter.to_dict() for inverter in SinglePhaseInverter.query.order_by(SinglePhaseInverter.date_created.desc()).all()]

	def get_data(self):
		return self.to_dict()

	def get_probe_names(self):
		return [probe.probe_name for probe in self.probe_list()]

	def get_probe_ids(self):
		return [probe.probe_id for probe in self.probe_list()]

	def device_config_dict(self):
		response = {
			"SignalThreshold": self.signal_threshold,
			"Rating": self.apparent_power_rating,
			"eThreshold": self.error_threshold(),
			"batteryChemistry": [bank.chemistry for bank in self.battery_bank],
			"chargeCurrent": self.bulk_charging_current,
			"batteryLife": [bank.get_battery_life() for bank in self.battery_bank],
			"ProbeID_FK": self.get_probe_ids(),
			"depthOfDischarge": [bank.depth_of_discharge for bank in self.battery_bank],
			"batteryCapacity": [bank.capacity for bank in self.battery_bank],
			"batteryBankVoltage": [bank.bank_voltage for bank in self.battery_bank],
			"Efficiency": self.efficiency,
			"sourceName": self.name,
			"NumberOfPhases": self.phase_type.num_id,
			"_id": self.single_phase_inverter_id,
			"cost": self.cost,
			"operatingWindow": [operating_window.to_dict() for operating_window in self.operating_window],
			"batteryCount": [len(bank.batteries.all()) for bank in self.battery_bank],
			"batteryEndOfLife": [bank.end_of_life for bank in self.battery_bank],
			"State": self.state,
			"MaxChannels": len(self.device_channels()),
			"batteryVoltage": [bank.bank_voltage for bank in self.battery_bank],
			"Type": "inverter",
		}
		return response

	def to_dict(self):
		type_dict = {
			'source_type': self.SOURCE_TYPE,
			'name': self.name,
			'installed': self.installed,
			'meter_id': self.single_phase_inverter_id,
			'meter_url': f"{self.SOURCE_TYPE.lower().replace(' ', '-')}/{self.single_phase_inverter_id}",
			'apparent_power_rating': self.apparent_power_rating,
			'efficiency': self.efficiency,
			'bulk_charging_current': self.bulk_charging_current,
			'cost': self.cost,
			'signal_threshold': self.signal_threshold,
			'date_created': str(self.date_created),
			'date_modified': str(self.date_modified),
			'input_channel': [self.input_channel.small_dict()] if self.input_channel else None,
			'output_channel': [self.output_channel.small_dict()] if self.output_channel else None,
			'probe_names': self.get_probe_names(),
			'operating_window': [operating_window.to_dict() for operating_window in self.operating_window],
			'battery_bank': [bank.to_dict() for bank in self.battery_bank] if self.battery_bank else None,
			'configuration': self.configuration.small_dict() if self.configuration else None,
		}
		return type_dict
