"""
	Module for interacting with the database
"""
from collections import namedtuple
from uuid import uuid4

from api import db
from api.models import PhaseType
from api.models.base import PowerBase
from api.models.models import Channels, Configuration
from api.models.power_sources.common import OperatingWindow
from api.models.power_sources.inverter.battery_bank import BatteryBank


class ThreePhaseInverter(PowerBase):
	"""docstring for ThreePhaseInverter"""
	SOURCE_TYPE = 'Three Phase Inverter'
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=False, nullable=False)
	signal_threshold = db.Column(db.String(100), unique=False, nullable=True)
	three_phase_inverter_id = db.Column(db.String(100), unique=True, nullable=False)
	apparent_power_rating = db.Column(db.Float, unique=False, nullable=True)
	efficiency = db.Column(db.Float, unique=False, nullable=True)
	bulk_charging_current = db.Column(db.Float, unique=False, nullable=True)
	cost = db.Column(db.Float, unique=False, nullable=True)
	configuration_id = db.Column(db.String(100), db.ForeignKey('configuration.configuration_id'))

	input_channel1_id = db.Column(db.String(100), db.ForeignKey('channels.channel_id'))
	input_channel2_id = db.Column(db.String(100), db.ForeignKey('channels.channel_id'))
	input_channel3_id = db.Column(db.String(100), db.ForeignKey('channels.channel_id'))

	output_channel1_id = db.Column(db.String(100), db.ForeignKey('channels.channel_id'))
	output_channel2_id = db.Column(db.String(100), db.ForeignKey('channels.channel_id'))
	output_channel3_id = db.Column(db.String(100), db.ForeignKey('channels.channel_id'))

	input_channel1 = db.relationship("Channels", foreign_keys=[input_channel1_id], backref='three_phase_input_channel1')
	input_channel2 = db.relationship("Channels", foreign_keys=[input_channel2_id], backref='three_phase_input_channel2')
	input_channel3 = db.relationship("Channels", foreign_keys=[input_channel3_id], backref='three_phase_input_channel3')

	output_channel1 = db.relationship("Channels", foreign_keys=[output_channel1_id], backref='three_phase_output_channel1')
	output_channel2 = db.relationship("Channels", foreign_keys=[output_channel2_id], backref='three_phase_output_channel2')
	output_channel3 = db.relationship("Channels", foreign_keys=[output_channel3_id], backref='three_phase_output_channel3')

	battery_bank = db.relationship('BatteryBank', cascade="all, delete-orphan")
	phase_id = db.Column(db.String(100), db.ForeignKey('phase_type.phase_id'))

	def __init__(self, name, signal_threshold, apparent_power_rating, efficiency, bulk_charging_current,
		cost, input_channels, output_channels, configuration):
		self.name = name.title()
		self.signal_threshold = signal_threshold
		self.apparent_power_rating = apparent_power_rating
		self.efficiency = efficiency
		self.bulk_charging_current = bulk_charging_current
		self.cost = cost
		self.configuration = configuration
		self.input_channel1_id = input_channels.channel1.assign_to_source().channel_id
		self.input_channel2_id = input_channels.channel2.assign_to_source().channel_id
		self.input_channel3_id = input_channels.channel3.assign_to_source().channel_id

		self.output_channel1_id = output_channels.channel1.assign_to_source().channel_id
		self.output_channel2_id = output_channels.channel2.assign_to_source().channel_id
		self.output_channel3_id = output_channels.channel3.assign_to_source().channel_id
		self.three_phase_inverter_id = str(uuid4())
		self.phase_type = PhaseType.get_three_phase()

	@staticmethod
	def create(name, signal_threshold, apparent_power_rating, efficiency, bulk_charging_current,
		cost, input_channels, output_channels, configuration_id, operating_window, battery):
		Input_Channels = namedtuple('Input_Channels', [
			'channel1',
			'channel2',
			'channel3',
		])

		Output_Channels = namedtuple('Output_Channels', [
			'channel1',
			'channel2',
			'channel3',
		])
		InputChannel = Input_Channels(
			channel1=Channels.get_channel(channel_id=input_channels['input_channel1']),
			channel2=Channels.get_channel(channel_id=input_channels['input_channel2']),
			channel3=Channels.get_channel(channel_id=input_channels['input_channel3']),
		)
		OutputChannel = Output_Channels(
			channel1=Channels.get_channel(channel_id=output_channels['output_channel1']),
			channel2=Channels.get_channel(channel_id=output_channels['output_channel2']),
			channel3=Channels.get_channel(channel_id=output_channels['output_channel3']),
		)
		new_inverter = ThreePhaseInverter(
			name=name,
			signal_threshold=signal_threshold,
			apparent_power_rating=apparent_power_rating,
			efficiency=efficiency,
			bulk_charging_current=bulk_charging_current,
			cost=cost,
			configuration=Configuration.get_configuration(configuration_id=configuration_id),
			input_channels=InputChannel,
			output_channels=OutputChannel,
			)
		new_inverter.create_operating_window(operating_window)
		new_inverter.create_battery_bank(battery)
		db.session.add(new_inverter)
		db.session.commit()
		db.session.refresh(new_inverter)
		return new_inverter.to_dict()

	def create_operating_window(self, window_data):
		for window in window_data:
			operating_window = OperatingWindow.new(**window)
			if operating_window not in self.operating_window:
				self.operating_window.append(operating_window)

	def create_battery_bank(self, battery_data):
		for battery in battery_data:
			new_bank = BatteryBank.create(**battery)
			if new_bank not in self.battery_bank:
				self.battery_bank.append(new_bank)

	def get_window_ids(self):
		return [window.window_id for window in self.operating_window]

	def edit_operating_window(self, window_data):
		existing_windows = set(self.get_window_ids())
		edited_window_ids = set([window['window_id'] for window in window_data])
		deleted_windows = existing_windows.difference(edited_window_ids)
		for window_id in deleted_windows:
			window_obj = OperatingWindow.get_operating_window(window_id)
			if window_obj in self.operating_window:
				self.operating_window.remove(window_obj)
				db.session.add(self)
		for window in window_data:
			operating_window = OperatingWindow.get_operating_window(window['window_id'])
			if operating_window:
				del window['window_id']
				operating_window.edit(**window)

	def edit_battery_bank(self, battery_data):
		for battery in battery_data:
			bank_id = battery['bank_id']
			battery_obj = BatteryBank.get_one(bank_id=bank_id)
			del battery['bank_id']
			battery_obj.edit(**battery)

	def check_clashing_charges(self, added_charge_map):
		"""Method to check if two or more charges do not clash"""
		pass

	def edit_channels(self, input_channel_dict, output_channel_dict):
		if self.input_channel1.channel_id != input_channel_dict['input_channel1']:
			self.input_channel1.remove_from_source()
			self.input_channel1 = Channels.get_channel(channel_id=input_channel_dict['input_channel1']).assign_to_source()
		if self.input_channel2.channel_id != input_channel_dict['input_channel2']:
			self.input_channel2.remove_from_source()
			self.input_channel2 = Channels.get_channel(channel_id=input_channel_dict['input_channel2']).assign_to_source()
		if self.input_channel3.channel_id != input_channel_dict['input_channel3']:
			self.input_channel3.remove_from_source()
			self.input_channel3 = Channels.get_channel(channel_id=input_channel_dict['input_channel3']).assign_to_source()

		if self.output_channel1.channel_id != output_channel_dict['output_channel1']:
			self.output_channel1.remove_from_source()
			self.output_channel1 = Channels.get_channel(channel_id=output_channel_dict['output_channel1']).assign_to_source()
		if self.output_channel2.channel_id != output_channel_dict['output_channel2']:
			self.output_channel2.remove_from_source()
			self.output_channel2 = Channels.get_channel(channel_id=output_channel_dict['output_channel2']).assign_to_source()
		if self.output_channel3.channel_id != output_channel_dict['output_channel3']:
			self.output_channel3.remove_from_source()
			self.output_channel3 = Channels.get_channel(channel_id=output_channel_dict['output_channel3']).assign_to_source()

	def edit(self, name, signal_threshold, apparent_power_rating, efficiency, bulk_charging_current,
		cost, input_channels, output_channels, configuration_id, operating_window,
		new_operating_window, battery):
		self.name = name
		self.signal_threshold = signal_threshold
		self.apparent_power_rating = apparent_power_rating
		self.efficiency = efficiency
		self.bulk_charging_current = bulk_charging_current
		self.cost = cost
		self.configuration = Configuration.get_configuration(configuration_id=configuration_id)

		self.edit_channels(input_channels, output_channels)
		self.edit_operating_window(operating_window)
		self.create_operating_window(new_operating_window)
		self.edit_battery_bank(battery)
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	@staticmethod
	def get_one(source_id):
		three_phase_inverter = ThreePhaseInverter.query.filter_by(three_phase_inverter_id=source_id).first()
		return three_phase_inverter

	@staticmethod
	def view_all():
		all_three_phase_inverters = ThreePhaseInverter.query.order_by(ThreePhaseInverter.name.asc()).all()
		if all_three_phase_inverters:
			data = [three_phase_inverter.to_dict() for three_phase_inverter in all_three_phase_inverters]
			return data
		return []

	def unassign_all_channels(self):
		self.input_channel1.remove_from_source()
		self.input_channel2.remove_from_source()
		self.input_channel3.remove_from_source()

		self.output_channel1.remove_from_source()
		self.output_channel2.remove_from_source()
		self.output_channel3.remove_from_source()
		return self

	def delete(self):
		inverter = self.unassign_all_channels()
		db.session.delete(inverter)
		db.session.commit()
		return

	def device_channels(self):
		channel_list = [
			self.input_channel1,
			self.input_channel2,
			self.input_channel3,
			self.output_channel1,
			self.output_channel2,
			self.output_channel3
		]
		return channel_list

	def error_threshold(self):
		channel_list = [channel.channel_threshold_current for channel in self.device_channels()]
		return channel_list

	def probe_list(self):
		"""Return a list of probes used to create this device"""
		probe_list = [
			self.input_channel1.probe,
			self.input_channel2.probe,
			self.input_channel3.probe,
			self.output_channel1.probe,
			self.output_channel2.probe,
			self.output_channel3.probe
		]
		probe_list = list(set(probe_list))
		return probe_list

	@staticmethod
	def get_all():
		return [inverter.to_dict() for inverter in ThreePhaseInverter.query.order_by(ThreePhaseInverter.date_created.desc()).all()]

	def get_data(self):
		return self.to_dict()

	def get_probe_ids(self):
		return [probe.probe_id for probe in self.probe_list()]

	def device_config_dict(self):
		response = {
			"SignalThreshold": self.signal_threshold,
			"Rating": self.apparent_power_rating,
			"eThreshold": self.error_threshold(),
			"batteryChemistry": [bank.chemistry for bank in self.battery_bank],
			"chargeCurrent": self.bulk_charging_current,
			"batteryLife": [bank.get_battery_life() for bank in self.battery_bank],
			"ProbeID_FK": self.get_probe_ids(),
			"depthOfDischarge": [bank.depth_of_discharge for bank in self.battery_bank],
			"batteryCapacity": [bank.capacity for bank in self.battery_bank],
			"batteryBankVoltage": [bank.bank_voltage for bank in self.battery_bank],
			"Efficiency": self.efficiency,
			"sourceName": self.name,
			"NumberOfPhases": self.phase_type.num_id,
			"_id": self.three_phase_inverter_id,
			"cost": self.cost,
			"operatingWindow": [operating_window.to_dict() for operating_window in self.operating_window],
			"batteryCount": [len(bank.batteries.all()) for bank in self.battery_bank],
			"batteryEndOfLife": [bank.end_of_life for bank in self.battery_bank],
			"State": self.state,
			"MaxChannels": len(self.device_channels()),
			"batteryVoltage": [bank.bank_voltage for bank in self.battery_bank],
			"Type": "inverter",
		}
		return response

	def to_dict(self):
		type_dict = {
			'source_type': self.SOURCE_TYPE,
			'name': self.name,
			'installed': self.installed,
			'meter_id': self.three_phase_inverter_id,
			'meter_url': f"{self.SOURCE_TYPE.lower().replace(' ', '-')}/{self.three_phase_inverter_id}",
			'signal_threshold': self.signal_threshold,
			'apparent_power_rating': self.apparent_power_rating,
			'efficiency': self.efficiency,
			'bulk_charging_current': self.bulk_charging_current,
			'cost': self.cost,
			'date_created': str(self.date_created),
			'date_modified': str(self.date_modified),
			'phase_one': {
				'input_channel': [self.input_channel1.small_dict()] if self.input_channel1 else None,
				'output_channel': [self.output_channel1.small_dict()] if self.output_channel1 else None,
				},
			'phase_two': {
				'input_channel': [self.input_channel2.small_dict()] if self.input_channel2 else None,
				'output_channel': [self.output_channel2.small_dict()] if self.output_channel2 else None,
				},
			'phase_three': {
				'input_channel': [self.input_channel3.small_dict()] if self.input_channel3 else None,
				'output_channel': [self.output_channel3.small_dict()] if self.output_channel3 else None,
				},
			'probe_names': list(set([probe.probe_name for probe in self.probe_list()])),
			'operating_window': [operating_window.to_dict() for operating_window in self.operating_window],
			'battery_bank': [bank.to_dict() for bank in self.battery_bank] if self.battery_bank else None,
			'configuration': self.configuration.small_dict() if self.configuration else None,
		}
		return type_dict
