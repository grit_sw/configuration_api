from flask import current_app

from api.models import Channels


def send_config(message):
	producer = current_app.kafka_producer
	topic = current_app.config.get('CONFIGURATION_TOPIC')
	producer.send_message(topic, message, use_avro=False)

def power_source_config(probe_channels):
	resp = []
	for channel in probe_channels:
		conf = channel.source_config_dict()
		if conf and conf not in resp:
			resp.append(conf)
	return resp


def create_probe_config(probe):
	probe_channels = probe.channels.order_by(Channels.id.asc()).all()
	configurations = probe.configurations
	config_dictionary = {}
	config_dictionary['topic'] = 'configuration'
	config_dictionary['configuration'] = {}
	config_dictionary['configuration']['Wifi_ssid'] = probe.wifi_network_name
	config_dictionary['configuration']['Wifi_password'] = probe.wifi_password
	config_dictionary['configuration']['powerSources'] = power_source_config(probe_channels)
	config_dictionary['configuration']['configuration'] = [conf.device_config_dict() for conf in configurations]
	config_dictionary['configuration']['PowerFactor'] = [channel.power_factor for channel in probe_channels]
	config_dictionary['configuration']['DefaultVoltage'] = [channel.default_voltage for channel in probe_channels]
	config_dictionary['configuration']['channels'] = [channel.device_config_dict(i) for i, channel in enumerate(probe_channels)]
	config_dictionary['configuration']['scalingFactorVoltage'] = [channel.scaling_factor_voltage for channel in probe_channels]
	config_dictionary['configuration']['scalingFactorCurrent'] = [channel.scaling_factor_current for channel in probe_channels]
	config_dictionary['configuration']['probeName'] = probe.probe_name
	config_dictionary['configuration']['commDevice'] = ""  # todo: comm device model
	return config_dictionary

def create_channel_config(channel):
	source = channel.get_source_obj()
	config_data = []
	if source:
		probes = source.get_probes()
		for probe in probes:
			data = create_probe_config(probe)
			config_data.append(data)
	return config_data


class Save:
	@staticmethod
	def save_probe(probe=None, channel=None):
		config_data = []
		if probe:
			config_data = create_probe_config(probe)
			send_config(config_data)
		else:
			config_data = create_channel_config(channel)
			for config in config_data:
				send_config(config)
		return config_data
