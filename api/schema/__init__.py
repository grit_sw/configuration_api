# Prosumer
from api.schema.prosumer import (
	EnergyPlanSchema,
	ProsumerParamsSchema,
	AssignProsumerSchema,
	ProsumerSchema,
	NewProsumerSchema,
)

# Prosumer Group
from api.schema.prosumer_group import (
	ProsumerGroupSchema,
	ProsumerToProsumerGroupSchema,
	ClientAdminProsumerGroupSchema,
	RemoveProsumerFromProsumerGroupSchema,
	RemoveClientAdminProsumerGroupSchema,
)

# Prosumer type
from api.schema.prosumer_type import ProsumerTypeSchema

# Prosumer class
from api.schema.prosumer_class import ProsumerClassSchema

# Probe
from api.schema.probe_schema import NewProbeSchema, EditProbeSchema

# Meter type
from api.schema.device_type import DeviceTypeSchema

# Sensor
from api.schema.voltage_sensor import VoltageSensorSchema
from api.schema.current_sensor import CurrentSensorSchema

# Configuration
from api.schema.configuration import NewConfigurationSchema

# Configuration group
from api.schema.configuration_group import NewConfigurationGroupSchema

# Grid
from api.schema.power_sources.grid.one_phase import SinglePhaseGridSchema, EditSinglePhaseGridSchema
from api.schema.power_sources.grid.three_phase import ThreePhaseGridSchema, EditThreePhaseGridSchema

# Generator
from api.schema.power_sources.generator.one_phase import SinglePhaseGeneratorSchema, EditSinglePhaseGeneratorSchema
from api.schema.power_sources.generator.three_phase import ThreePhaseGeneratorSchema, EditThreePhaseGeneratorSchema

# Inverter
from api.schema.power_sources.inverter.one_phase import SinglePhaseInverterSchema, EditSinglePhaseInverterSchema
from api.schema.power_sources.inverter.three_phase import ThreePhaseInverterSchema, EditThreePhaseInverterSchema

# Fuel
from api.schema.fuel import NewFuelSchema, EditFuelSchema

# Consumer group
from api.schema.power_consumers.consumer_group import NewConsumerGroupSchema, EditConsumerGroupSchema
from api.schema.power_consumers.consumer_group_type import NewConsumerGroupTypeSchema, EditConsumerGroupTypeSchema

# Consumer line
from api.schema.power_consumers.consumer_line.one_phase import NewSinglePhaseConsumerLineSchema, EditSinglePhaseConsumerLineSchema
from api.schema.power_consumers.consumer_line.three_phase import NewThreePhaseConsumerLineSchema, EditThreePhaseConsumerLineSchema

# Single and three phase schema
from api.schema.power_consumers.consumer.one_phase import NewSinglePhaseConsumerSchema, EditSinglePhaseConsumerSchema
from api.schema.power_consumers.consumer.three_phase import NewThreePhaseConsumerSchema, EditThreePhaseConsumerSchema
