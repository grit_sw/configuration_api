from collections import namedtuple

from marshmallow import Schema, ValidationError, fields, post_load, validates

from api.models import Probe, Channels


NewConfiguration = namedtuple('NewConfiguration', [
	'configuration_name',
	'error_threshold',
	'display_energy_today',
	'configuration_user',
	'configuration_members',
	'probe_channels',
])

ProbeChannel = namedtuple('ProbeChannel', [
	'probe_id',
	'channel_ids',
])

ConfigurationMember = namedtuple('ConfigurationMember', [
	'member_id',
	'member_name',
])

ConfigurationUser = namedtuple('ConfigurationUser', [
	'configuration_user_id',
	'configuration_user_name',
])


class ConfigUserSchema(Schema):
	configuration_user_id = fields.String(required=True)
	configuration_user_name = fields.String(required=True)

	@post_load
	def create_config(self, data):
		return ConfigurationUser(**data)

	@validates('configuration_user_id')
	def validate_configuration_user_id(self, value):
		if not value:
			raise ValidationError('Configuration user ID required.')

	@validates('configuration_user_name')
	def validate_configuration_user_name(self, value):
		if not value:
			raise ValidationError('Configuration user name required.')


class ConfigMemberSchema(Schema):
	member_id = fields.String(required=True)
	member_name = fields.String(required=True)

	@post_load
	def create_config(self, data):
		return ConfigurationMember(**data)

	@validates('member_id')
	def validate_member_id(self, value):
		if not value:
			raise ValidationError('Configuration member ID required.')

	@validates('member_name')
	def validate_member_name(self, value):
		if not value:
			raise ValidationError('Configuration member name required.')


class ProbeChannelSchema(Schema):
	probe_id = fields.String(required=True)
	channel_ids = fields.List(fields.String(), required=True)

	@post_load
	def create_config(self, data):
		return ProbeChannel(**data)

	@validates('probe_id')
	def validate_probe_id(self, value):
		if not Probe.get_probe(probe_id=value):
			raise ValidationError('Probe ID required.')

	@validates('channel_ids')
	def validate_channel_ids(self, value):
		if not isinstance(value, list):
			raise ValidationError('List of channel ID(s) required.')
		for channel_id in value:
			if not Channels.get_channel(channel_id=channel_id):
				raise ValidationError('Invalid channel found.')


class NewConfigurationSchema(Schema):
	configuration_name = fields.String(required=True)
	error_threshold = fields.Float(required=True, allow_none=True)
	display_energy_today = fields.Boolean(required=True, allow_none=True)
	configuration_user = fields.Nested('ConfigUserSchema', required=True)
	configuration_members = fields.Nested('ConfigMemberSchema', many=True, strict=True)
	probe_channels = fields.Nested('ProbeChannelSchema', many=True, strict=True)

	@post_load
	def create_config(self, data):
		return NewConfiguration(**data)

	@validates('configuration_name')
	def validate_configuration_name(self, value):
		if not value:
			raise ValidationError('Configuration name required.')

	@validates('error_threshold')
	def validate_error_threshold(self, value):
		if not isinstance(value, float):
			raise ValidationError('Error threshold required.')
		if value < 0:
			raise ValidationError('Invalid error threshold.')

