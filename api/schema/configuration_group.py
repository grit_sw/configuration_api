from collections import namedtuple

from marshmallow import Schema, ValidationError, fields, post_load, validates

from api.models import Configuration, ConfigurationMember


NewConfigurationGroup = namedtuple('NewConfigurationGroup', [
	'name',
	'admin_id',
	'source_config_id',
	'profit_margin',
	'group_members',
])


class NewConfigurationGroupSchema(Schema):
	name = fields.String(required=True)
	admin_id = fields.String(required=True)
	source_config_id = fields.String(required=True)
	profit_margin = fields.Float(required=True)
	group_members = fields.List(fields.String(), required=True)

	@post_load
	def create_config(self, data):
		return NewConfigurationGroup(**data)

	@validates('name')
	def validate_name(self, value):
		if not value:
			raise ValidationError('Configuration group name required.')

	@validates('admin_id')
	def validate_admin_id(self, value):
		if not Configuration.get_user(user_id=value):
			if not ConfigurationMember.get_member(member_id=value):
				raise ValidationError('Invalid user selected.')

	@validates('source_config_id')
	def validate_source_config_id(self, value):
		if not value:
			raise ValidationError('Source configuration required.')

	@validates('profit_margin')
	def validate_profit_margin(self, value):
		if not isinstance(value, float):
			raise ValidationError('Profit margin.')
		if value < 0:
			raise ValidationError('Invalid profit margin.')

	@validates('group_members')
	def validate_group_members(self, value):
		if not isinstance(value, list):
			raise ValidationError('List of group member ID(s) required.')
		for configuration_id in value:
			if not Configuration.get_configuration(configuration_id=configuration_id):
				raise ValidationError('Invalid configuration found.')
