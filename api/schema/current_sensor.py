from collections import namedtuple

from marshmallow import Schema, ValidationError, fields, post_load, validates


CurrentSensor = namedtuple('CurrentSensor', [
	'name',
	'rating',
	'scaling_factor_current',
])


class CurrentSensorSchema(Schema):
	name = fields.String(required=True)
	rating = fields.Float(required=True)
	scaling_factor_current = fields.Float(required=True)

	@post_load
	def edit_sensor_type(self, data):
		return CurrentSensor(**data)

	@validates('name')
	def validate_name(self, value):
		if not value:
			raise ValidationError('Current sensor name required.')
