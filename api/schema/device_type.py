from collections import namedtuple

from marshmallow import Schema, ValidationError, fields, post_load, validates

from api.models import DeviceType

DeviceType = namedtuple('DeviceType', [
	'name',
	'number_of_channels',
])


class DeviceTypeSchema(Schema):
	name = fields.String(required=True)
	number_of_channels = fields.Integer(required=True)

	@post_load
	def edit_device_type(self, data):
		return DeviceType(**data)

	@validates('name')
	def validate_name(self, value):
		if not value:
			raise ValidationError('Device type name required.')

	@validates('number_of_channels')
	def validate_number_of_channels(self, value):
		if not isinstance(value, int):
			raise ValidationError('Number of channels required.')
