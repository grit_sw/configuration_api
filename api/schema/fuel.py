from collections import namedtuple
from marshmallow import Schema, ValidationError, fields, post_load, validates


EditFuel = namedtuple('EditFuel', [
	'fuel_id',
	'name',
	'unit_price',
])

NewFuel = namedtuple('NewFuel', [
	'name',
	'unit_price',
])


class NewFuelSchema(Schema):
	name = fields.String(required=True)
	unit_price = fields.Float(required=True)

	@post_load
	def create_config(self, data):
		return NewFuel(**data)

	@validates('name')
	def validate_name(self, value):
		if not value:
			raise ValidationError('A fuel name is required.')


class EditFuelSchema(Schema):
	fuel_id = fields.String(required=False)
	name = fields.String(required=True)
	unit_price = fields.Float(required=True)

	@post_load
	def create_config(self, data):
		return EditFuel(**data)

	@validates('name')
	def validate_name(self, value):
		if not value:
			raise ValidationError('A fuel name is required.')
