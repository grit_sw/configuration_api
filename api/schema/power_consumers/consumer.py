from collections import namedtuple
from marshmallow import Schema, fields, post_load, validates, ValidationError
from api.models import SinglePhaseConsumerLine

# todo: validate consumer line

NewSinglePhaseConsumer = namedtuple('NewSinglePhaseConsumer', [
	'name',
	'line_id',
])


class NewSinglePhaseConsumerSchema(Schema):
	name = fields.String(required=True)
	line_id = fields.String(required=True)

	@post_load
	def create_consumer(self, data):
		return NewSinglePhaseConsumer(**data)

	@validates('line_id')
	def validate_line_id(self, value):
		line = SinglePhaseConsumerLine.get_one(line_id=value)
		if not line:
			raise ValidationError('Invalid consumer line.')


EditSinglePhaseConsumerSchema = NewSinglePhaseConsumerSchema
