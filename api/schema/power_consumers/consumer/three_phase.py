from collections import namedtuple
from marshmallow import Schema, fields, post_load, validates, ValidationError
from api.models import ThreePhaseConsumerLine

# todo: validate consumer line

NewThreePhaseConsumer = namedtuple('NewThreePhaseConsumer', [
	'name',
	'line_id',
])


class NewThreePhaseConsumerSchema(Schema):
	name = fields.String(required=True)
	line_id = fields.String(required=True)

	@post_load
	def create_consumer(self, data):
		return NewThreePhaseConsumer(**data)

	@validates('line_id')
	def validate_line_id(self, value):
		line = ThreePhaseConsumerLine.get_one(line_id=value)
		if not line:
			raise ValidationError('Invalid consumer line.')


EditThreePhaseConsumerSchema = NewThreePhaseConsumerSchema
