from collections import namedtuple
from marshmallow import Schema, fields, post_load


NewConsumerGroup = namedtuple('NewConsumerGroup', [
	'name',
	'configuration_id',
	'consumer_group_type_id',
])


class NewConsumerGroupSchema(Schema):
	name = fields.String(required=True)
	configuration_id = fields.String(required=True)
	consumer_group_type_id = fields.String(required=True)

	@post_load
	def create_battery(self, data):
		return NewConsumerGroup(**data)


EditConsumerGroupSchema = NewConsumerGroupSchema
