from collections import namedtuple
from marshmallow import Schema, fields, post_load


NewConsumerGroupType = namedtuple('NewConsumerGroupType', [
	'name',
])


class NewConsumerGroupTypeSchema(Schema):
	name = fields.String(required=True)

	@post_load
	def create_battery(self, data):
		return NewConsumerGroupType(**data)


EditConsumerGroupTypeSchema = NewConsumerGroupTypeSchema
