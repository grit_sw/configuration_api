from collections import namedtuple
from marshmallow import Schema, fields, post_load, validates, ValidationError
from api.models import Channels


NewSinglePhaseConsumerLine = namedtuple('NewSinglePhaseConsumerLine', [
	'name',
	'signal_threshold',
	'consumer_group_id',
	'channel_id',
])


class NewSinglePhaseConsumerLineSchema(Schema):
	name = fields.String(required=True)
	signal_threshold = fields.Float(required=False)
	consumer_group_id = fields.String(required=True)
	channel_id = fields.String(required=True)

	@post_load
	def create_battery(self, data):
		return NewSinglePhaseConsumerLine(**data)

	@validates('channel_id')
	def validate_channel_id(self, value):
		channel = Channels.get_channel(channel_id=value)
		if not channel:
			raise ValidationError('Invalid probe channel.')
		if channel.reserved:
			source = channel.get_source()[0]
			raise ValidationError(f"Channel already in use by {source['name']}.")


EditSinglePhaseConsumerLineSchema = NewSinglePhaseConsumerLineSchema
