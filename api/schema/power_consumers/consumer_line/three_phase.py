from collections import namedtuple
from marshmallow import Schema, fields, post_load, ValidationError, validates
from api.models import Channels


NewThreePhaseConsumerLine = namedtuple('NewThreePhaseConsumerLine', [
	'name',
	'signal_threshold',
	'consumer_group_id',
	'channels',
])


class NewThreePhaseConsumerLineSchema(Schema):
	name = fields.String(required=True)
	signal_threshold = fields.Float(required=False)
	consumer_group_id = fields.String(required=True)
	channels = fields.List(fields.String(required=True))

	@post_load
	def create_battery(self, data):
		return NewThreePhaseConsumerLine(**data)

	@validates('channels')
	def validate_channels(self, value):
		if not isinstance(value, list):
			raise ValidationError('Probe channels required.')
		if len(value) != 3:
			raise ValidationError(f'3 channels expected. Got {len(value)}.')
		for channel_id in value:
			channel = Channels.get_channel(channel_id=channel_id)
			if not channel:
				raise ValidationError('Invalid probe channel.')
			if channel.reserved:
				source = channel.get_source()[0]
				raise ValidationError(f"Channel already in use by {source['name']}.")


EditThreePhaseConsumerLineSchema = NewThreePhaseConsumerLineSchema
