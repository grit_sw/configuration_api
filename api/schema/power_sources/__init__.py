from api.schema.power_sources.common.battery import (EditBatterySchema,
                                                     NewBatterySchema)
from api.schema.power_sources.common.battery_meta import (EditBatteryMetaSchema,
                                                          NewBatteryMetaSchema)
from api.schema.power_sources.common.charge import (EditChargeSchema,
                                                    NewChargeSchema)
from api.schema.power_sources.common.operating_window import (EditWindowSchema,
                                                              NewWindowSchema)
from api.schema.power_sources.common.three_phase_channels import (InputThreePhaseChannelSchema,
                                                                  OutputThreePhaseChannelSchema)
from api.schema.power_sources.generator.generator_map import (EditGeneratorMapSchema,
                                                              NewGeneratorMapSchema)
