from collections import namedtuple
from marshmallow import Schema, fields, post_load


EditBatteryBank = namedtuple('EditBatteryBank', [
	'bank_id',
	'capacity',
	'count',
	'voltage',
	'bank_voltage',
	'depth_of_discharge',
	'chemistry',
	'end_of_life',
	'meta',
])

NewBattery = namedtuple('NewBattery', [
	'capacity',
	'count',
	'voltage',
	'bank_voltage',
	'depth_of_discharge',
	'chemistry',
	'end_of_life',
	'meta',
])


class NewBatterySchema(Schema):
	capacity = fields.Float(required=False)
	count = fields.Integer(required=False)
	voltage = fields.Float(required=False)
	bank_voltage = fields.Float(required=False)
	depth_of_discharge = fields.Float(required=False)
	chemistry = fields.String(required=False)
	end_of_life = fields.Float(required=False)
	meta = fields.Nested('NewBatteryMetaSchema', many=True, strict=True)

	@post_load
	def create_battery(self, data):
		return NewBattery(**data)

class EditBatterySchema(Schema):
	bank_id = fields.String(required=True)
	capacity = fields.Float(required=False)
	count = fields.Integer(required=False)
	voltage = fields.Float(required=False)
	bank_voltage = fields.Float(required=False)
	depth_of_discharge = fields.Float(required=False)
	chemistry = fields.String(required=False)
	end_of_life = fields.Float(required=False)
	meta = fields.Nested('EditBatteryMetaSchema', many=True, strict=True)

	@post_load
	def edit_battery(self, data):
		return EditBatteryBank(**data)

