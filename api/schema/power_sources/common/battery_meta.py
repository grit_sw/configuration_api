from collections import namedtuple
from marshmallow import Schema, fields, post_load


EditBatteryMeta = namedtuple('EditBatteryMeta', [
	'battery_id',
	'depth_of_discharge',
	'number_of_cycles',
	'battery_capacity',
])

NewBatteryMeta = namedtuple('NewBatteryMeta', [
	'depth_of_discharge',
	'number_of_cycles',
	'battery_capacity',
])


class NewBatteryMetaSchema(Schema):
	depth_of_discharge = fields.Float(required=False)
	number_of_cycles = fields.Float(required=False)
	battery_capacity = fields.Float(required=False)

	@post_load
	def create_meta(self, data):
		return NewBatteryMeta(**data)

class EditBatteryMetaSchema(Schema):
	battery_id = fields.String(required=True)
	depth_of_discharge = fields.Float(required=False)
	number_of_cycles = fields.Float(required=False)
	battery_capacity = fields.Float(required=False)

	@post_load
	def edit_meta(self, data):
		return EditBatteryMeta(**data)

