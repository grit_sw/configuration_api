from collections import namedtuple
from marshmallow import Schema, ValidationError, fields, post_load, validates
import arrow

from logger import logger

EditCharge = namedtuple('EditCharge', [
	'map_id',
	'start_time',
	'stop_time',
	'amount',
])

NewCharge = namedtuple('NewCharge', [
	'start_time',
	'stop_time',
	'amount',
])

class NewChargeSchema(Schema):
	start_time = fields.String(required=True)
	stop_time = fields.String(required=True)
	amount = fields.Float(required=True)

	@post_load
	def create_config(self, data):
		return NewCharge(**data)

	@validates('start_time')
	def validate_start_time(self, value):
		try:
			now = arrow.now()
			day = now.day if len(str(now.day)) == 2 else f'0{str(now.day)}'
			month = now.month if len(str(now.month)) == 2 else f'0{str(now.month)}'
			arrow.get(f'{now.year}-{month}-{day} {value}')
		except Exception as e:
			logger.exception(e)
			raise ValidationError('A valid start time is required.')

	@validates('stop_time')
	def validate_stop_time(self, value):
		try:
			now = arrow.now()
			day = now.day if len(str(now.day)) == 2 else f'0{str(now.day)}'
			month = now.month if len(str(now.month)) == 2 else f'0{str(now.month)}'
			arrow.get(f'{now.year}-{month}-{day} {value}')
		except Exception as e:
			logger.exception(e)
			raise ValidationError('A valid stop time is required.')


class EditChargeSchema(Schema):
	map_id = fields.String(required=False)
	start_time = fields.String(required=True)
	stop_time = fields.String(required=True)
	amount = fields.Float(required=True)

	@post_load
	def create_config(self, data):
		return EditCharge(**data)

	@validates('start_time')
	def validate_start_time(self, value):
		try:
			now = arrow.now()
			day = now.day if len(str(now.day)) == 2 else f'0{str(now.day)}'
			month = now.month if len(str(now.month)) == 2 else f'0{str(now.month)}'
			arrow.get(f'{now.year}-{month}-{day} {value}')
		except Exception as e:
			logger.exception(e)
			raise ValidationError('A valid start time is required.')

	@validates('stop_time')
	def validate_stop_time(self, value):
		try:
			now = arrow.now()
			day = now.day if len(str(now.day)) == 2 else f'0{str(now.day)}'
			month = now.month if len(str(now.month)) == 2 else f'0{str(now.month)}'
			arrow.get(f'{now.year}-{month}-{day} {value}')
		except Exception as e:
			logger.exception(e)
			raise ValidationError('A valid stop time is required.')
