"""
	Three phase grid schema validator.
"""

from collections import namedtuple

from flask import request
from marshmallow import (Schema, ValidationError, fields, post_load, validates,
                         validates_schema)

from api.models import Channels
from logger import logger

InputThreePhaseChannel = namedtuple('InputThreePhaseChannel', [
	'input_channel1',
	'input_channel2',
	'input_channel3',
])

OutputThreePhaseChannel = namedtuple('OutputThreePhaseChannel', [
	'output_channel1',
	'output_channel2',
	'output_channel3',
])


class InputThreePhaseChannelSchema(Schema):
	input_channel1 = fields.String(required=True)
	input_channel2 = fields.String(required=True)
	input_channel3 = fields.String(required=True)

	@post_load
	def create_channels(self, data):
		return InputThreePhaseChannel(**data)


	@validates('input_channel1')
	def validate_input_channel1(self, value):
		channel = Channels.get_channel(channel_id=value)
		if not channel:
			raise ValidationError('Invalid probe channel.')
		if channel.reserved:
			raise ValidationError(f'This channel is reserved.')

	@validates('input_channel2')
	def validate_input_channel2(self, value):
		channel = Channels.get_channel(channel_id=value)
		if not channel:
			raise ValidationError('Invalid probe channel.')
		if channel.reserved:
			raise ValidationError(f'This channel is reserved.')

	@validates('input_channel3')
	def validate_input_channel3(self, value):
		channel = Channels.get_channel(channel_id=value)
		if not channel:
			raise ValidationError('Invalid probe channel.')
		if channel.reserved:
			raise ValidationError(f'This channel is reserved.')



class EditInputThreePhaseChannelSchema(Schema):
	input_channel1 = fields.String(required=True)
	input_channel2 = fields.String(required=True)
	input_channel3 = fields.String(required=True)

	@post_load
	def create_channels(self, data):
		return InputThreePhaseChannel(**data)

	@validates_schema(pass_original=True)
	def validate_channels(self, _, original_data):
		logger.info(original_data)
		meter_id = list(request.view_args.values())[0]

		input_channel1 = original_data['input_channel1']
		input_channel2 = original_data['input_channel2']
		input_channel3 = original_data['input_channel3']

		channels = [input_channel1, input_channel2, input_channel3]
		for channel_id in channels:
			channel = Channels.get_channel(channel_id=channel_id)
			if not channel:
				raise ValidationError('An invalid probe channel was selected.')
			if channel.reserved:
				sources = set([source["meter_id"] for source in channel.get_source()])
				if meter_id in sources:
					continue
				else:
					raise ValidationError(f'This channel is reserved.')


class OutputThreePhaseChannelSchema(Schema):
	output_channel1 = fields.String(required=True)
	output_channel2 = fields.String(required=True)
	output_channel3 = fields.String(required=True)

	@post_load
	def create_channels(self, data):
		return OutputThreePhaseChannel(**data)

	@validates('output_channel1')
	def validate_output_channel1(self, value):
		channel = Channels.get_channel(channel_id=value)
		if not channel:
			raise ValidationError('Invalid probe channel.')
		if channel.reserved:
			raise ValidationError(f'This channel is reserved.')

	@validates('output_channel2')
	def validate_output_channel2(self, value):
		channel = Channels.get_channel(channel_id=value)
		if not channel:
			raise ValidationError('Invalid probe channel.')
		if channel.reserved:
			raise ValidationError(f'This channel is reserved.')

	@validates('output_channel3')
	def validate_output_channel3(self, value):
		channel = Channels.get_channel(channel_id=value)
		if not channel:
			raise ValidationError('Invalid probe channel.')
		if channel.reserved:
			raise ValidationError(f'This channel is reserved.')


class EditOutputThreePhaseChannelSchema(Schema):
	output_channel1 = fields.String(required=True)
	output_channel2 = fields.String(required=True)
	output_channel3 = fields.String(required=True)

	@post_load
	def create_channels(self, data):
		return OutputThreePhaseChannel(**data)

	@validates_schema(pass_original=True)
	def validate_channels(self, _, original_data):
		logger.info(original_data)
		meter_id = list(request.view_args.values())[0]
		output_channel1 = original_data['output_channel1']
		output_channel2 = original_data['output_channel2']
		output_channel3 = original_data['output_channel3']

		channels = [output_channel1, output_channel2, output_channel3]
		for channel_id in channels:
			channel = Channels.get_channel(channel_id=channel_id)
			if not channel:
				raise ValidationError('An invalid probe channel was selected.')
			if channel.reserved:
				sources = set([source["meter_id"] for source in channel.get_source()])
				if meter_id in sources:
					continue
				else:
					raise ValidationError(f'This channel is reserved.')
