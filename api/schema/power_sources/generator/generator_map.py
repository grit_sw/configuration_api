from collections import namedtuple
from marshmallow import Schema, fields, post_load

EditGeneratorMap = namedtuple('EditGeneratorMap', [
	'map_id',
	'quarter_load_price',
	'half_load_price',
	'three_quarter_load_price',
	'full_load_price',
])

NewGeneratorMap = namedtuple('NewGeneratorMap', [
	'quarter_load_price',
	'half_load_price',
	'three_quarter_load_price',
	'full_load_price',
])

class NewGeneratorMapSchema(Schema):
	quarter_load_price = fields.Float(required=True)
	half_load_price = fields.Float(required=True)
	three_quarter_load_price = fields.Float(required=True)
	full_load_price = fields.Float(required=True)

	@post_load
	def create_config(self, data):
		return NewGeneratorMap(**data)


class EditGeneratorMapSchema(Schema):
	map_id = fields.String(required=False)
	quarter_load_price = fields.Float(required=True)
	half_load_price = fields.Float(required=True)
	three_quarter_load_price = fields.Float(required=True)
	full_load_price = fields.Float(required=True)

	@post_load
	def create_config(self, data):
		return EditGeneratorMap(**data)
