"""
	Single phase grid schema validator.
"""

from collections import namedtuple
from marshmallow import Schema, ValidationError, fields, post_load, validates
from api.models import Configuration, Channels, Probe, Fuel


SinglePhaseGenerator = namedtuple('SinglePhaseGenerator', [
	'name',
	'signal_threshold',
	'configuration_id',
	'probe_id',
	'probe_channel_id',
	'apparent_power_rating',
	'fuel_id',
	'fuel_store_size',
	'operating_window',
	'charge_map',
	'generator_map',
])

EditSinglePhaseGenerator = namedtuple('EditSinglePhaseGenerator', [
	'name',
	'signal_threshold',
	'configuration_id',
	'probe_id',
	'probe_channel_id',
	'apparent_power_rating',
	'fuel_id',
	'fuel_store_size',
	'operating_window',
	'new_operating_window',
	'charge_map',
	'new_charge_map',
	'generator_map',
	'new_generator_map',
])


class SinglePhaseGeneratorSchema(Schema):
	name = fields.String(required=True)
	signal_threshold = fields.Float(required=True, allow_none=True)
	configuration_id = fields.String(required=True)
	probe_id = fields.String(required=True)
	probe_channel_id = fields.String(required=True)
	apparent_power_rating = fields.Float(required=True)
	fuel_id = fields.String(required=True)
	fuel_store_size = fields.Float(required=True, allow_none=True)
	operating_window = fields.Nested('NewWindowSchema', many=True, strict=True)
	charge_map = fields.Nested('NewChargeSchema', many=True, strict=True)
	generator_map = fields.Nested('NewGeneratorMapSchema', many=True, strict=True)

	@post_load
	def create_config(self, data):
		return SinglePhaseGenerator(**data)

	@validates('name')
	def validate_name(self, value):
		if not value:
			raise ValidationError('Power source name required.')

	@validates('signal_threshold')
	def validate_signal_threshold(self, value):
		if not value:
			raise ValidationError('Signal threshold required.')

	@validates('configuration_id')
	def validate_configuration_id(self, value):
		if not value:
			raise ValidationError('Configuration ID required.')
		config = Configuration.get_configuration(configuration_id=value)
		if not config:
			raise ValidationError('Invalid Configuration ID.')

	@validates('probe_id')
	def validate_probe_id(self, value):
		if not value:
			raise ValidationError('Probe ID required.')
		if not Probe.get_probe(probe_id=value):
			raise ValidationError('Invalid probe.')

	@validates('probe_channel_id')
	def validate_probe_channel_id(self, value):
		channel = Channels.get_channel(channel_id=value)
		if not channel:
			raise ValidationError('Invalid probe channel.')
		if channel.reserved:
			source = channel.get_source()
			raise ValidationError(f"Channel already in use by {source[0].get('name')}.")


	@validates('apparent_power_rating')
	def validate_apparent_power_rating(self, value):
		if not value:
			raise ValidationError('Apparent power rating required.')

	@validates('fuel_id')
	def validate_fuel_id(self, value):
		if not Fuel.get_fuel(fuel_id=value):
			raise ValidationError('Invalid fuel type.')


class EditSinglePhaseGeneratorSchema(Schema):
	name = fields.String(required=True)
	signal_threshold = fields.Float(required=True, allow_none=True)
	configuration_id = fields.String(required=True)
	probe_id = fields.String(required=True)
	probe_channel_id = fields.String(required=True)
	apparent_power_rating = fields.Float(required=True)
	fuel_id = fields.String(required=True)
	fuel_store_size = fields.Float(required=True, allow_none=True)
	operating_window = fields.Nested('EditWindowSchema', many=True, strict=True)
	new_operating_window = fields.Nested('NewWindowSchema', many=True, strict=True)
	charge_map = fields.Nested('EditChargeSchema', many=True, strict=True)
	new_charge_map = fields.Nested('NewChargeSchema', many=True, strict=True)
	generator_map = fields.Nested('EditGeneratorMapSchema', many=True, strict=True)
	new_generator_map = fields.Nested('NewGeneratorMapSchema', many=True, strict=True)

	@post_load
	def create_config(self, data):
		return EditSinglePhaseGenerator(**data)

	@validates('name')
	def validate_name(self, value):
		if not value:
			raise ValidationError('Configuration name required.')

	@validates('signal_threshold')
	def validate_signal_threshold(self, value):
		if not value:
			raise ValidationError('Signal threshold required.')

	@validates('configuration_id')
	def validate_configuration_id(self, value):
		if not value:
			raise ValidationError('Configuration ID required.')
		config = Configuration.get_configuration(configuration_id=value)
		if not config:
			raise ValidationError('Invalid Configuration ID.')

	@validates('probe_id')
	def validate_probe_id(self, value):
		if not value:
			raise ValidationError('Probe ID required.')
		if not Probe.get_probe(probe_id=value):
			raise ValidationError('Invalid probe.')

	@validates('probe_channel_id')
	def validate_probe_channel_id(self, value):
		channel = Channels.get_channel(channel_id=value)
		if not channel:
			raise ValidationError('Invalid probe channel.')
		if channel.reserved:
			source = channel.get_source()
			raise ValidationError(f"Channel already in use by {source[0].get('name')}.")

	@validates('apparent_power_rating')
	def validate_apparent_power_rating(self, value):
		if not value:
			raise ValidationError('Apparent power rating required.')

	@validates('fuel_id')
	def validate_fuel_id(self, value):
		if not Fuel.get_fuel(fuel_id=value):
			raise ValidationError('Invalid fuel type.')
