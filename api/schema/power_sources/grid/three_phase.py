"""
	Three phase grid schema validator.
"""

from collections import namedtuple
from marshmallow import Schema, ValidationError, fields, post_load, validates
from api.models import Configuration, Channels, Probe


ThreePhaseGrid = namedtuple('ThreePhaseGrid', [
	'name',
	'signal_threshold',
	'configuration_id',
	'probe_id',
	'probe_channels',
	'charge_map',
])

EditThreePhaseGrid = namedtuple('EditThreePhaseGrid', [
	'name',
	'signal_threshold',
	'configuration_id',
	'probe_id',
	'probe_channels',
	'charge_map',
	'new_charge_map',
])


class ThreePhaseGridSchema(Schema):
	name = fields.String(required=True)
	signal_threshold = fields.Float(required=True, allow_none=True)
	configuration_id = fields.String(required=True)
	probe_id = fields.String(required=True)
	probe_channels = fields.List(fields.String(required=True))
	charge_map = fields.Nested('NewChargeSchema', many=True, strict=True)

	@post_load
	def create_config(self, data):
		return ThreePhaseGrid(**data)

	@validates('name')
	def validate_name(self, value):
		if not value:
			raise ValidationError('Power source name required.')

	@validates('signal_threshold')
	def validate_signal_threshold(self, value):
		if not value:
			raise ValidationError('Signal threshold required.')

	@validates('configuration_id')
	def validate_configuration_id(self, value):
		if not value:
			raise ValidationError('Configuration ID required.')
		config = Configuration.get_configuration(configuration_id=value)
		if not config:
			raise ValidationError('Invalid Configuration ID.')

	@validates('probe_id')
	def validate_probe_id(self, value):
		if not value:
			raise ValidationError('Probe ID required.')
		if not Probe.get_probe(probe_id=value):
			raise ValidationError('Invalid probe.')

	@validates('probe_channels')
	def validate_probe_channels(self, value):
		if not isinstance(value, list):
			raise ValidationError('Probe channels required.')
		if len(value) != 3:
			raise ValidationError(f'3 channels expected. Got {len(value)}.')
		for channel_id in value:
			if not Channels.get_channel(channel_id=channel_id):
				raise ValidationError('Invalid probe channel.')


class EditThreePhaseGridSchema(Schema):
	name = fields.String(required=True)
	signal_threshold = fields.Float(required=True, allow_none=True)
	configuration_id = fields.String(required=True)
	probe_id = fields.String(required=True)
	probe_channels = fields.List(fields.String(required=True))
	charge_map = fields.Nested('EditChargeSchema', many=True, strict=True)
	new_charge_map = fields.Nested('NewChargeSchema', many=True, strict=True)

	@post_load
	def create_config(self, data):
		return EditThreePhaseGrid(**data)

	@validates('name')
	def validate_name(self, value):
		if not value:
			raise ValidationError('Configuration name required.')

	@validates('signal_threshold')
	def validate_signal_threshold(self, value):
		if not value:
			raise ValidationError('Signal threshold required.')

	@validates('configuration_id')
	def validate_configuration_id(self, value):
		if not value:
			raise ValidationError('Configuration ID required.')
		config = Configuration.get_configuration(configuration_id=value)
		if not config:
			raise ValidationError('Invalid Configuration ID.')

	@validates('probe_id')
	def validate_probe_id(self, value):
		if not value:
			raise ValidationError('Probe ID required.')
		if not Probe.get_probe(probe_id=value):
			raise ValidationError('Invalid probe.')

	@validates('probe_channels')
	def validate_probe_channels(self, value):
		if not isinstance(value, list):
			raise ValidationError('Probe channels required.')
		if len(value) != 3:
			raise ValidationError(f'3 channels expected. Got {len(value)}.')
		for channel_id in value:
			channel = Channels.get_channel(channel_id=channel_id)
			if not channel:
				raise ValidationError('Invalid probe channel.')
			if channel.reserved:
				source = channel.get_source()
				raise ValidationError(f'Channel already in use by {source.name}.')

