"""
	Single phase grid schema validator.
"""

from collections import namedtuple
from marshmallow import Schema, fields, post_load, validates, ValidationError
from api.models import Channels


SinglePhaseInverter = namedtuple('SinglePhaseInverter', [
	'name',
	'signal_threshold',
	'configuration_id',
	'input_channel_id',
	'output_channel_id',
	'apparent_power_rating',
	'efficiency',
	'bulk_charging_current',
	'cost',
	'operating_window',
	'battery',
])

EditSinglePhaseInverter = namedtuple('EditSinglePhaseInverter', [
	'name',
	'signal_threshold',
	'configuration_id',
	'input_channel_id',
	'output_channel_id',
	'apparent_power_rating',
	'efficiency',
	'bulk_charging_current',
	'cost',
	'operating_window',
	'battery',
	'new_operating_window',
])


class SinglePhaseInverterSchema(Schema):
	name = fields.String(required=True)
	signal_threshold = fields.Float(required=True, allow_none=True)
	configuration_id = fields.String(required=True)
	input_channel_id = fields.String(required=True)
	output_channel_id = fields.String(required=True)
	apparent_power_rating = fields.Float(required=True)
	efficiency = fields.Float(required=True)
	bulk_charging_current = fields.Float(required=True)
	cost = fields.Float(required=True, allow_none=True)
	operating_window = fields.Nested('NewWindowSchema', many=True, strict=True)
	battery = fields.Nested('NewBatterySchema', many=True, strict=True)

	@post_load
	def create_config(self, data):
		return SinglePhaseInverter(**data)

	@validates('input_channel_id')
	def validate_input_channel_id(self, value):
		channel = Channels.get_channel(channel_id=value)
		if not channel:
			raise ValidationError('Invalid probe channel for the input channel.')
		if channel.reserved:
			source = channel.get_source()
			raise ValidationError(f'Channel already in use by {source.name}.')

	@validates('output_channel_id')
	def validate_output_channel_id(self, value):
		channel = Channels.get_channel(channel_id=value)
		if not channel:
			raise ValidationError('Invalid probe channel for the output channel.')
		if channel.reserved:
			source = channel.get_source()
			raise ValidationError(f'Channel already in use by {source.name}.')


class EditSinglePhaseInverterSchema(Schema):
	name = fields.String(required=True)
	signal_threshold = fields.Float(required=True, allow_none=True)
	configuration_id = fields.String(required=True)
	input_channel_id = fields.String(required=True)
	output_channel_id = fields.String(required=True)
	apparent_power_rating = fields.Float(required=True)
	efficiency = fields.Float(required=True)
	bulk_charging_current = fields.Float(required=True)
	cost = fields.Float(required=True, allow_none=True)
	operating_window = fields.Nested('EditWindowSchema', many=True, strict=True)
	new_operating_window = fields.Nested('NewWindowSchema', many=True, strict=True)
	battery = fields.Nested('EditBatterySchema', many=True, strict=True)

	@post_load
	def create_config(self, data):
		return EditSinglePhaseInverter(**data)
