"""
	Three phase grid schema validator.
"""

from collections import namedtuple
from marshmallow import Schema, fields, post_load


ThreePhaseInverter = namedtuple('ThreePhaseInverter', [
	'name',
	'signal_threshold',
	'configuration_id',
	'input_channels',
	'output_channels',
	'apparent_power_rating',
	'efficiency',
	'bulk_charging_current',
	'cost',
	'operating_window',
	'battery',
])

EditThreePhaseInverter = namedtuple('EditThreePhaseInverter', [
	'name',
	'signal_threshold',
	'configuration_id',
	'input_channels',
	'output_channels',
	'apparent_power_rating',
	'efficiency',
	'bulk_charging_current',
	'cost',
	'operating_window',
	'battery',
	'new_operating_window',
])


class ThreePhaseInverterSchema(Schema):
	name = fields.String(required=True)
	signal_threshold = fields.Float(required=True, allow_none=True)
	configuration_id = fields.String(required=True)
	input_channels = fields.Nested('InputThreePhaseChannelSchema', strict=True)
	output_channels = fields.Nested('OutputThreePhaseChannelSchema', strict=True)
	apparent_power_rating = fields.Float(required=True)
	efficiency = fields.Float(required=True)
	bulk_charging_current = fields.Float(required=True)
	cost = fields.Float(required=True, allow_none=True)
	operating_window = fields.Nested('NewWindowSchema', many=True, strict=True)
	battery = fields.Nested('NewBatterySchema', many=True, strict=True)

	@post_load
	def create_config(self, data):
		return ThreePhaseInverter(**data)


class EditThreePhaseInverterSchema(Schema):
	name = fields.String(required=True)
	signal_threshold = fields.Float(required=True, allow_none=True)
	configuration_id = fields.String(required=True)
	input_channels = fields.Nested('EditInputThreePhaseChannelSchema', strict=True)
	output_channels = fields.Nested('EditOutputThreePhaseChannelSchema', strict=True)
	apparent_power_rating = fields.Float(required=True)
	efficiency = fields.Float(required=True)
	bulk_charging_current = fields.Float(required=True)
	cost = fields.Float(required=True, allow_none=True)
	operating_window = fields.Nested('EditWindowSchema', many=True, strict=True)
	new_operating_window = fields.Nested('NewWindowSchema', many=True, strict=True)
	battery = fields.Nested('EditBatterySchema', many=True, strict=True)

	@post_load
	def create_config(self, data):
		return EditThreePhaseInverter(**data)
