from collections import namedtuple

from marshmallow import Schema, ValidationError, fields, post_load, validates

from api.models import Probe, DeviceType, CurrentSensor, VoltageSensor, Channels


NewChannel = namedtuple('NewChannel', [
	'channel_threshold_current',
	'power_factor',
	'current_sensor_id',
	'voltage_sensor_id',
])

EditChannel = namedtuple('EditChannel', [
	'channel_id',
	'channel_threshold_current',
	'power_factor',
	'current_sensor_id',
	'voltage_sensor_id',
])

NewProbe = namedtuple('NewProbe', [
	'probe_name',
	'wifi_network_name',
	'wifi_password',
	'device_type_id',
	'channels',
])

EditProbe = namedtuple('EditProbe', [
	'probe_name',
	'probe_id',
	'wifi_network_name',
	'wifi_password',
	'channels',
])


class NewChannelSchema(Schema):
	channel_threshold_current = fields.Float(required=True, allow_none=True)
	power_factor = fields.Float(required=True, allow_none=True)
	current_sensor_id = fields.String(required=True)
	voltage_sensor_id = fields.String(required=True)

	@post_load
	def create_channel(self, data):
		return NewChannel(**data)

	@validates('current_sensor_id')
	def validate_current_sensor_id(self, value):
		if value:
			sensor = CurrentSensor.get_one(rating_id=value)
			if not sensor:
				raise ValidationError('Current sensor not found.')

	@validates('voltage_sensor_id')
	def validate_voltage_sensor_id(self, value):
		if value:
			sensor = VoltageSensor.get_one(sensor_id=value)
			if not sensor:
				raise ValidationError('Voltage sensor not found.')


class EditChannelSchema(Schema):
	channel_id = fields.String(required=True, allow_none=True)
	channel_threshold_current = fields.Float(required=True, allow_none=True)
	power_factor = fields.Float(required=True, allow_none=True)
	current_sensor_id = fields.String(required=True)
	voltage_sensor_id = fields.String(required=True)

	@post_load
	def edit_channel(self, data):
		return EditChannel(**data)

	@validates('current_sensor_id')
	def validate_current_sensor_id(self, value):
		if value:
			sensor = CurrentSensor.get_one(rating_id=value)
			if not sensor:
				raise ValidationError('Current sensor not found.')

	@validates('voltage_sensor_id')
	def validate_voltage_sensor_id(self, value):
		if value:
			sensor = VoltageSensor.get_one(sensor_id=value)
			if not sensor:
				raise ValidationError('Voltage sensor not found.')

	@validates('channel_id')
	def validate_channel_id(self, value):
		if value:
			channel = Channels.get_channel(channel_id=value)
			if not channel:
				raise ValidationError('Invalid channel ID.')


class NewProbeSchema(Schema):
	probe_name = fields.String(required=True)
	wifi_network_name = fields.String(required=True, allow_none=True)
	wifi_password = fields.String(required=True, allow_none=True)
	device_type_id = fields.String(required=True, allow_none=True)
	channels = fields.Nested('NewChannelSchema', many=True, strict=True)

	@post_load
	def create_probe(self, data):
		return NewProbe(**data)

	@validates('probe_name')
	def validate_probe_name(self, value):
		if not value:
			raise ValidationError('Probe name required.')
		probe = Probe.get_one(probe_name=value)
		if probe:
			raise ValidationError(f'Probe {value} already exists.')

	@validates('wifi_network_name')
	def validate_wifi_network_name(self, value):
		pass

	@validates('wifi_password')
	def validate_wifi_password(self, value):
		pass

	@validates('device_type_id')
	def validate_device_type_id(self, value):
		if not value:
			raise ValidationError('Device type ID required.')
		device_type = DeviceType.get_device_type(device_type_id=value)
		if not device_type:
			raise ValidationError('Device type not found.')


class EditProbeSchema(Schema):
	probe_name = fields.String(required=True)
	probe_id = fields.String(required=True)
	wifi_network_name = fields.String(required=True, allow_none=True)
	wifi_password = fields.String(required=True, allow_none=True)
	channels = fields.Nested('EditChannelSchema', many=True, strict=True)

	@post_load
	def edit_probe(self, data):
		return EditProbe(**data)

	@validates('probe_name')
	def validate_probe_name(self, value):
		if not value:
			raise ValidationError('Probe name required.')
		probe = Probe.get_one(probe_name=value)
		if not probe:
			raise ValidationError(f'Probe {value} not found.')

	@validates('probe_id')
	def validate_probe_id(self, value):
		if not value:
			raise ValidationError('Probe not found.')
		probe = Probe.get_one(probe_id=value)
		if not probe:
			raise ValidationError(f'Probe not found.')

	@validates('wifi_network_name')
	def validate_wifi_network_name(self, value):
		pass

	@validates('wifi_password')
	def validate_wifi_password(self, value):
		pass
