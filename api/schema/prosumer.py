# from api.models import Prosumer, ProsumerType, ProsumerGroup
from marshmallow import Schema, fields, post_load, validates, ValidationError
from collections import namedtuple

ProsumerParams = namedtuple('ProsumerParams', [
	'group_id',
	'prosumer_id',
])

AssignProsumer = namedtuple('AssignProsumer', [
	'group_id',
	'facility_id',
	'prosumer_id',
])

EnergyPlan = namedtuple('EnergyPlan', [
	'prosumer_id',
	'plan_id',
])

NewProsumer = namedtuple('NewProsumer', [
	'prosumers',
	'probe_id',
	'facility_id',
])

SubProsumer = namedtuple('SubProsumer', [
	'name',
	'type_id',
	'group_id',
])


class EnergyPlanSchema(Schema):
	prosumer_id = fields.String(required=True)
	plan_id = fields.String(required=True)

	@post_load
	def assign(self, data):
		return EnergyPlan(**data)

	@validates('plan_id')
	def validate_plan_id(self, value):
		if len(value) <= 0:
			raise ValidationError('UserID Required.')

	@validates('prosumer_id')
	def validate_prosumer_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer Name Required.')


class ProsumerParamsSchema(object):
	group_id = fields.String(required=True)
	prosumer_id = fields.String(required=True)

	@post_load
	def assign(self, data):
		return ProsumerParams(**data)

	@validates('group_id')
	def validate_group_id(self, value):
		# TODO: regex
		if len(value) <= 0:
			raise ValidationError('UserID Required.')

	@validates('prosumer_id')
	def validate_prosumer_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer Name Required.')
		if not Prosumer.get_prosumer(prosumer_id=value):
			raise ValidationError('Prosumer {} does not exist.'.format(value))


class AssignProsumerSchema(Schema):
	group_id = fields.String(required=True)
	facility_id = fields.String(required=True)
	prosumer_id = fields.String(required=True)

	@post_load
	def assign(self, data):
		return AssignProsumer(**data)

	@validates('group_id')
	def validate_group_id(self, value):
		if len(value) <= 0:
			raise ValidationError('User group ID Required.')

	@validates('facility_id')
	def validate_facility_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Facility Name Required.')

	@validates('prosumer_id')
	def validate_prosumer_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer Name Required.')
		if not Prosumer.get_prosumer(prosumer_id=value):
			raise ValidationError('Prosumer {} does not exist.'.format(value))


class ProsumerSchema(Schema):
	"""docstring for EmailSchema"""
	name = fields.String(required=True)
	type_id = fields.String(required=True)
	group_id = fields.String(required=True)

	@post_load
	def create_consumer(self, data):
		return SubProsumer(**data)

	@validates('name')
	def validate_name(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer name Required.')

	@validates('type_id')
	def validate_type_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer Type Required.')
		prosumer_type = ProsumerType.find_type(id=value)
		if not prosumer_type:
			raise ValidationError('Prosumer Type Does not exist.')

	@validates('group_id')
	def validate_group_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer group required.')
		prosumer_group = ProsumerGroup.get_prosumer_group_without_facility_id(id=value)
		if not prosumer_group:
			raise ValidationError('Prosumer group not exist.')



class NewProsumerSchema(Schema):
	probe_id = fields.String(required=True)
	facility_id = fields.String(required=True)
	prosumers = fields.Nested('ProsumerSchema', many=True, strict=True)

	@post_load
	def create_invite(self, data):
		return NewProsumer(**data)

	@validates('probe_id')
	def validate_probe_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Probe ID Required.')

	@validates('facility_id')
	def validate_facility_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Facility ID Required.')
