from marshmallow import Schema, fields, post_load, validates, ValidationError
from collections import namedtuple

NewProsumerClass = namedtuple('NewProsumerClass', [
	'name',
])

class ProsumerClassSchema(Schema):
	name = fields.String(required=True)

	@post_load
	def new_prosumer(self, data):
		return NewProsumerClass(**data)

	@validates('name')
	def validate_name(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer class name required.')

	def __repr__(self):
		return "ProsumerClassSchema <{}>".format(self.name)
