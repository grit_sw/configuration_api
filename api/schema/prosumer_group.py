from marshmallow import Schema, fields, post_load, validates, ValidationError
from collections import namedtuple

NewProsumerGroup = namedtuple('NewProsumerGroup', [
	'name',
	'facility_id',
])

ProsumerToProsumerGroup = namedtuple('ProsumerToProsumerGroup', [
	'prosumer_facility',
	'group_facility',
	'prosumer_id',
	'prosumer_group_id',
])

RemoveProsumer = namedtuple('RemoveProsumer', [
	'prosumer_id',
	'prosumer_group_id',
])

ClientAdminProsumerGroup = namedtuple('ClientAdminProsumerGroup', [
	'admin_facility',
	'group_facility',
	'client_admin_id',
	'prosumer_group_id',
])

RemoveClientAdmin = namedtuple('RemoveClientAdmin', [
	'client_admin_id',
	'prosumer_group_id',
])


class ProsumerGroupSchema(Schema):
	name = fields.String(required=True)
	facility_id = fields.String(required=True)

	@post_load
	def new_prosumer(self, data):
		return NewProsumerGroup(**data)

	@validates('name')
	def validate_name(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer group required.')

	@validates('facility_id')
	def validate_facility_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Facility required.')

	def __repr__(self):
		return "<ProsumerGroupSchema {}>".format(self.name)

class ProsumerToProsumerGroupSchema(Schema):
	prosumer_facility = fields.String(required=True)
	group_facility = fields.String(required=True)
	prosumer_id = fields.String(required=True)
	prosumer_group_id = fields.String(required=True)

	@post_load
	def prosumer_group(self, data):
		return ProsumerToProsumerGroup(**data)

	@validates('prosumer_facility')
	def validate_prosumer_facility(self, value):
		if len(value) <= 0:
			raise ValidationError('Facility ID of the prosumer is required.')

	@validates('group_facility')
	def validate_group_facility(self, value):
		if len(value) <= 0:
			raise ValidationError('Facility ID of the prosumer group is required.')

	@validates('prosumer_id')
	def validate_prosumer_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer required.')

	@validates('prosumer_group_id')
	def validate_prosumer_group_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer Group required.')

	def __repr__(self):
		return "<ProsumerToProsumerGroupSchema {}>".format(self.name)


class RemoveProsumerFromProsumerGroupSchema(Schema):
	prosumer_id = fields.String(required=True)
	prosumer_group_id = fields.String(required=True)

	@post_load
	def prosumer_group(self, data):
		return RemoveProsumer(**data)

	@validates('prosumer_id')
	def validate_prosumer_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer required.')

	@validates('prosumer_group_id')
	def validate_prosumer_group_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer Group required.')

	def __repr__(self):
		return "<RemoveProsumerFromProsumerGroupSchema {}>".format(self.name)


class ClientAdminProsumerGroupSchema(Schema):
	admin_facility = fields.String(required=True)
	group_facility = fields.String(required=True)
	client_admin_id = fields.String(required=True)
	prosumer_group_id = fields.String(required=True)

	@post_load
	def client_admin(self, data):
		return ClientAdminProsumerGroup(**data)

	@validates('client_admin_id')
	def validate_client_admin_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Client Admin required.')

	@validates('admin_facility')
	def validate_admin_facility(self, value):
		if len(value) <= 0:
			raise ValidationError('Facility ID of the admin is required.')

	@validates('group_facility')
	def validate_group_facility(self, value):
		if len(value) <= 0:
			raise ValidationError('Facility ID of the prosumer group is required.')

	@validates('prosumer_group_id')
	def validate_prosumer_group_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer Group required.')

	def __repr__(self):
		return "<ClientAdminProsumerGroupSchema {}>".format(self.name)


class RemoveClientAdminProsumerGroupSchema(Schema):
	client_admin_id = fields.String(required=True)
	prosumer_group_id = fields.String(required=True)

	@post_load
	def client_admin(self, data):
		return RemoveClientAdmin(**data)

	@validates('client_admin_id')
	def validate_client_admin_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Client Admin required.')

	@validates('prosumer_group_id')
	def validate_prosumer_group_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer Group required.')

	def __repr__(self):
		return "<RemoveClientAdminProsumerGroupSchema {}>".format(self.name)
