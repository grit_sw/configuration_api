from marshmallow import Schema, fields, post_load, validates, ValidationError
from collections import namedtuple

NewProsumerType = namedtuple('NewProsumerType', [
	'name',
	'class_id',
])

class ProsumerTypeSchema(Schema):
	name = fields.String(required=True)
	class_id = fields.String(required=True)

	@post_load
	def new_prosumer(self, data):
		return NewProsumerType(**data)

	@validates('name')
	def validate_name(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer type name required.')

	@validates('class_id')
	def validate_class_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer type class ID required.')

	def __repr__(self):
		return "ProsumerTypeSchema <{}>".format(self.name)
