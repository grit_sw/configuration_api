from collections import namedtuple

from marshmallow import Schema, ValidationError, fields, post_load, validates


VoltageSensor = namedtuple('VoltageSensor', [
	'name',
	'rating',
	'default_voltage',
	'scaling_factor_voltage',
])


class VoltageSensorSchema(Schema):
	name = fields.String(required=True)
	rating = fields.Float(required=True)
	default_voltage = fields.Float(required=True)
	scaling_factor_voltage = fields.Float(required=True)

	@post_load
	def edit_sensor_type(self, data):
		return VoltageSensor(**data)

	@validates('name')
	def validate_name(self, value):
		if not value:
			raise ValidationError('Voltage sensor name required.')
