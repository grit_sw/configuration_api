import os
from ast import literal_eval


class Config(object):
	DEBUG = False
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	SECRET_KEY = os.environ['SECRET_KEY']
	ADMINS = ['workshop@grit.systems', ]
	INVITE_EXPIRES = 600
	USERS_PER_PAGE = 20
	SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
	BOOTSTRAP_SERVERS = os.environ['BOOTSTRAP_SERVERS']
	SCHEMA_REGISTRY_URI = os.environ['SCHEMA_REGISTRY_URI']
	PROSUMER_SWITCH_TOPIC = os.environ['PROSUMER_SWITCH_TOPIC']
	CONFIGURATION_TOPIC = os.environ['CONFIGURATION_TOPIC']
	PROSUMER_UPDATE_TOPIC = os.environ['PROSUMER_UPDATE_TOPIC']
	TOPICS = literal_eval(os.environ['TOPICS'])
	SCHEMA_MAPPING = literal_eval(os.environ['SCHEMA_MAPPING'])
	TOPIC_SCHEMA_MAPPING = dict(zip(TOPICS, SCHEMA_MAPPING))

	@classmethod
	def init_app(cls, app):
		from api.producer import KafkaProducer
		# app.kafka_producer = KafkaProducer(
		# 	bootstrap_servers=cls.BOOTSTRAP_SERVERS,
		# 	schema_registry_uri=cls.SCHEMA_REGISTRY_URI,
		# 	topic_schema_mapping=cls.TOPIC_SCHEMA_MAPPING,
		# )
		# app.kafka_producer.check_registry()


class Development(Config):
	DEBUG = True
	JWT_SECRET_KEY = os.urandom(3)


	def __repr__(self):
		return '{}'.format(self.__class__.__name__)



class Testing(Config):
	"""TODO"""
	pass


class Staging(Config):
	"""TODO"""
	SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']

	@classmethod
	def init_app(cls, app):
		from api.producer import KafkaProducer
		app.kafka_producer = KafkaProducer(
			bootstrap_servers=cls.BOOTSTRAP_SERVERS,
			schema_registry_uri=cls.SCHEMA_REGISTRY_URI,
			topic_schema_mapping=cls.TOPIC_SCHEMA_MAPPING,
		)
		app.kafka_producer.init_schema()

	def __repr__(self):
		return '{}'.format(self.__class__.__name__)


class Production(Config):
	SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']

	@classmethod
	def init_app(cls, app):
		from api.producer import KafkaProducer
		app.kafka_producer = KafkaProducer(
			bootstrap_servers=cls.BOOTSTRAP_SERVERS,
			schema_registry_uri=cls.SCHEMA_REGISTRY_URI,
			topic_schema_mapping=cls.TOPIC_SCHEMA_MAPPING,
		)
		app.kafka_producer.init_schema()

	def __repr__(self):
		return '{}'.format(self.__class__.__name__)


config = {
	'Development': Development,
	'Testing': Testing,
	'Production': Production,
	'Staging': Staging,

	'default': Development,
}
