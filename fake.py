from api.models import Configuration, Probe, DeviceType, Channels


def get_configuration():
	from sqlalchemy import func
	# config = Configuration.query\
	# 	.outerjoin(Configuration.channels)\
	# 	.group_by(Configuration)\
	# 	.having(func.and_(
	# 			 func.count_(Configuration.channels.filter_by(reserved=False)) >= 3, func.count_(Configuration.channels.filter_by(reserved=False)) <= 8)).first()
	config = Configuration.query\
		.join(Configuration.channels)\
		.filter(Channels.reserved == False)\
		.group_by(Configuration)\
		.having(func.count_(Channels.reserved) >= 3).first()

	# config = Configuration.query.join(Configuration.channels, aliased=True).filter(func.count_(Channels.query.filter_by(reserved=False)) >= 3).first()
	return config

def get_configuration_six():
	from sqlalchemy import func
	config = Configuration.query\
		.join(Configuration.channels)\
		.filter(Channels.reserved == False)\
		.group_by(Configuration)\
		.having(func.count_(Channels.reserved) >= 6).first()
	return config

def get_one_unreserved_channel(config):
	channel = config.channels.filter_by(reserved=False).first()
	return channel


def get_three_unreserved_channels(config):
	from random import sample
	channel_list = sample(config.channels.filter_by(reserved=False).all(), 3)
	channel_list = [channel.channel_id for channel in channel_list]
	print(f'\n\n Channel {channel_list}')
	return channel_list


def get_six_unreserved_channels(config):
	from random import sample
	channel_list = sample(config.channels.filter_by(reserved=False).all(), 6)
	return channel_list


def get_consumer_group_type():
	from api.models.power_consumers.utils.consumer_group_type import ConsumerGroupType
	ConsumerGroupType.init_types()
	from random import choice
	type_dict = choice(ConsumerGroupType.get_all())
	type_id = type_dict['consumer_group_type_id']
	return type_id


def create_consumer_group(configuration_id):
	from api.models.power_consumers.consumer_group import ConsumerGroup
	from forgery_py.forgery.address import street_name
	group_type_id = get_consumer_group_type()
	group_dict = ConsumerGroup.create(
		name=street_name(),
		configuration_id=configuration_id,
		consumer_group_type_id=group_type_id
		)
	group_id = group_dict['group_id']
	return group_id


def get_one_consumer_group():
	from api.models.power_consumers.consumer_group import ConsumerGroup
	consumer_group = ConsumerGroup.query.first()
	if not consumer_group:
		config = get_configuration()
		group_id = create_consumer_group(config.configuration_id)
		return group_id
	return consumer_group.group_id

def create_single_phase_consumer_line():
	from random import uniform
	from forgery_py.forgery.name import company_name
	config = get_configuration()
	data = {
		'name': company_name(),
		'signal_threshold': round(uniform(2.5, 40.0), 2),
		'channel_id': get_one_unreserved_channel(config).channel_id,
		'consumer_group_id': get_one_consumer_group(),
	}
	from api.models.power_consumers.consumer_line.one_phase import SinglePhaseConsumerLine
	line_dict = SinglePhaseConsumerLine.create(**data)
	print(line_dict)
	line_id = line_dict['line_id']
	return line_id

def create_three_phase_consumer_line():
	from random import uniform
	from forgery_py.forgery.name import company_name
	config = get_configuration()
	data = {
		'name': company_name(),
		'signal_threshold': round(uniform(2.5, 40.0), 2),
		'channels': get_three_unreserved_channels(config),
		'consumer_group_id': get_one_consumer_group(),
	}
	from api.models.power_consumers.consumer_line.three_phase import ThreePhaseConsumerLine
	line_dict = ThreePhaseConsumerLine.create(**data)
	print(line_dict)
	line_id = line_dict['line_id']
	return line_id

def create_single_phase_consumer():
	COUNT = 150
	from api.models import SinglePhaseConsumer
	from forgery_py.forgery.name import company_name
	for _ in range(COUNT):
		line_id = create_single_phase_consumer_line()
		data = {
			'name': company_name(),
			'line_id': line_id,
		}
		print(SinglePhaseConsumer.create(**data))

def create_three_phase_consumers():
	COUNT = 150
	from api.models import ThreePhaseConsumer
	from forgery_py.forgery.name import company_name
	for _ in range(COUNT):
		line_id = create_three_phase_consumer_line()
		data = {
			'name': company_name(),
			'line_id': line_id,
		}
		print(ThreePhaseConsumer.create(**data))

def create_single_phase_grids():
	from api.models import SinglePhaseGrid
	from forgery_py.forgery.name import company_name
	from random import uniform
	# from forgery_py.forgery.name import last_name()
	def grid_data():
		config = get_configuration()
		channel = get_one_unreserved_channel(config)
		probe = channel.probe
		data = {
			"name": company_name(),
			"signal_threshold": round(uniform(2.5, 40.0), 2),
			"configuration_id": config.configuration_id,
			"probe_id": probe.probe_id,
			"probe_channel_id": channel.channel_id,
			"charge_map": [
				{
				"start_time": "00:00:00",
				"stop_time": "05:00:00",
				"amount": round(uniform(2.5, 40.0), 2)
				}
			]
		}
		return data
	COUNT = 150
	for _ in range(COUNT):
		data = grid_data()
		print(SinglePhaseGrid.create(**data))

def create_three_phase_grids():
	from api.models import ThreePhaseGrid
	from forgery_py.forgery.name import company_name
	from random import uniform
	def grid_data():
		config = get_configuration()
		channels = get_three_unreserved_channels(config)
		for channel_id in channels:
			probe = Channels.get_channel(channel_id=channel_id).probe
			break
		data = {
			"name": company_name(),
			"signal_threshold": round(uniform(2.5, 40.0), 2),
			"configuration_id": config.configuration_id,
			"probe_id": probe.probe_id,
			"probe_channels": channels,
			"charge_map": [
				{
				"start_time": "00:00:00",
				"stop_time": "05:00:00",
				"amount": round(uniform(2.5, 40.0), 2)
				}
			]
		}
		return data
	COUNT = 150
	for _ in range(COUNT):
		data = grid_data()
		print(ThreePhaseGrid.create(**data))

def create_single_phase_generators():
	from api.models import SinglePhaseGenerator
	from forgery_py.forgery.name import company_name
	from api.models import Fuel
	from random import uniform
	fuel = Fuel.query.first()
	def get_data():
		channel = None
		while True:
			config = get_configuration()
			if not hasattr(config, 'channels'):
				continue
			channel = config.channels.filter_by(reserved=False).first()
			if channel:
				break

		probe = channel.probe
		data = {
			"name": company_name(),
			"signal_threshold": round(uniform(2.5, 40.0), 2),
			"configuration_id": config.configuration_id,
			"probe_id": probe.probe_id,
			"probe_channel_id": channel.channel_id,
			"apparent_power_rating": round(uniform(2.5, 40.0), 2),
			"fuel_id": fuel.fuel_id,
			"fuel_store_size": round(uniform(2.5, 40.0), 2),
			"operating_window": [
				{
					"start_time": "00:00:00",
					"stop_time": "05:00:00",
				}
			],
			"charge_map": [
				{
					"start_time": "00:00:00",
					"stop_time": "05:00:00",
					"amount": round(uniform(2.5, 40.0), 2)
				}
			],
			"generator_map": [
				{
					"quarter_load_price": round(uniform(2.5, 40.0), 2),
					"half_load_price": round(uniform(2.5, 40.0), 2),
					"three_quarter_load_price": round(uniform(2.5, 40.0), 2),
					"full_load_price": round(uniform(2.5, 40.0), 2)
				}
  			]
		}
		return data
	COUNT = 150
	for _ in range(COUNT):
		data = get_data()
		print(SinglePhaseGenerator.create(**data))

def create_three_phase_generators():
	from api.models import ThreePhaseGenerator
	from forgery_py.forgery.name import company_name
	from api.models import Fuel
	from random import uniform
	fuel = Fuel.query.first()
	def get_data():
		config = get_configuration()
		channels = get_three_unreserved_channels(config)
		for channel_id in channels:
			probe = Channels.get_channel(channel_id=channel_id).probe
			break
		data = {
			"name": company_name(),
			"signal_threshold": round(uniform(2.5, 40.0), 2),
			"configuration_id": config.configuration_id,
			"probe_id": probe.probe_id,
			"probe_channels": channels,
			"apparent_power_rating": round(uniform(2.5, 40.0), 2),
			"fuel_id": fuel.fuel_id,
			"fuel_store_size": round(uniform(2.5, 40.0), 2),
			"operating_window": [
				{
					"start_time": "00:00:00",
					"stop_time": "05:00:00",
				}
			],
			"charge_map": [
				{
					"start_time": "00:00:00",
					"stop_time": "05:00:00",
					"amount": round(uniform(2.5, 40.0), 2)
				}
			],
			"generator_map": [
				{
					"quarter_load_price": round(uniform(2.5, 40.0), 2),
					"half_load_price": round(uniform(2.5, 40.0), 2),
					"three_quarter_load_price": round(uniform(2.5, 40.0), 2),
					"full_load_price": round(uniform(2.5, 40.0), 2)
				}
  			]
		}
		return data
	COUNT = 150
	for _ in range(COUNT):
		data = get_data()
		print(ThreePhaseGenerator.create(**data))


def create_single_phase_inverters():
	from api.models import SinglePhaseInverter
	from forgery_py.forgery.name import company_name
	from random import uniform
	def get_data():
		config = get_configuration()
		channels = get_three_unreserved_channels(config)
		input_ = channels[0]
		output_ = channels[1]
		data = {
			"name": company_name(),
			"signal_threshold": round(uniform(2.5, 40.0), 2),
			"configuration_id": "string",
			"input_channel_id": input_,
			"output_channel_id": output_,
			"apparent_power_rating": "1000",
			"efficiency": "95",
			"bulk_charging_current": "40",
			"cost": "20000",
			"operating_window": [
				{
					"start_time": "00:00:00",
					"stop_time": "05:00:00"
				}
			],
			"battery": [
				{
					"capacity": "200",
					"count": 4,
					"voltage": 24,
					"bank_voltage": 24,
					"depth_of_discharge": 70,
					"chemistry": "Lead",
					"end_of_life": 5,
					"meta": [
						{
							"depth_of_discharge": 70,
							"number_of_cycles": 7000,
							"battery_capacity": 200
						}
					]
				}
			]
		}
		return data
	COUNT = 150
	for _ in range(COUNT):
		data = get_data()
		print(SinglePhaseInverter.create(**data))

def create_three_phase_inverters():
	from api.models import ThreePhaseInverter
	from forgery_py.forgery.name import company_name
	def get_data():
		config = get_configuration_six()
		channels = get_six_unreserved_channels(config)
		input_0 = channels[0].channel_id
		input_1 = channels[1].channel_id
		input_2 = channels[2].channel_id
		output_0 = channels[3].channel_id
		output_1 = channels[4].channel_id
		output_2 = channels[5].channel_id
		data = {
			"name": company_name(),
			"signal_threshold": 0,
			"configuration_id": config.configuration_id,
			"input_channels": {
				"input_channel1": input_0,
				"input_channel2": input_1,
				"input_channel3": input_2
			},
			"output_channels": {
				"output_channel1": output_0,
				"output_channel2": output_1,
				"output_channel3": output_2
			},
			"apparent_power_rating": "50",
			"efficiency": "90",
			"bulk_charging_current": "90",
			"cost": "200000",
			"operating_window": [
				{
					"start_time": "00:00:00",
					"stop_time": "05:00:00"
				}
			],
			"battery": [
				{
					"capacity": "200",
					"count": 4,
					"voltage": 24,
					"bank_voltage": 24,
					"depth_of_discharge": 70,
					"chemistry": "Lead",
					"end_of_life": 5,
					"meta": [
						{
							"depth_of_discharge": 70,
							"number_of_cycles": 7000,
							"battery_capacity": 200
						}
					]
				}
			]
		}
		return data
	COUNT = 150
	for _ in range(COUNT):
		data = get_data()
		print(ThreePhaseInverter.create(**data))


def create_fake_consumers():
	create_single_phase_consumer()
	create_three_phase_consumers()

def create_fake_producers():
	create_single_phase_grids()
	create_three_phase_grids()
	create_single_phase_generators()
	create_three_phase_generators()
	create_single_phase_inverters()
	create_three_phase_inverters()

def create_fake_probes(probe_name, device_type_id):
	from api.manager import ProbeManager
	probe_data = {
		"probe_name": probe_name,
		"wifi_network_name": None,
		"wifi_password": None,
		"device_type_id": device_type_id,
		"channels": [
			{
			"channel_threshold_current": None,
			"power_factor": None,
			"current_sensor_id": None,
			"voltage_sensor_id": None,
			}
		]
		}

	new_probe = ProbeManager().new_probe(5, probe_data)
	print(new_probe)

def create_fake_configs(ten_users):
	from random import uniform, choice, randrange, getrandbits
	from forgery_py.forgery.address import city
	def rand_name_id():
		name = choice(list(ten_users.keys()))
		_id = ten_users[name]
		return name, _id
	def rand_users(count):
		return [rand_name_id() for _ in range(count)]

	random_user_id = choice(list(ten_users.keys()))
	random_user_name = ten_users[random_user_id]
	configuration_members = [
			{
			"member_id": user[0],
			"member_name": user[1]
			}
			for user in rand_users(randrange(2, 6))
		]
	configuration_members.insert(0, {
		"member_id": random_user_id,
		"member_name": random_user_name
	})
	unconfigured_probe = Probe.query.join(Probe.channels).filter(Channels.configuration_id == None).first()
	unconfigured_channels = unconfigured_probe.unconfigured_channels()
	config_data = None
	if len(unconfigured_channels) > 4:
		channel_list = [channel['channel_id'] for channel in unconfigured_channels]
		config_data = {
			"configuration_name": city(),
			"error_threshold": round(uniform(2.5, 40.0), 2),
			"display_energy_today": bool(getrandbits(1)),
			"configuration_user": {
				"configuration_user_id": random_user_id,
				"configuration_user_name": random_user_name
			},
			"configuration_members": configuration_members,
			"probe_channels": [
				{
				"probe_id": unconfigured_probe.probe_id,
				"channel_ids": channel_list
				}
			]
		}
	if config_data:
		from api.manager import ConfigurationManager
		response = ConfigurationManager().new_configuration(5, config_data)
		print(response[0]['data'])

def run():
	from api import db
	# from time import sleep
	# config = get_configuration()
	# print(f'\n\n\n\t\tconfig = {config}')
	probe_count = Probe.query.count()
	print(probe_count)

	# if probe_count < 100 or Configuration.query.count() < 10:
	if probe_count < 250 or Configuration.query.count() < 10:
		db.session.close()
		db.drop_all()
		db.create_all()
		device_type = DeviceType.query.first()
		if not device_type:
			device_type = DeviceType.new_device_type('GMID', 8)
		else:
			device_type = device_type.shallow_dict()
		probe_count = Probe.query.count()
		print(probe_count)
		for i in range(1000):
			if len(str(i)) == 1:
				i = int(f'0{i}')
			elif len(str(i)) == 2:
				i = int(f'{i}')
			elif len(str(i)) == 3:
				i = f'E{str(i)[0]}:{str(i)[1:]}'
			elif len(str(i)) == 4:
				i = f'{str(i)[:2]}:{str(i)[2:]}'
			create_fake_probes(f'TE:ST:PR:OB:{i}', device_type['device_type_id'])
			# sleep(0.001)
		from uuid import uuid4
		from forgery_py.forgery.name import full_name
		users = {str(uuid4()): full_name() for _ in range(200)}
		for _ in range(1000):
			create_fake_configs(users)
			# sleep(0.001)
		from api.models import Fuel, PhaseType
		Fuel.init_types()
		PhaseType.init_types()
		create_fake_consumers()
		create_fake_producers()

	from api.models import SinglePhaseConsumer, ThreePhaseConsumer, SinglePhaseGenerator, \
	ThreePhaseGenerator, SinglePhaseGrid, ThreePhaseGrid, SinglePhaseInverter, ThreePhaseInverter

	device_types = [device_type.shallow_dict() for device_type in DeviceType.query.all()]
	print(f'Device Types - {device_types}')
	print(f'Probe Count - {Probe.query.count()}')
	print(f'Configuration Count - {Configuration.query.count()}\n')
	print(f'Single Phase Consumer Count - {SinglePhaseConsumer.query.count()}\n')
	print(f'Three Phase Consumer Count - {ThreePhaseConsumer.query.count()}\n')
	print(f'Single Phase Generator Count - {SinglePhaseGenerator.query.count()}\n')
	print(f'Three Phase Generator Count - {ThreePhaseGenerator.query.count()}\n')
	print(f'Single Phase Grid Count - {SinglePhaseGrid.query.count()}\n')
	print(f'Three Phase Grid Count - {ThreePhaseGrid.query.count()}\n')
	print(f'Single Phase Inverter Count - {SinglePhaseInverter.query.count()}\n')
	print(f'Three Phase Inverter Count - {ThreePhaseInverter.query.count()}\n')
