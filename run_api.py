# Test Server.
from os import environ

from arrow import now
from flask import request
from flask_migrate import Migrate

from api import create_api, db
from logger import logger

if environ.get('FLASK_ENV') is None:
	print('FLASK_ENV not set')
mode = environ.get('FLASK_ENV') or 'Staging'
app = create_api(mode)
migrate = Migrate(app, db)


# @app.before_first_request
# def set_up():
with app.app_context():
	from api.models import PhaseType, Fuel, DeviceType, CurrentSensor, VoltageSensor
	print(PhaseType.init_types())
	Fuel.init_types()
	DeviceType.init_types()
	print(CurrentSensor.init_types())
	VoltageSensor.init_types()

# if mode not in {'Production', 'production'}:
# 	with app.app_context():
# 		from fake import run
# 		# set_up()
# 		# run()

@app.after_request
def log_info(response):
	try:
		log_data = {
			'connection': request.headers.get('Connection'),
			'ip_address': request.remote_addr,
			'browser_name': request.user_agent.browser,
			'user_device': request.user_agent.platform,
			'referrer': request.referrer,
			'request_url': request.url,
			'host_url': request.host_url,
			'status_code': response.status_code,
			'date': str(now('Africa/Lagos')),
			'location': response.location,
		}
		logger.info('configuration_api_logs : {}'.format(log_data))
	except Exception as e:
		logger.exception("configuration_api_logs: {}".format(e))
	return response


with app.app_context():
	try:
		migrate_old_devices = int(environ.get('MIGRATE_OLD_DEVICES'))
		if migrate_old_devices:
			print('Migrating configurations')
			from api.models.oldconfig import do_config_migration, do_power_source_migration
			from api.models import Configuration
			migration_list = do_config_migration()
			for migration_dict in migration_list:
				migrated_config = Configuration.migration_config(**migration_dict)
			do_power_source_migration()
			pass
			# todo uncomment to migrate
	except TypeError as e:
		logger.exception(e)
		logger.info('Not migrating') 


with app.app_context():
	# db.reflect()
	# db.drop_all()
	db.create_all()
	# upgrade()

# if __name__ == '__main__':
#     app.run()
