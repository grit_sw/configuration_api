import unittest
from api import create_api, db


class ProsumerClassTest(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		cls.api = create_api('Testing')
		cls.ctx = cls.api.app_context()
		cls.ctx.push()
		db.drop_all()
		db.create_all()

	def setUp(self):
		self.client = self.api.test_client()

	def tearDown(self):
		pass

	@classmethod
	def tearDownClass(cls):
		db.session.remove()
		db.drop_all()
		cls.ctx.pop()
